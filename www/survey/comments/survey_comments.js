(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }


    var margin = {top: 10, right: 0, bottom: 0, left: 5},
        width = 50 - margin.left - margin.right, 
        height = 63 - margin.top - margin.bottom;

    var y = d3.scale.ordinal().rangeRoundBands([0, height], .1); 
    var x = d3.scale.linear().range([width, 0]);

    var xAxis = d3.svg.axis().scale(x).orient('bottom');
    var yAxisLeft = d3.svg.axis().scale(y).orient("left");

    // var color = d3.scale.ordinal();
    // #003399

    var slice_size = 10;

    var usefullness_stati = sentiment_stati = disposition_stati = category_stati = {},
        filtered_stati = 1,
        exclusive_stati = [],
        additive_stati = [];
    

    var Loading = React.createClass({
        render: function() {
            return (
                <div className="row">
                    <h1>Loading ...</h1>
                </div>
            )
        }
    })

    var Comments = React.createClass({
        getInitialState: function() {
            return {
                comments: this.props.comments,
                // display_comments: this.filteredComments(0),
                loading: true,
                stati_filter: 1,
                displayed_rows: slice_size
            };
        },
        
        componentDidMount: function() {
            window.addEventListener('scroll', this.handleScroll);

            if ($('body').height() <= $(window).height()) {
                this.setState({'loading': true, displayed_rows: this.state.displayed_rows+slice_size});
            }
        },

        componentWillUnmount: function() {
            window.removeEventListener('scroll', this.handleScroll);
        },

        componentDidUpdate: function() {
            // console.debug(this.state.comments.length + ' < ' + this.state.displayed_rows);
            if (this.state.comments.length >= this.state.displayed_rows && $('body').height() <= $(window).height()) {
                this.setState({'loading': true, displayed_rows: this.state.displayed_rows+slice_size});
            }
        },

        handleDoneLoad: function(row_id) {
            // console.debug('handleDoneLoad: ' + row_id);
            
            var comments = this.state.comments;
            var index = _.findIndex(comments, function(v) { return v.row_id == row_id });
            // console.debug(index);
            comments[index]['loaded'] = true;

            // console.debug(this.state.displayed_rows);
            // console.debug(
            //     _.chain(comments.slice(0, this.state.displayed_rows))
            //                     .map(
            //                         function(el) {
            //                             return _.defaults(el, {'loaded': false})
            //                         }
            //                     )
            //                     .pluck('loaded')
            //                     .value()
            // )
            var all_loaded = _.chain(comments.slice(0, this.state.displayed_rows))
                                .map(
                                    function(el) {
                                        return _.defaults(el, {'loaded': false})
                                    }
                                )
                                .pluck('loaded')
                                .reduce(
                                    function(m, v) {
                                        // console.debug(m + ' && ' + v);
                                        return m && v;
                                    }, true
                                )
                                .value();

            // console.debug('all_loaded: ' + all_loaded);

            // this.setState({'loaded': all_loaded});
            this.setState({'loading': !all_loaded});
        },

        handleScroll: function(e) {
            // console.debug('handleScroll');
            // console.debug(e);
            // console.debug(window.innerHeight);
            // console.debug(document.body.scrollTop);
            // console.debug($('body').scrollTop());
            // console.debug(document.body.offsetHeight);
            if (((window.innerHeight + $(document).scrollTop()) / document.body.offsetHeight) > .9) {
                // var comments = _.sortBy(this.props.comments.slice(0, this.state.comments.length+slice_size), 'comment_stati');
                // this.setState({'comments': comments, 'loading': true});
                this.setState({'loading': true, displayed_rows: this.state.displayed_rows+slice_size});
                // this.handleFilter(this.state.stati_filter);
                // console.debug('comment length: ' + comments.length);
            }
        },

        showHide: function(row_id, e) {
            parent = $(e.target).parents('.comment-answers');
            parent.toggleClass('rounded').removeClass('no-border');
            parent.prev('.comment-answers').toggleClass('no-border');

            // parent.find('.comment').toggleClass('no-border');
            // parent.find('.answers').toggleClass('thick-border');
            // console.debug(this.getDOMNode());
            // console.debug('showHide');
            // console.debug('#answers-' + row_id);
            // console.debug($('#answers-' + row_id));
            // this.refs['answers-' + row_id].show();
            $('#answers-' + row_id).toggle();

            // console.debug('refs');
            // console.debug(this.refs);
            this.refs['answers-' + row_id].showAnswerBars();

        },

        removeSiblingStati: function(stati_group, current_stati, new_stati) {
            // console.debug('before: ' + current_stati);
            if (_.chain(stati_group).keys().contains(new_stati).value()) {
                // console.debug('contains ' + new_stati);
                _.each(_.keys(stati_group), function(v) {
                    // console.debug(current_stati + '&= ~' + v);
                    current_stati &= ~v;
                });
            }
            

            // console.debug('after: ' + current_stati);
            return current_stati;
        },

        updateRowCommentStatus: function(valname, row_id, stati) {
            // console.debug('updateRowCommentStatus: ' + valname + ', ' + row_id + ', ' + stati);
            var comments = this.state.comments;
            // var comments = this.filteredComments(this.state.stati_filter);

            var index = _.findIndex(comments, function(v) { return v.row_id == row_id });
            // console.debug('before: ' + comments[index].comment_stati);
            // comments[index].comment_stati = stati|(comments[index].comment_stati&4);//(comments[index].comment_stati|stati);
            // console.debug('after: ' + comments[index].comment_stati);
            // comments = _.sortBy(comments, 'comment_stati');

            var current_stati = comments[index][valname];
            // console.debug('before: ' + current_stati);
            if (valname == 'cat_stati') {
                
                if ((current_stati&stati) > 0) {
                    //  Stati bit already set, unset it
                    comments[index].cat_stati &= ~stati;
                } else {
                    //  Stati bit not set, set it
                    comments[index].cat_stati |= stati;
                }
            } else {
                if (current_stati == stati)
                    comments[index][valname] = 0;
                else
                    comments[index][valname] = stati;

            }
            // switch (valname) {
            //     case 'useful_val':
            //     case 'custsent_val':
            //     case 'disp_stati':
            //         comments[index][valname] = stati;
            //         break;
            //     case 'comment_stati':
            //         comments[index].comment_stati |= stati;
            // }
            // console.debug('after: ' + comments[index][valname]);


            //  Determine if this is an exclusive stati or additive
            // if (_.contains(additive_stati, stati)) {
            //     comments[index].comment_stati |= stati;
            // } else {
            //     // Gotta remove sibling stati first
            //     comments[index].comment_stati = this.removeSiblingStati(
            //         usefullness_stati, this.removeSiblingStati(
            //             sentiment_stati, this.removeSiblingStati(
            //                 disposition_stati, comments[index].comment_stati, stati
            //             ), stati
            //         ), stati
            //     ) | stati;
            // }

            var comment = comments[index];
            // console.debug(comment.row_id);
            // console.debug(comment.login_session_uid);

            $.ajax({
                url: 'http://dev.admapi.actkeytrain.com/dbwebapi/dbo.api_sp_set_stati/jsonp',
                dataType: 'jsonp',
                data: {
                    row_id: comment.row_id,
                    luid_str: comment.luid_str,
                    stati_col: valname,
                    stati_val: comments[index][valname]
                },
                success: function(data) {
                    // console.debug(data);
                },
                error: function() {
                    console.debug(arguments);
                }
            })

            // console.debug('new stati: ' + comments[index].comment_stati);


            this.setState({'comments': comments});
            // this.handleFilter(this.state.stati_filter);
        },

        filteredComments: function(stati) {
            var comments = this.state.comments;
            var displayed_rows = this.state.displayed_rows;
            // console.debug('filteredComments: ' + stati);
            // console.debug('displayed_rows: ' + displayed_rows);
            // console.debug('slice_size: ' + slice_size);
            // console.debug(comments.length);

            var filtered_stati = this.state.stati_filter;
            // console.debug('filtered_stati: ' + filtered_stati);

            comments = _.chain(comments)
                .filter(function(c) {
                    // console.debug(disposition_stati);
                    // // console.debug(c.comment_stati + ': ' + (c.comment_stati&3));
                    // if (stati == 0) {
                    //     //  New means no pos/neg, could be useless
                    //     // console.debug(c.comment_stati + ': ' + (c.comment_stati&3));
                    //     return ((c.comment_stati&3) == 0)
                    // } else {
                    //     console.debug(c.comment_stati + '&' + stati + ': ' + (c.comment_stati&stati));
                    //     return ((c.comment_stati&stati) == stati);
                    // }
                    //  Filter out all those with a disposition
                    // if (stati == 0) {
                    //     return ((c.comment_stati&filtered_stati) == 0);
                    // } else {
                    //     return ((c.comment_stati&stati) == stati);
                    // }
                    // console.debug(c.disp_stati + ' == ' + filtered_stati);
                    return (c.disp_stati == filtered_stati);
                })
                // .sortBy('comment_stati')
                .value()
                .slice(0, (displayed_rows > slice_size) ? displayed_rows : slice_size);

            // comments = comments.slice(0, (displayed_rows > slice_size) ? displayed_rows : slice_size);
            // console.debug(comments);


            // console.debug(comments.length);
            // console.debug((displayed_rows > slice_size) ? displayed_rows : slice_size);
            return comments;
        },

        handleFilter: function(stati) {
            // console.debug('handleFilter');
            // console.debug('handleFilter: ' + stati);
            // var comments = this.filteredComments(stati);
            // comments = _.filter(comments, function(c) {
            //     // console.debug(c.comment_stati&3);
            //     if (stati == 0) {
            //         //  New means no pos/neg, could be useless
            //         return ((c.comment_stati&3) == 0)
            //     } else {
            //         return ((c.comment_stati&stati) == stati);
            //     }
            // }).slice(0, (this.state.comments.length > slice_size) ? this.state.comments.length : slice_size);
            // console.debug(this.state.comments.length);
            // console.debug(comments.length);
            // this.setState({'stati_filter': stati, 'comments': comments, 'displayed_rows': comments.length});
            // this.setState({'stati_filter': stati});

            var self = this;
            this.setState({
                'comments': [],
                'loading': true
            })
            get_comments(stati, function(comments) {
                // console.debug(comments);
                self.setState({
                    'comments': comments,
                    'stati_filter': stati,
                    'loading': false
                })
            });
            
        },

        renderComments: function() {
            // console.debug('renderComments');
            var questions = this.props.questions;
            var question_ids = _.keys(this.props.questions);
            var answers = this.props.answers;
            var comments = _.map(this.filteredComments(0), function(comment, index) {
            // var comments = _.map(this.filteredComments(this.state.stati_filter), function(comment, index) {
            // var comments = _.map(this.state.comments, function(comment, index) {
                // console.debug(comment);
                var answer_key = "answers-" + comment.row_id;
                return (
                    <div className="comment-answers row" key={comment.row_id} onClick={this.showHide.bind(this, comment.row_id)}>
                        <div className="col-md-12">
                            <Comment datum={comment} key={index} questions={questions} questionIds={question_ids} key={comment.row_id} updateRowCommentStatus={this.updateRowCommentStatus} rowID={comment.row_id} handleDoneLoad={this.handleDoneLoad} />
                            <Answers datum={comment} answers={answers} questions={questions} key={answer_key} ref={answer_key} />
                        </div>
                    </div>
                )
            }, this);
            
            return comments;
        },

        renderDispositions: function() {
            var cx = React.addons.classSet;
            // console.debug(disposition_stati);
            var buttons = _.map(disposition_stati, function(v, k) {
                var classes = cx({
                    'btn': true,
                    'btn-primary': true,
                    'active': this.state.stati_filter == k
                });

                var key = 'disposition-' + k;
                return <button type="button" className={classes} onClick={this.handleFilter.bind(this, k)} key={key}>{v}</button>
            }, this);

            return buttons;
        },

        render: function() {
            // console.debug('render');
            var comments_block;

            var loading;
            if (this.state.loading)
                loading = <Loading />;

            var cx =React.addons.classSet;
            var new_classes = cx({
                'btn': true,
                'btn-primary': true,
                'active': (this.state.stati_filter == 0)
            });

            return (
                <div className="container">
                    <div className="row no-border">
                        <div className="col-md-12">
                            <div className="btn-group">
                                {this.renderDispositions()}
                            </div>
                        </div>
                    </div>
                    <div className="jumbotron">
                        <div className="col-md-6"><h4>Comment</h4></div>
                        <div className="col-md-1"><h4></h4></div>
                        <div className="col-md-3"><h4>Comment Quality</h4></div>
                    </div>
                    {this.renderComments()}
                    {loading}
                </div>
            )
        }
    });

    var Answers = React.createClass({
        getInitialState: function() {
            return {
                barsRendered: false
            }
        },

        showAnswerBars: function() {
            // console.debug('showAnswerBars');
            // console.debug(this.refs);
            if (!this.state.barsRendered) {
                _.map(this.props.answers, function(v, k) {
                    this.refs[this.props.datum.row_id + '-' + k].showAnswerBar();
                }, this);
                this.setState({'barsRendered': true});
            } 
        },

        renderAnswers: function() {
            // console.debug(this.props.answers);
            var answers = _.map(this.props.answers, function(v, k) {
                var question = this.props.questions[k];
                var answer = v[this.props.datum[k]-1];
                // console.debug(question);
                // console.debug(answer);
                var key = this.props.datum.row_id + '-' + k;
                var answer_id = 'answer-' + key;
                return (
                    <Answer question={question} answer={answer} key={key} answerID={answer_id} answerVal={this.props.datum[k]} ref={key} />
                )
            }, this);

            return answers;
        },

        render: function() {
            var answers_id = 'answers-' + this.props.datum.row_id;
            return (
                <div className="row answers" id={answers_id}>
                    {this.renderAnswers()}
                </div>
            )
        }

    })

    var Answer = React.createClass({
        showAnswerBar: function() {
            // console.debug('showAnswerBar');

            var svg = d3.select('#' + this.props.answerID)
                .append('svg')
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', 3 + margin.top + margin.bottom)
                .append('g')
                    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');


            var answer_val = this.props.answerVal;
            y.domain([0, 1]); // console.debug(x.range());
            x.domain([0, 6]);
            
            svg.selectAll('.answer')
                .data([answer_val])
            .enter().append('rect')
                .attr('height', 2 /*x.rangeBand()*/)
                .attr('x', function(d) {
                    // console.debug(d);
                    // // console.debug(questions);
                    // console.debug(datum[d]);
                    // console.debug(y(datum[d]));
                    // return width-x(datum[d]);
                    0;
                })
                .attr('width', function(d) {
                    // console.debug(answer_val + ': ' + x(6 - answer_val));
                    return width-x(answer_val == 0 || answer_val == 6 ? 0 : 6-answer_val);
                })
                .attr('y', function(d) {
                    return 0;
                    // return x(d) + (x.rangeBand() / question_ids.length)
                })
                .attr('class', 'answer-bar')
                .style('fill', '#003399');
        },

        render: function() {
            return (
                <div className="answer-block">
                    <div className="col-md-8">{this.props.question}</div>
                    <div className="col-md-2">{this.props.answer}</div>
                    <div className="col-md-1" id={this.props.answerID}></div>
                </div>
            )
        }
    })

    var Comment = React.createClass({
        getInitialState: function() {
            return {};
        },
        componentDidMount: function() {
            var svg = d3.select('#row-' + this.props.datum.row_id)
                .append('svg')
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', height + margin.top + margin.bottom)
                .append('g')
                    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

            // svg.append("g")
            //     .attr("class", "x axis")
            //     .attr("transform", "translate(0," + height + ")")
            //     .call(xAxis)
            //     // .selectAll('text')
            //     //     .style('text-anchor', 'end')
            //     //     .attr('dx', '-.8em')
            //     //     .attr('dy', '.15em')
            //     //     .attr('transform', 'rotate(-65)');

            // svg.append('g')
            //     .attr('class', 'y axis')
            //     .call(yAxisLeft)
            // .append('text')
            //     .attr('transform', 'rotate(-90)')
            //     .attr('y', 6)
            //     .attr('dy', '.71em')

            y.domain(this.props.questionIds); // console.debug(x.range());
            x.domain([0, 6]);
            
            var questions = this.props.questions;
            var question_ids = this.props.questionIds;
            var datum = this.props.datum;
            // console.debug(datum);
            // console.debug(question_ids);
            svg.selectAll('.answer')
                .data(question_ids)
            .enter().append('rect')
                .attr('height', 2 /*x.rangeBand()*/)
                .attr('x', function(d) {
                    // console.debug(d);
                    // // console.debug(questions);
                    // console.debug(datum[d]);
                    // console.debug(y(datum[d]));
                    // return width-x(datum[d]);
                    0;
                })
                .attr('width', function(d) {
                    // console.debug(datum[d] + ': ' + x(6 - datum[d]));
                    return width-x(datum[d] == 0 || datum[d] == 6 ? 0 : 6-datum[d]);
                })
                .attr('y', function(d) {
                    return y(d) + 3 //x.rangeBand();
                    // return x(d) + (x.rangeBand() / question_ids.length)
                })
                .attr('class', 'answer-bar')
                .style('fill', '#003399');




            // _.each(this.props.questions, function(q, id) {
            //     console.debug(this.props.datum[id]);
            //     // console.debug(q);
            // }, this)

            this.props.handleDoneLoad(this.props.rowID);
        },

        setStatus: function(valname, stati, e) {
            e.preventDefault();
            e.stopPropagation();
            $(e.target).blur();
            // console.debug(this);

            this.props.updateRowCommentStatus(valname, this.props.rowID, stati);
        },

        // handleChange: function(e) {
        //     // e.preventDefault();
        //     // e.stopPropagation();
        //     console.debug('handleChange');
        //     console.debug('current: ' + this.props.datum.comment_stati);

        //     var stati = this.props.datum.comment_stati;
        //     if ((stati&4) == 4) {
        //         console.debug('has status');
        //         // stati = stati^4;
        //         stati &= ~4;
        //     } else {
        //         console.debug('no has status');
        //         stati = stati|4;
        //     }
        //     console.debug('now: ' + stati);
        //     // this.props.updateRowCommentStatus(this.props.rowID, stati);
        //     this.setStatus(stati, e);
        // },

        noPropagate: function(e) {
            e.stopPropagation();
        },

        renderCategoryButtons: function() {
            return <div />
        },

        renderStatiButtons: function(valname, current_stati, avail_stati, grouped) {
            /*
                avail_stati: {1: 'Low', 2: 'Medium', 3: 'High'}
            */
            var cx = React.addons.classSet;

            var buttons = _.map(avail_stati, function(name, stati) {
                // console.debug(valname + ', ' + current_stati + ', ' + stati);
                var classes = cx({
                    'btn': true,
                    'btn-xs': true,
                    'btn-default': (valname != 'disp_stati' || (valname == 'disp_stati' && stati == 1)),
                    'btn-primary': (valname == 'disp_stati' && stati == 2),
                    'btn-danger': (valname == 'disp_stati' && stati == 4),
                    'btn-warning': (valname == 'disp_stati' && stati == 16),
                    'active': ((current_stati&stati) == stati)
                });

                var key = 'stati-' + stati;

                return <button type="button" className={classes} onClick={this.setStatus.bind(this, valname, stati)} key={key}>{name}</button>
            }, this);

            if (grouped) {
                buttons = <div className="btn-group">
                            {buttons}
                          </div>
            }
            
            return (
                <div className="col-md-9">
                    {buttons}
                </div>
            )
        },

        setSentiment: function(stati, e) {
            // console.debug(arguments);
            this.noPropagate(e);
        },

        setUsefulness: function(stati, e) {
            this.noPropagate(e);
        },

        setDisposition: function(stati, e) {
            this.noPropagate(e);
        },

        setCategory: function(stati, e) {
            this.noPropagate(e);
        },

        render: function() {
            // console.debug('stati: ' + this.props.datum.comment_stati)
            // console.debug(this.props);
            var row_id = 'row-' + this.props.datum.row_id;
            // var answers_id = 'answers-' + this.props.datum.row_id;
            // var cx = React.addons.classSet;
            // var classes = cx({
            //     'row': true,
            //     'bg-success': ((this.props.datum.comment_stati&1) == 1),
            //     'bg-warning': ((this.props.datum.comment_stati&2) == 2)
            // });

            // var useless_checked = ((this.props.datum.comment_stati&4) == 4);
            // console.debug('useless_checked: ' + useless_checked + ', ' + this.props.datum.comment_stati + ', ' + (this.props.datum.comment_stati&4));

            



            // var usefullness = 2,
            //     sentiment = 2,
            //     categories = 0,
            //     disposition = 0;

            return (
                <div className="row comment">
                    <div className="col-md-6">
                        <div className="row no-border"><div className="col-md-4">{this.props.datum.datetime_str}</div></div>
                        <div className="row no-border"><div className="col-md-12"><strong>{this.props.datum.comments}</strong></div></div>
                        <div className="row no-border">
                            <div className="col-md-3">{this.props.datum.lesson_tag}</div>
                            <div className="col-md-2">Lang: {this.props.datum.lang_code}</div>
                            <div className="col-md-2">State: {this.props.datum.orgz_state}</div>
                            <div className="col-md-5">{this.props.datum.pkg_orgz_desc}</div>
                            {/*<div className="col-md-3">State: TN</div>
                            <div className="col-md-3">Course: Applied Mathematics 3</div>
                            <div className="col-md-3">Language: EN</div>*/}
                        </div>
                    </div>
                    <div className="col-md-1" id={row_id}></div>
                    <div className="col-md-5">
                        <div className="row stati">
                            <div className="col-md-3"><strong>Usefulness: </strong></div>
                            {this.renderStatiButtons('useful_val', this.props.datum.useful_val, usefullness_stati, true)}
                        </div>
                        <div className="row stati">
                            <div className="col-md-3"><strong>Sentiment: </strong></div>
                            {this.renderStatiButtons('custsent_val', this.props.datum.custsent_val, sentiment_stati, true)}
                        </div>
                        <div className="row stati">
                            <div className="col-md-3"><strong>Categories: </strong></div>
                            {this.renderStatiButtons('cat_stati', this.props.datum.cat_stati, category_stati, false)}
                        </div>
                        <div className="row stati">
                            <div className="col-md-3"><strong>Disposition: </strong></div>
                            {this.renderStatiButtons('disp_stati', this.props.datum.disp_stati, disposition_stati, true)}
                        </div>
                    </div>
                    {/*}
                    <div className="col-md-1"><button type="button" className="btn btn-success btn-xs" onClick={this.setStatus.bind(this, 1)}>Positive</button></div>
                    <div className="col-md-1"><button type="button" className="btn btn-warning btn-xs" onClick={this.setStatus.bind(this, 2)}>Negative</button></div>
                    <div className="col-md-1"><label><input type="checkbox" checked={useless_checked} onChange={this.handleChange} onClick={this.noPropagate} />Useless</label></div>
                    */}
                </div>
            )
        }
    });

    var SparkChart = React.createClass({
        getInitialState: function() {
            return {};
        },
        render: function() {

        }
    });

    var fill_stati_obj = function(all_stati, prefix) {
        // console.debug('fill_stati_obj: ' + prefix);
        var prefix_re = new RegExp('^' + prefix + '_');
        var tmp = _.filter(all_stati, function(status_obj) {
            return status_obj.status_tag.match(prefix_re);
        });

        var out_obj = {}
        _.each(tmp, function(el) {
            out_obj[el.status_val] = el.status_desc;
        });

        return out_obj
    }


    function get_stati_vals(valname, cb) {
        $.ajax({
            // url: 'http://es.run.keytrain.com/ajax/get_stati.asp',
            url: 'http://dev.admapi.actkeytrain.com/dbwebapi/gen.sp_get_stati',
            data: {
                'objname': 'log_tbl_session_rating',
                'valname': valname
            },
            dataType: 'jsonp',
            success: function(data) {
                // console.debug(data);
                
                data = data.ResultSets[0];
                cb(data);
                // disposition_stati = fill_stati_obj(data, 'disp');
                // // category_stati = fill_stati_obj(data, 'cat');

                // filtered_stati = _.chain(disposition_stati).keys().reduce(function(m, v) { return m|v; }, 0).value();

                // get_questions();

            }
        });
    }


    // console.debug('do ajax');
    $.ajax({
        //url: 'http://es.run.keytrain.com/ajax/get_stati.asp',
        url: 'http://dev.admapi.actkeytrain.com/dbwebapi/gen.sp_get_stati',
        data: {
            'objname': 'log_tbl_session_rating',
            'valname': 'comment_stati'
        },
        dataType: 'jsonp',
        success: function(data) {
            // console.debug(data);
            // var fill_stati_obj = function(all_stati, prefix) {
            //     var prefix_re = new RegExp('^' + prefix + '_');
            //     var tmp = _.filter(all_stati, function(status_obj) {
            //         return status_obj.status_tag.match(prefix_re);
            //     });

            //     var out_obj = {}
            //     _.each(tmp, function(el) {
            //         out_obj[el.status_val] = el.status_desc;
            //     });

            //     return out_obj
            // }

            data = data.ResultSets[0];
            // console.debug(data);

            // usefullness_stati = fill_stati_obj(data, 'usefull');
            // sentiment_stati = fill_stati_obj(data, 'sentiment');
            // disposition_stati = fill_stati_obj(data, 'disposition');
            // category_stati = fill_stati_obj(data, 'cat');

            
            // exclusive_stati = _.keys(usefullness_stati).concat(_.keys(sentiment_stati)).concat(_.keys(disposition_stati));
            // additive_stati = _.keys(category_stati);

            // get_disposition_stati();
            get_stati_vals('disp_stati', function(stati) {
                disposition_stati = fill_stati_obj(stati, 'disp');
                // console.debug(disposition_stati);

                get_stati_vals('useful_val', function(stati) {
                    usefullness_stati = fill_stati_obj(stati, 'useful');

                    get_stati_vals('custsent_val', function(stati) {
                        sentiment_stati = fill_stati_obj(stati, 'custsent');

                        get_stati_vals('cat_stati', function(stati) {
                            category_stati = fill_stati_obj(stati, 'cat');
                            get_questions();
                        })
                    })
                })
            })
        }
    });

    // function get_disposition_stati() {
    //     $.ajax({
    //         // url: 'http://es.run.keytrain.com/ajax/get_stati.asp',
    //         url: 'http://dev.admapi.actkeytrain.com/dbwebapi/gen.sp_get_stati',
    //         data: {
    //             'objname': 'log_tbl_session_rating',
    //             'valname': 'disp_stati'
    //         },
    //         dataType: 'jsonp',
    //         success: function(data) {
    //             // console.debug(data);
                
    //             data = data.ResultSets[0];
    //             disposition_stati = fill_stati_obj(data, 'disp');
    //             // category_stati = fill_stati_obj(data, 'cat');

    //             filtered_stati = _.chain(disposition_stati).keys().reduce(function(m, v) { return m|v; }, 0).value();

    //             get_questions();
    //         }
    //     });
    // }

    

    // var usefullness_stati = {
    //     0x0001: 'Low',
    //     0x0002: 'Medium',
    //     0x0004: 'High'
    // };

    // var sentiment_stati = {
    //     0x0008: 'Negative',
    //     0x0010: 'Neutral',
    //     0x0020: 'Positive'
    // };

    // var disposition_stati = {
    //     0x0040: 'Favorite',
    //     0x0080: 'Delete',
    //     0x0100: 'Archive'
    // };

    // var category_stati = {
    //     1: 'Tech',
    //     2: 'Context',
    //     4: 'Program',
    //     8: 'Suggestion',
    //     16: 'Random'
    // }

    function get_questions() {
        // console.debug(category_stati);
        // console.debug(exclusive_stati);

        $.ajax({
            url: 'http://es.run.keytrain.com/cmiscorm/survey_questions.asp',
            data: {'mode': 1},
            dataType: 'jsonp',
            success: function(data) {
                // console.debug(data);
                var questions = {};
                var answers = {};
                _.each(data, function(obj, i) {
                    questions[obj.id] = obj.question;
                    answers[obj.id] = obj.answers;
                });

                
                show_comments(questions, answers);
            },
            error: function(a, b, c) {
                console.debug(a);
                console.debug(b);
                console.debug(c);
            }
        });
    }

    function get_comments(disp_stati, cb) {
        // console.debug('get_comments');
        $.ajax({
            // url: 'http://es.run.keytrain.com/ajax/survey_data.asp',
            url: 'http://dev.admapi.actkeytrain.com/dbwebapi/dbo.rpt_sp_survey_data/jsonp',
            data: {
                'disp_stati': disp_stati
            },
            headers: {
                'Accept': 'text/javascript'
            },
            dataType: 'jsonp',
            success:  function(survey_data) {
                // console.debug(survey_data);
                // var comments = _.filter(survey_data, function(post) {
                //     return post.comments != '';
                // }).reverse();
                var comments = survey_data.ResultSets[0];
                // console.debug(comments);
                cb(comments);
                
            },
            error: function() {
                console.debug(arguments);
            }
        })
    }

    function show_comments(questions, answers) {
        // console.debug('show_comments');

        get_comments(1, function(comments) {
            // console.debug('callback');
            React.render(<Comments comments={comments} questions={questions} answers={answers} />, document.getElementById('comments'));
        })
        // $.ajax({
        //     // url: 'http://es.run.keytrain.com/ajax/survey_data.asp',
        //     url: 'http://dev.admapi.actkeytrain.com/dbwebapi/dbo.rpt_sp_survey_data/jsonp',
        //     headers: {
        //         'Accept': 'text/javascript'
        //     },
        //     dataType: 'jsonp',
        //     success:  function(survey_data) {
        //         // console.debug(survey_data);
        //         // var comments = _.filter(survey_data, function(post) {
        //         //     return post.comments != '';
        //         // }).reverse();
        //         var comments = survey_data.ResultSets[0];

        //         // var nostati_comments = _.filter(comments, function(post) {
        //         //     post.comment_stati == 0
        //         // });

        //         // var positive_comments = _.filter(comments, function(post) {

        //         // })
        //         // var comments = all_comments.slice(0, 20);
        //         // console.debug('comment length: ' + comments.length);
        //         // comments[1].comment_stati = 4;
        //         React.render(<Comments comments={comments} questions={questions} answers={answers} />, document.getElementById('comments'));
        //     },
        //     error: function() {
        //         console.debug(arguments);
        //     }
        // })
    }

}());
