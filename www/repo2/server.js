require('./globals');

var fs = require('fs');
var path = require('path');
var express = require('express');
var compression = require('compression');
var cookieParser = require('cookie-parser');
var request = require('request');
var atob = require('atob');
var bodyParser = require('body-parser');

// var sticky = require('sticky-session');

var app = express();
app.use(compression());
app.use(cookieParser());
app.use(express.static(path.join(process.cwd(), 'public')));
// app.use(bodyParser.urlencoded({
//   extended: false
// }));
app.use(bodyParser.json({limit: '50mb'}));

var pg = require('pg');

var server = require('http').Server(app);
var io = require('socket.io')(server);

var rpackage = require('./package.json');
var Router = require('react-router').Router;
var Location = require('react-router/lib/Location');

var defaultAppName = process.env.APP ? process.env.APP : 'app';
var node_env = process.env.NODE_ENV ? process.env.NODE_ENV : 'dev';

var routes = require('./public/js/' + defaultAppName + '/' + defaultAppName + '.node.js');

var webpack_host = process.env.WHOST ? process.env.WHOST : 'localhost';
var webpack_dev_server_port = process.env.WPORT ? process.env.WPORT : 8077;

var html = fs.readFileSync(path.join(process.cwd(), 'src', 'jsx', defaultAppName, 'index.html'), {
  encoding: 'utf8'
});



var createStyleTag = function(file, media) {
  media = media || 'screen';
  return "    <link media='"+media+"' rel='stylesheet' type='text/css' href='"+file+"'>\n";
};

var stylesheets = '';
if(process.env.NODE_ENV === 'development') {
  stylesheets += createStyleTag('/css/'+defaultAppName+'/raw/{dir}/main.css', 'screen,print');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/raw/{dir}/theme.css');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/raw/{dir}/colors.css');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/raw/{dir}/font-faces.css');
  html = html.replace(new RegExp('{appscript}', 'g'), 'http://'+webpack_host+':'+webpack_dev_server_port+'/scripts/bundle.js');
} else {
  stylesheets += createStyleTag('/css/'+defaultAppName+'/blessed/{dir}/main-blessed1.css', 'screen,print');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/blessed/{dir}/main.css', 'screen,print');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/blessed/{dir}/theme.css');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/blessed/{dir}/colors-blessed1.css');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/blessed/{dir}/colors.css');
  stylesheets += createStyleTag('/css/'+defaultAppName+'/blessed/{dir}/font-faces.css');
  html = html.replace(new RegExp('{appscript}', 'g'), '/js/'+defaultAppName+'/'+defaultAppName+'.min.js');
}

html = html.replace(new RegExp('{app}', 'g'), defaultAppName);
html = html.replace(new RegExp('{stylesheets}', 'g'), stylesheets);
html = html.replace(new RegExp('{version}', 'g'), rpackage.version);

var ltr = html.replace(new RegExp('{dir}', 'g'), 'ltr');
var rtl = html.replace(new RegExp('{dir}', 'g'), 'rtl');

/** BEGIN X-EDITABLE ROUTES */

app.get('/xeditable/groups', function(req, res) {
  res.send([
    {value: 0, text: 'Guest'},
    {value: 1, text: 'Service'},
    {value: 2, text: 'Customer'},
    {value: 3, text: 'Operator'},
    {value: 4, text: 'Support'},
    {value: 5, text: 'Admin'}
  ]);
});

app.get('/xeditable/status', function(req, res) {
  res.status(500).end();
});

app.post('/xeditable/address', function(req, res) {
  res.status(200).end();
});

app.post('/dropzone/file-upload', function(req, res) {
  res.status(200).end();
});

/** END X-EDITABLE ROUTES */

app.get('/ltr', function(req, res, next) {
  res.redirect('/');
});

app.get('/rtl', function(req, res, next) {
  res.redirect('/');
});

app.use('*', function(req, res, next) {
  //  Check authorization
  /*  BTH: Basic Auth user hack */
  var auth = (req.get('Authorization')) ? req.get('Authorization') : 'Basic YmVuOmJlbg==';
  auth = atob(auth.replace('Basic ', ''));
  var auth_parts = auth.split(':');
  var user = auth_parts[0];

  /*  Fetch roles */
  // console.log('http://dev.admapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_auth/json?optflags=2&u=' + user);
  request('http://dev.admapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_auth/json?optflags=2&u=' + user, function(err, resp, body) {
    // console.log(err, resp, body);
    if (!err && resp.statusCode == 200) {
      // console.log(body);
      var results = JSON.parse(body);
      var roles = results.ResultSets[0][0].roles;
      // console.log(roles);
      // console.log(defaultAppName);

      if (roles.indexOf('|' + defaultAppName + '|') === -1) return res.status(403).send('Access Denied');

      res.header('auth-roles', roles);
      req.auth_roles = roles;

      next();
    } else {
      return res.status(403).send('Access Denied');
    }
  })
})

app.post('/api/:apiroot/:spname', function(req, res, next) {
  //  proxy api POSTs to get around cross-domain issues
  // var parts = req.path.split('/');
  // // ['api', '<edapi|admapi>', '<sp name>', '...{fields}']
  // parts.shift();
  // var api_root = parts.shift();
  // var sp_name = parts.shift();

  var api_url = 'http://dev.' + req.params.apiroot + '.actkeytrain.com/dbwebapi/' + req.params.spname;

  console.log(api_url);
  console.log(req.body);


  request.post({
    url: api_url,
    json: req.body
  }, function(err, resp, body) {
    // console.log(body);
    if (err) {
      console.error(err);
      res.status(500).send('Error');
    } else {
      console.log(body);
      console.log(body.ResultSets[0]);
      var status = body.ResultSets[0][0].status;
      console.log(status);
      // if (status >= 100) {
      //   console.log(body.ResultSets[0][0].statdesc);
      //   res.status(500).send(body.ResultSets[0][0].statdesc);
      // } else {
      //   res.send(body);
      // }
      res.send(body);
    }
  });
});


/**********************
  socket.io stuff
**********************/

// var chunks = io.of('/chunks');
// app.post('/socket/chunks/updated', function(req, res, next) {
//   console.log(req.body);
//   chunks.emit('update', req.body);

//   res.send('DONE');
// });

/*if (defaultAppName === 'admin' || defaultAppName === 'admin-1') {
  var ktadmin = io.of('/ktadmin');
  var port = node_env === 'production' ? 5433 : 5432;
  var pg_server = node_env === 'production' ? '108.166.6.6' : '127.0.0.1';
  var con_string = 'tcp://tdsremote:ag36.bl0TD2cknhMhMzh@' + pg_server + ':' + port + '/tdsremote';
  var pg_client = new pg.Client(con_string);
  // console.info(pg_client);
  pg_client.connect();
  var query = pg_client.query('LISTEN logins_insert');
  pg_client.query('LISTEN logins_delete');
  pg_client.query('LISTEN history_insert');

  // io.sockets.on('connection', function (socket) {
  ktadmin.on('connection', function(socket) {
      socket.emit('connected', { connected: true });
      console.info('connected');

      socket.on('ready for data', function (data) {
          console.info('ready for data', data);
          // console.info(socket);

          pg_client.query("select student_id, pkg_id from tds_tbl_logins", function(err, result) {
              // console.info(err, result);
              if (err) {
                console.error(err);
              } else {
                for (var i=0; i < result.rows.length; i++) {
                  var row = result.rows[i];
                  // console.info(row);
                  socket.emit('initial', { message: { payload: row.student_id + '|' + row.pkg_id }} )
                }
              }
          });


          pg_client.on('notification', function(notification) {
            console.info(notification);
              switch (notification.channel) {
                case 'logins_insert':
                  console.info('emit insert');
                  socket.emit('insert', { message: notification });
                  break;
                case 'logins_delete':
                  console.info('emit delete');
                  socket.emit('delete', { message: notification });
                  break;
                case 'history_insert':
                  console.info('emit history insert');
                  socket.emit('history_insert', { message: notification });
                  break;
                default:
                  console.error('Unknown notification', notification);
              }

          });
      });
  });

  app.get('/pgapi/logins/current', function(req, res, next) {
    pg_client.query("select * from tds_tbl_logins", function(err, result) {
        // console.info(err, result);
        if (err) {
          console.error(err);
          return next();
        } else {
          res.send(JSON.stringify(result.rows));
        }
    });
  });

  app.get('/pgapi/logins/history/:interval/:since', function(req, res, next) {
    console.info("select * from tds_history_interval('" + req.params.interval + "', '" + req.params.since + "')");
    pg_client.query("select * from tds_history_interval('" + req.params.interval + "', '" + req.params.since + "')", function(err, result) {
    // pg_client.query("select * from tds_tbl_login_history order by insert_date asc", function(err, result) {

      if (err) {
        console.error(err);
        return next();
      } else {
        console.info(result.rows);
        res.send(JSON.stringify(result.rows));
      }
    })
    
  });

  // console.log('node_env', node_env);
  // if (node_env === 'production') {
  //   var Connection = require('tedious').Connection;
  //   var Request = require('tedious').Request;

  //   var config = {
  //     userName: 'stracking_tds',
  //     password: 'r3m0te!',
  //     server: 'bartok.hosts.actkeytrain.com',
  //     options: {
  //       port: 1433,
  //       database: 'ktadmin'
  //     }
  //   };

  //   app.get('/pgapi/')

  //   var connection = new Connection(config);

  //   connection.on('connect', function(err) {
  //     console.log('tedious connected');
  //     if (err) {
  //       console.error('connection error');
  //       console.error(err);
  //     }

  //     var request = new Request("select 42, 'hello world'", function(err, rowCount) {
  //       if (err) {
  //         console.error('request error');
  //         console.error(err);
  //       } else {
  //         console.log(rowCount + 'rows');
  //       }
  //     });

  //     request.on('row', function(columns) {
  //       columns.forEach(function(column) {
  //         console.log(column.value);
  //       })
  //     });

  //     connection.execSql(request);
  //   })
  // }
}



/** CATCH-ALL ROUTE **/
app.get('*', function(req, res, next) {
  console.info(req.url);
  if(req.url === '/favicon.ico'
    || (req.url.search('.l20n') !== -1)) return next();
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  var isRTL = req.cookies.rubix_dir === 'rtl' ? true : false;

  var location = new Location(req.path, req.query);

  /*  BTH: Basic Auth user hack */
  var auth = (req.get('Authorization')) ? req.get('Authorization') : 'Basic YmVuOmJlbg==';
  // console.log('roles', req.auth_roles);
  // auth += req.get('auth-roles');
  // auth = atob(auth.replace('Basic ', ''));
  // var auth_parts = auth.split(':');
  // var user = auth_parts[0];

  
  var this_rtl = rtl.replace(new RegExp('{auth}', 'g'), auth);
  var this_ltr = ltr.replace(new RegExp('{auth}', 'g'), auth);

  this_rtl = this_rtl.replace(new RegExp('{roles}', 'g'), req.auth_roles);
  this_ltr = this_ltr.replace(new RegExp('{roles}', 'g'), req.auth_roles);

  try {
    Router.run(routes(), location, function(e, i, t) {
      var str = React.renderToString(
                  React.createElement(Router, i));

      if(isRTL) {
        res.send(this_rtl.replace(new RegExp('{container}', 'g'), str));
      } else {
        res.send(this_ltr.replace(new RegExp('{container}', 'g'), str));
      }
    });
  } catch(e) {
    console.error('Route Error', req.url, e);
    console.error(e.stack);
    return next();
  }
});

var noop = function() {};
var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
];
var length = methods.length;
while (length--) {
  method = methods[length];

  if (!console[method]) {
    console[method] = noop;
  }
}




server.listen(process.env.PORT, function() {
  try {
    process.send('CONNECTED');
  } catch (e) {}
});

// var server = app.listen(process.env.PORT, function() {
//   try {
//     process.send('CONNECTED');
//   } catch(e) {}
// });

process.on('uncaughtException', function(err) {
  console.log(arguments);
  process.exit(-1);
});
