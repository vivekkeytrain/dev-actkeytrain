import { combineReducers } from 'redux';
import {
    SET_UNIT, SET_COURSE, SET_LESSON, SET_TOPIC, SET_SELECTED_ROW, SET_FIELD_IMMEDIATELY, SET_SORT_FIELD,
    ADD_MESSAGE, CLOSE_MESSAGE, SET_USER, SET_QA_STATUS, SET_LIVE_UPDATE,
    UPDATE_DISPLAY_FIELD,
    FETCH_EDITOR_CONTENT_REQUEST, FETCH_EDITOR_CONTENT_SUCCESS, FETCH_EDITOR_CONTENT_FAILURE,
    FETCH_IMAGES_SUCCESS, FETCH_IMAGES_FAILURE, FETCH_IMAGES_REQUEST,
    REFETCH_IMAGES_SUCCESS, REFETCH_IMAGES_FAILURE, REFETCH_IMAGES_REQUEST,
    FETCH_IMAGE_DETAIL_SUCCESS, FETCH_IMAGE_DETAIL_FAILURE, FETCH_IMAGE_DETAIL_REQUEST,
    DO_LOGIN_LESSON_REQUEST, DO_LOGIN_LESSON_SUCCESS, DO_LOGIN_LESSON_FAILURE,
    UPDATE_FIELD_VAL_SUCCESS, UPDATE_FIELD_VAL_REQUEST, UPDATE_FIELD_VAL_FAILURE,
    FETCH_APP_CONSTANTS_SUCCESS, FETCH_APP_CONSTANTS_REQUEST, FETCH_APP_CONSTANTS_FAILURE,
    DisplayFields
} from './actions';

import { map, chain, fromPairs, findIndex, forEach, keys, reduce, omit } from 'lodash';

function sessions(state = {}, action) {
    switch (action.type) {
        case DO_LOGIN_LESSON_SUCCESS:
        case DO_LOGIN_LESSON_REQUEST:
        case DO_LOGIN_LESSON_FAILURE:
            if (!action.lang) return state;

            return Object.assign({}, state, {
                [action.lang]: lang_sessions(state[action.lang], action)
            });
        default:
            return state;
    }
}

function lang_sessions(state = {}, action) {
    // console.debug(state);
    // console.debug(action);
    switch (action.type) {
        case DO_LOGIN_LESSON_SUCCESS:
            return Object.assign({}, state, {
                [action.lesson_id]: {
                    session_id: action.session_id,
                    login_session_uid: action.login_session_uid,
                    isFetching: false,
                    lastUpdated: action.receivedAt
                }
            });
        case DO_LOGIN_LESSON_REQUEST:
            return Object.assign({}, state, {
                [action.lesson_id]: {
                    isFetching: true
                }
            });
        case DO_LOGIN_LESSON_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state.sessions, {
                [action.lesson_id]: {
                    isFetching: false
                }
            });
        default:
            return state;
    }
}

function displayFields(state = DisplayFields, action) {
    switch (action.type) {
        case UPDATE_DISPLAY_FIELD:
            let ind = findIndex(state, {'name': action.field_name});
            // console.debug(ind);
            // console.debug(state);
            return [
                ...state.slice(0, ind),
                Object.assign({}, state[ind], {
                    isOn: action.isOn
                }),
                ...state.slice(ind+1)
            ];
        default:
            return state;
    }
}

function filters(state = {
    unit: '',
    course: '',
    lesson: '',
    topic: '',
    selectedRow: -1,
    messages: [],
    sessions: {},
    displayFields: DisplayFields,
    sort_field: null,
    sort_dir: 'asc',
    username: '',
    qa_status: null
}, action) {
    switch (action.type) {
        case SET_UNIT:
            return Object.assign({}, state, {
                unit: action.unit,
                course: '',
                lesson: '',
                topic: ''
            });
        case SET_COURSE:
            //  Can't set course if we haven't set unit
            if (state.unit == '') return state;

            return Object.assign({}, state, {
                course: action.course,
                lesson: '',
                topic: ''
            });
        case SET_LESSON:
            //  Can't set lesson if we haven't set unit and course
            if (state.unit == '' || state.course == '') return state;

            return Object.assign({}, state, {
                lesson: action.lesson,
                topic: ''
            });
        case SET_TOPIC:
            //  Can't set topic if we haven't set unit, course, and lesson
            if (state.unit == '' || state.course == '' || state.lesson == '') return state;

            return Object.assign({}, state, {
                topic: action.topic
            });
        case SET_SELECTED_ROW:
            return Object.assign({}, state, {
                selectedRow: action.row_index
            });

        case DO_LOGIN_LESSON_SUCCESS:
        case DO_LOGIN_LESSON_REQUEST:
            return Object.assign({}, state, {
                sessions: sessions(state.sessions, action)
            });

        case FETCH_EDITOR_CONTENT_FAILURE:
        case FETCH_IMAGES_FAILURE:
        // case DO_LOGIN_LESSON_FAILURE:
        case FETCH_APP_CONSTANTS_FAILURE:
        case UPDATE_FIELD_VAL_FAILURE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: 'danger',
                        message: action.ex.message,
                        visible: true
                    }
                ]
            });

        // case FETCH_EDITOR_CONTENT_SUCCESS:
        //     return Object.assign({}, state, {
        //         messages: [
        //             ...state.messages,
        //             {
        //                 type: 'success',
        //                 message: 'Message',
        //                 visible: true
        //             }
        //         ]
        //     });

        case ADD_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: action.message_type,
                        message: action.message,
                        visible: true
                    }
                ]
            });

        case CLOSE_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages.slice(0, action.index),
                    Object.assign({}, state.messages[action.index], {
                        visible: false
                    }),
                    ...state.messages.slice(action.index+1)
                ]
            });

        case UPDATE_DISPLAY_FIELD:
            return Object.assign({}, state, {
                displayFields: displayFields(state.displayFields, action)
            });

        case SET_SORT_FIELD:
            return Object.assign({}, state, {
                sort_field: action.field_name,
                sort_dir: action.sort_dir
            });

        case SET_USER:
            return Object.assign({}, state, {
                username: action.username
            });

        case SET_QA_STATUS:
            return Object.assign({}, state, {
                qa_status: action.val
            });

        default:
            return state;
    }
}

function editor_content(state = {
    isFetching: false,
    content: []
}, action) {
    switch (action.type) {
        case FETCH_EDITOR_CONTENT_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                content: action.content,
                lastUpdated: action.receivedAt
            });
        case FETCH_EDITOR_CONTENT_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_EDITOR_CONTENT_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

function images_by_tag(state = {}, action) {
    switch (action.type) {
        case FETCH_IMAGES_SUCCESS:
        case FETCH_IMAGES_REQUEST:
        case FETCH_IMAGES_FAILURE:
        case REFETCH_IMAGES_SUCCESS:
        case REFETCH_IMAGES_REQUEST:
        case REFETCH_IMAGES_FAILURE:
        case FETCH_IMAGE_DETAIL_SUCCESS:
            return Object.assign({}, state, {
                [action.tag]: actual_tag_images(state[action.tag], action)
            });
        default:
            return state;
    }
}

function actual_tag_images(state = {
    isFetching: false,
    items: [],
    lastUpdated: null
}, action) {
    switch (action.type) {
        case FETCH_IMAGES_SUCCESS:
            // let image_items = map(action.images, 'image_id');

            let page_image_items = chain(action.images)
                .map(page_image => {
                    return [`${page_image.image_id}-${page_image.pageid}`, page_image];
                })
                .fromPairs()
                .value();

            // console.debug(page_image_items);

            return Object.assign({}, state, {
                isFetching: false,
                items: page_image_items,
                lastUpdated: action.receivedAt
            });
        case FETCH_IMAGES_REQUEST:
        case REFETCH_IMAGES_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_IMAGES_FAILURE:
        case REFETCH_IMAGES_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            });
        case REFETCH_IMAGES_SUCCESS:
            let items = reduce(action.data, (result, image) => {
                return results[`${image.image_id}-${image.pageid}`] = Object.assign({}, results[`${image.image_id}-${image.pageid}`], image)
            }, state.items);

            // console.debug(items);

            return Object.assign({}, state, {
                isFetching: false,
                items
            });

        case FETCH_IMAGE_DETAIL_SUCCESS:
            // console.debug(state);
            let these_keys = chain(state.items)
                                .keys()
                                .filter((key) => {
                                    return key.indexOf(`${action.data.image_id}-`) == 0;
                                })
                                .value();

            // console.debug(these_keys);

            let new_items;
            if (these_keys.length > 0) 
                new_items = reduce(these_keys, (items, key) => {
                    items[key] = Object.assign({}, items[key], omit(action.data, ['pageno', 'pageid', 'image_seq', 'row_seq', 'page_title', 'lesson_topic', 'is_first_page', 'course_tag', 'lesson_tag', 'lang_id']));
                    return items;
                }, state.items);
            else
                new_items = state.items;

            // console.debug(new_items);
            return Object.assign({}, state, {
                items: new_items
            })


            // return Object.assign({}, state, {
            //     items: Object.assign({}, state.items, {
            //         []
            //     })
            // })

        default:
            return state;
    }
}

function images(state={}, action) {
    // console.debug(action);
    switch (action.type) {
        case FETCH_IMAGES_SUCCESS:
        case FETCH_IMAGES_REQUEST:
        case FETCH_IMAGES_FAILURE:
            let image_items = chain(action.images)
                .map(image => {
                    image.live_update = true;
                    return [image.image_id, image];
                })
                .fromPairs()
                .value();
            // console.debug(action.images);
            // console.debug(image_items);
            return Object.assign({}, state, image_items);

        case FETCH_IMAGE_DETAIL_SUCCESS:
        case SET_FIELD_IMMEDIATELY:
        case UPDATE_FIELD_VAL_SUCCESS:
        case SET_LIVE_UPDATE:
            return Object.assign({}, state, {
                [action.image_id]: image(state[action.image_id], action)
            });
            // return Object.assign({}, state, {
            //     [action.image_id]: action.data
            // })
        default:
            return state;

    }
}

function image(state={
    live_update: true
}, action) {
    switch (action.type) {
        case FETCH_IMAGE_DETAIL_SUCCESS:
            //  We're going to merge the detail stuff with the list stuff to make sure we have everything.
            //  For Spanish we also need to merge with the English to get the defaults.
            console.debug(state);
            let out;
            if (action.data.is_primary) 
                out = Object.assign({}, state, action.data);
            else {
                out = Object.assign({}, state, action.parent);
                console.debug('parent', out);
                out = Object.assign({}, out, action.data);
                out;
            }
            console.debug(out);
            return out;
        case SET_FIELD_IMMEDIATELY:
            return Object.assign({}, state, {
                [action.field_name]: action.field_val
            })
        case UPDATE_FIELD_VAL_SUCCESS:
            // console.debug(action.field_name, action.field_val);
            // let d = new Date();
            // let ds = `${d.getFullYear()}-${(d.getMonth()<9) ? '0': ''}${d.getMonth()+1}-${(d.getDate()<10) ? '0' : ''}${d.getDate()} ${(d.getHours()<10 ? '0' : '')}${d.getHours()}:${(d.getMinutes()<10 ? '0' : '')}${d.getMinutes()}`

            return Object.assign({}, state, {
                [action.field_name]: action.field_val,
                // update_date: ds
            });

        case SET_LIVE_UPDATE:
            return Object.assign({}, state, {
                live_update: action.islive
            })


            
        default:
            return state;
    }
}

function constants(state={
    qa_status_val: [],
    qa_problem_bits: []
}, action) {
    switch (action.type) {
        case FETCH_APP_CONSTANTS_SUCCESS:
            let data_key = action.data[0];

            let constants_obj = {};
            forEach(data_key, (obj) => {
                constants_obj[obj.colname] = action.data[obj.row_id];
            });

            return Object.assign({}, state, constants_obj);
        default:
            return state;
    }
}

const svgApp = combineReducers({
    editor_content,
    filters,
    images_by_tag,
    images,
    constants
});

export default svgApp;