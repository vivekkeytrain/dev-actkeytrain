import fetchJsonp from 'fetch-jsonp';
import fetch from 'isomorphic-fetch';
import { invert, map } from 'lodash';

import PageLinks from './components/PageLinks';

/*  Action types    */

export const SET_UNIT = 'SET_UNIT';
export const SET_COURSE = 'SET_COURSE';
export const SET_LESSON = 'SET_LESSON';
export const SET_TOPIC = 'SET_TOPIC';
export const SET_SELECTED_ROW = 'SET_SELECTED_ROW';

export const SET_SESSION = 'SET_SESSION';

export const SET_FIELD_IMMEDIATELY = 'SET_FIELD_IMMEDIATELY';

export const UPDATE_DISPLAY_FIELD = 'UPDATE_DISPLAY_FIELD';
export const LOAD_DISPLAY_FIELDS = 'LOAD_DISPLAY_FIELDS';

export const SET_SORT_FIELD = 'SET_SORT_FIELD';

export const SET_USER = 'SET_USER';

export const SET_QA_STATUS = 'SET_QA_STATUS';

export const SET_LIVE_UPDATE = 'SET_LIVE_UPDATE';

export const ADD_MESSAGE = 'ADD_MESSAGE';
export const CLOSE_MESSAGE = 'CLOSE_MESSAGE';

/*  Async action types   */

export const FETCH_EDITOR_CONTENT_SUCCESS = 'FETCH_EDITOR_CONTENT_SUCCESS';
export const FETCH_EDITOR_CONTENT_FAILURE = 'FETCH_EDITOR_CONTENT_FAILURE';
export const FETCH_EDITOR_CONTENT_REQUEST = 'FETCH_EDITOR_CONTENT_REQUEST';

export const FETCH_IMAGES_SUCCESS = 'FETCH_IMAGES_SUCCESS';
export const FETCH_IMAGES_FAILURE = 'FETCH_IMAGES_FAILURE';
export const FETCH_IMAGES_REQUEST = 'FETCH_IMAGES_REQUEST';

export const REFETCH_IMAGES_SUCCESS = 'REFETCH_IMAGES_SUCCESS';
export const REFETCH_IMAGES_FAILURE = 'REFETCH_IMAGES_FAILURE';
export const REFETCH_IMAGES_REQUEST = 'REFETCH_IMAGES_REQUEST';

export const FETCH_IMAGE_DETAIL_SUCCESS = 'FETCH_IMAGE_DETAIL_SUCCESS';
export const FETCH_IMAGE_DETAIL_FAILURE = 'FETCH_IMAGE_DETAIL_FAILURE';
export const FETCH_IMAGE_DETAIL_REQUEST = 'FETCH_IMAGE_DETAIL_REQUEST';

export const UPDATE_SVG_STATUS_SUCCESS = 'UPDATE_SVG_STATUS_SUCCESS';
export const UPDATE_SVG_STATUS_FAILURE = 'UPDATE_SVG_STATUS_FAILURE';
export const UPDATE_SVG_STATUS_REQUEST = 'UPDATE_SVG_STATUS_REQUEST';

export const DO_LOGIN_LESSON_REQUEST = 'DO_LOGIN_LESSON_REQUEST';
export const DO_LOGIN_LESSON_SUCCESS = 'DO_LOGIN_LESSON_SUCCESS';
export const DO_LOGIN_LESSON_FAILURE = 'DO_LOGIN_LESSON_FAILURE';

export const UPDATE_FIELD_VAL_SUCCESS = 'UPDATE_FIELD_VAL_SUCCESS';
export const UPDATE_FIELD_VAL_FAILURE = 'UPDATE_FIELD_VAL_FAILURE';
export const UPDATE_FIELD_VAL_REQUEST = 'UPDATE_FIELD_VAL_REQUEST';

export const FETCH_APP_CONSTANTS_SUCCESS = 'FETCH_APP_CONSTANTS_SUCCESS';
export const FETCH_APP_CONSTANTS_FAILURE = 'FETCH_APP_CONSTANTS_FAILURE';
export const FETCH_APP_CONSTANTS_REQUEST = 'FETCH_APP_CONSTANTS_REQUEST';

/*  Other constants */
export const LangOptions = {
    EN: 1,
    ES: 2
};

// export const QA_STATUS_VALS = {
//     1: 'No-Go',
//     2: 'Liveable',
//     4: 'Perfect'
// }

// export const QA_PROBLEM_BITS = {

// }

export const IMAGE_ROOT_URL = 'http://es.run.keytrain.com/objects/'

export const DisplayFields = [
    {
        name: 'pageid',
        isOn: true
    },
    {
        name: 'image_id',
        label: 'ImageID',
        isOn: true
    },
    {
        name: 'image_tag',
        label: 'Tag',
        isOn: true
    },
    {
        name: 'lesson_topic',
        label: 'Lesson - Topic',
        isOn: true
    },
    {
        name: 'page_title',
        label: 'Page Title',
        isOn: true
    },
    {
        name: 'pageno',
        label: `#`,
        isOn: true
    },
    {
        name: 'lang_id',
        label: 'Lang',
        isOn: true,
        displayFunc: (val) => {
            let lang_display;

            if (val == 3) {
                lang_display = `EN,${String.fromCharCode(160)}ES`; // 160 is nbsp
            } else {
                lang_display = invert(LangOptions)[val];
            }
            return lang_display;
        }
    },
    {
        name: 'image_type_desc',
        label: 'Types',
        isOn: true
    },
    {
        name: 'qa_status_desc',
        label: 'Status',
        isOn: true
    },
    {
        name: 'image_orig_desc',
        label: 'Origination',
        isOn: true
    },
    // {
    //     name: 'page_count',
    //     isOn: true
    // },
    // {
    //     name: 'pageid',
    //     isOn: true
    // },
    {
        name: 'update_date',
        label: 'Updated',
        isOn: true,
        displayFunc: (val) => {
            // console.debug(val);
            if (!val) return '';
            let d = new Date(val);
            let m = ((d.getMonth() < 9) ? '0' : '') + (d.getMonth()+1);
            let dy = ((d.getDate() < 10) ? '0' : '') + d.getDate();
            return d.getFullYear() + String.fromCharCode(8209) + m + String.fromCharCode(8209) + dy; // 8209 is non-breaking dash
        }
    },
    // {
    //     name: 'update_by',
    //     label: 'Updated By',
    //     isOn: true
    // },
    {
        name: 'pageid',
        label: 'Link',
        isOn: true,
        sortable: false,
        displayFunc: (val, props) => {
            return <PageLinks {...props} />
        }
    }
];

export const DETAIL_FIELDS = [
    'is_primary', 'has_svg', 'image_desc', 'content_notes', 'qa_notes', 'update_by', 'width', 'height', 'alt_text', 'svg_code', 'export_filename', 'qa_problem_desc', 'qa_problem_bits', 'qa_status_val'
];

export const EDITABLE_FIELDS = [
    'image_desc', 'update_by', 'svg_code', 'alt_text', 'width', 'height', 'production_notes', 'qa_notes', 'qa_problem_desc', 'qa_problem_bits', 'qa_status_val', 'content_notes', 'export_filename'
]


/*  Action creators */

export function setUnit(unit) {
    return function(dispatch) {
        localStorage.setItem('content_unit', unit);
        dispatch(setUnitStore(unit));
    }
}

export function setUnitStore(unit) {
    return { type: SET_UNIT, unit };
}

export function setCourse(course) {
    return function(dispatch) {
        localStorage.setItem('content_course', course);
        dispatch(setCourseStore(course));
    }
}

export function setCourseStore(course) {
    return { type: SET_COURSE, course };
}

export function setLesson(lesson) {
    return function(dispatch) {
        localStorage.setItem('content_lesson', lesson);
        dispatch(setLessonStore(lesson));
    }
}

export function setLessonStore(lesson) {
    return { type: SET_LESSON, lesson };
}

export function setTopic(topic) {
    return function(dispatch) {
        localStorage.setItem('content_topic', topic);
        dispatch(setTopicStore(topic));
    }
}

export function setTopicStore(topic) {
    return { type: SET_TOPIC, topic };
}

export function setSelectedRow(row_index) {
    return { type: SET_SELECTED_ROW, row_index };
}

export function setSession(session_id, login_session_uid) {
    return {
        type: SET_SESSION,
        session_id,
        login_session_uid
    }
}

export function setFieldImmediately(image_id, field_name, field_val) {
    return {
        type: SET_FIELD_IMMEDIATELY,
        image_id,
        field_name,
        field_val
    }
}

export function updateDisplayField(field_name, isOn) {
    return {
        type: UPDATE_DISPLAY_FIELD,
        field_name,
        isOn
    }
}

export function setSortField(field_name, sort_dir) {
    return {
        type: SET_SORT_FIELD,
        field_name,
        sort_dir
    }
}

export function updateQAStatus(val) {
    return {
        type: SET_QA_STATUS,
        val
    }
}

export function setLiveUpdate(islive, image_id) {
    return {
        type: SET_LIVE_UPDATE,
        islive,
        image_id
    }
}

export function addMessage(message_type, message) {
    return {
        type: ADD_MESSAGE,
        message_type,
        message
    }
}

export function closeMessage(index) {
    return {
        type: CLOSE_MESSAGE,
        index
    }
}

export function updateLocalDisplayField(field_name, isOn) {
    // console.debug('updateLocalDisplayField', field_name, isOn);
    return function(dispatch) {
        localStorage.setItem('display_field_' + field_name, isOn ? 'true' : 'false');
        // console.debug(field_name, localStorage.getItem('display_field_' + field_name));
        dispatch(updateDisplayField(field_name, isOn));
    }
}

export function loadDisplayFields() {
    // console.debug(DisplayFields);
    
    let localDisplayFields = [];
    DisplayFields.forEach((field, ind) => {
        // console.debug(field.name, localStorage.getItem('display_field_' + field.name));
        let isOn = localStorage.getItem('display_field_' + field.name) ? localStorage.getItem('display_field_' + field.name) === 'true' : field.isOn;
        localDisplayFields[ind] = Object.assign({}, field, {
            isOn: isOn
        });
    });
    // console.debug(localDisplayFields);


    return function(dispatch) {
        map(localDisplayFields, (field, index) => {
            dispatch(updateLocalDisplayField(field.name, field.isOn))
        });
        // return {
        //     type: LOAD_DISPLAY_FIELDS
        // }
    }
}

export function setUser(username) {
    return {
        type: SET_USER,
        username
    }
}

/*  Async actions   */

export function fetchEditorContentSuccess(content) {
    // console.debug('fetchEditorContentSuccess');
    // console.debug(content);
    return {
        type: FETCH_EDITOR_CONTENT_SUCCESS,
        content
    }
}

export function fetchEditorContentFailure(ex) {
    return {
        type: FETCH_EDITOR_CONTENT_FAILURE,
        ex
    }
}

export function fetchEditorContentRequest() {
    return { type: FETCH_EDITOR_CONTENT_REQUEST };
}

export function fetchEditorContent() {
    // console.log('fetchEditorContent');
    return function(dispatch) {
        dispatch(fetchEditorContentRequest());

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_content_list/jsonp', {
                timeout: 20000
            })
            .then(
                response => response.json(),
                ex => dispatch(fetchEditorContentFailure(ex))
            )
            .then(json => dispatch(fetchEditorContentSuccess(json.ResultSets[0])))
            .catch(ex => dispatch(fetchEditorContentFailure(ex)));
    }
}

function updateSVGStatusSuccess(lang, svg_id, status) {
    return {
        type: UPDATE_SVG_STATUS_SUCCESS,
        lang,
        svg_id,
        status
    }
}

function updateSVGStatusFailure(ex) {
    return {
        type: UPDATE_SVG_STATUS_FAILURE,
        ex
    }
}

function updateSVGStatusRequest(lang, svg_id, status) {
    return {
        type: UPDATE_SVG_STATUS_REQUEST,
        lang,
        svg_id,
        status
    }
}

export function updateSVGStatus(lang, svg_id, status) {
    return function(dispatch) {
        dispatch(updateSVGStatusRequest(lang, svg_id, status));

        return fetchJsonp('')
            .then(
                response => response.json(),
                ex => dispatch(updateSVGStatusFailure(ex))
            )
            .then(json => dispatch(updateSVGStatusSuccess(lang, svg_id, status)))
            .catch(ex => dispatch(updateSVGStatusFailure(ex)));
    }
}

function fetchImagesSuccess(tag, images) {
    return {
        type: FETCH_IMAGES_SUCCESS,
        tag,
        images,
        receivedAt: Date.now()
    }
}

function fetchImagesFailure(ex, tag) {
    return {
        type: FETCH_IMAGES_FAILURE,
        ex,
        tag
    }
}

function fetchImagesRequest(tag) {
    return {
        type: FETCH_IMAGES_REQUEST,
        tag
    }
}

function fetchImages(tag) {
    console.debug('fetchImages', tag);
    return function(dispatch) {
        dispatch(fetchImagesRequest(tag));
        return fetchJsonp(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_image_data/?content_tag=${tag}`, { timeout: 20000 })
            .then(
                response => response.json(),
                ex => dispatch(fetchImagesFailure(ex, tag))
            )
            .then(json => dispatch(fetchImagesSuccess(tag, json.ResultSets[0])))
            .catch(ex => dispatch(fetchImagesFailure(ex, tag)))
    }
}

function shouldFetchImages(state, tag) {
    if (!tag) return false;

    let images = state.images_by_tag;
    if (!images[tag]) return true;

    if (images[tag].isFetching) {
        return false;
    } else {
        return (Date.now() - images[tag].lastUpdated) > (1000 * 60 * 10); //    Refetch after 10 minutes
    }
}

export function fetchImagesIfNeeded(unit, course, lesson, topic, force=false) {
    console.debug('fetchImagesIfNeeded', unit, course, lesson, topic);
    return (dispatch, getState) => {
        let tag;

        if (topic && topic != -1)
            tag = 'T' + topic;
        else if (lesson && lesson != -1 && topic == -1)
            tag = 'L' + lesson;
        else if (course && course != -1 && lesson == -1)
            tag = 'C' + course;
        else if (unit && course == -1)
            tag = 'U' + unit;

        if (force || shouldFetchImages(getState(), tag)) {
            return dispatch(fetchImages(tag));
        } else {
            Promise.resolve();
        }
    }
}

function refetchImagesRequest(image_id, content_tag) {
    return {
        type: REFETCH_IMAGES_REQUEST,
        image_id,
        content_tag
    }
}

function refetchImagesFailure(ex, image_id, content_tag) {
    return {
        type: REFETCH_IMAGES_FAILURE,
        image_id,
        content_tag,
        ex
    }
}

function refetchImagesSuccess(image_id, content_tag, data) {
    return {
        type: REFETCH_IMAGES_SUCCESS,
        image_id,
        content_tag,
        data
    }
}

export function refetchImages(image_id, tag) {
    console.debug('refetchImages', image_id, tag);
    return function(dispatch) {
        dispatch(refetchImagesRequest(image_id, tag));
        return fetchJsonp(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_image_data/?content_tag=${tag}&image_id=${image_id}`, { timeout: 20000 })
            .then(
                response => response.json(),
                ex => dispatch(refetchImagesFailure(ex, image_id, tag))
            )
            .then(json => dispatch(refetchImagesSuccess(image_id, tag, json.ResultSets[0])))
            .catch(ex => dispatch(refetchImagesFailure(ex, image_id, tag)))
    }
}

function fetchImageDetailRequest(image_id) {
    return {
        type: FETCH_IMAGE_DETAIL_REQUEST,
        image_id
    }
}

function fetchImageDetailFailure(ex, image_id) {
    return {
        type: FETCH_IMAGE_DETAIL_FAILURE,
        ex,
        image_id
    }
}

function fetchImageDetailSuccess(image_id, data, parent, tag) {
    return {
        type: FETCH_IMAGE_DETAIL_SUCCESS,
        image_id,
        data,
        parent,
        tag
    }
}

export function fetchImageDetail(image_id, parent_image, tag) {
    console.debug('fetchImageDetail', image_id, parent_image);
    if (!image_id) return;

    return function(dispatch) {
        dispatch(fetchImageDetailRequest(image_id));
        console.debug(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_image_data/?image_id=${image_id}`);
        return fetchJsonp(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_image_data/?image_id=${image_id}`, { timeout: 20000 })
            .then(
                response => response.json(),
                ex => dispatch(fetchImageDetailFailure(ex, image_id))
            )
            .then(json => dispatch(fetchImageDetailSuccess(image_id, json.ResultSets[0][0], parent_image, tag)))
            // .catch(ex => dispatch(fetchImageDetailFailure(ex, image_id)))
    }
}



function doLoginLessonSuccess(session_id, login_session_uid, lesson_id, lang) {
    // console.debug('login_session_uid: ' + login_session_uid);

    return { 
        type: DO_LOGIN_LESSON_SUCCESS,
        session_id,
        login_session_uid,
        lesson_id,
        lang,
        receivedAt: Date.now()
    };
}

function doLoginLessonFailure(ex) {
    return {
        type: DO_LOGIN_LESSON_FAILURE,
        ex
    }
}

function doLoginLessonRequest(lesson_id, lang) {
    return { 
        type: DO_LOGIN_LESSON_REQUEST,
        lesson_id,
        lang
    };
}


function shouldDoLoginLesson(state, lesson_id, lang) {
    // console.debug('shouldDoLoginLesson: ' + lesson_id + ', ' + lang);
    if (!lesson_id || lesson_id == -1) return false;
    const sessions = state.filters.sessions;
    // console.debug(sessions);
    if (!sessions[lang]) return true;
    const session = sessions[lang][lesson_id];
    // console.debug(session);

    if (!session) {
        return true;
    } else if (session.isFetching) {
        return false
    } else {
        return (Date.now() - session.lastUpdated) > (1000 * 60 * 15); //   15 minutes old, refetch
    }
}


function doLoginLesson(lesson_id, lang, try_count=0) {
    return function(dispatch) {
        dispatch(doLoginLessonRequest(lesson_id, lang));

        let lang_code = invert(LangOptions)[lang];

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_init_review_session?lesson_id=' + lesson_id + '&lang_code=' + lang_code)
            .then(
                response => response.json(),
                ex => {
                    dispatch(doLoginLessonFailure(ex));
                    if (try_count < 3)
                        dispatch(doLoginLesson(lesson_id, lang, ++try_count)); //    Try again if we've failed. Not sure why we do
                }
            )
            .then(json => dispatch(doLoginLessonSuccess(json.ResultSets[0][0].session_id, json.ResultSets[0][0].luid_str, lesson_id, lang)))
            .catch(ex => dispatch(doLoginLessonFailure(ex)));
    }
}

export function doLoginIfNeeded(lesson_id, lang) {
    console.debug('doLoginIfNeeded', lesson_id, lang);
    return (dispatch, getState) => {
        if (shouldDoLoginLesson(getState(), lesson_id, lang)) {
            // console.debug('doLogin');
            return dispatch(doLoginLesson(lesson_id, lang));
        } else {
            return Promise.resolve();
        }
    }
}

function updateFieldSuccess(image_id, field_name, field_val) {
    return {
        type: UPDATE_FIELD_VAL_SUCCESS,
        image_id,
        field_name,
        field_val
    }
}

function updateFieldRequest(image_id, field_name, field_val, updated_by) {
    return {
        type: UPDATE_FIELD_VAL_REQUEST,
        image_id,
        field_name,
        field_val,
        updated_by
    }
}

function updateFieldFailure(image_id, field_name, ex) {
    return {
        type: UPDATE_FIELD_VAL_FAILURE,
        image_id,
        field_name,
        ex
    }
}

export function updateField(image_id, field_name, field_val, updated_by) {
    // console.debug(image_id, field_name, field_val, updated_by);
    return function(dispatch) {
        dispatch(updateFieldRequest(image_id, field_name, field_val, updated_by));

        // console.debug('/api/edapi/dbo.api_sp_image_setval');
        // console.debug(JSON.stringify({
        //         image_id,
        //         varname: field_name,
        //         varval: field_val,
        //         update_by: updated_by
        //     }))

        return fetch('/api/edapi/dbo.api_sp_image_setval', {
            method: 'post',
            body: JSON.stringify({
                image_id,
                varname: field_name,
                varval: field_val,
                update_by: updated_by
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': window.auth
            }
        })
            .then(
                response => response.json(),
                ex => dispatch(updateFieldFailure(image_id, field_name, ex))
            )
            .then(json => {
                console.log(json);
                let status = json.ResultSets[0][0].status;
                if (status < 100) {
                    dispatch(updateFieldSuccess(image_id, field_name, field_val));
                } else {
                    dispatch(updateFieldFailure(image_id, field_name, {message: json.ResultSets[0][0].statdesc}));
                }
            })
            .catch(ex => {
                console.log('Catch error')
                console.log(ex);
                dispatch(updateFieldFailure(image_id, field_name, ex))

            });

  //       request.post({
  //           '/api/edapi/dbo.api_sp_image_setval'
  //       })

  //       request.post({
  //   url: api_url,
  //   json: req.body
  // }, function(err, resp, body) {
  //   if (err) {
  //     console.log(err);
  //     res.status(500).send('Error');
  //   } else {
  //     res.send(body);
  //   }
  // });

        // console.debug(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_image_setval?image_id=${image_id}&varname=${field_name}&varval=${field_val}&update_by=${updated_by}`);
        // return fetchJsonp(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_image_setval?image_id=${image_id}&varname=${field_name}&varval=${field_val}&update_by=${updated_by}`)
        //     .then(
        //         response => response.json(),
        //         ex => dispatch(updateFieldFailure(image_id, field_name, ex))
        //     )
        //     .then(json => {
        //         let status = json.ResultSets[0][0].status;
        //         if (status < 100) {
        //             dispatch(updateFieldSuccess(image_id, field_name, field_val));
        //         } else {
        //             dispatch(updateFieldFailure(image_id, field_name, json.ResultSets[0].statdesc));
        //         }
        //     })
        //     .catch(ex => dispatch(updateFieldFailure(image_id, field_name, ex)));
    }
}


export function fetchAppConstantsSuccess(data) {
    // console.debug('fetchEditorContentSuccess');
    // console.debug(content);
    return {
        type: FETCH_APP_CONSTANTS_SUCCESS,
        data
    }
}

export function fetchAppConstantsFailure(ex) {
    return {
        type: FETCH_APP_CONSTANTS_FAILURE,
        ex
    }
}

export function fetchAppConstantsRequest() {
    return { type: FETCH_APP_CONSTANTS_REQUEST };
}

export function fetchAppConstants() {
    // console.log('fetchEditorContent');
    return function(dispatch) {
        dispatch(fetchAppConstantsRequest());

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_app_constants?appname=svgmanager')
            .then(
                response => response.json(),
                ex => dispatch(fetchAppConstantsFailure(ex))
            )
            .then(json => dispatch(fetchAppConstantsSuccess(json.ResultSets)))
            .catch(ex => dispatch(fetchAppConstantsFailure(ex)));
    }
}