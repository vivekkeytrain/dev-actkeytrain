import { Component, PropTypes } from 'react';
import { map, invert } from 'lodash';
import PageLinks from './PageLinks';
import { LangOptions } from '../actions';
// import PageFieldSelect from './PageFieldSelect';
// import PageTextField from './PageTextField';
// import EditableFields from './EditableFields';
// import ReadOnlyFields from './ReadOnlyFields';

import classNames from 'classnames';

export default class ImageRow extends Component {
    renderCell(displayField) {
        let cell_data = this.props.image[displayField.name];

        if (typeof displayField.displayFunc === 'function') {
            let dfunc = displayField.displayFunc.bind(this, cell_data, this.props);
            cell_data = dfunc();
        }

        return cell_data;
    }

    setFocus(index, e) {
        // console.debug(index);
        // console.debug(this['row_' + index]);
        // this['row_' + index].getDOMNode().focus();
        this.props.setHighlightedRow(index);
        this.props.setSelectedRow(index);
    }

    render() {
        // console.debug(this.props);
        let classes = classNames({
            warning: (this.props.selectedRow == this.props.index),
            notFirst: (this.props.image.is_first_page == 0),
            isFirst: (this.props.image.is_first_page == 1 && this.props.image.page_count > 1),
            highlighted: this.props.highlighted
        });

        let lang_display;
        if (this.props.image.lang_id == 3) {
            lang_display = 'Both';
        } else {
            lang_display = invert(LangOptions)[this.props.image.lang_id];
        }


        return (
            <tr onClick={this.setFocus.bind(this, this.props.index)} className={classes} ref={(ref) => this["row_" + this.props.index] = ref}>
                {
                    this.props.displayFields.map((field, ind) => {
                        return <td key={ind}>{this.renderCell(field)}</td>
                    })
                }
                <td style={{'textAlign': 'center'}}><i onClick={this.setFocus.bind(this, this.props.index)} className='glyphicon glyphicon-list-alt' style={{'fontSize': '1.5em', 'cursor': 'pointer'}}></i></td>
{/*                <td>{this.props.image.image_id}</td>
                <td>{this.props.image.image_tag}</td>
                <td>{this.props.image.lesson_topic}</td>
                <td>{this.props.image.page_title}</td>
                <td>{this.props.image.pageno}</td>
                <td>{lang_display}</td>
                <td>{this.props.image.image_type_desc}</td>
                <td>{this.props.image.image_orig_desc}</td>
                <td>{this.props.image.last_updated}</td>
                <td>{this.props.image.update_by}</td>
                <PageLinks {...this.props}></PageLinks>*/}
            </tr>
        )
    }
}

