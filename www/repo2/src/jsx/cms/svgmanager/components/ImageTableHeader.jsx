import { Component, PropTypes } from 'react';

import SortHeader from './SortHeader';

export default class ImageTableHeader extends Component {
    render() {
        return (
            <thead>
                <tr>
                    {
                        this.props.displayFields.map((field, ind) => {
                            return <SortHeader displayField={field} sortField={this.props.sortField} sortDir={this.props.sortDir} key={ind} setSortField={this.props.setSortField} />
                        })
                    }
                    <th>Detail</th>
{/*                    <th>Image ID</th>
                    <th>Tag</th>
                    <th>Lesson - Topic</th>
                    <th>Page Title</th>
                    <th>Page #</th>
                    <th>Lang</th>
                    <th>Type</th>
                    <th>Origination</th>
                    <th>Last Updated</th>
                    <th>Updated By</th>
                    <th>Link</th>*/}
                </tr>
            </thead>
        )
    }
}