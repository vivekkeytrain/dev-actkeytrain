import { Component } from 'react';
import { isEqual } from 'lodash';

import StatiButtonGroup from './StatiButtonGroup';

import ReactStyle from 'global/jsx/react-styles/src/ReactStyle';

export default class SVGDisplay extends Component {
    constructor(props) {
        super(props);

        this.state = {
            svg_height: 100,
            svg_width: 100
        }
    }

    // componentDidMount() {
    //     $('input.floatlabel').floatlabel({
    //       slideInput: false,
    //       labelStartTop: '5px',
    //       labelEndTop: '-2px'
    //     });
    // }

    componentWillMount() {
        ReactStyle.addRules(ReactStyle.create({
            ['#svg_size_slider_' + this.props.langCode + ' .slider-selection']: {
                background: '#55C9A6'
            }
        }));
    }

    componentDidMount() {
        this.initSliders();
    }

    componentWillUnmount() {
        $('#svg_size_' + this.props.langCode).off('slide');
    }

    componentDidUpdate(prev_props, prev_state) {
        //  Only reinit if we're actually changing images
        if (this.props.image.export_filename != prev_props.image.export_filename)
            this.initSliders()
    }

    initSliders() {
        // let slider_val = (100 * parseFloat($(`#svg-container-${this.props.langCode}`).css('width')) / parseFloat($(`#svg-container-${this.props.langCode}`).parent().css('width')));
        // console.debug(slider_val);

        $('#svg_size_' + this.props.langCode).slider({
            min: 0,
            max: 100,
            step: 1,
            value: 100,
            precision: 1,
            // tooltip: 'hide',
            postfix: '%',
            formater: (value) => {
                return `${value}%`
            }
        });
        $('#svg_size_' + this.props.langCode).off('slide').on('slide', (slideEvt) => {
            $(`#svg-container-${this.props.langCode}`).width(slideEvt.value + '%')
        });

        $(`#svg-container-${this.props.langCode}`).width('100%')

        //  Explicitly set the svg-holder width/height so it is static
        //  First we have to remove what is there so it will naturally size, then we'll set those in stone
        let svg_holder = $(`#svg-holder-${this.props.langCode}`);
        // svg_holder.css({'width': '', 'height': ''});
        // svg_holder.css({'width': svg_holder.width() + 'px', 'height': svg_holder.height() + 'px'});
        $('#svg_size_' + this.props.langCode).parents('.slider-horizontal').width((svg_holder.width()) + 'px');
        
    }

    // componentWillReceiveProps(props) {
    //     let update_fields = ['svg_code'];

    //     update_fields.forEach((field) => {
    //         let val = props.image[field];
    //         if (val) {
    //             React.findDOMNode(this[`_${field}`]).value = val;
    //         } else {
    //             React.findDOMNode(this[`_${field}`]).value = '';
    //         }
    //     })
    // }

    immediateUpdateField(field_name, e) {
        if (this.props.image.live_update)
            this.props.setFieldImmediately(this.props.image.image_id, field_name, e.target.value);
    }
    
    handleSVGCodeUpdate(e) {
        // console.debug(arguments);
        // console.debug(e.target);
        this.props.onUpdateFieldValue(this.props.image.image_id, 'svg_code', btoa(unescape(encodeURIComponent(e.target.value))))
    }

    unescapeSVGCode(str) {
        if (!str) return str;

        // console.debug(str);
        // alert(str);
        // console.debug(atob(str));
        // console.debug(escape(atob(str)));
        // console.debug(decodeURIComponent(escape(atob(str))));
        str = escape(atob(str));
        // console.debug(str);
        try {
            str = decodeURIComponent(str);
        } catch (e) { // Malformed URI. Unicode issue.
            str = unescape(str);
        }
        // console.debug(str);

        let r = /\\x([\d\w]{2})/gi;
        str = str.replace(r, (match, grp) => {
            return String.fromCharCode(parseInt(grp, 16)); 
        });

        r = /\\u([\d\w]{4})/gi;
        str = str.replace(r, (match, grp) => {
            return String.fromCharCode(parseInt(grp, 16)); 
        });
        return str;
    }

    dangerousHTML() {
        let str = this.props.image.svg_code;

        if (!str) str = '';
        if (str && str.trim().indexOf('<') != 0) {
            // str = this.unescapeSVGCode(decodeURIComponent(escape(atob(this.props.image.svg_code))));
            str = this.unescapeSVGCode(this.props.image.svg_code);
        }

        return {
            __html: str
        }
    }

    statiSelect(field_name, exclusive, field_val, e) {
        console.debug(arguments);

        let current_stati = this.props.image[field_name];
        console.debug(this.props.image);
        if (!exclusive) {
            console.debug(current_stati);
            console.debug(current_stati, field_val,  current_stati&field_val);
            if ((current_stati&field_val) > 0) {
                //  Stati bit already set, unset it
                field_val = current_stati & ~field_val;
            } else {
                //  Stati bit not set, set it
                field_val = current_stati | field_val;
            }
        }

        this.props.setFieldImmediately(this.props.image.image_id, field_name, field_val);
        this.props.onUpdateFieldValue(this.props.image.image_id, field_name, field_val);
    }

    saveSVGCode(e) {
        // console.debug(this);
        // console.debug();

        let el = this['_svg_code'].getDOMNode();

        el.focus();
        this.setSelectionRange(el, el.selectionStart, el.selectionEnd);
    }

    setSelectionRange(input, selectionStart, selectionEnd) {
        if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(selectionStart, selectionEnd);
        } else if (input.createTextRange) {
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', selectionEnd);
            range.moveStart('character', selectionStart);
            range.select();
        }
    }

    setCaretToPos (input, pos) {
        setSelectionRange(input, pos, pos);
    }

    showPreview() {
        this.props.showPreview(this.props.image);
    }

    onUpdateIsLive(e) {
        console.debug(e.target.checked);
        this.props.onLiveUpdateChange(e.target.checked, this.props.image.image_id);
    }

    render() {
        console.debug(this.props);

        let svg_code = this.props.image.svg_code || '';
        if (svg_code && svg_code.trim().indexOf('<') != 0) {
            // console.debug(svg_code);
            // console.debug(atob(svg_code));
            // console.debug(escape(atob(svg_code)));
            // console.debug(decodeURIComponent(escape(atob(svg_code))));
            // svg_code = this.unescapeSVGCode(decodeURIComponent(escape(atob(svg_code))));
            svg_code = this.unescapeSVGCode(svg_code);
        }

        let svg_textarea;
        if (this.props.image.live_update) {
            svg_textarea = <textarea id={'svg_code_' + this.props.langCode} className='form-control' rows='15' onChange={this.immediateUpdateField.bind(this, 'svg_code')} onBlur={this.handleSVGCodeUpdate.bind(this)} value={svg_code} ref={(ref) => this['_svg_code'] = ref}></textarea>
        } else {
            svg_textarea = <textarea id={'svg_code_' + this.props.langCode} className='form-control' rows='15' onBlur={this.handleSVGCodeUpdate.bind(this)} defaultValue={svg_code} ref={(ref) => this['_svg_code'] = ref}></textarea>
        }
        // console.debug(svg_textarea);
        // console.debug(svg_code);

        //  Nothing to display 
        return (
            <div>
                <Row>
                    <Col xs={3}>
                        <label>QA Status</label>
                    </Col>
                    <Col xs={9}>
                        <StatiButtonGroup currentVal={this.props.image.qa_status_val} buttonSelected={this.statiSelect.bind(this, 'qa_status_val', true)} bits={this.props.constants.qa_status_val} grouped={true} />
                    </Col>
                </Row>
                <Row>
                    <Col xs={3}>
                        <label>Problem Types</label>
                    </Col>
                    <Col xs={9}>
                        <StatiButtonGroup currentVal={this.props.image.qa_problem_bits} buttonSelected={this.statiSelect.bind(this, 'qa_problem_bits', false)} bits={this.props.constants.qa_problem_bits} grouped={false} />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} id={`svg-holder-${this.props.langCode}`}>
                        <figure>
                            <div className="svg-container" id={`svg-container-${this.props.langCode}`}>
                                <div dangerouslySetInnerHTML={this.dangerousHTML()} />
                            </div>
                        </figure>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} className="label-input">
                        <Row className="collapsed">
                            <Col xs={3} collapseLeft><label htmlFor={"scale" + this.props.langCode}>Scale Factor (%)</label></Col>
                            <Col xs={2} collapseLeft collapseRight><Input md type="text" defaultValue={this.props.image.scale} id={"scale" + this.props.langCode} onBlur={this.props.onUpdateField.bind(this, this.props.image.image_id, 'scale')} ref={(ref) => this._alt_text = ref} /></Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <Input id={'svg_size_' + this.props.langCode} ref={'svg_size_' + this.props.langCode} type='text' data-slider-id={'svg_size_slider_' + this.props.langCode}  />
                    </Col>
                </Row>
                <Row>
                    <Col xs={3}>
                        <label htmlFor={'svg_code_' + this.props.langCode}>SVG Code</label>
                    </Col>
                    <Col xs={2} mdOffset={2} className="text-center">
                        <Button xs onClick={this.showPreview.bind(this)}>Preview</Button>
                    </Col>
                    <Col xs={3} collapseRight className="pull-right">
                        <span className='checkbox' style={{'display': 'inline', 'paddingRight': '5px'}}>
                            <label style={{'display': 'inline'}}>
                                <input type='checkbox' name='islive' onChange={this.onUpdateIsLive.bind(this)} checked={this.props.image.live_update} />
                                <span>Live</span>
                            </label>
                        </span>

                        {(!this.props.image.live_update) ? <Button xs onClick={this.saveSVGCode.bind(this)}>Save</Button> : <div />}
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        {svg_textarea}
                    </Col>
                </Row>
            </div>
        );
    }
}
