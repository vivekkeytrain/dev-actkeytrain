import { Component } from 'react';
import PageLink from './PageLink';

import { LangOptions } from '../actions';

export default class PageLinks extends Component {
    render() {
        // console.debug(this.props);
        if (!this.props.sessions || !this.props.sessions.EN) return <i className='glyphicon glyphicon-remove-circle'></i>

        let links = [<PageLink courseTag={this.props.image.course_tag} lessonTag={this.props.image.lesson_tag} lang={LangOptions.EN} sessionId={this.props.sessions.EN.session_id} loginSessionUID={this.props.sessions.EN.login_session_uid} pageID={this.props.image.pageid} key='pagelink_en' />];

        if (this.props.sessions.ES) {
            links.push(String.fromCharCode(160)/* &nbsp; */ + '/' + String.fromCharCode(160));
            links.push(<PageLink courseTag={this.props.image.course_tag} lessonTag={this.props.image.lesson_tag} lang={LangOptions.ES} sessionId={this.props.sessions.ES.session_id} loginSessionUID={this.props.sessions.ES.login_session_uid} pageID={this.props.image.pageid} key='pagelink_es' />)
        }

        return <span>{links}</span>
    }
}

