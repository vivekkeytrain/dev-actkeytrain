import React, { Component, PropTypes } from 'react';
import { filter } from 'lodash';
import classNames from 'classnames';

import FilterCheck from './FilterCheck';
// import FilterText from './FilterText';

import { DisplayFields } from '../actions';

export default class Filters extends Component {
    
    componentDidMount() {
        $('input.floatlabel').floatlabel({
          slideInput: false,
          labelStartTop: '5px',
          labelEndTop: '-2px'
        });
    }

    render() {
        // console.debug(filter(this.props.displayFields, { 'isOn': true }));
        // console.debug(FilterSelect);
        // console.debug(this.props);

        let classes = classNames({
            'glyphicon': true,
            'glyphicon-refresh': true,
            'spinning': this.props.isFetching
        });
        // console.debug(classes);

        return (
            <Row>
                <Col sm={12}>
                    <Grid>
                        <Row>
                            <Col xs={12} sm={1} collapseLeft>
                              <Dropdown>
                                <DropdownButton md bsStyle='blue'>
                                  <span><Icon bundle='fontello' glyph='cog-5'/></span> <Caret/>
                                </DropdownButton>
                                <Menu bsStyle='blue' style={{paddingLeft: '10px'}}>

                                    {this.props.displayFields.map((field, index) =>
                                        <FilterCheck onUpdateDisplayField={this.props.onUpdateDisplayField} field={field} key={index} />
                                    )}
                                </Menu>
                              </Dropdown>
                            </Col>

                            <Col xs={12} sm={2} collapseLeft>
                                <Select control id="qa_status_select" onChange={this.props.setQAStatusFilter.bind(this)} value={this.props.qaStatus}>
                                    <option value=''>Select Status</option>
                                    {this.props.constants.qa_status_val.map((option, index) => 
                                        <option key={index} value={option.status_val}>{option.status_desc}</option>
                                    )}
                                </Select>
                            </Col>

                            <Col xs={12} sm={3} collapseLeft>
                                <Button bsStyle='blue' md onClick={this.props.forceRefresh}>Refresh <i className={classes}></i></Button>
                            </Col>
                            {/*<Col sm={3} collapseLeft>
                            <FilterText name="pageid" label="PageID" val={this.props.page} onUpdateTextFilter={this.props.onUpdateTextFilter} key="pageid" />
                            </Col>
                            <Col sm={3} collapseLeft>
                            <FilterText name="pattern" label="Pattern" val={this.props.pattern} onUpdateTextFilter={this.props.onUpdateTextFilter} key="pattern" />
                            </Col>
                            <Col sm={3} collapseLeft>
                                <Button bsStyle='blue' md onClick={this.props.onTextFilterFetch}>Filter</Button>
                            </Col>*/}
                        </Row>
                    </Grid>
                </Col>
            </Row>
        )
    }
} 