import { Component } from 'react';

import { IMAGE_ROOT_URL, LangOptions } from '../actions';

import LangImageColumn from './LangImageColumn';
import PageLinks from './PageLinks';
import SVGPreview from './SVGPreview';

import { forEach } from 'lodash';

export default class ImageDetailView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            svg_preview: null
        }
    }

    componentDidMount() {
        // $('input.floatlabel').floatlabel({
        //   slideInput: false,
        //   labelStartTop: '5px',
        //   labelEndTop: '-2px'
        // });
        // console.debug(this.props.rowImages[0].image_desc);
    }

    hidePreview() {
        this.setState({
            svg_preview: null
        })
    }

    showPreview(image) {
        console.debug('showPreview');
        console.debug(this.state);
        console.debug(this);

        this.setState({
            svg_preview: image
        })


    }

    // componentWillMount() {
    //     this.setState({
    //         'en_image': this.props.rowImages[0],
    //         'es_image': this.props.rowImages[1]
    //     })
    // }

    componentWillReceiveProps(props) {
        console.debug(props);
        // console.debug(props.rowImages[0].image_desc);
        // console.debug(React.findDOMNode(this._image_desc));
        // let image_desc = props.rowImages[0].image_desc;
        // if (image_desc)
        //     React.findDOMNode(this._image_desc).value = image_desc;
        // else 
        //     React.findDOMNode(this._image_desc).value = '';
        // this.setState({
        //     'en_image': props.rowImages[0],
        //     'es_image': props.rowImages[1],
        //     'image_desc': props.rowImages[0].image_desc
        // })

        let update_fields = ['image_desc', 'keywords'];

        if (props.rowImages[0]) {
            update_fields.forEach((field) => {
                let val = props.rowImages[0][field];
                let rfield = this[`_${field}`];
                if (rfield) {
                    let dfield = React.findDOMNode(rfield);
                    if (dfield) {
                        if (val)
                            dfield.value = val;
                        else
                            dfield.value = '';
                    }
                }
            });
        }
    }

    immediateUpdateField(image_id, field_name, e) {
        this.props.setFieldImmediately(image_id, field_name, e.target.value);
    }

    onUpdateField(image_id, field_name, e) {
        console.debug(this, field_name, e);
        this.props.setFieldImmediately(image_id, field_name, e.target.value);
        this.props.onUpdateField(image_id, field_name, e.target.value)
    }

    onUpdateFieldValue(image_id, field_name, val) {
        this.props.setFieldImmediately(image_id, field_name, val);
        this.props.onUpdateField(image_id, field_name, val);
    }

    onUpdateDesc(e) {
        let val = e.target.value;
        let en_image = this.props.rowImages[0];
        let image_id = en_image.image_id;
        if (val && !this.props.rowImages[0].alt_text) {
            //  No English alt text
            React.findDOMNode(this._lang_column_EN._alt_text).value = val;
            this.props.setFieldImmediately(image_id, 'alt_text', val);
            this.props.onUpdateField(image_id, 'alt_text', val);
        }
        this.props.onUpdateField(image_id, 'image_desc', val)
    }

    closeWindow() {
        this.props.setSelectedRow(-1);
        this.props.doRefetchImages(this.props.rowImages[0].image_id);
    }

    handleBackgroundClick(e) {
        if (e.target.id === 'modal-container') this.closeWindow();
    }

    changeSelectedImage(increment, e) {
        e.preventDefault();
        this.props.changeSelectedImage(increment);
    }

    render() {
        console.debug(this.props);
        let [en_image, es_image, page_image] = this.props.rowImages;
        // let en_image = this.state.en_image,
        //     es_image = this.state.es_image
        // console.debug(en_image, es_image);
        console.debug(en_image.image_desc);

        let prev_page, next_page, offset = 3;
        if (page_image.page_count > 1) {
            prev_page = <Button bsStyle='info' onClick={this.props.changeSelectedImage.bind(this, -1, true)}><i className="glyphicon glyphicon-chevron-left"></i> Previous Page</Button>

            next_page = <Button bsStyle='info' className="pull-right" onClick={this.props.changeSelectedImage.bind(this, 1, true)}>Next Page<i className="glyphicon glyphicon-chevron-right"></i></Button>


            // offset = 1;
        }

        let sessions = {}
        forEach(LangOptions, (lang_id, lang_code) => {
            if (this.props.allSessions[lang_id] && this.props.allSessions[lang_id][en_image.lesson_id]) sessions[lang_code] = this.props.allSessions[lang_id][en_image.lesson_id];
        });

        if (!en_image && !es_image) return <div />;

        let svg_preview;
        if (this.state.svg_preview) {
            svg_preview = <SVGPreview image={this.state.svg_preview} hidePreview={this.hidePreview.bind(this)} />
        }
        console.debug('svg_preview');
        console.debug(svg_preview);


        return (
            <div className='cms-modal-detail'>
                <div className="modal-backdrop fade in"></div>
                <div className="cms-modal-detail-modal" id="modal-container" onClick={this.handleBackgroundClick.bind(this)}>
                    <div className="cms-modal-detail">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.closeWindow.bind(this)}><span aria-hidden="true">×</span></button>
                            <Grid>
                                <Row>
                                    <Col xs={2}>
                                        <h4 className="modal-title">{en_image.image_tag}</h4>
                                    </Col>
                                    <Col xs={4}>
                                        {page_image.lesson_topic}
                                    </Col>
                                    <Col xs={3}>
                                        {page_image.page_title}
                                    </Col>
                                    <Col xs={2}>
                                        Launch: <PageLinks image={page_image} sessions={sessions} />
                                    </Col>
                                </Row>
                            </Grid>
                        </div>
                        <div className="modal-body">
                            <Grid>
                                <Row>
                                    <Col xs={4} bsStyle="blue" className="pull-left" collapseLeft>
                                        <Button onClick={this.props.changeSelectedImage.bind(this, -1)}><i className="glyphicon glyphicon-chevron-left"></i> Previous Image</Button>
                                        {prev_page}
                                    </Col>
                                    
                                    <Col xs={2} mdOffset={1}>
                                        Found on {page_image.page_count} page{(page_image.page_count > 1) ? 's' : ''}
                                    </Col>
                                    <Col xs={4} bsStyle="blue" className="pull-right" collapseRight>
                                        <Button className="pull-right" onClick={this.props.changeSelectedImage.bind(this, 1)}>Next Image<i className="glyphicon glyphicon-chevron-right"></i></Button>
                                        {next_page}
                                    </Col>
                                    
                                </Row>
                                <Row>
                                    <Col xs={6}>
                                        <label>Original Filename</label>
                                        {en_image.export_filename}
                                    </Col>
                                    <Col xs={6}>
                                        <label>Type</label>
                                        {en_image.image_orig_desc}
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} className="label-input">
                                        <Row className="collapsed">
                                            <Col xs={1} collapseLeft><label htmlFor="image_desc">Description</label></Col>
                                            <Col xs={11} collapseLeft collapseRight><Input md type='text' id='image_desc' defaultValue={en_image.image_desc} onBlur={this.onUpdateDesc.bind(this)} ref={(ref) => this['_image_desc'] = ref} /></Col>
                                        </Row>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} className="label-input">
                                        <Row className="collapsed">
                                            <Col xs={1} collapseLeft><label htmlFor="keywords">Keywords</label></Col>
                                            <Col xs={11} collapseLeft collapseRight><Input md type='text' id='keywords' defaultValue={en_image.keywords} onBlur={this.onUpdateField.bind(this, en_image.image_id, 'keywords')} ref={(ref) => this['_keywords'] = ref} /></Col>
                                        </Row>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12}>
                                        <hr />
                                    </Col>
                                </Row>
                                <Row>
                                    <LangImageColumn image={en_image} langCode='EN' langDisplay='English' key='en_column' constants={this.props.constants} setFieldImmediately={this.props.setFieldImmediately} onUpdateField={this.onUpdateField.bind(this)} onUpdateFieldValue={this.onUpdateFieldValue.bind(this)} showPreview={this.showPreview.bind(this)} onLiveUpdateChange={this.props.onLiveUpdateChange} ref={(ref) => this['_lang_column_EN'] = ref} />
                                    <LangImageColumn image={es_image} langCode='ES' langDisplay='Spanish' key='es_column' constants={this.props.constants} setFieldImmediately={this.props.setFieldImmediately} onUpdateField={this.onUpdateField.bind(this)} onUpdateFieldValue={this.onUpdateFieldValue.bind(this)} showPreview={this.showPreview.bind(this)} onLiveUpdateChange={this.props.onLiveUpdateChange} ref={(ref) => this['_lang_column_ES'] = ref} />
                                </Row>
                            </Grid>
                        </div>

                    </div>
                    {svg_preview}
                </div>
{/*
                <div className="modal fade in" tabIndex="-1" role="dialog">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                
                            </div>
                            <div className="modal-body">
                                <Grid>
                                    <Row>
                                        <Col xs={12}>
                                            <p>{en_image.orig_filename}</p>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12}>
                                            <BLabel>Desc</BLabel>
                                            <Input type='text' placeholder='Description' value={en_image.image_desc} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12}>
                                            <BLabel>Type</BLabel>
                                            {(en_image.is_page) ? 'Page Image' : ((en_image.is_addl) ? 'Additional HTML Image' : ((en_image.is_chapter) ? 'Chapter' : 'Aux'))}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <LangImageColumn image={en_image} langCode='EN' langDisplay='English' key='en_column' />
                                        <LangImageColumn image={es_image} langCode='ES' langDisplay='Spanish' key='es_column' />
                                    </Row>
                                </Grid>
                            </div>
                        </div>
                    </div>
                </div>
*/}
            
            </div>
        );
    }
}
