import { Component } from 'react';

import { IMAGE_ROOT_URL } from '../actions';

import SVGDisplay from './SVGDisplay';

export default class LangImageColumn extends Component {
    constructor(props) {
        console.debug('constructor', props);
        super(props);

        this.state = {
            width: '',
            height: '',
            resized: false,
            img_path: this.getImgPath(props)
        }
    }

    // componentWillMount() {
    //     this.setState({
    //         width: this.props.image.width,
    //         height: this.props.image.height
    //     })
    // }

    // componentDidMount() {
    //     // this.handleImgResize();

    //     $('input.floatlabel').floatlabel({
    //       slideInput: false,
    //       labelStartTop: '5px',
    //       labelEndTop: '-2px'
    //     });
    // }

    componentWillUnmount() {
        if (!this._jpg) return;
        console.debug('componentWillUnmount');
    }

    componentWillReceiveProps(props) {
        // console.debug(props);
        // console.debug(this);
        let update_fields = ['alt_text', 'qa_notes', 'content_notes'];

        if (props.image) {
            update_fields.forEach((field) => {
                let val = props.image[field];
                let rfield = this[`_${field}`];
                if (rfield) {
                    let dfield = React.findDOMNode(rfield);
                    if (dfield) {
                        if (val)
                            dfield.value = val;
                        else
                            dfield.value = '';
                    }
                }
            });
        }

        // if (this._jpg && this.getImgPath(props) != this.state.img_path) {
        //     console.debug('new img', this.getImgPath(props), this.state.img_path);
        //     let img = this._jpg.getDOMNode();
        //     $(img).removeAttr('width').removeAttr('height');
        //     // img.width = '';
        //     // img.height = '';

        //     this.setState({
        //         img_path: this.getImgPath(props)
        //     })
        // }

        // if (this._jpg) {
        //     let img = this._jpg.getDOMNode();
        //     $(img).removeAttr('width').removeAttr('height');
        //     // console.debug(img);
        // }

        this.setState({
            width: '',
            height: '',
            resized: false
        })
    }

    componentDidUpdate(prev_props, prev_state) {
        if (this._jpg) this.handleImgResize();
    }

    getImgPath(props) {
        if (!(props && props.image && props.langCode)) return '';
        return IMAGE_ROOT_URL + '/' + props.image.course_tag + '/' + props.image.lesson_tag + '/' + props.langCode + '/images/' + props.image.export_filename
    }

    immediateUpdateField(field_name, e) {
        this.props.setFieldImmediately(this.props.image.image_id, field_name, e.target.value);
    }

    handleFieldPost(field_name, e) {
        // console.debug(field_name, e);
        this.props.setField(this.props.image.image_id, field_name, e.target.value);
    }

    // updateWidthHeight(field_name, e) {
    //     this.immediateUpdateField(field_name, e);

    //     this.handleImgResize();
    // }

    handleImgResize() {
        // console.debug(arguments);
        // console.debug(e.target.width);
        // console.debug(this._col.getDOMNode());
        // console.debug($);

        // console.debug(e.target);
        if (!this._jpg) return;
        // console.debug(this._jpg.getDOMNode());
        let img = this._jpg.getDOMNode();
        let col = this._col.getDOMNode();
        let col_width = $(col).width();

        // console.debug(img.naturalWidth, col_width, img.naturalHeight);
        if (!this.state.resized && img.naturalWidth > col_width) {
            this.setState({
                width: col_width,
                height: img.naturalHeight * (col_width / img.naturalWidth),
                resized: true
            })
            // img.height = img.height * (col_width / img.width);
            // img.width = col_width;
        }
        // console.debug(this.state);

        // let width = this.props.image.width || img.width,
        //     height = this.props.image.height || img.height;
        // if (width > 400) {
        //     width = 400;
        //     height = 400 / e.target.width * e.target.height;
        // } else {
        //     width = img.width;
        //     height = img.height;
        // }

        // console.debug(width, height);

        // this.setState({
        //     width,
        //     height
        // });
    }

    formatDate(val) {
        let d = new Date(val);
        let m = ((d.getMonth() < 9) ? '0' : '') + (d.getMonth()+1);
        let dy = ((d.getDate() < 10) ? '0' : '') + d.getDate();
        let h = ((d.getHours() < 10) ? '0' : '') + d.getHours();
        let min = ((d.getMinutes() < 10) ? '0' : '') + d.getMinutes();
        let s = ((d.getSeconds() < 10) ? '0' : '') + d.getSeconds();
        return `${d.getFullYear()}${String.fromCharCode(8209)}${m}${String.fromCharCode(8209)}${dy} ${h}:${min}:${s}`; // 8209 is non-breaking dash
    }

    render() {
        console.debug(this.props.image);
        console.debug(this.state);
        //  Nothing to display 
        if (!this.props.image) return <div />
        if (this.props.langCode === 'ES' && this.props.image.lang_id != 2) return <div />;

        let svg_display = (this.props.image.has_svg) ? <SVGDisplay {...this.props} /> : '';

        return (
            <Col xs={12} sm={6} ref={(c) => this._col = c}>
                <h4>{this.props.langDisplay}</h4>
                {/*<div dangerouslySetInnerHTML={image.src} />*/}
                <Row>
                    <Col xs={12} className='text-center'>
                        <img src={IMAGE_ROOT_URL + '/' + this.props.image.course_tag + '/' + this.props.image.lesson_tag + '/' + this.props.langCode + '/images/' + this.props.image.export_filename} width={this.state.width} height={this.state.height} onLoad={this.handleImgResize.bind(this)} ref={(c) => this._jpg = c}/><br />
                        {(this.props.image.width && this.props.image.height) ? <div className="text-center">{this.props.image.width} X {this.props.image.height}</div> : ''}
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <hr />
                    </Col>
                </Row>
                {/*<Row>
                    <Col xs={6}>
                        <Input md className="floatlabel" type='text' value={this.props.image.width} placeholder="Width" onChange={this.updateWidthHeight.bind(this, 'width')} onBlur={this.props.onUpdateField.bind(this, this.props.image.image_id, 'width')} />
                    </Col>
                    <Col xs={6}>
                        <Input md className="floatlabel" type='text' value={this.props.image.height} placeholder="Height" onChange={this.updateWidthHeight.bind(this, 'height')} onBlur={this.props.onUpdateField.bind(this, this.props.image.image_id, 'height')} />
                    </Col>
                </Row>*/}
                <Row>
                    <Col xs={12} className="label-input">
                        <Row className="collapsed">
                            <Col xs={3} collapseLeft><label htmlFor={"alt_text" + this.props.langCode}>Alt Text</label></Col>
                            <Col xs={9} collapseLeft collapseRight><Input md type="text" defaultValue={this.props.image.alt_text} id={"alt_text" + this.props.langCode} onBlur={this.props.onUpdateField.bind(this, this.props.image.image_id, 'alt_text')} ref={(ref) => this._alt_text = ref} /></Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} className="label-input">
                        <Row className="collapsed">
                            <Col xs={3} collapseLeft><label htmlFor={'qa_notes' + this.props.langCode}>QA Notes</label></Col>
                            <Col xs={9} collapseLeft collapseRight><Input md type='text' defaultValue={this.props.image.qa_notes} id={"qa_notes" + this.props.langCode} onBlur={this.props.onUpdateField.bind(this, this.props.image.image_id, 'qa_notes')} ref={(ref) => this._qa_notes = ref} /></Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} className="label-input">
                        <Row className="collapsed">
                            <Col xs={3} collapseLeft><label htmlFor={'content_notes' + this.props.langCode}>Content Notes</label></Col>
                            <Col xs={9} collapseLeft collapseRight><Input md type='text' defaultValue={this.props.image.content_notes} id={"content_notes" + this.props.langCode} onBlur={this.props.onUpdateField.bind(this, this.props.image.image_id, 'content_notes')} ref={(ref) => this._content_notes = ref} /></Col>
                        </Row>
                    </Col>
                </Row>
                {svg_display}
                <Row>
                    <Col xs={6}>
                        <label>Last Updated</label>
                        {this.formatDate(this.props.image.update_date)}
                    </Col>
                    <Col xs={6}>
                        <label>Updated By</label>
                        {this.props.image.update_by}
                    </Col>
                </Row>
            </Col>

        );
    }
}
