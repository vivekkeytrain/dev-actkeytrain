import fetchJsonp from 'fetch-jsonp';
import React from 'react';
import { map, find, invert } from 'lodash';

import PageLinks from './components/PageLinks';

/*  Action types    */

export const ADD_MESSAGE = 'ADD_MESSAGE';
export const CLOSE_MESSAGE = 'CLOSE_MESSAGE';

export const UPDATE_DISPLAY_FIELD = 'UPDATE_DISPLAY_FIELD';
export const LOAD_DISPLAY_FIELDS = 'LOAD_DISPLAY_FIELDS';
export const SET_SORT_FIELD = 'SET_SORT_FIELD';
export const SET_HIGHLIGHTED_ROW = 'SET_HIGHLIGHTED_ROW';

export const SET_SESSION = 'SET_SESSION';

/*  Async action types  */

export const FETCH_PAGE_ISSUES_REQUEST = 'FETCH_PAGE_ISSUES_REQUEST';
export const FETCH_PAGE_ISSUES_SUCCESS = 'FETCH_PAGE_ISSUES_SUCCESS';
export const FETCH_PAGE_ISSUES_FAILURE = 'FETCH_PAGE_ISSUES_FAILURE';

export const DO_LOGIN_LESSON_REQUEST = 'DO_LOGIN_LESSON_REQUEST';
export const DO_LOGIN_LESSON_SUCCESS = 'DO_LOGIN_LESSON_SUCCESS';
export const DO_LOGIN_LESSON_FAILURE = 'DO_LOGIN_LESSON_FAILURE';

export const FETCH_EDITOR_CONTENT_SUCCESS = 'FETCH_EDITOR_CONTENT_SUCCESS';
export const FETCH_EDITOR_CONTENT_FAILURE = 'FETCH_EDITOR_CONTENT_FAILURE';
export const FETCH_EDITOR_CONTENT_REQUEST = 'FETCH_EDITOR_CONTENT_REQUEST';

/*  Other constants */

export const LangOptions = {
    EN: 1,
    ES: 2
};

export const DisplayFields = [
    {
        name: 'pageID',
        label: 'Page Link',
        isOn: true,
        sortable: false,
        displayFunc: (val, props) => {
            return <PageLinks {...props} />
        }
    },
    {
        name: 'pageID',
        isOn: true
    },
    {
        name: 'pageno',
        label: '#',
        isOn: true,
        style: {whiteSpace: 'nowrap'}
    },
    // {
    //     name: 'lang',
    //     label: 'Lang',
    //     isOn: true
    // },
    {
        name: 'insert_date_str',
        label: 'Insert Date',
        isOn: true,
        style: {whiteSpace: 'nowrap'}
    },
    {
        name: 'course_id',
        label: 'Course',
        isOn: true,
        displayFunc: (val, props) => {
            let course = find(props.courses, {'id': val});
            return course ? course.display : '';
        }
    },
    {
        name: 'lesson_id',
        label: 'Lesson',
        isOn: true,
        displayFunc: (val, props) => {
            let lesson = find(props.lessons, {'id': val});
            return lesson ? lesson.display : val;
        }
    },
    {
        name: 'topic',
        label: 'Topic',
        isOn: true
    },
    {
        name: 'problem',
        label: 'Problem',
        isOn: true,
        sortable: false
        /*style: {width: '35%'}*/
    }
]

/*  Action creators */

export function addMessage(message_type, message, position='top-center') {
    return {
        type: ADD_MESSAGE,
        message_type,
        message,
        position
    }
}

export function closeMessage(index) {
    return {
        type: CLOSE_MESSAGE,
        index
    }
}

export function updateDisplayField(field_name, isOn) {
    return {
        type: UPDATE_DISPLAY_FIELD,
        field_name,
        isOn
    }
}

export function updateLocalDisplayField(field_name, isOn) {
    return function(dispatch) {
        localStorage.setItem('display_field_' + field_name, isOn ? 'true' : 'false');
        dispatch(updateDisplayField(field_name, isOn));
    }
}

export function loadDisplayFields() {
    let localDisplayFields = [];
    DisplayFields.forEach((field, ind) => {
        let isOn = localStorage.getItem('display_field_' + field.name) ? localStorage.getItem('display_field_' + field.name) === 'true' : field.isOn;
        localDisplayFields[ind] = Object.assign({}, field, {
            isOn: isOn
        });
    });


    return function(dispatch) {
        map(localDisplayFields, (field, index) => {
            dispatch(updateLocalDisplayField(field.name, field.isOn))
        });
    }
}

export function setSortField(field_name, sort_dir) {
    return {
        type: SET_SORT_FIELD,
        field_name,
        sort_dir
    }
}

export function setHighlightedRow(index) {
    return {
        type: SET_HIGHLIGHTED_ROW,
        index
    }
}

export function setSession(session_id, login_session_uid) {
    return {
        type: SET_SESSION,
        session_id,
        login_session_uid
    }
}

/*  Async action creators   */

function fetchPageIssuesSuccess(page_issues) {
    return {
        type: FETCH_PAGE_ISSUES_SUCCESS,
        page_issues,
        receivedAt: Date.now()
    }
}

function fetchPageIssuesFailure(ex) {
    return {
        type: FETCH_PAGE_ISSUES_FAILURE,
        ex
    }
}

function fetchPageIssuesRequest() {
    return {
        type: FETCH_PAGE_ISSUES_REQUEST
    }
}

export function fetchPageIssues() {
    return function(dispatch) {
        dispatch(fetchPageIssuesRequest());

        return fetchJsonp(`${editor_api_url}dbo.api_sp_get_page_issues`)
            .then(
                response => response.json(),
                ex => dispatch(fetchPageIssuesFailure(ex))
            )
            .then(json => dispatch(fetchPageIssuesSuccess(json.ResultSets[0])))
            .catch(ex => dispatch(fetchPageIssuesFailure(ex)));
    }
}

function doLoginLessonSuccess(session_id, login_session_uid, lesson_id, lang) {
    // console.debug('doLoginLessonSuccess', session_id, login_session_uid, lesson_id, lang);
    // console.debug('login_session_uid: ' + login_session_uid);
    // lang = lang.toUpperCase();
    return { 
        type: DO_LOGIN_LESSON_SUCCESS,
        session_id,
        login_session_uid,
        lesson_id,
        lang,
        receivedAt: Date.now()
    };
}

function doLoginLessonFailure(ex) {
    return {
        type: DO_LOGIN_LESSON_FAILURE,
        ex
    }
}

function doLoginLessonRequest() {
    return { type: DO_LOGIN_LESSON_REQUEST };
}


function shouldDoLoginLesson(state, lesson_id, lang) {
    // console.debug('shouldDoLoginLesson: ' + lesson_id + ', ' + lang);
    if (!lesson_id || lesson_id == -1) return false;
    const sessions = state.filters.sessions;
    // console.debug(sessions);
    if (!sessions[lang]) return true;
    const session = sessions[lang][lesson_id];
    // console.debug(session);

    if (!session) {
        return true;
    } else if (session.isFetching) {
        return false
    } else {
        return (Date.now() - session.lastUpdated) > (1000 * 60 * 15); //   15 minutes old, refetch
    }
}


function doLoginLesson(lesson_id, lang) {
    return function(dispatch) {
        dispatch(doLoginLessonRequest());

        let lang_code = invert(LangOptions)[lang];

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_init_review_session?lesson_id=' + lesson_id + '&lang_code=' + lang_code, { timeout: 20000 })
            .then(
                response => response.json(),
                ex => dispatch(doLoginLessonFailure(ex))
            )
            .then(json => dispatch(doLoginLessonSuccess(json.ResultSets[0][0].session_id, json.ResultSets[0][0].luid_str, lesson_id, lang)))
            .catch(ex => dispatch(doLoginLessonFailure(ex)));
    }

}

export function doLoginIfNeeded(lesson_id, lang) {
    // console.debug('doLoginIfNeeded', lesson_id, lang);
    return (dispatch, getState) => {
        if (shouldDoLoginLesson(getState(), lesson_id, lang)) {
            // console.debug('doLogin');
            return dispatch(doLoginLesson(lesson_id, lang));
        } else {
            return Promise.resolve();
        }
    }
}

function fetchEditorContentSuccess(content) {
    return {
        type: FETCH_EDITOR_CONTENT_SUCCESS,
        content
    }
}

function fetchEditorContentFailure(ex) {
    return {
        type: FETCH_EDITOR_CONTENT_FAILURE,
        ex
    }
}

function fetchEditorContentRequest() {
    return { type: FETCH_EDITOR_CONTENT_REQUEST };
}

export function fetchEditorContent() {
    return function(dispatch) {
        dispatch(fetchEditorContentRequest());

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_content_list/jsonp', {
                timeout: 20000
            })
            .then(
                response => response.json(),
                ex => dispatch(fetchEditorContentFailure(ex))
            )
            .then(json => dispatch(fetchEditorContentSuccess(json.ResultSets[0])))
            .catch(ex => dispatch(fetchEditorContentFailure(ex)));
    }
}
