import React, { Component, PropTypes } from 'react';

export default class FilterSelect extends Component {
    onUpdateDisplayField(name, e) {
        this.props.onUpdateDisplayField(name, e.target.checked);
    }

    handleSelection(itemprops) {
        // access any property attached to MenuItem child component.
        // ex: itemprops.keyaction === 'another-action' if MenuItem
        // with "Another action" is clicked.
        var value = itemprops.children;
        alert(value);
        if(itemprops.keyaction === 'another-action')
          alert('You clicked another-action');
    }


    render() {
        // console.debug(this.props.field);
        // console.debug('render');
        let ref = 'checkbox_' + this.props.field.name;
        return (
            
                <Checkbox name={this.props.field.name} ref={ref} onChange={this.onUpdateDisplayField.bind(this, this.props.field.name)} checked={this.props.field.isOn}>
                    {(this.props.field.label) ? this.props.field.label : this.props.field.name}
                </Checkbox>
            
        );
    }
} 



