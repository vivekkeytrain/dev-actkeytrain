import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

import IssueTableHeader from './IssueTableHeader';

export default class IssueTable extends Component {
    render() {
        let row_count = (this.props.pageIssues) ? this.props.pageIssues.length : 0;

        let disp_table;
        if (this.props.issuesFetching) {
            disp_table = <div className={classes} style={{width: '100%'}}>
                            <span><strong>Fetching Data, Please Hold ...</strong></span>
                         </div>
        } else {
            disp_table = <div>
                <div>Row Count: {row_count}</div>
                <table
                    key='dataTable'
                    ref='dataTable'
                    className='tablesaw tablesaw-sortable tablesaw-stack table-striped table-bordered'>
                    <thead>
                        <IssueTableHeader displayFields={this.props.displayFields} sortField={this.props.sortField} sortDir={this.props.sortDir} setSortField={this.props.setSortField} />
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        }

        return disp_table;
    }
}