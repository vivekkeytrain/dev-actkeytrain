import React, { Component, PropTypes } from 'react';
import { filter } from 'lodash';

import FilterCheck from './FilterCheck';

export default class Filters extends Component {
    
    componentDidMount() {
        
        // $('input.floatlabel').floatlabel({
        //   slideInput: false,
        //   labelStartTop: '5px',
        //   labelEndTop: '-2px'
        // });

    }

    render() {
        return (
            <Row>

                <Col sm={12}>

                    <Grid>
                    <Row>
                        <Col sm={1} collapseLeft>
                          <Dropdown>
                            <DropdownButton md bsStyle='blue'>
                              <span><Icon bundle='fontello' glyph='cog-5'/></span> <Caret/>
                            </DropdownButton>
                            <Menu bsStyle='blue' onItemSelect={this.handleSelection} style={{paddingLeft: '10px'}}>

                                {this.props.displayFields.map((field, index) =>
                                    <FilterCheck onUpdateDisplayField={this.props.onUpdateDisplayField} field={field} key={index} />
                                )}

                            </Menu>
                          </Dropdown>
                        </Col>
                    </Row>
                    </Grid>

                </Col>



            </Row>
        )
    }
} 