import { Component } from 'react';
// import PageLink from './PageLink';

import { LangOptions } from '../actions';

export default class PageLinks extends Component {

    handlePageLinkClick(lang, e) {
        e.preventDefault();
        e.stopPropagation();
        this.props.handlePageLinkClick(this.props.issue, this.props.issue.lesson_id, lang);
    }

    render() {
        // console.debug(this.props);

        /*  
            Gotta do a login based upon the lesson, then load up the link   
            Actually, let's just do the login on click
        */



        // if (!(this.props.enSession && this.props.enSession[this.props.lesson] && this.props.enSession[this.props.lesson].session_id)) return <i className='glyphicon glyphicon-remove-circle'></i>

        // let links = [<PageLink courseTag={this.props.courseTag} lessonTag={this.props.lessonTag} lang={LangOptions.EN} sessionId={this.props.enSession[this.props.lesson].session_id} loginSessionUID={this.props.enSession[this.props.lesson].login_session_uid} pageID={this.props.pageid} key='pagelink_en' />];

        // if (this.props.esSession && this.props.esSession[this.props.lesson]) {
        //     links.push(String.fromCharCode(160)/* &nbsp; */ + '/' + String.fromCharCode(160));
        //     links.push(<PageLink courseTag={this.props.courseTag} lessonTag={this.props.lessonTag} lang={LangOptions.ES} sessionId={this.props.esSession[this.props.lesson].session_id} loginSessionUID={this.props.esSession[this.props.lesson].login_session_uid} pageID={this.props.pageid} key='pagelink_es' />)
        // }
        // console.log(links);
        return <span>
            <a href='#' onClick={this.handlePageLinkClick.bind(this, 'EN')}>EN</a>
            {String.fromCharCode(160)/* &nbsp; */ + '/' + String.fromCharCode(160)}
            <a href='#' onClick={this.handlePageLinkClick.bind(this, 'ES')}>ES</a>
        </span>
    }
}