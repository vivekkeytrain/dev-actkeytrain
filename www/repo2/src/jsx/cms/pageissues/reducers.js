import { combineReducers } from 'redux';
import {
    ADD_MESSAGE, CLOSE_MESSAGE,
    UPDATE_DISPLAY_FIELD, SET_SORT_FIELD, SET_HIGHLIGHTED_ROW,
    FETCH_PAGE_ISSUES_REQUEST, FETCH_PAGE_ISSUES_SUCCESS, FETCH_PAGE_ISSUES_FAILURE,
    DO_LOGIN_LESSON_REQUEST, DO_LOGIN_LESSON_SUCCESS, DO_LOGIN_LESSON_FAILURE,
    FETCH_EDITOR_CONTENT_REQUEST, FETCH_EDITOR_CONTENT_SUCCESS, FETCH_EDITOR_CONTENT_FAILURE,
    DisplayFields
} from './actions';

import { findIndex } from 'lodash';

function sessions(state = {}, action) {
    switch (action.type) {
        case DO_LOGIN_LESSON_SUCCESS:
        case DO_LOGIN_LESSON_REQUEST:
        case DO_LOGIN_LESSON_FAILURE:
            if (!action.lang) return state;

            return Object.assign({}, state, {
                [action.lang]: lang_sessions(state[action.lang], action)
            });
        default:
            return state;
    }
}

function lang_sessions(state = {}, action) {
    switch (action.type) {
        case DO_LOGIN_LESSON_SUCCESS:
            return Object.assign({}, state, {
                [action.lesson_id]: {
                    session_id: action.session_id,
                    login_session_uid: action.login_session_uid,
                    isFetching: false,
                    lastUpdated: action.receivedAt
                }
            });
        case DO_LOGIN_LESSON_REQUEST:
            return Object.assign({}, state, {
                [action.lesson_id]: {
                    isFetching: true
                }
            });
        case DO_LOGIN_LESSON_FAILURE:
            return Object.assign({}, state.sessions, {
                [action.lesson_id]: {
                    isFetching: false
                }
            });
        default:
            return state;
    }
}

function page_issues(state = {
    isFetching: false,
    issues: [],
    lastUpdated: null
}, action) {
    switch (action.type) {
        case FETCH_PAGE_ISSUES_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                lastUpdated: action.receivedAt,
                issues: action.page_issues
            });
        case FETCH_PAGE_ISSUES_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_PAGE_ISSUES_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

function displayFields(state = DisplayFields, action) {
    switch (action.type) {
        case UPDATE_DISPLAY_FIELD:
            let ind = findIndex(state, {'name': action.field_name});
            return [
                ...state.slice(0, ind),
                Object.assign({}, state[ind], {
                    isOn: action.isOn
                }),
                ...state.slice(ind+1)
            ];
        default:
            return state;
    }
}

function filters(state = {
    messages: [],
    displayFields: DisplayFields,
    sort_field: null,
    sort_dir: 'asc',
    highlighed_row: -1,
    sessions: {},
}, action) {
    switch (action.type) {
        case FETCH_EDITOR_CONTENT_FAILURE:
        case FETCH_PAGE_ISSUES_FAILURE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: 'danger',
                        message: action.ex.message,
                        visible: true
                    }
                ]
            });

        case ADD_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: action.message_type,
                        message: action.message,
                        visible: true
                    }
                ]
            });

        case CLOSE_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages.slice(0, action.index),
                    Object.assign({}, state.messages[action.index], {
                        visible: false
                    }),
                    ...state.messages.slice(action.index+1)
                ]
            });

        case UPDATE_DISPLAY_FIELD:
            return Object.assign({}, state, {
                displayFields: displayFields(state.displayFields, action)
            });

        case SET_SORT_FIELD:
            return Object.assign({}, state, {
                sort_field: action.field_name,
                sort_dir: action.sort_dir
            });

        case SET_HIGHLIGHTED_ROW:
            return Object.assign({}, state, {
                highlighted_row: action.index
            });

        case DO_LOGIN_LESSON_SUCCESS:
        case DO_LOGIN_LESSON_REQUEST:
            return Object.assign({}, state, {
                sessions: sessions(state.sessions, action)
            });

        default:
            return state;
    }
}

function editor_content(state = {
    isFetching: false,
    content: []
}, action) {
    switch (action.type) {
        case FETCH_EDITOR_CONTENT_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                content: action.content,
                lastUpdate: action.recievedAt
            });
        case FETCH_EDITOR_CONTENT_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_EDITOR_CONTENT_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

const issuesApp = combineReducers({
    page_issues,
    filters,
    editor_content
});

export default issuesApp;