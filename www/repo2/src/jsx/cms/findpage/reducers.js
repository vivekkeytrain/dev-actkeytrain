import { combineReducers } from 'redux';
import { 
    SET_LANG, SET_UNIT, SET_COURSE, SET_LESSON, SET_TOPIC, SET_MODE, SET_PAGE, SET_PAGEID, SET_PATTERN, SET_PATTERN_FILTER, SET_SESSION, UPDATE_DISPLAY_FIELD, SET_EDIT_MODE, SET_EDIT_ID, SET_SORT_FIELD, ADD_MESSAGE, CLOSE_MESSAGE, SET_USER, SET_FILTER_STATUS, SET_UPDATED_DATE, SET_UPDATED_DATE_FILTER,
    FETCH_EDITOR_CONTENT_REQUEST, FETCH_EDITOR_CONTENT_SUCCESS, FETCH_EDITOR_CONTENT_FAILURE,
    FETCH_PAGES_REQUEST, FETCH_PAGES_FAILURE, FETCH_PAGES_SUCCESS,
    FETCH_PAGE_EVERYTHING_SUCCESS, FETCH_PAGE_EVERYTHING_REQUEST, FETCH_PAGE_EVERYTHING_FAILURE,
    FETCH_PAGE_CHANGES_SUCCESS, FETCH_PAGE_CHANGES_REQUEST, FETCH_PAGE_CHANGES_FAILURE,
    FETCH_PAGE_CHUNKS_SUCCESS, FETCH_PAGE_CHUNKS_REQUEST, FETCH_PAGE_CHUNKS_FAILURE,
    FETCH_PAGE_QA_CONTENT_REQUEST, FETCH_PAGE_QA_CONTENT_SUCCESS, FETCH_PAGE_QA_CONTENT_FAILURE,
    FETCH_APP_CONSTANTS_SUCCESS, FETCH_APP_CONSTANTS_REQUEST, FETCH_APP_CONSTANTS_FAILURE,
    SET_CHUNK_VAL_SUCCESS, SET_CHUNK_VAL_REQUEST, SET_CHUNK_VAL_FAILURE,
    SET_CHUNK_MESSAGE, REMOVE_CHUNK_MESSAGE,
    FIX_PAGE_REQUEST, FIX_PAGE_SUCCESS, FIX_PAGE_FAILURE,
    SET_PASTEBOARD, SET_FIELD_MESSAGE,
    UPDATE_FIELD_REQUEST, UPDATE_FIELD_SUCCESS, UPDATE_FIELD_FAILURE,
    UPDATE_EVERYTHING_FIELD_REQUEST, UPDATE_EVERYTHING_FIELD_SUCCESS, UPDATE_EVERYTHING_FIELD_FAILURE,
    DO_LOGIN_LESSON_REQUEST, DO_LOGIN_LESSON_SUCCESS, DO_LOGIN_LESSON_FAILURE, SET_SELECTED_ROW, SET_SELECTED_ATTACHMENT, SET_COLUMN_WIDTH,
    DO_PAGE_CHUNK_ACTION_FAILURE, DO_PAGE_CHUNK_ACTION_SUCCESS, DO_PAGE_CHUNK_ACTION_REQUEST,
    SET_HIGHLIGHTED_ROW,
    SET_META_DISPLAY_STATI, SET_FRAMEWORK_DISPLAY_STATI,
    LangOptions, ModeFilters, DisplayFields, EditModes,
    fetchPagesSuccess, fetchPagesRequest
} from './actions';
const { QA } = ModeFilters;
const { EN } = LangOptions;
const { LIST } = EditModes;
import { chain, map, fromPairs, sortBy, findIndex, forEach, cloneDeep, forOwn } from 'lodash';


function sessions(state = {}, action) {
    switch (action.type) {
        case DO_LOGIN_LESSON_SUCCESS:
        case DO_LOGIN_LESSON_REQUEST:
        case DO_LOGIN_LESSON_FAILURE:
            if (!action.lang) return state;

            return Object.assign({}, state, {
                [action.lang]: lang_sessions(state[action.lang], action)
            });
        default:
            return state;
    }
}

function lang_sessions(state = {}, action) {
    // console.debug(state);
    // console.debug(action);
    switch (action.type) {
        case DO_LOGIN_LESSON_SUCCESS:
            // console.debug('DO_LOGIN_LESSON_SUCCESS', action.lesson_id, action.session_id, action.login_session_uid);
            return Object.assign({}, state, {
                [action.lesson_id]: {
                    session_id: action.session_id,
                    login_session_uid: action.login_session_uid,
                    isFetching: false,
                    lastUpdated: action.receivedAt
                }
            });
        case DO_LOGIN_LESSON_REQUEST:
            return Object.assign({}, state, {
                [action.lesson_id]: {
                    isFetching: true
                }
            });
        case DO_LOGIN_LESSON_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state.sessions, {
                [action.lesson_id]: {
                    isFetching: false
                }
            });
        default:
            return state;
    }
}

function displayFields(state = DisplayFields, action) {
    switch (action.type) {
        case UPDATE_DISPLAY_FIELD:
            let ind = findIndex(state, {'name': action.field_name});
            // console.debug(ind);
            // console.debug(state);
            return [
                ...state.slice(0, ind),
                Object.assign({}, state[ind], {
                    isOn: action.isOn
                }),
                ...state.slice(ind+1)
            ];
        default:
            return state;
    }
}

function filters(state = {
    lang: EN,
    mode: QA,
    unit: '',
    course: '',
    lesson: '',
    topic: '',
    page: '',
    pageid: '',
    pattern: '',
    pattern_filter: '',
    updated_date: '',
    updated_date_filter: '',
    sessions: {},
    displayFields: DisplayFields,
    selectedRow: -1,
    selectedAttachment: null,
    edit_mode: LIST,
    edit_id: null,
    sort_field: null,
    sort_dir: 'asc',
    messages: [],
    highlighted_row: -1,
    pasteboard: null,
    pasteboard_cut: false,
    stati: 0,
    meta_display_stati: 1,
    framework_display_stati: 2
}, action) {
    switch (action.type) {
        case SET_LANG:
            return Object.assign({}, state, {
                lang: action.lang
            });
        case SET_MODE:
            return Object.assign({}, state, {
                mode: action.mode
            });
        case SET_UNIT:
            return Object.assign({}, state, {
                unit: action.unit,
                course: '',
                lesson: '',
                topic: ''
            });
        case SET_COURSE:
            //  Can't set course if we haven't set unit
            if (state.unit == '') return state;

            return Object.assign({}, state, {
                course: action.course,
                lesson: '',
                topic: ''
            });
        case SET_LESSON:
            //  Can't set lesson if we haven't set unit and course
            if (state.unit == '' || state.course == '') return state;

            return Object.assign({}, state, {
                lesson: action.lesson,
                topic: ''
            });
        case SET_TOPIC:
            //  Can't set topic if we haven't set unit, course, and lesson
            if (state.unit == '' || state.course == '' || state.lesson == '') return state;

            return Object.assign({}, state, {
                topic: action.topic,
                pattern: ''
            });
        case SET_PAGE:
            return Object.assign({}, state, {
                page: action.page
            })
        case SET_PAGEID:
            if (action.pageid) {
                return Object.assign({}, state, {
                    topic: '',
                    pageid: action.pageid,
                    pattern: '',
                    pattern_filter: '',
                    unit: '',
                    course: '',
                    lesson: ''
                });
            } else {
                return Object.assign({}, state, {
                    pageid: action.pageid
                })
            }
        case SET_PATTERN:
            return Object.assign({}, state, {
                pattern: action.pattern
            });
        case SET_PATTERN_FILTER:
            if (action.pattern) {
                return Object.assign({}, state, {
                    pattern_filter: action.pattern,
                    pageid: '',
                    page: ''
                })
            } else 
                return state;

        case SET_UPDATED_DATE:
            return Object.assign({}, state, {
                updated_date: action.updated_date
            });

        case SET_UPDATED_DATE_FILTER:
            return Object.assign({}, state, {
                updated_date_filter: action.updated_date
            });

        case DO_LOGIN_LESSON_SUCCESS:
        case DO_LOGIN_LESSON_REQUEST:
            return Object.assign({}, state, {
                sessions: sessions(state.sessions, action)
            });

        case UPDATE_DISPLAY_FIELD:
            return Object.assign({}, state, {
                displayFields: displayFields(state.displayFields, action)
            });

        case SET_SELECTED_ROW:
            return Object.assign({}, state, {
                selectedRow: action.index
            });

        case SET_SELECTED_ATTACHMENT:
            return Object.assign({}, state, {
                selectedAttachment: action.attach_tag,
                rowTop: action.top
            });

        case SET_EDIT_MODE:
            return Object.assign({}, state, {
                edit_mode: action.mode
            });

        case SET_EDIT_ID:
            return Object.assign({}, state, {
                edit_id: action.pageid
            });

        // case SET_COLUMN_WIDTH:
        //     return Object.assign({}, state, {
        //         widths: widths(state.widths, action)
        //     })

        case SET_SORT_FIELD:
            return Object.assign({}, state, {
                sort_field: action.field_name,
                sort_dir: action.sort_dir
            });

        case FETCH_EDITOR_CONTENT_FAILURE:
        case FETCH_PAGES_FAILURE:
        case FETCH_PAGE_EVERYTHING_FAILURE:
        case FETCH_PAGE_CHANGES_FAILURE:
        case UPDATE_FIELD_FAILURE:
        case UPDATE_EVERYTHING_FIELD_FAILURE:
        // case DO_LOGIN_LESSON_FAILURE:
        case FETCH_PAGE_CHUNKS_FAILURE:
        case SET_CHUNK_VAL_FAILURE:
        case FETCH_PAGE_QA_CONTENT_FAILURE:
        case FETCH_APP_CONSTANTS_FAILURE:
        case DO_PAGE_CHUNK_ACTION_FAILURE:
        case FIX_PAGE_FAILURE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: 'danger',
                        message: action.ex.message,
                        visible: true
                    }
                ]
            });

        case ADD_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: action.message_type,
                        message: action.message,
                        visible: true
                    }
                ]
            });

        case CLOSE_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages.slice(0, action.index),
                    Object.assign({}, state.messages[action.index], {
                        visible: false
                    }),
                    ...state.messages.slice(action.index+1)
                ]
            });

        case SET_HIGHLIGHTED_ROW:
            return Object.assign({}, state, {
                highlighted_row: action.index
            });

        case SET_PASTEBOARD:
            return Object.assign({}, state, {
                pasteboard: action.chunk,
                pasteboard_cut: action.cut
            });

        case SET_FILTER_STATUS:
            // console.debug('old', state.stati);
            // console.debug('new', action.stati);
            // let new_stati_val = 0;
            // if (state.stati&action.stati > 0) {
            //     new_stati_val = state.stati & ~action.stati;
            // } else {
            //     new_stati_val = state.stati | action.stati;
            // }
            // console.debug('newest', new_stati_val);
            // return Object.assign({}, state, {
            //     stati: new_stati_val
            // });
            return Object.assign({}, state, {
                stati: action.stati
            });

        case SET_META_DISPLAY_STATI:
            return Object.assign({}, state, {
                meta_display_stati: action.stati
            });

        case SET_FRAMEWORK_DISPLAY_STATI:
            return Object.assign({}, state, {
                framework_display_stati: action.stati
            });

        default:
            return state;

    }
}

// function widths(state = {}, action) {
//     switch (action.type) {
//         case SET_COLUMN_WIDTH:
//             return Object.assign({}, state, {
//                 [action.field_name]: action.width
//             });
//         default:
//             return state;
//     }
// }

function actual_pages(state = {}, action) {
    // console.debug(action);
    // console.debug(state);
    switch (action.type) {
        case FETCH_PAGES_SUCCESS:
            //  Re-orient by pageid. Turns array into dict
            let page_items = chain(action.pages)
                .map(page => {
                    // console.debug(page.page_number);
                    return [page.PageID, page];
                })
                .fromPairs()
                .value();
            // console.debug(page_items);

            return Object.assign({}, state, page_items);
        // case FETCH_PAGE_DETAIL_SUCCESS:
        //     return Object.assign({}, state, {
        //         [action.pageid]: action.page_data
        //     });
        

        

        case FETCH_PAGES_REQUEST:
        case FETCH_PAGES_FAILURE:
        default:
            return state;
    }
}

function chunk_messages(state = {}, action) {
    switch (action.type) {
        case SET_CHUNK_MESSAGE:
        case REMOVE_CHUNK_MESSAGE:
            return Object.assign({}, state, {
                [action.lang]: lang_chunk_messages(state[action.lang], action)
            })
        default:
            return state;
    }
}

function lang_chunk_messages(state={}, action) {
    switch (action.type) {
        case SET_CHUNK_MESSAGE:
        case REMOVE_CHUNK_MESSAGE:
            return Object.assign({}, state, {
                [action.pageid]: messages(state[action.pageid], action)
            });
        default:
            return state;
    }
}

function messages(state = {
    chunks: []
}, action) {
    let chunk_ind;
    switch (action.type) {
        case SET_CHUNK_MESSAGE:
            chunk_ind = findIndex(state.chunks, {'chunkvar': action.chunkvar});
            console.debug(state);
            console.debug(state.chunks[chunk_ind]);

            if (chunk_ind === -1) {
                return Object.assign({}, state, {
                    chunks:[
                    ...state.chunks,
                    {
                        chunkvar: action.chunkvar,
                        pageid: action.pageid,
                        lang_id: action.lang,
                        message: {
                            message: action.message,
                            type: action.message_type
                        }
                    }]
                })
            } else {
                return Object.assign({}, state, {
                    chunks: [
                        ...state.chunks.slice(0, chunk_ind),
                        Object.assign({}, state.chunks[chunk_ind], {
                            message: {
                                message: action.message,
                                type: action.message_type
                            }
                        }),
                        ...state.chunks.slice(chunk_ind+1)
                    ]
                });
            }
        case REMOVE_CHUNK_MESSAGE:
            chunk_ind = findIndex(state.chunks, {'chunkvar': action.chunkvar});
            console.debug(state);
            console.debug(chunk_ind);
            console.debug(state.chunks[chunk_ind]);
            return Object.assign({}, state, {
                // chunks: [
                //     ...state.chunks.slice(0, chunk_ind),
                //     ...state.chunks.slice(chunk_ind+1)
                // ]
                chunks: [
                    ...state.chunks.slice(0, chunk_ind),
                    Object.assign({}, state.chunks[chunk_ind], {
                        message: {}
                    }),
                    ...state.chunks.slice(chunk_ind+1)
                ]
            });
        default:
            return state;
    }
}

function chunks(state = {}, action) {
    let chunk_ind;
    switch (action.type) {
        case FETCH_PAGE_CHUNKS_SUCCESS:
            return Object.assign({}, state, {
                chunks_loading: false,
                chunks: action.chunks,
                PageID: action.pageid
            });
        case SET_CHUNK_VAL_SUCCESS:
            chunk_ind = findIndex(state.chunks, {'chunkvar': action.chunkvar});
            // console.debug(state);
            // console.debug(action);
            // console.debug(chunk_ind);
            return Object.assign({}, state, {
                chunks: [
                    ...state.chunks.slice(0, chunk_ind),
                    Object.assign({}, state.chunks[chunk_ind], {
                        chunkval: action.chunkval,
                        chunkvar: action.chunkvar,
                        update_by: action.update_by,
                        update_date: (new Date()).toString()
                    }),
                    ...state.chunks.slice(chunk_ind+1)
                ]
            });
        
        default:
            return state;
    }
}


function items(state = {}, action) {
    // console.debug(state);
    switch (action.type) {
        case UPDATE_FIELD_SUCCESS:
            let update = {};
            update[action.fieldName] = action.fieldVal;
            // console.debug(update);

            return Object.assign({}, state, update);
        case UPDATE_FIELD_FAILURE:
            // console.debug(action.ex);
        case UPDATE_FIELD_REQUEST:
        default: 
            return state;
    }
}

/*
pages_by_tag: {
        'EN': {
            T1007: {
                isFetching: false,
                items: {
                    ALL: [1,2,3,5,6,7,8]
                    PATTERN_<pattern>: [5,6,7,8],
                    PATTERN_<pattern2>: [1,2,3]
                }
                lastUpdated: 123545
            },
            L105: {
                isFetching: false,
                items: [...],
                lastUpdated: 123455
            }
        }
    }
*/

function pages_by_tag(state = {}, action) {
    switch (action.type) {
        case FETCH_PAGES_SUCCESS:
        case FETCH_PAGES_REQUEST:
        case FETCH_PAGES_FAILURE:
            return Object.assign({}, state, {
                [action.lang]: lang_pages_by_tag(state[action.lang], action)
            });
        default:
            return state;
    }
}

function lang_pages_by_tag(state = {}, action) {
    switch (action.type) {
        case FETCH_PAGES_SUCCESS:
        case FETCH_PAGES_REQUEST:
        case FETCH_PAGES_FAILURE:
            return Object.assign({}, state, {
                [action.tag]: actual_tag_pages(state[action.tag], action)
            });
        default:
            return state;
    }
}

function actual_tag_pages(state = {
    isFetching: false,
    items: {},
    lastUpdated: null
}, action) {
    switch (action.type) {
        case FETCH_PAGES_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                items: lang_tag_pattern_pages(state.items, action),
                lastUpdated: action.receivedAt
            });
        case FETCH_PAGES_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_PAGES_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state, {
                isFetching: false
            })
        default:
            return state;
    }
}

function lang_tag_pattern_pages(state = {}, action) {
    switch (action.type) {
        case FETCH_PAGES_SUCCESS:
            let key = 'ALL';
            if (action.pattern) 
                key = `PATTERN_${action.pattern}`;
            else if (action.updated_date)
                key = `UPDATED_DATE_${action.updated_date}`;

            console.debug('lang_tag_pattern_pages', key);
            // let key = (action.pattern) ? 'PATTERN_' + action.pattern : 'ALL';
            let page_items = map(action.pages, 'PageID');
            return Object.assign({}, state, {
                [key]: page_items
            });

        case FETCH_PAGES_REQUEST:
        case FETCH_PAGES_FAILURE:
        default:
            return state;
    }
}

// function section_pages(state = {}, action, key) {
//     // console.debug(state);
//     switch (action.type) {
//         case FETCH_PAGES_SUCCESS:
//         case FETCH_PAGES_REQUEST:
//         case FETCH_PAGES_FAILURE:
//             return Object.assign({}, state, {
//                 [action[key]]: actual_pages(state[action[key]], action)
//             });
//         default:
//             return state;
//     }
// }

// function pages(state = {}, action) {
//     switch(action.type) {
//         case FETCH_PAGES_SUCCESS:
//             // if (action.pageid) {
//             //     //  If we've got the pageid then we ought to have one page from which we can set the unit/course/topic
//             //     let page = action.pages[0];
//             //     let filter_action = {
//             //         type: SET_COURSE,
//             //         course: page.
//             //     }

//             //     state = Object.assign({}, state, {

//             //     })
//             // }
//         case FETCH_PAGES_REQUEST:
//         case FETCH_PAGES_FAILURE:
//             if (action.topicid) {
//                 return Object.assign({}, state, {
//                     topics: section_pages(state.topics, action, 'topicid')
//                 });
//             } else if (action.pageid) {
//                 return Object.assign({}, state, {
//                     pages: section_pages(state.pages, action, 'pageid')
//                 });
//             } else if (action.pattern) {
//                 return Object.assign({}, state, {
//                     patterns: section_pages(state.patterns, action, 'pattern')
//                 });
//             }
//         default:
//             return state;
//     }
// }

function pages(state = {}, action) {
    // console.debug('**************************************************')
    // console.debug(state);
    switch (action.type) {
        case FETCH_PAGES_SUCCESS:
        case FETCH_PAGES_REQUEST:
        case FETCH_PAGES_FAILURE:
        // case FETCH_PAGE_DETAIL_SUCCESS:
        // case FETCH_PAGE_DETAIL_REQUEST:
        // case FETCH_PAGE_DETAIL_FAILURE:
        case UPDATE_FIELD_SUCCESS:
        case UPDATE_FIELD_REQUEST:
        case UPDATE_FIELD_FAILURE:
            return Object.assign({}, state, {
                [action.lang]: actual_pages(state[action.lang], action)
            });
        default:
            return state;
    }
}

function page_everythings(state = {}, action) {
    switch(action.type) {
        case FETCH_PAGE_EVERYTHING_SUCCESS:
            return Object.assign({}, state, {
                [action.pageid]: page_everything(state[action.pageid], action)
            })


            // let page_data = Object.assign({}, action.page_data, {
            //     messages: {},
            //     chagnes: []
            // });
            

            // console.debug(page_data);

            // return Object.assign({}, state, {
            //     [action.pageid]: page_data
            // });
        case SET_FIELD_MESSAGE:
            return Object.assign({}, state, {
                [action.pageid]: Object.assign({}, state[action.pageid], page_everythings_messages(state[action.pageid], action))
            });
        case UPDATE_EVERYTHING_FIELD_SUCCESS:
            console.debug(state);
            let page_state = Object.assign({}, state[action.pageid], {
                [action.field_name]: action.field_val
            });
            console.debug(state[action.pageid]);
            console.debug({
                [action.field_name]: action.field_val
            });
            console.debug(page_state);
            return Object.assign({}, state, {
                [action.pageid]: page_state
            });


            // return Object.assign({}, state, {
            //     [action.pageid]: Object.assign({}, state[action.pageid], {
            //         [action.field_name]: action.field_val
            //     })
            // });
        case FETCH_PAGE_CHANGES_SUCCESS:
            return Object.assign({}, state, {
                [action.pageid]: Object.assign({}, state[action.pageid], page_changes(state[action.pageid], action))
            });
        default:
            return state;
    }
}

function page_everything(state = {
    messages: {},
    changes: []
}, action) {
    switch (action.type) {
        case FETCH_PAGE_EVERYTHING_SUCCESS:
            let page_data = Object.assign({}, state, action.page_data);

            forOwn(action.page_data, (val, key) => {
                page_data.messages[key] = {
                    message: null,
                    type: null
                }
            });

            return page_data;
        default:
            return state;
    }
    

}

function page_changes(state = {}, action) {
    console.debug('page_changes', state, action);
    switch (action.type) {
        case FETCH_PAGE_CHANGES_SUCCESS:
            return Object.assign({}, state, {
                changes: action.changes
            })
        default:
            return state;
    }
}

function page_everythings_messages(state = {}, action) {
    console.debug('page_everythings_messages', state, action);
    switch(action.type) {
        case SET_FIELD_MESSAGE:
            return Object.assign({}, state, {
                messages: page_everythings_messages_field(state.messages, action)
            })

        default:
            return state;
    }
}

function page_everythings_messages_field(state = {}, action) {
    console.debug('page_everythings_messages_field', state, action);
    switch (action.type) {
        case SET_FIELD_MESSAGE:
            return Object.assign({}, state, {
                [action.field_name]: {
                    message: action.message,
                    type: action.message_type
                }
            });
        default:
            return state;
    }
}

function page_details(state = {}, action) {
    switch (action.type) {
        case FETCH_PAGE_CHUNKS_REQUEST:
        case FETCH_PAGE_CHUNKS_FAILURE:
        case FETCH_PAGE_CHUNKS_SUCCESS:
        case SET_CHUNK_VAL_SUCCESS:
        case FETCH_PAGE_QA_CONTENT_SUCCESS:
        case UPDATE_FIELD_SUCCESS:
            return Object.assign({}, state, {
                [action.lang]: lang_page_details(state[action.lang], action)
            })
        case FIX_PAGE_REQUEST:
        case FIX_PAGE_SUCCESS:
        case FIX_PAGE_FAILURE:
            //  Gotta update both langs
            return Object.assign({}, state, {
                1: lang_page_details(state[1], action),
                2: lang_page_details(state[2], action)
            });
        default:
            return state;
    }
}

function lang_page_details(state={}, action) {
    switch (action.type) {
        case FETCH_PAGE_CHUNKS_REQUEST:
        case FIX_PAGE_REQUEST:
            return Object.assign({}, state, {
                [action.pageid]: Object.assign({}, state[action.pageid], {
                    chunks_loading: true
                })
            });

        case FETCH_PAGE_CHUNKS_FAILURE:
        case FIX_PAGE_FAILURE:
            return Object.assign({}, state, {
                [action.pageid]: Object.assign({}, state[action.pageid], {
                    chunks_loading: false
                })
            });

        case FIX_PAGE_SUCCESS:
            return Object.assign({}, state, {
                [action.pageid]: Object.assign({}, state[action.pageid], {
                    chunks_loading: false
                })
            });


        case SET_CHUNK_VAL_SUCCESS:
        case FETCH_PAGE_CHUNKS_SUCCESS:
            // console.debug(state);
            // console.debug(state[action.pageid])
            return Object.assign({}, state, {
                [action.pageid]: chunks(state[action.pageid], action)
            });

        case FETCH_PAGE_QA_CONTENT_SUCCESS:
            // console.debug(state[action.pageid]);
            return Object.assign({}, state, {
                [action.pageid]: Object.assign({}, state[action.pageid], {
                    'qa_fields': action.data
                })
            });

        case UPDATE_FIELD_SUCCESS:
            // console.debug(state);
            // console.debug(state[action.pageid])
            // console.debug(state[action.pageid].qa_fields);
            return Object.assign({}, state, {
                // [action.pageid]: items(state[action.pageid], action)
                [action.pageid]: Object.assign({}, state[action.pageid], {
                    qa_fields: Object.assign({}, state[action.pageid].qa_fields, {
                        [action.fieldName]: action.fieldVal
                    }),
                    PageID: action.pageid
                })
            });

        default:
            return state;
    }
}


function editor_content(state = {
    isFetching: false,
    content: []
}, action) {
    switch (action.type) {
        case FETCH_EDITOR_CONTENT_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                content: action.content,
                lastUpdate: action.recievedAt
            });
        case FETCH_EDITOR_CONTENT_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_EDITOR_CONTENT_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

function constants(state = {
    langs: LangOptions,
    modes: ModeFilters,
    qa_status_val: {},
    qa_problem_bits: {},
    user: ''
}, action) {
    switch (action.type) {
        // case DO_LOGIN_SUCCESS:
        //     return Object.assign({}, state, {
        //         login_session_uid: action.login_session_uid
        //     });
        // case DO_LOGIN_FAILURE:
        //     return Object.assign({}, state, {
        //         login_session_uid: ''
        //     });
        // case DO_LOGIN_REQUEST:
        //     console.debug(action.ex);

        case SET_USER:
            return Object.assign({}, state, {
                user: action.user
            });

        case FETCH_APP_CONSTANTS_SUCCESS:
            let data_key = action.data[0];

            let constants_obj = {};
            forEach(data_key, (obj) => {
                constants_obj[obj.colname] = action.data[obj.row_id];
            });

            return Object.assign({}, state, constants_obj);

        default:
            return state;
    }
}





const qaApp = combineReducers({
    filters,
    pages_by_tag,
    pages,
    page_details,
    page_everythings,
    chunk_messages,
    editor_content,
    constants
});

export default qaApp;


/*
{
    filters: {
        lang: 'EN',
        mode: 'QA',
        unit: 'KT',
        course: 'aplmath',
        lesson: 'amath4',
        topic: '01-money-time-and-quantity',
        topic_slug: 'aplmath_amath4_01-money-time-and-quantity'
    },
    pages_by_tag: {
        'EN': {
            T1007: {
                isFetching: false,
                items: {
                    ALL: [1,2,3,5,6,7,8]
                    PATTERN_<pattern>: [5,6,7,8],
                    PATTERN_<pattern2>: [1,2,3]
                }
                lastUpdated: 123545
            },
            L105: {
                isFetching: false,
                items: [...],
                lastUpdated: 123455
            }
        }
    }
    pages: {
        'EN': {
            5: {
                ...
            },
            6: {
                ...
            }
        }
        'ES': {
            ...
        }
    },
    editor_content: {
        isFetching: false,
        content: []
    }
}
*/
