import React, { Component, PropTypes } from 'react';

export default class ClickInputField extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editing: false
        }
    }

    onClick(e) {
        e.preventDefault();
        this.setState({
            editing: true
        })
    }

    onBlur(e) {
        this.setState({
            editing: false
        })
        this.props.updateFieldVal(this._textarea.getDOMNode().value);
    }

    render() {
        let field;
        if (this.state.editing) {
            field = <textarea defaultValue={this.props.value} onBlur={this.onBlur.bind(this)} style={{width: '100%'}} cols={this.props.cols} rows={this.props.rows} ref={(r) => this._textarea = r} />
            // field = <input type="text" size={this.props.size} defaultValue={this.props.value} onBlur={this.onBlur.bind(this)} style={{width: '100%'}} />
        } else {
            field = <span onClick={this.onClick.bind(this)}>{this.props.value}</span>
        }

        return field;
    }
}

ClickInputField.defaultProps = {
    cols: "",
    rows: ""
}