import React, { Component, PropTypes } from 'react';

export default class PagePreviewImageBlock extends Component {

    render() {
        const { everythingPage } = this.props;

        if (!everythingPage.UseImage) return <div />

        let retval = <div class="img-responsive">
                            <figure>
                                <img src={everythingPage.image_path} height={everythingPage.ImageHeight} className="img-responsive with-border fadeImage" />

                                {everythingPage.ImageCaption ? <figcaption>{everythingPage.ImageCaption}</figcaption> : '' }
                            </figure>
                        </div>

        if (true /*everythingPage.page_type_id != 0*/) {
            retval =    <Col xs={5}>
                            {retval}
                        </Col>

        }

        return retval;
    }
} 