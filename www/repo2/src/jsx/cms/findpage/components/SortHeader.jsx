import React, { Component, PropTypes } from 'react';

import classNames from 'classnames';

export default class SortHeader extends Component {
    handleSortHeaderClick(e) {
        e.preventDefault();

        if (this.props.displayField.sortable === false) return; // Not a sortable field, do nothing

        let sort_dir = (this.props.sortField == this.props.displayField.name && this.props.sortDir == 'asc') ? 'desc' : 'asc';
        this.props.setSortField(this.props.displayField.name, sort_dir);
    }

    render() {
        // console.debug(this.props);
        let classes = classNames({
            'sortable-head': true,
            'sortable-descending': (this.props.sortField == this.props.displayField.name && this.props.sortDir == 'desc'),
            'sortable-ascending': (this.props.sortField == this.props.displayField.name && this.props.sortDir == 'asc')
        })

        return (
            <th data-sortable-col="true" className={classes}>
                <button onClick={this.handleSortHeaderClick.bind(this)}>{this.props.displayField.label ? this.props.displayField.label : this.props.displayField.name}</button>
            </th>
        )
    }
} 