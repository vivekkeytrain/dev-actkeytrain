import { Component } from 'react';
import { LangOptions } from '../actions';

import { invert } from 'lodash';

export default class PageLink extends Component {
    handleClick(e) {
        e.stopPropagation();
    }

    render() {
        // console.log(this.props);
        if (!(this.props.sessionId && this.props.loginSessionUID)) return <span />//<i className='glyphicon glyphicon-remove-circle'></i>

        let lang_code = invert(LangOptions)[this.props.lang]
        let url = 'http://es.run.keytrain.com/objects/' + this.props.courseTag + '/' + this.props.lessonTag + '/' + lang_code + '/html5_course.htm#load/' + this.props.sessionId + '/' + this.props.loginSessionUID + '/pageid/' + this.props.pageID;
        // console.debug(url);
        
        let target="_lesson_" + lang_code;

        return <a href={url} target={target} onClick={this.handleClick.bind(this)}>{lang_code}</a>
    }
}
