import React, { Component } from 'react';
// import classNames from 'classnames';
import { LangOptions } from '../actions';

export default class Header extends Component {
    renderButton(lang_id, lang) {
        // let classes = classNames({
        //     'btn': true,
        //     'btn-primary': true,
        //     'active': (lang.toLowerCase() == this.props.lang.toLowerCase())
        // });

        

        // console.log('renderButton', lang_id, lang);

        return <Button sm outlined bsStyle='darkblue' active={lang_id == this.props.lang} onClick={() => this.props.onSetLang(lang_id) }>{lang}</Button>
    }

    render() {
        return (
            <Row>
                <Col sm={3}>
                    <BLabel>Language: </BLabel>
                    <ButtonGroup>
                        {this.renderButton(LangOptions.EN, 'EN')}
                        {this.renderButton(LangOptions.ES, 'ES')}
                    </ButtonGroup>
                </Col>
            </Row>
        )
    }
}

