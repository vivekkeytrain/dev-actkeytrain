import { Component, PropTypes } from 'react';

import SortHeader from './SortHeader';

export default class PageTableHeader extends Component {
    render() {
        return (
            <thead>
                <tr>
                    {
                        this.props.displayFields.map((field, ind) => {
                            return <SortHeader displayField={field} sortField={this.props.sortField} sortDir={this.props.sortDir} key={ind} setSortField={this.props.setSortField} />
                        })
                    }
                </tr>
            </thead>
        )
    }
}