import { Component, PropTypes } from 'react';
import classNames from 'classnames';

class PasteboardView extends Component {
    closeButtonClick(e) {
        e.preventDefault();
        this.props.onPasteboardClear();
    }

    dangerousHTML(str) {
        // console.debug(str);
        if (!str) str = '&nbsp;';
        return {
            __html: str
        }
    }

    render() {
        console.debug(this.props);

        let liClasses = classNames({
            'messenger-message-slot': true,
            'messenger-shown': true,
            'messenger-first': true
        })

        let messageClasses = classNames({
            'messenger-message': true,
            'message': true,
            'alert': true,
            'info': true,
            'alert-info': true
        });

        if (this.props.chunk) {
            return (
                <li className={liClasses}>
                    <div className={messageClasses}>
                        <button type="button" className="messenger-close" data-dismiss="alert" onClick={this.closeButtonClick.bind(this)}>×</button>
                        <div className="messenger-message-inner"><strong><em>Pasteboard</em></strong></div>
                        <div className="messenger-message-inner" dangerouslySetInnerHTML={this.dangerousHTML(this.props.chunk.chunkval)}></div>
                        <div className="messenger-spinner">
                            <span className="messenger-spinner-side messenger-spinner-side-left">
                                <span className="messenger-spinner-fill"></span>
                            </span>
                            <span className="messenger-spinner-side messenger-spinner-side-right">
                                <span className="messenger-spinner-fill"></span>
                            </span>
                        </div>
                    </div>
                </li>
            )
        } else {
            return <li />
        }

    }
    
}

// PasteboardView.propTypes = {
//     chunk: PropTypes.string.isRequired
// }

export default class PastboardViewer extends Component {

    onPasteboardClear(message_index) {
        this.props.setPasteboard(null);
    }


    render() {
        // if (!this.props.children) return '';

        let classes = classNames({
            'messenger': true,
            'messenger-fixed': true,
            'messenger-on-bottom': true,
            'messenger-on-right': true,
            'messenger-theme-flat': true,
            'messenger-empty': (!this.props.children)
        })

        return (
            <ul className={classes}>
                {React.Children.map(this.props.children, (pasteboard) => {
                    return <PasteboardView chunk={pasteboard.props.children} onPasteboardClear={this.onPasteboardClear.bind(this)} />
                }, this)}
            </ul>
        )
    }
}


