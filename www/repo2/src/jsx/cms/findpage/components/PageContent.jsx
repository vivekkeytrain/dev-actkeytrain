import React, { Component, PropTypes } from 'react';

import { EditModes } from '../actions';

import PageListView from './PageListView';
import PageDetailView from './PageDetailView';

export default class PageContent extends Component {
    render() {
        console.debug(this.props);
        // let viewComponent = (this.props.editMode === EditModes.LIST) ? <PageListView {...this.props} /> : <PageDetailView {...this.props} />

        let page_details;
        if (this.props.editMode === EditModes.PREVIEW) {
            page_details = <PageDetailView {...this.props} />
        }

        // console.debug(viewComponent);
        return (
            <div>
                <PageListView {...this.props} />
                {page_details}
            </div>
        )
    }
} 