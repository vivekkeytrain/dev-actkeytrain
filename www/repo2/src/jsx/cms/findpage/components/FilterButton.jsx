import React, { Component, PropTypes } from 'react';

export default class FilterButton extends Component {
    onButtonClick(stati_val, e) {
        this.props.onButtonClick(stati_val);
        e.target.blur();
    }

    render() {
        return (
            <Button bsStyle='blue' md active={(this.props.filterStati == this.props.statiVal)} onClick={this.onButtonClick.bind(this, this.props.statiVal)}>{this.props.children}</Button>
        );
    }
} 