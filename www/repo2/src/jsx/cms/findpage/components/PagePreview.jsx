import React, { Component, PropTypes } from 'react';

import { EditModes } from '../actions';

import PagePreviewFramework from './PagePreviewFramework';
import PagePreviewMeta from './PagePreviewMeta';

import { sortBy, map } from 'lodash';

export default class PagePreview extends Component {

    changeSelectedRow(increment) {
        this.props.changeSelectedRow(increment);
    }

    setMetaDisplayStati(e) {
        this.props.setMetaDisplayStati(e.target.value);
    }

    setFrameworkDisplayStati(e) {
        this.props.setFrameworkDisplayStati(e.target.value);
    }

    render() {
        const { everythingPage, filters } = this.props;

        if (!everythingPage) return <div />

        console.debug(this.props);

        let content_obj = [
            {
                obj: <PagePreviewMeta everythingPage={everythingPage} />,
                stati: filters.meta_display_stati
            },
            {
                obj: <PagePreviewFramework everythingPage={everythingPage} updateEverythingField={this.props.updateEverythingField} />,
                stati: filters.framework_display_stati
            }
        ]

        let content = map(sortBy(content_obj, ['stati']), (o) => {
            if (o.stati != 0) return o.obj;
            return null;
        });

/*
<PagePreviewMeta everythingPage={everythingPage} />
                <PagePreviewFramework everythingPage={everythingPage} updateEverythingField={this.props.updateEverythingField} />
*/

        return (
            <div className="page-preview">
                <Row className="controls">
                    <Col xs={3} className="pull-left" collapseLeft>
                        <Button onClick={this.changeSelectedRow.bind(this, -1)}><i className="glyphicon glyphicon-chevron-left"></i> Previous Page</Button>
                    </Col>
                    <Col xsOffset={1} xs={5}>
                        <label>Meta:</label><input type="text" size="1" value={filters.meta_display_stati} onChange={this.setMetaDisplayStati.bind(this)} />
                        <label>Framework:</label><input type="text" size="1" value={filters.framework_display_stati} onChange={this.setFrameworkDisplayStati.bind(this)} />
                    </Col>
                    <Col xs={3} className="pull-right" collapseRight>
                        <Button className="pull-right" onClick={this.changeSelectedRow.bind(this, 1)}>Next Page<i className="glyphicon glyphicon-chevron-right"></i></Button>
                    </Col>
                </Row>
                <h2 className="title">{everythingPage.page_Title}</h2>
                {content}
                
            </div>
        )
    }
} 

/*
Page Type
Chapter Type
Page #
Title
Page Number / Code
Page Text
Problem Text 
Hint
Solution Steps
Image
Additional Images
Flash Paper
Question Group Code
Distractors
Question Text
Sound

{
        "PageID": 9737,
        "page_isactive": true,
        "pageno": 1,
        "page_Title": "Welcome to KeyTrain",
        "page_type_id": 0,
        "page_attrs": 0,
        "lang_id": 3,
        "stati": 0,
        "PageNumber": "",
        "page_text": "<p>Welcome to KeyTrain, ACT's&nbsp;online learning program for the&nbsp;WorkKeys® job skills assessment system! </p>\r\n<p>What is WorkKeys? WorkKeys is a job skills assessment system measuring \"real world\" skills that employers believe are critical to job success. These skills are valuable for any occupation – skilled or professional – and at any level of education. </p>\r\n<p>In this introduction to KeyTrain you will: </p>\r\n<ul>\r\n<li>learn to navigate KeyTrain courses and topic lessons\r\n</li><li>learn useful features found in many of the KeyTrain topic lessons\r\n</li><li>learn the correct method for exiting KeyTrain lessons</li></ul>\r\n<p>Let’s get started! Click on the <i>Next</i> button&nbsp;below to continue. </p>",
        "problem_text": "",
        "MCQuestion": null,
        "MCSelectedAnswer": null,
        "MCDistractorA": null,
        "MCDistractorB": null,
        "MCDistractorC": null,
        "MCDistractorD": null,
        "MCFeedbackA": null,
        "MCFeedbackB": null,
        "MCFeedbackC": null,
        "MCFeedbackD": null,
        "FIBQuestion": null,
        "FIBAnswers": null,
        "FIBFeedbackCorrect": null,
        "FIBFeedbackIncorrect": null,
        "SolutionSteps": null,
        "Hint": null,
        "Calculator": "False",
        "QuestionGroup": null,
        "insert_date": "2006-04-09T00:00:00",
        "update_date": "2014-01-24T09:13:19.767",
        "xc_update": "2015-12-01T21:48:53.14",
        "update_by": null,
        "LastEditedBy": "tom",
        "UseCustomProblemText": "False",
        "UseProblemText": "False",
        "ProblemTextID": null,
        "Sound": true,
        "SoundUploaded": "True",
        "SoundAlwaysUse": "False",
        "SoundDescription": "ABOUT_KT_P01",
        "SoundFileName": "About_KT_P01.mp3",
        "UseFlashPaper": "False",
        "FlashPaperUploaded": null,
        "FlashPaperLocationID": null,
        "FlashPaperMenuName": null,
        "FlashPaperDescription": null,
        "FlashPaperFileName": null,
        "UseImage": "True",
        "UseChapterImage": "False",
        "ImageZoomable": "False",
        "ImageUploaded": "True",
        "ImageIsMovie": "False",
        "ImageCaption": null,
        "ImageDescription": null,
        "ImageFileName": "K:\\Project Iowa\\Listening for Understanding\\Generic Photos\\41812829.thb.jpg",
        "CustomFlashUploaded": null,
        "CustomFlashDescription": null,
        "CustomFlashFileName": null,
        "ProblemTextHeight": "-1",
        "QuestionBoxWidth": "-1",
        "ImageWidth": "193",
        "ImageHeight": "290",
        "ZoomImageWidth": "-1",
        "ZoomImageHeight": "-1",
        "ImageCaptionWidth": "0",
        "ImageCaptionHeight": "0",
        "ProgrammingNotes": null,
        "ReportProblem": "False",
        "ReportProblemTitle": null,
        "ReportProblemDescription": null,
        "topic_id": 1981,
        "topic_tag": "ABOUTKT_Q",
        "topic_desc": "About Keytrain Quiz",
        "topic_slug": "00-about-keytrain-quiz",
        "topic_stati": 1,
        "topic_isactive": 1,
        "topic_seq": 1,
        "topic_is_fq": false,
        "lesson_id": 115,
        "lesson_tag": "INTROKT",
        "lesson_seq": 11,
        "lesson_isactive": 1,
        "lesson_desc": "Introduction to KeyTrain",
        "lesson_abbrev": "Intro to KeyTrain",
        "lesson_level": null,
        "lesson_is_pretest": 0,
        "media_types": 6,
        "course_id": 16,
        "course_tag": "KTINTRO",
        "course_seq": 1,
        "course_isactive": true,
        "course_desc": "Introduction to KeyTrain",
        "course_abbrev": "KeyTrain Intro",
        "unit_id": 1,
        "unit_tag": "KT",
        "unit_seq": 1,
        "unit_desc": "KeyTrain Workplace Skills",
        "unit_abbrev": "KeyTrain",
        "page_match_str": "|U1|C16|L115|T1981|P9737|",
        "LastUpdated": "2014-01-24T09:13:19.767"
      }
*/