import { Component } from 'react';
import PageLink from './PageLink';

import { LangOptions } from '../actions';

export default class PageLinks extends Component {
    render() {
        // console.log(this.props);

        // console.debug(this.props.enSession);
        // console.debug(this.props.lesson);
        // console.debug(this.props.enSession[this.props.lesson]);
        // console.debug(this.props.enSession[this.props.lesson].session_id)
        if (!(this.props.enSession && this.props.enSession[this.props.lesson] && this.props.enSession[this.props.lesson].session_id)) return <i className='glyphicon glyphicon-remove-circle'></i>

        let links = [<PageLink courseTag={this.props.courseTag} lessonTag={this.props.lessonTag} lang={LangOptions.EN} sessionId={this.props.enSession[this.props.lesson].session_id} loginSessionUID={this.props.enSession[this.props.lesson].login_session_uid} pageID={this.props.pageid} key='pagelink_en' />];

        if (this.props.esSession && this.props.esSession[this.props.lesson]) {
            links.push(String.fromCharCode(160)/* &nbsp; */ + '/' + String.fromCharCode(160));
            links.push(<PageLink courseTag={this.props.courseTag} lessonTag={this.props.lessonTag} lang={LangOptions.ES} sessionId={this.props.esSession[this.props.lesson].session_id} loginSessionUID={this.props.esSession[this.props.lesson].login_session_uid} pageID={this.props.pageid} key='pagelink_es' />)
        }
        // console.log(links);
        return <span>{links}</span>
    }
}


// renderPageLinks(pageID) {
//         let en_url = 'http://dev.run.keytrain.com/objects2/' + this.props.courseTag + '/' + this.props.lessonTag + '/en/html5_course.htm#load/' + this.props.enSession.session_id + '/' + this.props.enSession.login_session_uid + '/pageid/' + pageID;
//         let es_url = 'http://dev.run.keytrain.com/objects2/' + this.props.courseTag + '/' + this.props.lessonTag + '/es/html5_course.htm#load/' + this.props.esSession.session_id + '/' + this.props.esSession.login_session_uid + '/pageid/' + pageID;

//         let en_key = pageID + "_en";
//         let es_key = pageID + "_es";

//         return [<a href={en_url} target="_lesson_en" key={en_key} onClick={this.onPageLinkClick.bind(this, pageID, LangOptions.EN)}>EN</a>, ' / ', <a href={es_url} target="_lesson_es" key={es_key} onClick={this.onPageLinkClick.bind(this, pageID, LangOptions.ES)}>ES</a>];
//     }