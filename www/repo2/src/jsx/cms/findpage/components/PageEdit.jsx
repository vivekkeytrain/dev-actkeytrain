import React, { Component, PropTypes } from 'react';

import { EditModes } from '../actions';

export default class PageEdit extends Component {
    onImmediateFieldChange(name, e) {
        this.props.onImmediateUpdatePageField(this.props.detailPage.PageID, name, e.target.value);
    }

    onFieldChange(name, e) {
        this.props.onUpdatePageField(this.props.detailPage.PageID, name, e.target.value);
    }

    previewPage(e) {
        e.preventDefault();
        this.props.setEditMode(EditModes.PREVIEW);
    }

    returnToList(e) {
        e.preventDefault();
        this.props.setEditMode(EditModes.LIST);
    }

    render() {
        console.debug(this.props);
        return (
            <div>
                <p>Title: <input type="text" value={this.props.detailPage.Title} onChange={this.onImmediateFieldChange.bind(this, 'Title')} onBlur={this.onFieldChange.bind(this, 'Title')} /></p>
                <p>PageText: <input type="text" value={this.props.detailPage.PageText} onChange={this.onImmediateFieldChange.bind(this, 'PageText')} onBlur={this.onFieldChange.bind(this, 'PageText')} /></p>
                <p><button onClick={this.previewPage.bind(this)}>Preview Page</button></p>
                <p><button onClick={this.returnToList.bind(this)}>Return to List</button></p>
            </div>
        )
    }
} 