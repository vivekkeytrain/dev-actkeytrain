import {Component} from 'react';

export default class PageLangColumn extends Component {
    render() {
        return (
            <Col xs={12} sm={6}>
                <Row>
                    <Col xs={12}>
                        <h4>{this.props.lang}</h4>
                    </Col>
                </Row>
            </Col>
        )
    }
}
