import React, { Component, PropTypes } from 'react';
 
import ContentTree from '../../common/ContentTree';
import MessageCenter from '../../common/MessageCenter';
import Filters from './Filters';
import PageList from './PageList';

export default class PageListView extends Component {
    render() {
        return (
            <Grid>
                <Row>
                    <Col xs={12}>
                        <PanelContainer plain >
                            <Panel style={{background: 'white', minHeight: '500px'}}>
                                <PanelHeader className='bg-blue fg-white' >
                                    <Grid>
                                        <Row>
                                            <Col xs={12}>
                                                <p>
                                                    <ContentTree {...this.props} />
                                                </p>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelHeader>
                                <MessageCenter onMessageClose={index => this.props.handleMessageClose(index)}>
                                    {this.props.messages.map((message, ind) => {
                                        return <div type={message.type} index={ind} visible={message.visible} key={"message_" + ind} >{message.message}</div>
                                    })}
                                </MessageCenter>
                                <PanelBody>
                                    <Grid>
                                        <Row>
                                            <Col xs={12}>
                                                <Filters {...this.props} />
                                                <PageList {...this.props} />
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelBody>
                            </Panel>
                        </PanelContainer>
                    </Col>
                </Row>
            </Grid>
        )
    }
} 