import React, { Component, PropTypes } from 'react';

export default class FilterText extends Component {
    componentDidMount() {
        $(this._filterText.getDOMNode());

        let self = this;
        $(this._filterText.getDOMNode()).keyup(function(event){
            if(event.keyCode == 13){ // Enter key
                self.props.onUpdateTextFilter(self.props.name, this.value);
                self.props.onTextFilterFetch();
            }
        });
    }

    onUpdateTextFilter(name, e) {
        // console.debug(this);
        // console.debug(this.props);
        // console.debug(e.target.value)
        this.props.onUpdateTextFilter(name, e.target.value);
    }

    render() {
        // console.debug('render');
        // onChange={this.onUpdateTextFilter.bind(this, this.props.name)}
        return (
                <input className="form-control floatlabel" type='text' placeholder={this.props.label} name={this.props.name} defaultValue={this.props.val} onBlur={this.onUpdateTextFilter.bind(this, this.props.name)} disabled={this.props.disabled} ref={(r) => this._filterText = r} />
            
        );
    }
} 