import { Component, PropTypes } from 'react';
import { map, invert } from 'lodash';
import PageLinks from './PageLinks';
import { LangOptions } from '../actions';

import classNames from 'classnames';

export default class PageRow extends Component {
    // renderCell(name, row) {
    //     // console.debug(name, row);
    //     let displayField = find(this.props.displayFields, {'name': name});
    //     // console.debug(displayField)

    //     let cellData = row[name];
    //     // console.debug(cellData);
    //     if (typeof displayField.displayFunc === 'function') {
    //         let dfunc = displayField.displayFunc.bind(this, cellData, row);
    //         cellData = dfunc();
    //     }

    //     if (typeof cellData === 'boolean') {
    //         cellData = (cellData) ? <i className="glyphicon glyphicon-ok"></i> : <i className="glyphicon glyphicon-minus"></i>;
    //     }

    //     // console.debug('renderCell', name, cellData);
    //     return cellData;
    // }

    renderCell(displayField) {
        // console.debug(this.props);
        let cell_data = this.props.page[displayField.name];
        // console.debug(displayField.displayFunc);
        if (typeof displayField.displayFunc === 'function') {
            let dfunc = displayField.displayFunc.bind(this, cell_data, this.props, this.props.index);
            cell_data = dfunc();
        }

        return cell_data;
    }

    setFocus(index, e) {
        // console.debug(index);
        // console.debug(this['row_' + index]);
        // this['row_' + index].getDOMNode().focus();
        this.props.setHighlightedRow(index);
        // this.props.onRowSelect(index);
    }

    render() {
        // console.debug(this.props);
        let classes = classNames({
            warning: (this.props.selectedRow == this.props.index),
            highlighted: this.props.highlighted
        });

        return (
            <tr onClick={this.setFocus.bind(this, this.props.index)} className={classes} ref={(ref) => this["row_" + this.props.index] = ref}>
                {
                    this.props.displayFieldsOn.map((field, ind) => {
                        return <td key={ind}>{this.renderCell(field)}</td>
                    })
                }
            </tr>
        )
    }
}

