import { Component } from 'react';
import { map } from 'lodash';

export default class StatiButtonGroup extends Component {
    buttonClicked(status_val, e) {
        console.debug(e.target);
        e.target.blur();
        this.props.buttonSelected.call(this, status_val);
    }

    render() {
        console.debug(this.props);
        let buttons;
        if (this.props.bits) {
            buttons = this.props.bits.map((bit_obj, index) => {
                        return <Button sm bsStyle='blue' active={(this.props.currentVal & bit_obj.status_val) > 0} onClick={this.buttonClicked.bind(this, bit_obj.status_val)}>{bit_obj.status_desc}</Button>
                    });
        } 

        if (this.props.grouped) {
            buttons =   <ButtonGroup sm>
                            {buttons}
                        </ButtonGroup>
        }


        return (
            <div>
                {buttons}
            </div>
        );
    }
}
