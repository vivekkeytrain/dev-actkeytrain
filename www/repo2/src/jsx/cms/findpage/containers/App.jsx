import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { 
    LangOptions, setUnit, setCourse, setLesson, setTopic, setLang, setPage, setPageID, setPattern, setPatternFilter, setUpdatedDate, setUpdatedDateFilter, updateLocalDisplayField, loadDisplayFields, openPage, setEditMode, setEditID, setSortField, closeMessage, setChunkVal, setUser, setPasteboard, doPageChunkAction, setFilterStatus,
    fetchEditorContent, fetchPagesIfNeeded, doLoginIfNeeded, setSelectedRow, setSelectedAttachment, DetailFields, updateColumnWidth, updatePageField, immediateUpdatePageField, fetchPageChunksIfNeeded, fetchPageQAContent, fetchAppConstants, setHighlightedRow, addMessage, doFixPage, 
        fetchPageEverything, fetchPageChanges,
    EditModes, setChunkMessage, removeChunkMessage, updateEverythingField, setMetaDisplayStati, setFrameworkDisplayStati
} from '../actions';

import { filteredContentSelector, selectCourseTag, selectLessonTag } from '../selectors';
// import Header from '../components/Header';
// import ContentTree from 'common/ContentTree';
// import PageList from '../components/PageList';
// import Filters from '../components/Filters';
// import PageContent from '../components/PageContent';

import ContentTree from '../../common/ContentTree';
import MessageCenter from '../../common/MessageCenter';
import Filters from '../components/Filters';
import PageList from '../components/PageList';
import PageDetailView from '../components/PageDetailView';
import PasteboardViewer from '../components/PasteboardViewer';
import PagePreviewHeader from '../components/PagePreviewHeader';
import PagePreview from '../components/PagePreview';

import { DetailContainer, DetailContainerHeader, DetailContainerBody } from '../../common/DetailContainer';


import { invert, findIndex } from 'lodash';


class App extends Component {
    // state = {
    //     refreshTimer: null
    // }

    clearTimer() {
        if (this.state && this.state.refreshTimer) {
            window.clearTimeout(this.state.refreshTimer);
            this.setState({
                refreshTimer: null
            });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        // console.debug('componentDidMount', prevProps.highlighted_row, this.props.highlighted_row);
        if (prevProps.highlighted_row != this.props.highlighted_row && $('tr.highlighted').size() > 0) {
            $('tr.highlighted').get(0).scrollIntoView(false);
        }
    }

    componentDidMount() {
        let { dispatch, unit, course, lesson, topic, lang } = this.props;
        // dispatch(doLogin());
        dispatch(loadDisplayFields());
        dispatch(fetchEditorContent())
            .then(() => {
                unit = parseInt(localStorage.getItem('content_unit'), 10);
                course = parseInt(localStorage.getItem('content_course'), 10);
                lesson = parseInt(localStorage.getItem('content_lesson'), 10);
                topic = parseInt(localStorage.getItem('content_topic'), 10);

                if (unit) dispatch(setUnit(unit));
                if (course) dispatch(setCourse(course));
                if (lesson) dispatch(setLesson(lesson));
                if (topic) dispatch(setTopic(topic));

                dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang));

                dispatch(doLoginIfNeeded(lesson, LangOptions.EN));
                dispatch(doLoginIfNeeded(lesson, LangOptions.ES));


            });

        /*  Look for logged in user */
        /*  window.auth is inserted by nginx on the server and hardcoded for dev    */
        if (!window.auth) window.auth = 'Basic YmVuOmJlbg=='

        let auth = atob(window.auth.replace('Basic ', ''));
        // console.debug(auth);
        let auth_parts = auth.split(':');
        dispatch(setUser(auth_parts[0]));
        dispatch(fetchAppConstants());
    }

    render() {
        console.debug(this.props);
        //  Injected by connect() call:
        const { dispatch, content, unit, units, course, courses, course_tag, lesson, lessons, lesson_tag, topic, topics, lang, page, pageid, pattern, patternFilter, updated_date, updated_date_filter, displayFields, displayFieldsOn, all_pages, filtered_pages, /*unit_pages, course_pages, pages_fetching, topic_pages, lesson_pages,*/ pages_fetching, constants, selectedRow, selectedAttach, rowTop, all_sessions, enSession, esSession, edit_mode, edit_id, detailPage, detailPages, sort_field, sort_dir, messages, selected_page_id, highlighted_row, pasteboard, pasteboard_cut, filter_stati, everything_page, filters } = this.props;

        let handleUnitClick = (unit) => {
            dispatch(setPage(''));
            dispatch(setPageID(''));
            dispatch(setUnit(unit));
            dispatch(setFilterStatus(0));
            dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang, pageid, patternFilter, updated_date_filter, 0));
        }

        let handleCourseClick = (course) => {
            dispatch(setPage(''));
            dispatch(setPageID(''));
            dispatch(setCourse(course));
            dispatch(setFilterStatus(0));
            dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang, pageid, patternFilter, updated_date_filter, 0));
        }

        let handleLessonClick = (lesson) => {
            dispatch(setPage(''));
            dispatch(setPageID(''));
            dispatch(setLesson(lesson));
            dispatch(setFilterStatus(0));
            dispatch(doLoginIfNeeded(lesson, LangOptions.EN));
            dispatch(doLoginIfNeeded(lesson, LangOptions.ES));
            dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang, pageid, patternFilter, updated_date_filter, 0));
        }

        let handleTopicClick = (topic) => {
            dispatch(setPage(''));
            dispatch(setPageID(''));
            dispatch(setTopic(topic));
            dispatch(setFilterStatus(0));
            dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang, pageid, patternFilter, updated_date_filter, 0));
        }

        let handleLangClick = (lang) => {
            dispatch(setLang(lang_id));
            dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang, pageid, patternFilter, updated_date_filter, filter_stati));
        }

        let handleTextFilterUpdate = (field_name, val) => {
            dispatch(setFilterStatus(0))
            if (field_name == 'pageid') {
                dispatch(setPage(val));
            } else if (field_name == 'pattern') {
                dispatch(setPattern(val));
            } else if (field_name == 'updated_date') {
                dispatch(setUpdatedDate(val));
            }
            // dispatch(fetchPagesIfNeeded(topic, pageid, pattern));
        }

        let handleTextFilterFetch = () => {
            console.debug('handleTextFilterFetch');
            dispatch(setPageID(page));
            dispatch(setPatternFilter(pattern));
            dispatch(setUpdatedDateFilter(updated_date));
            console.debug(this.props)
            dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang, page, pattern, updated_date, filter_stati))
                .then(() => {
                    if (page && this.props.all_pages[lang] && this.props.all_pages[lang][page]) {
                        //  If we've only go one page we'll reset the U/C/L/T selectors
                        let this_page = this.props.all_pages[lang][page];
                        dispatch(setUnit(this_page.unit_id));
                        dispatch(setCourse(this_page.course_id));
                        dispatch(setLesson(this_page.lesson_id));
                        dispatch(setTopic(this_page.topic_id));

                        //  Reset page because it gets unset while setting U/C/L/T
                        // dispatch(setPage(page));

                        //  Actually now we're just going to highlight the page
                        dispatch(setPageID(''));
                        dispatch(fetchPagesIfNeeded(this_page.unit_id, this_page.course_id, this_page.lesson_id, this_page.topic_id, lang, '', pattern, updated_date, filter_stati))
                            .then(() => {
                                dispatch(setHighlightedRow(findIndex(this.props.filtered_pages, {'PageID': parseInt(page)})));
                            });
                        // dispatch(setHighlightedRow(new_selected_index));
                    }
                });
        }

        let handleFilterStatusUpdate = (new_stati) => {
            dispatch(setFilterStatus(new_stati));
            dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang, page, pattern, updated_date, new_stati));
        }

        let handlePageLinkClick = (pageid, lang_id) => {
            let this_page = all_pages[1][pageid]; // For now we're only loading english pages, so this_page is en no matter which link clicked
            console.debug(this_page);
            let lesson_id = this_page.lesson_id;
            console.debug(lesson_id);
            dispatch(doLoginIfNeeded(lesson_id, lang_id))
                .then(() => {
                        let all_sessions = this.props.all_sessions;
                        let session = all_sessions[lang_id][lesson_id];
                        let lang_code = invert(LangOptions)[lang_id];
                        
                        if (!unit) dispatch(setUnit(this_page.unit_id));
                        if (!course) dispatch(setCourse(this_page.course_id));

                        console.debug(this.props);

                        let course_tag = selectCourseTag(this_page.course_id, this.props.courses);
                        let lesson_tag = selectLessonTag(this_page.lesson_id, this.props.lessons);

                        let url = 'http://es.run.keytrain.com/objects2/' + course_tag + '/' + lesson_tag + '/' + lang_code + '/html5_course.htm#load/' + session.session_id + '/' + session.login_session_uid + '/pageid/' + pageid;
                        window.open(url, '_lesson_' + lang_code);
                        dispatch(openPage(pageid, lang_code));
                    }
                );
        }

        let refreshSession = () => {
            if (this.state && !this.state.refreshTimer) {
                if ((enSession && enSession.login_session_uid)|| (esSession && esSession.login_session_uid)) {
                    let time_left = (1000 * 60 * 15) - (Date.now() - enSession.lastUpdated) + 1000; //   15 minutes from last update (plus 1 second so it's after the doLoginIfNeeded check)
                    // console.debug(time_left);

                    let timer = window.setTimeout(() => {
                        dispatch(doLoginIfNeeded(lesson, LangOptions.EN));
                        dispatch(doLoginIfNeeded(lesson, LangOptions.ES));
                        this.clearTimer();
                        refreshSession();
                    }, time_left);
                    this.setState({
                        refreshTimer: timer
                    });
                }
            }
            // dispatch(doLoginIfNeeded(lesson, lang));
        }

        let handleUpdatePageField = (pageid, fieldName, fieldVal, lang) => {
            console.debug('handleUpdatePageField', pageid, fieldName, fieldVal, lang);
            dispatch(updatePageField(pageid, fieldName, fieldVal, lang))
        }

        let handleImmediateUpdatePageField = (pageid, fieldName, fieldVal, lang) => {
            // console.debug(pageid, fieldName, fieldVal);
            dispatch(immediateUpdatePageField(pageid, fieldName, fieldVal, lang))
        }

        let handleMessageClose = (index) => {
            dispatch(closeMessage(index));
        }

        let handleRowSelect = (index, page) => {
            dispatch(setSelectedRow(index))
            if (page) {
                dispatch(fetchPageChunksIfNeeded(page.PageID, 1));
                dispatch(fetchPageChunksIfNeeded(page.PageID, 2));
                dispatch(fetchPageQAContent(page.PageID, 1));
                dispatch(fetchPageQAContent(page.PageID, 2));
                if (page.lesson_id) {
                    dispatch(doLoginIfNeeded(page.lesson_id, LangOptions.EN));
                    dispatch(doLoginIfNeeded(page.lesson_id, LangOptions.ES));
                }
            }
            
            dispatch(setEditMode(EditModes.PREVIEW));
        }

        let showPageDetails = (index, page) => {
            console.debug('showPageDetails', index, page);
            dispatch(setSelectedRow(index));
            if (page) {
                dispatch(fetchPageEverything(page.PageID)).then(() => {
                    dispatch(fetchPageChanges(page.PageID));
                    dispatch(setEditMode(EditModes.EDIT));
                })
            }
        }

        let handleChangeSelectedRow = (increment) => {
            let new_selected_index = selectedRow + increment;

            if (new_selected_index < 0) new_selected_index = filtered_pages.length-1;
            if (new_selected_index >= filtered_pages.length) new_selected_index = 0;

            handleRowSelect(new_selected_index, filtered_pages[new_selected_index]);
            dispatch(setHighlightedRow(new_selected_index));
        }

        let handleChangeDetailRow = (increment) => {
            let new_selected_index = selectedRow + increment;

            if (new_selected_index < 0) new_selected_index = filtered_pages.length-1;
            if (new_selected_index >= filtered_pages.length) new_selected_index = 0;

            showPageDetails(new_selected_index, filtered_pages[new_selected_index]);
            dispatch(setHighlightedRow(new_selected_index));
        }

        let pages;
        // console.debug(topic);
        // console.debug(topic_pages);
        // console.debug(lesson);
        // console.debug(lesson_pages);
        // console.debug(course);
        // console.debug(course_pages);
        // console.debug(unit);
        // console.debug(unit_pages);
        // console.debug(this.props);
        // console.debug(pageid);
        // console.debug(page);
        // console.debug(all_pages[lang]);

        // console.debug(this.props.displayFields);

        // if (pageid && all_pages[lang] && all_pages[lang][pageid]) {
        //     // console.debug(all_pages[lang][pageid]);
        //     pages = [all_pages[lang][pageid]];
        // } else if (topic && topic !== -1 && topic_pages) {
        //     pages = topic_pages;
        // } else if (lesson && lesson !== -1 && lesson_pages) {
        //     pages = lesson_pages;
        // } else if (course && course !== -1 && course_pages) {
        //     pages = course_pages;
        // } else if (unit && unit_pages) {
        //     pages = unit_pages;
        // } else {
        //     pages = [];
        // }

        // console.log(pages);

        let preview_container_visible = edit_mode === EditModes.EDIT;

        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col xs={12}>
                            <PanelContainer plain>
                                <Panel style={{background: 'white', minHeight: '500px'}}>
                                    <PanelHeader className='bg-blue fg-white'>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <p>
                                                        <ContentTree 
                                                            unit={unit}
                                                            course={course}
                                                            lesson={lesson}
                                                            topic={topic}
                                                            units={units}
                                                            courses={courses}
                                                            lessons={lessons}
                                                            topics={topics}
                                                            content={content}
                                                            onUpdateUnit={unit =>
                                                                handleUnitClick(unit)
                                                            }
                                                            onUpdateCourse={course =>
                                                                handleCourseClick(course)
                                                            }
                                                            onUpdateLesson={lesson =>
                                                                handleLessonClick(lesson)
                                                            }
                                                            onUpdateTopic={topic =>
                                                                handleTopicClick(topic)
                                                            }
                                                        />
                                                    </p>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelHeader>
                                    <PanelBody>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <Filters
                                                        page={page}
                                                        pattern={pattern}
                                                        displayFields={this.props.displayFields}
                                                        filterStati={this.props.filter_stati}
                                                        onTextFilterFetch={()=>
                                                            handleTextFilterFetch()
                                                        }
                                                        onUpdateTextFilter={(field_name, val) =>
                                                            handleTextFilterUpdate(field_name, val)
                                                        }
                                                        onUpdateDisplayField={(field_name, isOn) =>
                                                            dispatch(updateLocalDisplayField(field_name, isOn))
                                                        }
                                                        onFilterStatusChange={(stati_val) => {
                                                            handleFilterStatusUpdate(stati_val);
                                                        }}
                                                    />
                                                    <PageList
                                                        pages={filtered_pages}
                                                        displayFields={this.props.displayFields}
                                                        displayFieldsOn={this.props.displayFieldsOn}
                                                        sortField={sort_field}
                                                        sortDir={sort_dir}
                                                        enSession={enSession}
                                                        esSession={esSession}
                                                        lesson={lesson}
                                                        courses={courses}
                                                        lessons={lessons}
                                                        courseTag={course_tag}
                                                        lessonTag={lesson_tag}
                                                        refreshSession={refreshSession}
                                                        highlightedRow={highlighted_row}
                                                        pagesFetching={pages_fetching}
                                                        setSortField={(field_name, sort_dir) => 
                                                            dispatch(setSortField(field_name, sort_dir))
                                                        }
                                                        onRowSelect={(index, page, e) => {
                                                            console.debug('onRowSelect', index, page, e);
                                                            e.preventDefault();
                                                            handleRowSelect(index, page);
                                                        }}
                                                        showPageDetails={(index, page, e) => {
                                                            e.preventDefault();
                                                            showPageDetails(index, page);
                                                        }}
                                                        setHighlightedRow={(index) => {
                                                            dispatch(setHighlightedRow(index));
                                                        }}
                                                    />
                                                    <PageDetailView
                                                        editMode={edit_mode}
                                                        detailPage={detailPage}
                                                        detailPages={detailPages}
                                                        constants={constants}
                                                        enSession={enSession}
                                                        esSession={esSession}
                                                        lesson={lesson}
                                                        courseTag={course_tag}
                                                        lessonTag={lesson_tag}
                                                        pasteboard={pasteboard}
                                                        pasteboardCut={pasteboard_cut}
                                                        setEditMode={(mode) =>
                                                            dispatch(setEditMode(mode))
                                                        }
                                                        setChunkVal={(pageid, lang_id, chunkvar, chunkval, row_id) =>
                                                            dispatch(setChunkVal(pageid, lang_id, chunkvar, chunkval, constants.user, row_id))
                                                        }
                                                        onUpdatePageField={(pageid, fieldName, fieldVal, lang) => {
                                                            handleUpdatePageField(pageid, fieldName, fieldVal, lang);
                                                            dispatch(fetchPageQAContent(pageid, lang));
                                                        }}
                                                        changeSelectedRow={(increment) => {
                                                            handleChangeSelectedRow(increment);
                                                        }}
                                                        setChunkMessage={(message, message_type, pageid, chunkvar, lang) => {
                                                            dispatch(setChunkMessage(message, message_type, pageid, chunkvar, lang))
                                                        }}
                                                        removeChunkMessage={(pageid, chunkvar, lang) => {
                                                            dispatch(removeChunkMessage(pageid, chunkvar, lang))
                                                        }}
                                                        setPasteboard={(chunk, cut) => {
                                                            dispatch(setPasteboard(chunk, cut))
                                                        }}
                                                        doPageChunkAction={(pageid, lang, src_chunk, dest_chunk, chunk_action) => {
                                                            dispatch(doPageChunkAction(pageid, lang, src_chunk, dest_chunk, constants.user, chunk_action))
                                                        }}
                                                        doFixPage={(pageid, optflags) => {
                                                            dispatch(doFixPage(pageid, optflags));
                                                        }}
                                                    />
                                                    <DetailContainer
                                                        visible={preview_container_visible}
                                                        closeDetailContainer={(e) => {
                                                            dispatch(setEditMode(EditModes.LIST));
                                                        }}>
                                                        <DetailContainerHeader
                                                            closeDetailContainer={(e)=> {
                                                                dispatch(setEditMode(EditModes.LIST));
                                                            }}
                                                            >
                                                            <PagePreviewHeader 
                                                                enSession={enSession}
                                                                esSession={esSession}
                                                                lesson={lesson}
                                                                courseTag={course_tag}
                                                                lessonTag={lesson_tag}
                                                                everythingPage={everything_page} />
                                                        </DetailContainerHeader>
                                                        <DetailContainerBody>
                                                            <PagePreview 
                                                                everythingPage={everything_page} 
                                                                filters={filters}
                                                                changeSelectedRow={(increment) => {
                                                                    handleChangeDetailRow(increment);
                                                                }}
                                                                updateEverythingField={(pageid, field_name, field_val) => {
                                                                    dispatch(updateEverythingField(pageid, field_name, field_val, constants.user));
                                                                }}
                                                                setMetaDisplayStati={(stati) => {
                                                                    dispatch(setMetaDisplayStati(stati));
                                                                }}
                                                                setFrameworkDisplayStati={(stati) => {
                                                                    dispatch(setFrameworkDisplayStati(stati));
                                                                }}
                                                            />
                                                            {/*<Col xs={12}>{container_body_children}</Col>*/}
                                                        </DetailContainerBody>
                                                    </DetailContainer>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelBody>
                                    <MessageCenter 
                                        onMessageClose={(index) => 
                                            handleMessageClose(index)
                                        }>
                                        {messages.map((message, ind) => {
                                            return <div type={message.type} index={ind} visible={message.visible} key={"message_" + ind}>{message.message}</div>
                                        })}
                                    </MessageCenter>
                                    <PasteboardViewer
                                        setPasteboard={(chunk) =>
                                            dispatch(setPasteboard(chunk))
                                        }>
                                        <div>{pasteboard}</div>
                                    </PasteboardViewer>
                                </Panel>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        )

        // return (

        //   <Container id='body'>
        //     <PageContent
        //         messages={messages}
        //         unit={unit}
        //         course={course}
        //         lesson={lesson}
        //         topic={topic}
        //         units={units}
        //         courses={courses}
        //         lessons={lessons}
        //         topics={topics}
        //         content={content}
        //         onUpdateUnit={unit =>
        //             handleUnitClick(unit)
        //         }
        //         onUpdateCourse={course =>
        //             handleCourseClick(course)
        //         }
        //         onUpdateLesson={lesson =>
        //             handleLessonClick(lesson)
        //         }
        //         onUpdateTopic={topic =>
        //             handleTopicClick(topic)
        //         } 


        //         displayFields={this.props.displayFields}
        //         displayFieldsOn={this.props.displayFieldsOn}
        //         page={page}
        //         pageid={pageid}
        //         pattern={pattern}
        //         patternFilter={patternFilter}
        //         constants={constants}
        //         onUpdateTextFilter={(field_name, val) => 
        //             handleTextFilterUpdate(field_name, val)
        //         }
        //         onTextFilterFetch={() => 
        //             handleTextFilterFetch()
        //         }
        //         onUpdateDisplayField={(field_name, isOn) => 
        //             dispatch(updateLocalDisplayField(field_name, isOn)) 
        //         } 
        //         setChunkVal={(pageid, lang_id, chunkvar, chunkval) =>
        //             dispatch(setChunkVal(pageid, lang_id, chunkvar, chunkval, constants.user))
        //         }


        //         pages={pages}
        //         detailFields={DetailFields}
        //         selectedRow={selectedRow}
        //         rowTop={rowTop}
        //         courseTag={course_tag}
        //         lessonTag={lesson_tag}
        //         enSession={enSession}
        //         esSession={esSession}
        //         refreshSession={refreshSession}
        //         pagesFetching={pages_fetching}
        //         editMode={edit_mode}
        //         editID={edit_id}
        //         detailPage={detailPage}
        //         detailPages={detailPages}
        //         sortField={sort_field}
        //         sortDir={sort_dir}
        //         onRowSelect={(index, page) => {
        //             console.debug('onRowSelect', index, page);
        //             dispatch(setSelectedRow(index))
        //             if (page) {
        //                 dispatch(fetchPageChunksIfNeeded(page.PageID, 1));
        //                 dispatch(fetchPageChunksIfNeeded(page.PageID, 2));
        //                 dispatch(fetchPageQAContent(page.PageID, 1));
        //                 dispatch(fetchPageQAContent(page.PageID, 2));
        //                 if (page.lesson_id) {
        //                     dispatch(doLoginIfNeeded(page.lesson_id, LangOptions.EN));
        //                     dispatch(doLoginIfNeeded(page.lesson_id, LangOptions.ES));
        //                 }
        //             }
                    
        //             dispatch(setEditMode(EditModes.PREVIEW));
        //         }}
        //         onRowUnselect={() => 
        //             dispatch(setSelectedRow(null, null))
        //         } 
        //         onAttachSelect={(attach_tag, top) => 
        //             dispatch(setSelectedAttachment(attach_tag, top)) 
        //         }
        //         onAttachUnselect={() => 
        //             dispatch(setSelectedAttachment(null, null))
        //         } 
        //         onPageLinkClick={(pageid, lang) => 
        //             handlePageLinkClick(pageid, lang)
        //         }
        //         setEditMode={(mode) =>
        //             dispatch(setEditMode(mode))
        //         }
        //         setEditID={(pageid) => {
        //             dispatch(setEditID(pageid))
        //             dispatch(fetchPageChunksIfNeeded(pageid, 1));
        //             dispatch(fetchPageChunksIfNeeded(pageid, 2));
        //             dispatch(fetchPageQAContent(page.PageID, 1));
        //             dispatch(fetchPageQAContent(page.PageID, 2));
        //         }}
        //         onImmediateUpdatePageField={(pageid, fieldName, fieldVal, lang) =>
        //             handleImmediateUpdatePageField(pageid, fieldName, fieldVal, lang)
        //         }
        //         onUpdatePageField={(pageid, fieldName, fieldVal, lang) => {
        //             handleUpdatePageField(pageid, fieldName, fieldVal, lang);
        //             dispatch(fetchPageQAContent(pageid, lang));
        //         }}
        //         setSortField={(field_name, sort_dir) => 
        //             dispatch(setSortField(field_name, sort_dir))
        //         }
        //         handleMessageClose={(index) => 
        //             handleMessageClose(index)
        //         }
        //         />


        //   </Container>


        // )
    }
}

App.state = {
    refreshTimer: null
}

export default connect(filteredContentSelector)(App);
