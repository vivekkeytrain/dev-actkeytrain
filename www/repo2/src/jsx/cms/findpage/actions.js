import fetchJsonp from 'fetch-jsonp';
import fetch from 'isomorphic-fetch';
import React from 'react';
import { map, invert, find } from 'lodash';
import { Link } from 'react-router';

import PageLinks from './components/PageLinks';

/*  Action types    */

export const SET_LANG = 'SET_LANG';
export const SET_UNIT = 'SET_UNIT';
export const SET_COURSE = 'SET_COURSE';
export const SET_LESSON = 'SET_LESSON';
export const SET_TOPIC = 'SET_TOPIC';
export const SET_MODE = 'SET_MODE';
export const SET_USER = 'SET_USER';
export const SET_PAGE = 'SET_PAGE';
export const SET_PAGEID = 'SET_PAGEID';
export const SET_PATTERN = 'SET_PATTERN';
export const SET_PATTERN_FILTER = 'SET_PATTERN_FILTER';
export const SET_UPDATED_DATE = 'SET_UPDATED_DATE';
export const SET_UPDATED_DATE_FILTER = 'SET_UPDATED_DATE_FILTER';
export const SET_SELECTED_ROW = 'SET_SELECTED_ROW';
export const SET_SELECTED_ATTACHMENT = 'SET_SELECTED_ATTACHMENT';
export const OPEN_PAGE = 'OPEN_PAGE';
export const SET_EDIT_MODE = 'SET_EDIT_MODE';
export const SET_EDIT_ID = 'SET_EDIT_ID';
export const SET_SORT_FIELD = 'SET_SORT_FIELD';
export const ADD_MESSAGE = 'ADD_MESSAGE';
export const CLOSE_MESSAGE = 'CLOSE_MESSAGE';
export const SET_HIGHLIGHTED_ROW = 'SET_HIGHLIGHTED_ROW';
export const SET_PASTEBOARD = 'SET_PASTEBOARD';

export const SET_SESSION = 'SET_SESSION';

export const UPDATE_DISPLAY_FIELD = 'UPDATE_DISPLAY_FIELD';
export const LOAD_DISPLAY_FIELDS = 'LOAD_DISPLAY_FIELDS';

export const SET_CHUNK_MESSAGE = 'SET_CHUNK_MESSAGE';
export const REMOVE_CHUNK_MESSAGE = 'REMOVE_CHUNK_MESSAGE';

export const SET_FILTER_STATUS = 'SET_FILTER_STATUS';

export const SET_FIELD_MESSAGE = 'SET_FIELD_MESSAGE';

export const SET_META_DISPLAY_STATI = 'SET_META_DISPLAY_STATI';
export const SET_FRAMEWORK_DISPLAY_STATI = 'SET_FRAMEWORK_DISPLAY_STATI';

/*  Async action types   */

export const FETCH_EDITOR_CONTENT_SUCCESS = 'FETCH_EDITOR_CONTENT_SUCCESS';
export const FETCH_EDITOR_CONTENT_FAILURE = 'FETCH_EDITOR_CONTENT_FAILURE';
export const FETCH_EDITOR_CONTENT_REQUEST = 'FETCH_EDITOR_CONTENT_REQUEST';

export const FETCH_PAGES_REQUEST = 'FETCH_PAGES_REQUEST';
export const FETCH_PAGES_FAILURE = 'FETCH_PAGES_FAILURE';
export const FETCH_PAGES_SUCCESS = 'FETCH_PAGES_SUCCESS';

export const FETCH_PAGE_EVERYTHING_REQUEST = 'FETCH_PAGE_EVERYTHING_REQUEST';
export const FETCH_PAGE_EVERYTHING_FAILURE = 'FETCH_PAGE_EVERYTHING_FAILURE';
export const FETCH_PAGE_EVERYTHING_SUCCESS = 'FETCH_PAGE_EVERYTHING_SUCCESS';

export const FETCH_PAGE_CHANGES_REQUEST = 'FETCH_PAGE_CHANGES_REQUEST';
export const FETCH_PAGE_CHANGES_FAILURE = 'FETCH_PAGE_CHANGES_FAILURE';
export const FETCH_PAGE_CHANGES_SUCCESS = 'FETCH_PAGE_CHANGES_SUCCESS';

export const FETCH_PAGE_CHUNKS_REQUEST = 'FETCH_PAGE_CHUNKS_REQUEST';
export const FETCH_PAGE_CHUNKS_FAILURE = 'FETCH_PAGE_CHUNKS_FAILURE';
export const FETCH_PAGE_CHUNKS_SUCCESS = 'FETCH_PAGE_CHUNKS_SUCCESS';

export const UPDATE_FIELD_REQUEST = 'UPDATE_FIELD_REQUEST';
export const UPDATE_FIELD_FAILURE = 'UPDATE_FIELD_FAILURE';
export const UPDATE_FIELD_SUCCESS = 'UPDATE_FIELD_SUCCESS';

export const DO_LOGIN_LESSON_REQUEST = 'DO_LOGIN_LESSON_REQUEST';
export const DO_LOGIN_LESSON_SUCCESS = 'DO_LOGIN_LESSON_SUCCESS';
export const DO_LOGIN_LESSON_FAILURE = 'DO_LOGIN_LESSON_FAILURE';

export const SET_CHUNK_VAL_REQUEST = 'SET_CHUNK_VAL_REQUEST';
export const SET_CHUNK_VAL_SUCCESS = 'SET_CHUNK_VAL_SUCCESS';
export const SET_CHUNK_VAL_FAILURE = 'SET_CHUNK_VAL_FAILURE';

export const DO_PAGE_CHUNK_ACTION_REQUEST = 'DO_PAGE_CHUNK_ACTION_REQUEST';
export const DO_PAGE_CHUNK_ACTION_SUCCESS = 'DO_PAGE_CHUNK_ACTION_SUCCESS';
export const DO_PAGE_CHUNK_ACTION_FAILURE = 'DO_PAGE_CHUNK_ACTION_FAILURE';

export const FETCH_PAGE_QA_CONTENT_REQUEST = 'FETCH_PAGE_QA_CONTENT_REQUEST';
export const FETCH_PAGE_QA_CONTENT_SUCCESS = 'FETCH_PAGE_QA_CONTENT_SUCCESS';
export const FETCH_PAGE_QA_CONTENT_FAILURE = 'FETCH_PAGE_QA_CONTENT_FAILURE';

export const FETCH_APP_CONSTANTS_SUCCESS = 'FETCH_APP_CONSTANTS_SUCCESS';
export const FETCH_APP_CONSTANTS_FAILURE = 'FETCH_APP_CONSTANTS_FAILURE';
export const FETCH_APP_CONSTANTS_REQUEST = 'FETCH_APP_CONSTANTS_REQUEST';

export const FIX_PAGE_REQUEST = 'FIX_PAGE_REQUEST';
export const FIX_PAGE_SUCCESS = 'FIX_PAGE_SUCCESS';
export const FIX_PAGE_FAILURE = 'FIX_PAGE_FAILURE';

export const UPDATE_EVERYTHING_FIELD_REQUEST = 'UPDATE_EVERYTHING_FIELD_REQUEST';
export const UPDATE_EVERYTHING_FIELD_SUCCESS = 'UPDATE_EVERYTHING_FIELD_SUCCESS';
export const UPDATE_EVERYTHING_FIELD_FAILURE = 'UPDATE_EVERYTHING_FIELD_FAILURE';

// export const SET_COLUMN_WIDTH = 'SET_COLUMN_WIDTH';

/*  Other contstants    */

export const LangOptions = {
    EN: 1,
    ES: 2
};

export const ModeFilters = {
    QA: 'QA',
    REVIEW: 'REVIEW',
    NO_GO: 'NO-GO'
};

export const EditModes = {
    LIST: 'LIST',
    PREVIEW: 'PREVIEW',
    EDIT: 'EDIT'
}

function displayAttach(val, rowData) {
    // console.debug(this);
    // "this" is the PageList component
    let query = {
        unit_id: rowData.unit_id,
        course_id: rowData.course_id,
        lesson_id: rowData.lesson_id,
        topic_id: rowData.topic_id,
        attach_id: rowData.attach_id
    }
    return <Link to="/attachments/" query={query} >{val}</Link>


    // return <a href="#" onClick={this.onAttachClick.bind(this, val, row)}>{val}</a>
}

export const DisplayFields = [
    {
        name: 'PageID',
        label: 'Page Link',
        isOn: true,
        sortable: false,
        displayFunc: (val, props) => {
            return <PageLinks {...props} />
        }
    },
    {
        name: 'PageID',
        isOn: true
    },
    {
        name: 'pageno',
        label: '#',
        isOn: true
    },
    {
        name: 'course_id',
        label: 'Course',
        isOn: false,
        displayFunc: (val, props) => {
            let course = find(props.courses, {'id': val});
            return course ? course.display : '';
        }
    },
    {
        name: 'lesson_id',
        label: 'Lesson',
        isOn: false,
        displayFunc: (val, props) => {
            let lesson = find(props.courses, {'id': val});
            return lesson ? lesson.display : '';
        }
    },
    {
        name: 'ed_chapter_Title',
        label: 'Topic',
        isOn: false
    },
/*    {
        name: 'qa_status_val',
        label: 'Status',
        isOn: false
    },*/
    {
        name: 'qa_statdesc',
        label: 'Status Desc',
        isOn: true
    },
    {
        name: 'Title',
        isOn: true
    },
    {
        name: 'Sound',
        isOn: false
    },
    {
        name: 'attach_tag',
        label: 'Attachment',
        isOn: false,
        displayFunc: displayAttach/*(val) => {
            console.debug(this);
            return <a href="#">{val}</a>
        }*/
    },
    {
        name: 'LastUpdated',
        label: 'Last Updated',
        isOn: true,
        displayFunc: (val) => {
            let d = new Date(val);
            let m = ((d.getMonth() < 9) ? '0' : '') + (d.getMonth()+1);
            let dy = ((d.getDate() < 10) ? '0' : '') + d.getDate();
            return d.getFullYear() + String.fromCharCode(8209) + m + String.fromCharCode(8209) + dy; // 8209 is non-breaking dash
        }
    },
    {
        name: 'PageID',
        label: 'Chunks',
        isOn: true,
        sortable: false,
        displayFunc: (val, props, index) => {
            // console.debug(index, props.pages);
            // console.debug(props.pages[index])
            // console.debug(props);
            // this.props.onRowSelect(row_index, row_data, index);
            // return 'Chunk'
            //  
            return <a href='#' onClick={props.onRowSelect.bind(this, index, props.pages[index])}>Chunks</a>
        }
    },
    {
        name: 'PageID',
        label: 'Page Details',
        isOn: true,
        sortable: false,
        displayFunc: (val, props, index) => {
            return <a href='#' onClick={props.showPageDetails.bind(this, index, props.pages[index])}>Page Details</a>
        }
    }
];

/*
    This array controls the PageDetail display

    Fields will be displayed in order of this array. 
    
    Fields not represented in this array will be dumped raw at end of PageDetail. This allows
    for the adding of fields via the SP without changing code but means that in order to suppress
    a field it must be added to the array with display: false.

    {
        name: <field name from db>
        label: <display label. will coalesce to name if undefined>
        display: <true|false>
        displayFunc: <format func for val>
    }

*/

export const DetailFields = [
    {
        name: 'PageID',
        label: 'Page ID',
        display: true
    },
    {
        name: 'Pageno',
        label: 'Page Number',
        display: true
    },
    {
        name: 'Title',
        label: 'Page Title',
        display: true
    },
    {
        name: 'ed_ProjectID',
        label: 'Project ID',
        display: true
    },
    {
        name: 'ed_project_Title',
        label: 'Project Title',
        display: true
    },
    {
        name: 'ed_Courseid',
        label: 'Course ID',
        display: true
    },
    {
        name: 'ed_CourseLevel',
        label: 'Course Level',
        display: true
    },
    {
        name: 'ed_course_Title',
        label: 'Course Title',
        display: true
    },
    {
        name: 'ed_CourseNumber',
        label: 'Course Number',
        display: true
    },
    {
        name: 'topic_id',
        label: 'Topic ID',
        display: true
    },
    {
        name: 'ed_ChapterNumber',
        label: 'Chapter Number',
        display: true
    },
    {
        name: 'ed_ChapterCode',
        label: 'Chapter Code',
        display: true
    },
    {
        name: 'ed_chapter_Description',
        label: 'Chapter Description',
        display: true
    },
    {
        name: 'ed_chapter_Title',
        label: 'Chapter Title',
        display: true
    },
    {
        name: 'topic_slug',
        label: 'Topic Slug',
        display: true
    },
    {
        name: 'number_topic',
        label: 'Number Topic',
        display: true
    },
    {
        name: 'ChapterID',
        label: 'Chapter ID',
        display: true
    },
    {
        name: 'Description',
        display: true
    },
    {
        name: 'Description2',
        display: true
    },
    {
        name: 'Sound',
        display: true,
        displayFunc: (val) => ((val) ? <i className="glyphicon glyphicon-ok"></i> : <i className="glyphicon glyphicon-minus"></i>)
    },
    {
        name: 'LastUpdated',
        label: 'Last Updated',
        display: true,
        displayFunc: (val) => {
            let d = new Date(val);
            let m = ((d.getMonth() < 10) ? '0' : '') + (d.getMonth()+1);
            let dy = ((d.getDate() < 10) ? '0' : '') + d.getDate();
            return d.getFullYear() + '-' + m + '-' + dy;
        }
    },
    {
        name: 'PageNumber',
        display: false
    },
    {
        name: 'ChapterTypeID',
        display: false
    },
    {
        name: 'PageTypeID',
        display: false
    },
    {
        name: 'unit_id',
        label: 'Unit ID',
        display: true
    },
    {
        name: 'lesson_id',
        label: 'Lesson ID',
        display: true
    },
    {
        name: 'course_id',
        label: 'Course ID',
        display: true
    },
    {
        name: 'ed_ChapterID',
        label: 'ED Chapter ID',
        display: true
    },
    {
        name: 'attach_count',
        label: 'Attachemnt Count',
        display: true
    },
    {
        name: 'attach_id',
        label: 'Attachment ID',
        display: true
    },
    {
        name: 'attach_tag',
        label: 'Attachment Tag',
        display: true
    },
    {
        name: 'attach_name',
        label: 'Attachment Name',
        display: true
    }
]

/*  Action creators */

export function setLang(lang) { 
    return { type: SET_LANG, lang };
}

export function setUnit(unit) {
    return function(dispatch) {
        localStorage.setItem('content_unit', unit);
        dispatch(setUnitStore(unit));
    }
}

export function setUnitStore(unit) {
    return { type: SET_UNIT, unit };
}

export function setCourse(course) {
    return function(dispatch) {
        localStorage.setItem('content_course', course);
        dispatch(setCourseStore(course));
    }
}

export function setCourseStore(course) {
    return { type: SET_COURSE, course };
}

export function setLesson(lesson) {
    return function(dispatch) {
        localStorage.setItem('content_lesson', lesson);
        dispatch(setLessonStore(lesson));
    }
}

export function setLessonStore(lesson) {
    return { type: SET_LESSON, lesson };
}

export function setTopic(topic) {
    return function(dispatch) {
        localStorage.setItem('content_topic', topic);
        dispatch(setTopicStore(topic));
    }
}

export function setTopicStore(topic) {
    return { type: SET_TOPIC, topic };
}

export function setMode(mode) {
    return { type: SET_MODE, mode };
}

export function setUser(user) {
    return { type: SET_USER, user };
}

export function setPage(page) {
    return { type: SET_PAGE, page };
}

export function setPageID(pageid) {
    return { type: SET_PAGEID, pageid };
}

export function setPattern(pattern) {
    return { type: SET_PATTERN, pattern };
}

export function setPatternFilter(pattern) {
    return { type: SET_PATTERN_FILTER, pattern };
}

export function setUpdatedDate(updated_date) {
    return { type: SET_UPDATED_DATE, updated_date };
}

export function setUpdatedDateFilter(updated_date) {
    return { type: SET_UPDATED_DATE_FILTER, updated_date };
}

export function setSelectedRow(index) {
    return { type: SET_SELECTED_ROW, index };
}

export function setSelectedAttachment(attach_tag, top) {
    return { type: SET_SELECTED_ATTACHMENT, attach_tag, top };
}

export function addMessage(message_type, message, position='top-center') {
    return {
        type: ADD_MESSAGE,
        message_type,
        message,
        position
    }
}

export function closeMessage(index) {
    return {
        type: CLOSE_MESSAGE,
        index
    }
}

export function setPasteboard(chunk, cut=false) {
    return {
        type: SET_PASTEBOARD,
        chunk,
        cut
    }
}

export function openPage(pageid, lang) {
    return {
        type: OPEN_PAGE,
        pageid,
        lang
    }
}

export function setEditMode(mode) {
    return {
        type: SET_EDIT_MODE,
        mode
    }
}

export function setFilterStatus(stati) {
    return {
        type: SET_FILTER_STATUS,
        stati
    }
}

// export function setSelectedRow(index) {
//     return {
//         type: SET_SELECTED_ROW
//     }
// }

export function setEditID(pageid) {
    return {
        type: SET_EDIT_ID,
        pageid
    }
}

export function setSortField(field_name, sort_dir) {
    return {
        type: SET_SORT_FIELD,
        field_name,
        sort_dir
    }
}

export function setSession(session_id, login_session_uid) {
    return {
        type: SET_SESSION,
        session_id,
        login_session_uid
    }
}

export function setHighlightedRow(index) {
    return {
        type: SET_HIGHLIGHTED_ROW,
        index
    }
}

export function updateDisplayField(field_name, isOn) {
    return {
        type: UPDATE_DISPLAY_FIELD,
        field_name,
        isOn
    }
}

export function updateLocalDisplayField(field_name, isOn) {
    // console.debug('updateLocalDisplayField', field_name, isOn);
    return function(dispatch) {
        localStorage.setItem('display_field_' + field_name, isOn ? 'true' : 'false');
        // console.debug(field_name, localStorage.getItem('display_field_' + field_name));
        dispatch(updateDisplayField(field_name, isOn));
    }
}

export function loadDisplayFields() {
    // console.debug(DisplayFields);
    
    let localDisplayFields = [];
    DisplayFields.forEach((field, ind) => {
        // console.debug(field.name, localStorage.getItem('display_field_' + field.name));
        let isOn = localStorage.getItem('display_field_' + field.name) ? localStorage.getItem('display_field_' + field.name) === 'true' : field.isOn;
        localDisplayFields[ind] = Object.assign({}, field, {
            isOn: isOn
        });
    });
    // console.debug(localDisplayFields);


    return function(dispatch) {
        map(localDisplayFields, (field, index) => {
            dispatch(updateLocalDisplayField(field.name, field.isOn))
        });
        // return {
        //     type: LOAD_DISPLAY_FIELDS
        // }
    }
}

// export function setColumnWidth(field_name, width) {
//     return { type: SET_COLUMN_WIDTH, field_name, width };
// }

// export function updateColumnWidth(field_name, width) {
//     return function(dispatch) {
//         localStorage.setItem('column_widths_' + field_name, width);
//         dispatch(setColumnWidth(field_name, width));
//     }
// }

// export function loadWidths() {
//     // console.debug('loadWidths');
//     // console.debug(DisplayFields);
//     return function(dispatch) {
//         DisplayFields.forEach((field) => {
//             let ls_name = 'column_widths_' + field.name;
//             localStorage.removeItem(ls_name);
//             dispatch(updateColumnWidth(field.name, localStorage.getItem(ls_name) ? parseInt(localStorage.getItem(ls_name), 10) : field.width));
//         });
//     }
// }

export function setFieldMessage(pageid, field_name, message, message_type) {
    return {
        type: SET_FIELD_MESSAGE,
        pageid,
        field_name,
        message,
        message_type
    }
}

export function setMetaDisplayStati(stati) {
    return {
        type: SET_META_DISPLAY_STATI,
        stati
    }
}

export function setFrameworkDisplayStati(stati) {
    return {
        type: SET_FRAMEWORK_DISPLAY_STATI,
        stati
    }
}

/*  Async actions   */

export function fetchEditorContentSuccess(content) {
    return {
        type: FETCH_EDITOR_CONTENT_SUCCESS,
        content
    }
}

export function fetchEditorContentFailure(ex) {
    return {
        type: FETCH_EDITOR_CONTENT_FAILURE,
        ex
    }
}

export function fetchEditorContentRequest() {
    return { type: FETCH_EDITOR_CONTENT_REQUEST };
}

export function fetchEditorContent() {
    return function(dispatch) {
        dispatch(fetchEditorContentRequest());

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_content_list/jsonp', {
                timeout: 20000
            })
            .then(
                response => response.json(),
                ex => dispatch(fetchEditorContentFailure(ex))
            )
            .then(json => dispatch(fetchEditorContentSuccess(json.ResultSets[0])))
            .catch(ex => dispatch(fetchEditorContentFailure(ex)));
    }
}

function fetchPageEverythingSuccess(pageid, page_data) {
    return {
        type: FETCH_PAGE_EVERYTHING_SUCCESS,
        pageid,
        page_data
    }
}

function fetchPageEverythingRequest(pageid) {
    return {
        type: FETCH_PAGE_EVERYTHING_REQUEST,
        pageid
    }
}

function fetchPageEverythingFailure(pageid, ex) {
    return {
        type: FETCH_PAGE_EVERYTHING_FAILURE,
        pageid,
        ex
    }
}

export function fetchPageEverything(pageid) {
    return function(dispatch) {
        dispatch(fetchPageEverythingRequest(pageid));
        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_page_everything?pageid=' + pageid)
            .then(
                response => response.json(),
                ex => dispatch(fetchPageEverythingFailure(pageid, ex))
            )
            .then(json => dispatch(fetchPageEverythingSuccess(pageid, json.ResultSets[0][0])))
            .catch(ex => dispatch(fetchPageEverythingFailure(pageid, ex)));
    }
}

function fetchPageChangesSuccess(pageid, changes) {
    return {
        type: FETCH_PAGE_CHANGES_SUCCESS,
        pageid,
        changes
    }
}

function fetchPageChangesRequest(pageid) {
    return {
        type: FETCH_PAGE_CHANGES_REQUEST,
        pageid
    }
}

function fetchPageChangesFailure(pageid, ex) {
    return {
        type: FETCH_PAGE_CHANGES_FAILURE,
        pageid,
        ex
    }
}

export function fetchPageChanges(pageid) {
    return function(dispatch) {
        dispatch(fetchPageChangesRequest(pageid));
        return fetchJsonp(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_page_log?pageid=${pageid}`)
            .then(
                response => response.json(),
                ex => dispatch(fetchPageChangesFailure(pageid, ex))
            )
            .then(json => { console.debug(json); dispatch(fetchPageChangesSuccess(pageid, json.ResultSets[0]))})
            .catch(ex => dispatch(fetchPageChangesFailure(pageid, ex)));
    }
}

function fetchPageChunksSuccess(pageid, lang, chunks) {
    return {
        type: FETCH_PAGE_CHUNKS_SUCCESS,
        pageid,
        lang,
        chunks
    }
}

function fetchPageChunksRequest(pageid, lang) {
    return {
        type: FETCH_PAGE_CHUNKS_REQUEST,
        pageid, 
        lang
    }
}

function fetchPageChunksFailure(pageid, lang, ex) {
    return {
        type: FETCH_PAGE_CHUNKS_FAILURE,
        pageid,
        lang,
        ex
    }
}

function fetchPageChunks(pageid, lang) {
    return function(dispatch) {
        dispatch(fetchPageChunksRequest(pageid, lang));
        console.debug(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_page_chunks?pageid=${pageid}&lang_id=${lang}`);
        return fetchJsonp(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_page_chunks?pageid=${pageid}&lang_id=${lang}`)
            .then(
                response => response.json(),
                ex => dispatch(fetchPageChunksFailure(pageid, lang, ex))
            )
            .then(json => {
                console.debug(json);
                dispatch(fetchPageChunksSuccess(pageid, lang, json.ResultSets[0]));
            })
            .catch(ex => dispatch(fetchPageChunksFailure(pageid, lang, ex)));
    }
}

function shouldFetchPageChunks(state, pageid, lang) {
    if (!(pageid && lang)) return false;

    const pages = state.pages;
    if (!(pages[lang] && pages[lang][pageid] && pages[lang][pageid].chunks)) return true;

    return false;
}

export function fetchPageChunksIfNeeded(pageid, lang) {
    console.debug('fetchPageChunksIfNeeded', pageid, lang);
    return (dispatch, getState) => {
        if (shouldFetchPageChunks(getState(), pageid, lang)) {
            return dispatch(fetchPageChunks(pageid, lang));
        } else {
            return Promise.resolve();
        }
    }
}

function fetchPagesSuccess(tag, lang, pattern, updated_date, pages) {
    return {
        type: FETCH_PAGES_SUCCESS,
        tag,
        lang,
        pattern,
        updated_date,
        pages,
        receivedAt: Date.now()
    }
}

function fetchPagesRequest(tag, lang, pattern, updated_date) {
    return {
        type: FETCH_PAGES_REQUEST,
        tag,
        lang,
        pattern,
        updated_date
    }
}

function fetchPagesFailure(ex) {
    return {
        type: FETCH_PAGES_FAILURE,
        ex
    }
}

function fetchPages(tag, lang, pattern='', updated_date='', stati=0) {
    console.debug('fetchPages', tag, lang, pattern, updated_date);
    return function(dispatch) {
        dispatch(fetchPagesRequest(`${tag}_${stati}`, lang, pattern, updated_date));
        console.debug(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_page_data?conunit_tag=${tag}&pattern=${pattern}&qa_status_val=${stati}&updated_after=${updated_date}`);
        return fetchJsonp(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_page_data?conunit_tag=${tag}&pattern=${pattern}&qa_status_val=${stati}&updated_after=${updated_date}`, {
            timeout: 20000
        })
            .then(
                response => response.json(),
                ex => dispatch(fetchPagesFailure(ex))
            )
            .then(json => dispatch(fetchPagesSuccess(`${tag}_${stati}`, lang, pattern, updated_date, json.ResultSets[0])))
            .catch(ex => dispatch(fetchPagesFailure(ex)));
    }
}

function shouldFetchPages(state, content_tag, lang, pattern, updated_date, stati) {
    console.debug('shouldFetchPages', content_tag, lang, pattern, updated_date, stati);
    if (!(content_tag && lang) && !(content_tag && pattern && lang) && !(content_tag && updated_date && lang)) return false;
    
    const pages = state.pages_by_tag;
    if (!pages[lang]) return true;

    const lang_pages = pages[lang];
    console.debug(lang_pages);

    let tag = `${content_tag}_${stati}`
    console.debug('tag', tag);
    if (content_tag && !lang_pages[tag]) return true;
    if (pattern && !lang_pages['PATTERN_' + pattern]) return true;
    if (updated_date && !lang_pages[`UPDATED_DATE_${updated_date}`]) return true;

    // const tag_pages = (tag) ? lang_pages[tag] : lang_pages['PATTERN_' + pattern];
    let tag_pages;
    if (tag)
        tag_pages = lang_pages[tag];
    else if (updated_date)
        tag_pages = lang_pages['UPDATED_DATE_${updated_date}'];
    else if (pattern)
        tag_pages = lang_pages[`PATTERN_${pattern}`];

    if (tag_pages.isFetching) {
        return true;
    } else {
        return (Date.now() - lang_pages[tag].lastUpdated) > (1000 * 60 * 10); //   10 minutes old, refetch
    }

    // if (topicid) {
    //     if (!pages.topics || !pages.topics[topicid]) return true;

    //     return (Date.now() - pages.topics[topicid].lastUpdated) > (1000 * 60 * 10); //   10 minutes old, refetch
    // } else if (pageid) {
    //     console.debug(pageid);
    //     if (!pages.pages || !pages.pages[pageid]) return true;

    //     return (Date.now() - pages.pages[pageid].lastUpdated) > (1000 * 60 * 10); //   10 minutes old, refetch
    // } else if (pattern) {
    //     if (!pages.patterns || !pages.patterns[pattern]) return true;

    //     return (Date.now() - pages.patterns[pattern].lastUpdated) > (1000 * 60 * 10); //   10 minutes old, refetch
    // }
}

export function fetchPagesIfNeeded(unit, course, lesson, topic, lang, pageid, pattern, updated_date, stati=0) {
    console.debug('fetchPagesIfNeeded', unit, course, lesson, topic, lang, pageid, pattern, updated_date, stati);
    return (dispatch, getState) => {
        let tag;

        if (pageid) 
            tag = 'P' + pageid;
        else if (topic && topic != -1)
            tag = 'T' + topic;
        else if (lesson && topic == -1)
            tag = 'L' + lesson;
        else if (course && lesson == -1)
            tag = 'C' + course;
        else if (unit && course == -1)
            tag = 'U' + unit;

        console.debug(tag);
        if (shouldFetchPages(getState(), tag, lang, pattern, updated_date, stati)) {
            console.debug('fetching');
            return dispatch(fetchPages(tag, lang, pattern, updated_date, stati));
        } else {
            console.debug('not fetching');
            return Promise.resolve();
        }
    }
}

function doLoginLessonSuccess(session_id, login_session_uid, lesson_id, lang) {
    // console.debug('doLoginLessonSuccess', session_id, login_session_uid, lesson_id, lang);
    // console.debug('login_session_uid: ' + login_session_uid);
    // lang = lang.toUpperCase();
    return { 
        type: DO_LOGIN_LESSON_SUCCESS,
        session_id,
        login_session_uid,
        lesson_id,
        lang,
        receivedAt: Date.now()
    };
}

function doLoginLessonFailure(ex) {
    return {
        type: DO_LOGIN_LESSON_FAILURE,
        ex
    }
}

function doLoginLessonRequest() {
    return { type: DO_LOGIN_LESSON_REQUEST };
}


function shouldDoLoginLesson(state, lesson_id, lang) {
    // console.debug('shouldDoLoginLesson: ' + lesson_id + ', ' + lang);
    if (!lesson_id || lesson_id == -1) return false;
    const sessions = state.filters.sessions;
    // console.debug(sessions);
    if (!sessions[lang]) return true;
    const session = sessions[lang][lesson_id];
    // console.debug(session);

    if (!session) {
        return true;
    } else if (session.isFetching) {
        return false
    } else {
        return (Date.now() - session.lastUpdated) > (1000 * 60 * 15); //   15 minutes old, refetch
    }
}


function doLoginLesson(lesson_id, lang) {
    return function(dispatch) {
        dispatch(doLoginLessonRequest());

        let lang_code = invert(LangOptions)[lang];

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_init_review_session?lesson_id=' + lesson_id + '&lang_code=' + lang_code, { timeout: 20000 })
            .then(
                response => response.json(),
                ex => dispatch(doLoginLessonFailure(ex))
            )
            .then(json => dispatch(doLoginLessonSuccess(json.ResultSets[0][0].session_id, json.ResultSets[0][0].luid_str, lesson_id, lang)))
            .catch(ex => dispatch(doLoginLessonFailure(ex)));
    }

}

export function doLoginIfNeeded(lesson_id, lang) {
    // console.debug('doLoginIfNeeded', lesson_id, lang);
    return (dispatch, getState) => {
        if (shouldDoLoginLesson(getState(), lesson_id, lang)) {
            // console.debug('doLogin');
            return dispatch(doLoginLesson(lesson_id, lang));
        } else {
            return Promise.resolve();
        }
    }
}

function updateEverythingFieldRequest(pageid, field_name, field_val, updated_by) {
    return {
        type: UPDATE_EVERYTHING_FIELD_REQUEST,
        pageid,
        field_name,
        field_val,
        updated_by
    }
}

function updateEverythingFieldSuccess(pageid, field_name, field_val, updated_by) {
    return {
        type: UPDATE_EVERYTHING_FIELD_SUCCESS,
        pageid,
        field_name,
        field_val,
        updated_by
    }
}

function updateEverythingFieldFailure(pageid, field_name, ex) {
    return {
        type: UPDATE_EVERYTHING_FIELD_FAILURE,
        pageid, 
        field_name,
        ex
    }
}

export function updateEverythingField(pageid, field_name, field_val, updated_by) {
    console.debug('updateEverythingField', pageid, field_name, field_val, updated_by);

    return function(dispatch) {
        dispatch(updateEverythingFieldRequest(pageid, field_name, field_val, updated_by));
        dispatch(setFieldMessage(pageid, field_name, 'Saving', 'info'));

        let url = `http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_page_setval?pageid=${pageid}&varname=${field_name}&varval=${field_val}&update_by=${updated_by}&update_ui=${window.location.href}`;
        console.debug(url);

        return fetch('/api/edapi/dbo.api_sp_page_setval', {
            method: 'post',
            body: JSON.stringify({
                pageid,
                varname: field_name,
                varval: field_val,
                update_by: updated_by,
                update_ui: window.location.href
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': window.auth
            }
        })
            .then(
                response => response.json(),
                ex => {
                    dispatch(updateEverythingFieldFailure(pageid, field_name, ex));
                    dispatch(setFieldMessage(pageid, field_name, 'Update Failed', 'error'));
                }
            )
            .then(json => {
                console.debug(json);
                let status = json.ResultSets[0][0].status;
                // console.debug('status: ' + status);
                if (status < 100) {
                    dispatch(updateEverythingFieldSuccess(pageid, field_name, field_val));
                    dispatch(setFieldMessage(pageid, field_name, 'Saved', 'success'));
                    dispatch(fetchPageEverything(pageid));
                } else {
                    // console.debug('failed');
                    dispatch(updatePageFieldFailure(pageid, fieldName, lang, json.ResultSets[0].statdesc));
                    dispatch(setFieldMessage(pageid, field_name, 'Update Failed', 'error'));
                }

                //  Remove the message after 5 seconds
                setTimeout(() => {
                    dispatch(setFieldMessage(pageid, field_name, null, null));
                }, 5000)
            })
            .catch(ex => {
                dispatch(updateEverythingFieldFailure(pageid, field_name, ex))
                dispatch(setFieldMessage(pageid, field_name, 'Update Failed', 'error'));
            })


        // return fetchJsonp(url)
        //     .then(
        //         response => response.json(),
        //         ex => {
        //             dispatch(updateEverythingFieldFailure(pageid, field_name, ex));
        //             dispatch(setFieldMessage(pageid, field_name, 'Failed', 'error'));
        //         }
        //     )
        //     .then(json => {
        //         dispatch(updateEverythingFieldSuccess(pageid, field_name, field_val));
        //         dispatch(setFieldMessage(pageid, field_name, 'Saved', 'success'));

        //         //  Remove the message after 5 seconds
        //         setTimeout(() => {
        //             dispatch(setFieldMessage(pageid, field_name, null, null));
        //         }, 5000)
        //     })
        //     .catch(ex => {
        //         dispatch(updateEverythingFieldFailure(pageid, field_name, ex))
        //         dispatch(setFieldMessage(pageid, field_name, 'Failed', 'error'));
        //     });
    }
}

function updatePageFieldSuccess(pageid, fieldName, fieldVal, lang) {
    return {
        type: UPDATE_FIELD_SUCCESS,
        pageid,
        fieldName,
        fieldVal,
        lang
    }
}

function updatePageFieldRequest(pageid, fieldName, fieldVal, lang) {
    // console.debug(arguments);
    return {
        type: UPDATE_FIELD_REQUEST,
        pageid,
        fieldName,
        fieldVal,
        lang
    }
}

function updatePageFieldFailure(pageid, fieldName, lang, ex) {
    return {
        type: UPDATE_FIELD_FAILURE,
        pageid,
        fieldName,
        lang,
        ex
    }
}

/*  Immediately update the field without having to post update which will wait for onBlur   */
export function immediateUpdatePageField(pageid, fieldName, fieldVal, lang) {
    return function(dispatch) {
        dispatch(updatePageFieldSuccess(pageid, fieldName, fieldVal, lang));
    }
}

export function updatePageField(pageid, fieldName, fieldVal, lang) {
    console.log('updatePageField', pageid, fieldName, fieldVal, lang);
    console.log(invert(LangOptions));
    return function(dispatch) {
        dispatch(updatePageFieldRequest(pageid, fieldName, fieldVal, lang));
        // dispatch(updatePageFieldSuccess(topic, pageid, fieldName, fieldVal));
        let lang_code = invert(LangOptions)[lang];
        console.debug('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_qa_setval?pageid=' + pageid + '&lang_code=' + lang_code + '&varname=' + fieldName + '&varval=' + fieldVal);
        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_qa_setval?pageid=' + pageid + '&lang_code=' + lang_code + '&varname=' + fieldName + '&varval=' + fieldVal, { timeout: 20000 })
            .then(
                response => response.json(),
                ex => dispatch(updatePageFieldFailure(pageid, fieldName, lang, ex))
            )
            .then(json => {
                console.debug(json);
                // console.debug(json.ResultSets);
                // console.debug(json.ResultSets[0][0]);
                let status = json.ResultSets[0][0].status;
                // console.debug('status: ' + status);
                if (status < 100) {
                    dispatch(updatePageFieldSuccess(pageid, fieldName, fieldVal, lang))
                } else {
                    // console.debug('failed');
                    dispatch(updatePageFieldFailure(pageid, fieldName, lang, json.ResultSets[0].statdesc))
                }

                
            })
            .catch(ex => dispatch(updatePageFieldFailure(pageid, fieldName, lang, ex)));
    }
}

export function setChunkMessage(message, message_type, pageid, chunkvar, lang) {
    return {
        type: SET_CHUNK_MESSAGE,
        message,
        message_type,
        pageid,
        chunkvar,
        lang
    }
}

export function removeChunkMessage(pageid, chunkvar, lang) {
    return {
        type: REMOVE_CHUNK_MESSAGE,
        pageid,
        chunkvar,
        lang
    }
}

function setChunkValRequest(pageid, lang, chunkvar, chunkval, update_by, row_id) {
    return {
        type: SET_CHUNK_VAL_REQUEST,
        pageid,
        lang,
        chunkvar,
        chunkval,
        update_by, 
        row_id
    }
}

function setChunkValSuccess(pageid, lang, chunkvar, chunkval, update_by, row_id) {
    return {
        type: SET_CHUNK_VAL_SUCCESS,
        pageid,
        lang,
        chunkvar,
        chunkval,
        update_by, 
        row_id
    }
}

function setChunkValFailure(pageid, lang, chunkvar, ex) {
    return {
        type: SET_CHUNK_VAL_FAILURE,
        pageid,
        lang,
        chunkvar,
        ex
    }
}

export function setChunkVal(pageid, lang_id, chunkvar, chunkval, update_by, row_id) {
    console.log('setChunkVal', pageid, lang_id, chunkvar, chunkval, update_by, row_id);
    return function(dispatch) {
        dispatch(setChunkValRequest(pageid, lang_id, chunkvar, chunkval, update_by, row_id));
        dispatch(setChunkMessage('Saving ...', 'warning', pageid, chunkvar, lang_id));
        return fetch('/api/edapi/dbo.api_sp_update_page_chunk', {
            method: 'post',
            body: JSON.stringify({
                row_id,
                chunkval,
                update_by
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': window.auth
            }
        })
            .then(
                response => response.json(),
                ex => dispatch(setChunkValFailure(pageid, lang_id, chunkvar, ex))
            )
            .then(json => {
                console.debug(json);
                let status = json.ResultSets[0][0].status;
                if (status < 100) {
                    dispatch(setChunkValSuccess(pageid, lang_id, chunkvar, chunkval, update_by));
                    dispatch(setChunkMessage('Saved.', 'success', pageid, chunkvar, lang_id));

                    if ((status&1) == 1) {
                        //  status of 1 means we need to refresh the page chunks
                        dispatch(fetchPageChunks(pageid, lang_id));
                    }
                } else {
                    dispatch(setChunkValFailure(pageid, lang_id, chunkvar, { message: json.ResultSets[0].statdesc }));
                    dispatch(setChunkMessage('Not Saved!', 'error', pageid, chunkvar, lang_id));
                }
                //  Remove the message after 5 seconds
                setTimeout(() => {
                    dispatch(removeChunkMessage(pageid, chunkvar, lang_id));
                }, 5000)
            })
            .catch(ex => {
                dispatch(setChunkValFailure(pageid, lang_id, chunkvar, ex))
                dispatch(setChunkMessage('Not Saved!', 'error', pageid, chunkvar, lang_id));
            });
    }
}

function fixPageRequest(pageid, optflags) {
    return {
        type: FIX_PAGE_REQUEST,
        pageid,
        optflags
    }
}

function fixPageFailure(pageid, ex) {
    return {
        type: FIX_PAGE_FAILURE,
        pageid,
        ex
    }
}

function fixPageSuccess(pageid) {
    return {
        type: FIX_PAGE_SUCCESS,
        pageid
    }
}

export function doFixPage(pageid, optflags) {
    return function(dispatch) {
        dispatch(fixPageRequest(pageid, optflags));

        return fetchJsonp(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_page_action?pageid=${pageid}&optflags=${optflags}`)
            .then(
                response => response.json(),
                ex => dispatch(fixPageFailure(pageid, ex))
            )
            .then(
                json => {
                    dispatch(fixPageSuccess(pageid))
                }
            )
            .catch(
                ex => dispatch(fixPageFailure(pageid, ex))
            )
    }
}

function doPageChunkActionRequest(pageid, lang, src_chunk, dest_chunk, update_by, chunk_action) {
    return {
        type: DO_PAGE_CHUNK_ACTION_REQUEST,
        pageid,
        lang,
        src_chunk,
        dest_chunk,
        update_by,
        chunk_action
    }
}

function doPageChunkActionFailure(pageid, lang, ex) {
    return {
        type: DO_PAGE_CHUNK_ACTION_FAILURE,
        pageid,
        lang,
        ex
    }
}

function doPageChunkActionSuccess(pageid, lang, data) {
    return {
        type: DO_PAGE_CHUNK_ACTION_SUCCESS,
        pageid,
        lang,
        data
    }
}

export function doPageChunkAction(pageid, lang, src_chunk, dest_chunk, update_by, chunk_action) {
    console.debug('doPageChunkAction', pageid, lang, src_chunk, dest_chunk, update_by, chunk_action);
    return function(dispatch) {
        dispatch(doPageChunkActionRequest(pageid, lang, src_chunk, dest_chunk, chunk_action));

        let optflags;
        switch(chunk_action) {
            case 'set_blank':
                dispatch(setChunkMessage('Saving ...', 'warning', pageid, src_chunk.chunkvar, lang));
                optflags=1;
                break;
            case 'copy_paste':
                optflags=2;
                break;
            case 'cut_paste':
                dispatch(setChunkMessage('Saving ...', 'warning', pageid, src_chunk.chunkvar, lang));
                optflags=3;
                break;
            case 'lucky':
                dispatch(setChunkMessage('Saving ...', 'warning', pageid, src_chunk.chunkvar, lang));
                optflags=8;
                break;
        }

        let dest_row_id = dest_chunk ? dest_chunk.row_id : '';

        console.debug(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_page_chunk_action?row_id_src=${src_chunk.row_id}&row_id_dest=${dest_row_id}&update_by=${update_by}&optflags=${optflags}`);
        return fetchJsonp(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_page_chunk_action?row_id_src=${src_chunk.row_id}&row_id_dest=${dest_row_id}&update_by=${update_by}&optflags=${optflags}`)
            .then(
                response => response.json(),
                ex => {
                    dispatch(doPageChunkActionFailure(pageid, lang, ex))
                    if (chunk_action != 'set_blank') {
                        dispatch(setChunkMessage('Not Saved!', 'error', pageid, dest_chunk.chunkvar, lang))
                        //  Remove the message after 5 seconds
                        setTimeout(() => {
                            dispatch(removeChunkMessage(pageid, dest_chunk.chunkvar, lang));
                        }, 5000)
                    }

                    if (chunk_action != 'copy_paste') {
                        dispatch(setChunkMessage('Not Saved!', 'error', pageid, src_chunk.chunkvar, lang))
                        //  Remove the message after 5 seconds
                        setTimeout(() => {
                            dispatch(removeChunkMessage(pageid, src_chunk.chunkvar, lang));
                        }, 5000)
                    }


                }
            )
            .then(json => {
                console.debug(json);
                dispatch(doPageChunkActionSuccess(pageid, lang, json))

                let status = json.ReturnValue;
                console.debug('status', status);
                if (status < 100) {
                    if ((status&1) == 1) {
                        //  status of 1 means we need to refresh the page chunks
                        //  Load both langs
                        dispatch(fetchPageChunks(pageid, 1));
                        dispatch(fetchPageChunks(pageid, 2));
                    }

                    if (chunk_action == 'set_blank' || chunk_action == 'cut_paste' || chunk_action == 'lucky') {
                        dispatch(setChunkMessage('Saved.', 'success', pageid, src_chunk.chunkvar, lang));
                        //  Remove the message after 5 seconds
                        setTimeout(() => {
                            dispatch(removeChunkMessage(pageid, src_chunk.chunkvar, lang));
                        }, 5000)
                    }

                    if (chunk_action != 'set_blank' && chunk_action != 'lucky') {
                        dispatch(setChunkMessage('Saved.', 'success', pageid, dest_chunk.chunkvar, lang));
                        //  Remove the message after 5 seconds
                        setTimeout(() => {
                            dispatch(removeChunkMessage(pageid, dest_chunk.chunkvar, lang));
                        }, 5000)
                    }
                } else if ((status&100) == 100) {
                    dispatch(setChunkMessage('No Match!', 'warning', pageid, src_chunk.chunkvar, lang));
                    //  Remove the message after 5 seconds
                    setTimeout(() => {
                        dispatch(removeChunkMessage(pageid, src_chunk.chunkvar, lang));
                    }, 5000)
                } else {
                    dispatch(setChunkValFailure(pageid, lang, src_chunk.chunkvar, { message: json.ResultSets[0][0].statdesc }));
                    dispatch(setChunkMessage('Not Saved!', 'error', pageid, src_chunk.chunkvar, lang));
                    //  Remove the message after 5 seconds
                    setTimeout(() => {
                        dispatch(removeChunkMessage(pageid, src_chunk.chunkvar, lang));
                    }, 5000)
                }

                
            })
            .catch(ex => {
                dispatch(doPageChunkActionFailure(pageid, lang, ex));

                if (chunk_action != 'set_blank') {
                    dispatch(setChunkMessage('Not Saved!', 'error', pageid, dest_chunk.chunkvar, lang))
                    setTimeout(() => {
                        dispatch(removeChunkMessage(pageid, dest_chunk.chunkvar, lang));
                    }, 5000)
                }

                if (chunk_action != 'copy_paste') {
                    dispatch(setChunkMessage('Not Saved!', 'error', pageid, src_chunk.chunkvar, lang))
                    setTimeout(() => {
                        dispatch(removeChunkMessage(pageid, src_chunk.chunkvar, lang));
                    }, 5000)
                }
            });
    }
}

function fetchPageQAContentRequest(pageid, lang) {
    return {
        type: FETCH_PAGE_QA_CONTENT_REQUEST,
        pageid,
        lang
    }
}

function fetchPageQAContentFailure(pageid, lang, ex) {
    return {
        type: FETCH_PAGE_QA_CONTENT_FAILURE,
        lang,
        ex
    }
}

function fetchPageQAContentSuccess(pageid, lang, data) {
    return {
        type: FETCH_PAGE_QA_CONTENT_SUCCESS,
        pageid,
        lang,
        data
    }
}

export function fetchPageQAContent(pageid, lang) {
    return function(dispatch) {
        dispatch(fetchPageQAContentRequest(pageid, lang));
        let lang_code = invert(LangOptions)[lang];
        let content_tag = `P${pageid}`;

        console.debug('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_qa_pages?content_tag=' + content_tag + '&lang_code=' + lang_code);
        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_qa_pages?content_tag=' + content_tag + '&lang_code=' + lang_code)
            .then(
                response => response.json(),
                ex => dispatch(fetchPageQAContentFailure(pageid, lang, ex))
            )
            .then(json => dispatch(fetchPageQAContentSuccess(pageid, lang, json.ResultSets[0][0])))
            .catch(ex => dispatch(fetchPageQAContentFailure(pageid, lang, ex)));
    }
}

function fetchAppConstantsSuccess(data) {
    // console.debug('fetchEditorContentSuccess');
    // console.debug(content);
    return {
        type: FETCH_APP_CONSTANTS_SUCCESS,
        data
    }
}

function fetchAppConstantsFailure(ex) {
    return {
        type: FETCH_APP_CONSTANTS_FAILURE,
        ex
    }
}

function fetchAppConstantsRequest() {
    return { type: FETCH_APP_CONSTANTS_REQUEST };
}

export function fetchAppConstants() {
    // console.log('fetchEditorContent');
    return function(dispatch) {
        dispatch(fetchAppConstantsRequest());

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_app_constants?appname=svgmanager')
            .then(
                response => response.json(),
                ex => dispatch(fetchAppConstantsFailure(ex))
            )
            .then(json => dispatch(fetchAppConstantsSuccess(json.ResultSets)))
            .catch(ex => dispatch(fetchAppConstantsFailure(ex)));
    }
}
