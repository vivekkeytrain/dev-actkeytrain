import { combineReducers } from 'redux';
import { 
    SET_LANG, SET_UNIT, SET_COURSE, SET_LESSON, SET_TOPIC, SET_SESSION, SET_SELECTED_ROW, SET_USER,
    ADD_MESSAGE, CLOSE_MESSAGE,
    FETCH_EDITOR_CONTENT_REQUEST, FETCH_EDITOR_CONTENT_SUCCESS, FETCH_EDITOR_CONTENT_FAILURE,
    FETCH_ATTACHMENTS_REQUEST, FETCH_ATTACHMENTS_FAILURE, FETCH_ATTACHMENTS_SUCCESS,
    FETCH_PAGE_DETAIL_REQUEST, FETCH_PAGE_DETAIL_SUCCESS, FETCH_PAGE_DETAIL_FAILURE,
    UPDATE_FIELD_REQUEST, UPDATE_FIELD_SUCCESS, UPDATE_FIELD_FAILURE,
    DO_LOGIN_LESSON_REQUEST, DO_LOGIN_LESSON_SUCCESS, DO_LOGIN_LESSON_FAILURE,
    FETCH_ATTACHMENT_DATA_REQUEST, FETCH_ATTACHMENT_DATA_SUCCESS, FETCH_ATTACHMENT_DATA_FAILURE,
    LangOptions, DisplayFields, EditFields,
    fetchEditorContent
} from './actions';
const { EN } = LangOptions;
import { chain, map, fromPairs, sortBy, findIndex } from 'lodash';

function sessions(state = {}, action) {
    switch (action.type) {
        case DO_LOGIN_LESSON_SUCCESS:
        case DO_LOGIN_LESSON_REQUEST:
        case DO_LOGIN_LESSON_FAILURE:
            if (!action.lang) return state;

            return Object.assign({}, state, {
                [action.lang]: lang_sessions(state[action.lang], action)
            });
        default:
            return state;
    }
}

function lang_sessions(state = {}, action) {
    // console.debug(state);
    // console.debug(action);
    switch (action.type) {
        case DO_LOGIN_LESSON_SUCCESS:
            return Object.assign({}, state, {
                [action.lesson_id]: {
                    session_id: action.session_id,
                    login_session_uid: action.login_session_uid,
                    isFetching: false,
                    lastUpdated: action.receivedAt
                }
            });
        case DO_LOGIN_LESSON_REQUEST:
            return Object.assign({}, state, {
                [action.lesson_id]: {
                    isFetching: true
                }
            });
        case DO_LOGIN_LESSON_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state.sessions, {
                [action.lesson_id]: {
                    isFetching: false
                }
            });
        default:
            return state;
    }
}


function filters(state = {
    lang: EN,
    unit: '',
    course: '',
    lesson: '',
    topic: '',
    selectedRow: -1,
    sessions: {},
    messages: []
}, action) {

    switch (action.type) {
        case SET_LANG:
            return Object.assign({}, state, {
                lang: action.lang
            });
        case SET_UNIT:
            return Object.assign({}, state, {
                unit: action.unit,
                course: '',
                lesson: '',
                topic: ''
            });
        case SET_COURSE:
            //  Can't set course if we haven't set unit
            if (state.unit == '') return state;

            return Object.assign({}, state, {
                course: action.course,
                lesson: '',
                topic: ''
            });
        case SET_LESSON:
            //  Can't set lesson if we haven't set unit and course
            if (state.unit == '' || state.course == '') return state;

            return Object.assign({}, state, {
                lesson: action.lesson,
                topic: ''
            });
        case SET_TOPIC:
            //  Can't set topic if we haven't set unit, course, and lesson
            if (state.unit == '' || state.course == '' || state.lesson == '') return state;

            return Object.assign({}, state, {
                topic: action.topic,
                pattern: '',
                page: ''
            });
        case SET_SELECTED_ROW:
            return Object.assign({}, state, {
                selectedRow: action.row_index
            });

        case DO_LOGIN_LESSON_SUCCESS:
        case DO_LOGIN_LESSON_REQUEST:
            return Object.assign({}, state, {
                sessions: sessions(state.sessions, action)
            });

        case FETCH_EDITOR_CONTENT_FAILURE:
        case FETCH_ATTACHMENTS_FAILURE:
        case FETCH_ATTACHMENT_DATA_FAILURE:
        case FETCH_PAGE_DETAIL_FAILURE:
        case UPDATE_FIELD_FAILURE:
        // case DO_LOGIN_LESSON_FAILURE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: 'danger',
                        message: action.ex.message,
                        visible: true
                    }
                ]
            });

        case ADD_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: action.message_type,
                        message: action.message,
                        visible: true
                    }
                ]
            });

        case CLOSE_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages.slice(0, action.index),
                    Object.assign({}, state.messages[action.index], {
                        visible: false
                    }),
                    ...state.messages.slice(action.index+1)
                ]
            });

        default:
            return state;

    }
}

function attachments(state = {}, action) {
    // console.debug(state);
    switch (action.type) {
        case FETCH_ATTACHMENTS_SUCCESS:
        case FETCH_ATTACHMENTS_REQUEST:
        case FETCH_ATTACHMENTS_FAILURE:
        case UPDATE_FIELD_SUCCESS:
        case UPDATE_FIELD_REQUEST:
        case UPDATE_FIELD_FAILURE:
        case FETCH_ATTACHMENT_DATA_SUCCESS:
            return Object.assign({}, state, {
                [action.lang]: actual_attachments(state[action.lang], action)
            });
        default:
            return state;
    }
}

function actual_attachments(state = {}, action) {
    console.debug(action);
    console.debug(state);
    switch (action.type) {
        case FETCH_ATTACHMENTS_SUCCESS:
            let page_items = chain(action.attachments)
                .map(page => {
                    // console.debug(page);
                    return [page.attach_id, page];
                })
                .fromPairs()
                .value();
            // console.debug(page_items);
            return Object.assign({}, state, page_items);

        case UPDATE_FIELD_SUCCESS:
        case UPDATE_FIELD_REQUEST:
        case UPDATE_FIELD_FAILURE:
            return Object.assign({}, state, {
                [action.attach_id]: items(state[action.attach_id], action)
            })
        case FETCH_ATTACHMENT_DATA_SUCCESS:
            console.debug('[' + action.attach_id + ']: ', action.attachment);
            return Object.assign({}, state, {
                [action.attach_id]: action.attachment
            })
        case FETCH_ATTACHMENTS_REQUEST:
        case FETCH_ATTACHMENTS_FAILURE:
        default:
            return state;
    }
}

function items(state = {}, action) {
    console.debug(state);
    switch (action.type) {
        case UPDATE_FIELD_SUCCESS:
            let update = {};
            update[action.fieldName] = action.fieldVal;
            console.debug(update);

            return Object.assign({}, state, update);
        case UPDATE_FIELD_FAILURE:
            // console.debug(action.ex);
        case UPDATE_FIELD_REQUEST:
        default: 
            return state;
    }
}

function attachments_by_tag(state = {}, action) {
    switch (action.type) {
        case FETCH_ATTACHMENTS_SUCCESS:
        case FETCH_ATTACHMENTS_REQUEST:
        case FETCH_ATTACHMENTS_FAILURE:
            return Object.assign({}, state, {
                [action.lang]: lang_attachments_by_tag(state[action.lang], action)
            });
        default:
            return state;
    }
}

function lang_attachments_by_tag(state = {}, action) {
    switch (action.type) {
        case FETCH_ATTACHMENTS_SUCCESS:
        case FETCH_ATTACHMENTS_REQUEST:
        case FETCH_ATTACHMENTS_FAILURE:
            return Object.assign({}, state, {
                [action.tag]: actual_tag_attachments(state[action.tag], action)
            });
        default:
            return state;
    }
}

function actual_tag_attachments(state = {
    isFetching: false,
    items: {},
    lastUpdated: null
}, action) {
    switch (action.type) {
        case FETCH_ATTACHMENTS_SUCCESS:
            // console.debug(action.pages);
            let page_items = map(action.attachments, 'attach_id');
            // console.debug(page_items);

            return Object.assign({}, state, {
                isFetching: false,
                items: page_items,
                lastUpdated: action.receivedAt
            });
        case FETCH_ATTACHMENTS_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_ATTACHMENTS_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state, {
                isFetching: false
            })
        default:
            return state;
    }
}

function pages(state = {}, action) {
    switch (action.type) {
        case FETCH_PAGE_DETAIL_SUCCESS:
            state[action.pageid] = action.page_data;
        default:
            return state;
    }
}

function editor_content(state = {
    isFetching: false,
    content: []
}, action) {
    switch (action.type) {
        case FETCH_EDITOR_CONTENT_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                content: action.content,
                lastUpdated: action.receivedAt
            });
        case FETCH_EDITOR_CONTENT_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_EDITOR_CONTENT_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

function constants(state = {
    langs: LangOptions,
    displayFields: DisplayFields,
    editFields: EditFields,
    user: ''
}, action) {
    switch (action.type) {
        case SET_USER:
            return Object.assign({}, state, {
                user: action.user
            })
        default:
            return state;
    }
}

const attachApp = combineReducers({
    filters,
    attachments_by_tag,
    attachments,
    editor_content,
    constants,
    pages
});

export default attachApp;