import { createSelector } from 'reselect';
import { chain, uniq, filter, map, find, merge, includes, sortBy, keyBy } from 'lodash';
import { LangOptions } from './actions';

/*  Get a unique set of units   */
function selectUniqueUnits(content, tags) {
    return map(tags, (tag) => {
        let match = find(content, {'unit_tag': tag});

        return {
            id: match.unit_id,
            tag: match.unit_tag,
            desc: match.unit_desc,
            seq: match.unit_seq,
            display: match.unit_display
        }
    });
}

/*  Select a unique set of unit tags that will be used to create a unique set of units  */
function selectUnitTags(content) {
    return chain(content)
        .map('unit_tag')
        .uniq()
        .value();
}


function selectUniqueCourses(content, tags) {
    // console.debug('selectUniqueCourses', content, tags);
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'course_tag': tag});

            return {
                id: match.course_id,
                tag: match.course_tag,
                desc: match.course_desc,
                seq: match.course_seq,
                display: match.course_display
            }
        })
        .value();
}

function selectCourseTags(content, unit) {
    // console.debug('selectCourseTags', content, unit);
    return chain(content)
        .filter({'unit_id': unit})
        .map('course_tag')
        .uniq()
        .value();
}

function selectUniqueLessons(content, tags) {
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'lesson_tag': tag});

            return {
                id: match.lesson_id,
                tag: match.lesson_tag,
                desc: match.lesson_desc,
                seq: match.lesson_seq,
                display: match.lesson_display
            }
        })
        .value();
}

function selectLessonTags(content, course) {
    return chain(content)
        .filter({'course_id': course})
        .map('lesson_tag')
        .uniq()
        .value();
}

function selectUniqueTopics(content, tags) {
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'topic_tag': tag});

            return {
                id: match.topic_id,
                tag: match.topic_tag,
                desc: match.topic_desc,
                seq: match.topic_seq,
                display: match.topic_display
            }
        })
        .value();
}

function selectTopicTags(content, lesson) {
    return chain(content)
        .filter({'lesson_id': lesson})
        .map('topic_tag')
        .uniq()
        .value();
}

/*  Content filtered by current selected state */
function selectContent(content, unit, course, lesson, topic) {
    // console.debug(content);
    // console.debug([unit, course, lesson, topic]);
    let filtered_content = {
        units: content,
        courses: [],
        lessons: [],
        topics: []
    }
    if (unit != '')
        filtered_content.courses = filter(content, {'unit_id': unit});
    
    if (course != '') 
        filtered_content.lessons = filter(content, {'course_id': course});

    if (lesson != '') 
        filtered_content.topics = filter(content, {'lesson_id': lesson});

    // console.debug(filtered_content);

    return filtered_content;
}

function getTagAttachments(attachments, attachments_by_tag, lang, tag) {
    // console.debug(attachments);
    // console.debug(attachments_by_tag);
    // console.debug(lang, tag);
    if (!(attachments[lang] && attachments_by_tag[lang] && attachments_by_tag[lang][tag])) {
        return [];
    } else {
        let attachment_ids = attachments_by_tag[lang][tag].items;
        let attachment_objs = attachments[lang];

        let out_attachments = map(attachment_ids, (attachmentid) => {
            // console.debug(pageid);
            return attachment_objs[attachmentid]    //  Turn pageids into actual page objects
        });

        // out_attachments = sortBy(out_attachments, 'page_number');

        // console.debug(out_pages);
        return out_attachments;
    }
}

function selectAttachmentsByTag(attachments, attachments_by_tag, lang, tag_type, unit, course, lesson, topic) {
    // console.log('selectAttachmentsByTag', lang, tag_type, unit, course, lesson, topic);

    //  Only return pages if 'ALL' (-1) is selected
    if (tag_type === 'U' && course !== -1) return [];
    if (tag_type === 'C' && lesson !== -1) return [];
    if (tag_type === 'L' && topic !== -1) return [];

    let tag = tag_type;
    if (tag_type === 'U') tag += unit;
    if (tag_type === 'C') tag += course;
    if (tag_type === 'L') tag += lesson;
    if (tag_type === 'T') tag += topic;

    return getTagAttachments(attachments, attachments_by_tag, lang, tag);
}

function selectAttachmentsIsFetching(attachments_by_tag, lang, unit, course, lesson, topic) {
    console.debug('selectAttachmentsIsFetching', attachments_by_tag, lang, unit, course, lesson, topic);
    if (!attachments_by_tag[lang]) return false;

    const lang_attachments = attachments_by_tag[lang];

    let tag;
    if (topic && topic != -1)
        tag = 'T' + topic;
    else if (lesson && topic == -1)
        tag = 'L' + lesson;
    else if (course && lesson == -1)
        tag = 'C' + course;
    else if (unit && course == -1)
        tag = 'U' + unit;

    if (!lang_attachments[tag]) return false;

    console.debug(tag);
    console.debug(lang_attachments[tag]);

    return lang_attachments[tag].isFetching;
}

export function selectCourseTag(course, courses) {
    if (!course || !courses || course == -1) return '';
    // console.debug('selectCourseTag', course, courses);
    return chain(courses).find({'id': course}).value().tag;
}

export function selectLessonTag(lesson, lessons) {
    if (!lesson || !lessons || lesson == -1) return '';

    return chain(lessons).find({'id': lesson}).value().tag;
}

function selectSession(sessions, lang) {
    // console.debug('selectSession', lang);
    // console.debug(sessions);
    if (!sessions[lang]) return {};

    return sessions[lang] || {};
}

function selectDisplayFieldsHash(displayFields) {
    return keyBy(displayFields, 'name');
}

function selectEditViewDisplayFields(displayFieldsHash) {
    return filter(displayFieldsHash, 'edit_display');
}

function selectReadOnlyFields(editFields) {
    return filter(editFields, (field) => {
        return !field.readonly_hide;
    })
}

function selectEditFieldsHash(editFields) {
    return keyBy(editFields, 'name');
}

function selectAttachmentsList(unit, course, lesson, topic, unit_attachments, course_attachments, lesson_attachments, topic_attachments) {
    if (topic && topic !== -1 && topic_attachments) {
        return topic_attachments;
    } else if (lesson && lesson !== -1 && lesson_attachments) {
        return lesson_attachments;
    } else if (course && course !== -1 && course_attachments) {
        return course_attachments;
    } else if (unit && unit_attachments) {
        return unit_attachments;
    } else {
        return [];
    }
}

// function selectRowAttachments9

const contentSelector = state => state.editor_content.content;
const unitSelector = state => state.filters.unit;
const courseSelector = state => state.filters.course;
const lessonSelector = state => state.filters.lesson;
const topicSelector = state => state.filters.topic;
const attachmentsSelector = state => state.attachments;
const tagAttachmentsSelector = state => state.attachments_by_tag;
const constantsSelector = state => state.constants;
const selectedRowSelector = state => state.filters.selectedRow;
const langSelector = state => state.filters.lang;
const sessionsSelector = state => state.filters.sessions;
const displayFieldsSelector = state => state.constants.displayFields;
const editFieldsSelector = state => state.constants.editFields;
const pagesSelector = state => state.pages;
const messagesSelector = state => state.filters.messages;

const contentDataSelector = createSelector(
    contentSelector,
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    (content, unit, course, lesson, topic) => {
        return selectContent(content, unit, course, lesson, topic);
    }
);

const unitTagsSelector = createSelector(
    contentSelector,
    (content) => {
        return selectUnitTags(content);
    }
);

const unitsSelector = createSelector(
    [contentSelector, unitTagsSelector],
    (content, tags) => {
        return selectUniqueUnits(content, tags);
    }
);

const courseTagsSelector = createSelector(
    [contentDataSelector, unitSelector],
    (content, unit) => {
        return selectCourseTags(content.courses, unit);
    }
);

const coursesSelector = createSelector(
    [contentDataSelector, courseTagsSelector],
    (content, tags) => {
        return selectUniqueCourses(content.courses, tags);
    }
);

/*  Selector to get the course tag of the currently selected course_id */
const courseTagSelector = createSelector(
    [courseSelector, coursesSelector],
    (course, courses) => {
        return selectCourseTag(course, courses);
    }
);

const lessonTagsSelector = createSelector(
    [contentDataSelector, courseSelector],
    (content, course) => {
        return selectLessonTags(content.lessons, course);
    }
);

const lessonsSelector = createSelector(
    [contentDataSelector, lessonTagsSelector],
    (content, tags) => {
        return selectUniqueLessons(content.lessons, tags);
    }
);

const lessonTagSelector = createSelector(
    [lessonSelector, lessonsSelector],
    (lesson, lessons) => {
        return selectLessonTag(lesson, lessons); 
    }
);

const topicTagsSelector = createSelector(
    [contentDataSelector, lessonSelector],
    (content, lesson) => {
        return selectTopicTags(content.topics, lesson);
    }
);

const topicsSelector = createSelector(
    [contentDataSelector, topicTagsSelector],
    (content, tags) => {
        return selectUniqueTopics(content.topics, tags);
    }
);

const unitAttachmentsSelector = createSelector(
    attachmentsSelector,
    tagAttachmentsSelector,
    langSelector,
    () => 'U',
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    (attachments, attachments_by_tag, lang, tag_type, unit, course, lesson, topic) => {
        return selectAttachmentsByTag(attachments, attachments_by_tag, lang, tag_type, unit, course, lesson, topic)
    }
);

const courseAttachmentsSelector = createSelector(
    attachmentsSelector,
    tagAttachmentsSelector,
    langSelector,
    () => 'C',
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    (attachments, attachments_by_tag, lang, tag_type, unit, course, lesson, topic) => {
        return selectAttachmentsByTag(attachments, attachments_by_tag, lang, tag_type, unit, course, lesson, topic)
    }
);

const lessonAttachmentsSelector = createSelector(
    attachmentsSelector,
    tagAttachmentsSelector,
    langSelector,
    () => 'L',
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    (attachments, attachments_by_tag, lang, tag_type, unit, course, lesson, topic) => {
        return selectAttachmentsByTag(attachments, attachments_by_tag, lang, tag_type, unit, course, lesson, topic)
    }
);

const topicAttachmentsSelector = createSelector(
    attachmentsSelector,
    tagAttachmentsSelector,
    langSelector,
    () => 'T',
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    (attachments, attachments_by_tag, lang, tag_type, unit, course, lesson, topic) => {
        return selectAttachmentsByTag(attachments, attachments_by_tag, lang, tag_type, unit, course, lesson, topic)
    }
);

const attachmentsIsFetchingSelector = createSelector(
    tagAttachmentsSelector,
    langSelector,
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    (attachments_by_tag, lang, unit, course, lesson, topic) => {
        return selectAttachmentsIsFetching(attachments_by_tag, lang, unit, course, lesson, topic);
    }
);

const enSessionSelector = createSelector(
    sessionsSelector,
    () => LangOptions.EN,
    (sessions, lang) => {
        return selectSession(sessions, lang);
    }
);

const esSessionSelector = createSelector(
    sessionsSelector,
    () => LangOptions.ES,
    (sessions, lang) => {
        return selectSession(sessions, lang);
    }
);

const displayFieldsHashSelector = createSelector(
    displayFieldsSelector,
    (displayFields) => {
        return selectDisplayFieldsHash(displayFields);
    }
);

const editViewDisplayFieldsSelector = createSelector(
    displayFieldsHashSelector,
    (displayFieldsHash) => {
        return selectEditViewDisplayFields(displayFieldsHash);
    }
);

const readOnlyFieldsSelector = createSelector(
    editFieldsSelector,
    (editFields) => {
        return selectReadOnlyFields(editFields);
    }
);

const editFieldsHashSelector = createSelector(
    editFieldsSelector,
    (editFields) => {
        return selectEditFieldsHash(editFields);
    }
);

const attachmentsListSelector = createSelector(
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    unitAttachmentsSelector,
    courseAttachmentsSelector,
    lessonAttachmentsSelector,
    topicAttachmentsSelector,
    (unit, course, lesson, topic, unit_attachments, course_attachments, lesson_attachments, topic_attachments) => {
        return selectAttachmentsList(unit, course, lesson, topic, unit_attachments, course_attachments, lesson_attachments, topic_attachments);
    }
);

export const filteredContentSelector = createSelector(
    contentDataSelector,
    unitSelector,
    unitsSelector,
    courseSelector,
    coursesSelector,
    courseTagSelector,
    lessonSelector,
    lessonsSelector,
    lessonTagSelector,
    topicSelector,
    topicsSelector,
    langSelector,
    attachmentsSelector,
    unitAttachmentsSelector,
    courseAttachmentsSelector,
    lessonAttachmentsSelector,
    topicAttachmentsSelector,
    attachmentsIsFetchingSelector,
    constantsSelector,
    sessionsSelector,
    enSessionSelector,
    esSessionSelector,
    selectedRowSelector,
    displayFieldsHashSelector,
    editViewDisplayFieldsSelector,
    readOnlyFieldsSelector,
    editFieldsHashSelector,
    pagesSelector,
    messagesSelector,
    attachmentsListSelector,
    (content, unit, units, course, courses, course_tag, lesson, lessons, lesson_tag, topic, topics, lang, all_attachments, unit_attachments, course_attachments, lesson_attachments, topic_attachments, attachments_fetching, constants, all_sessions, enSession, esSession, selectedRow, displayFieldsHash, editViewDisplayFields, readOnlyFields, editFieldsHash, pages, messages, attachments_list) => {
        return {
            content,
            unit,
            units,
            course,
            courses,
            course_tag,
            lesson,
            lessons,
            lesson_tag,
            topic,
            topics,
            lang,
            all_attachments,
            unit_attachments,
            course_attachments,
            lesson_attachments,
            topic_attachments,
            attachments_fetching,
            constants,
            all_sessions,
            enSession,
            esSession, 
            selectedRow, 
            displayFieldsHash, 
            editViewDisplayFields, 
            readOnlyFields, 
            editFieldsHash, 
            pages, 
            messages,
            attachments_list
        }
    }
);