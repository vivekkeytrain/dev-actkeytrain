import fetchJsonp from 'fetch-jsonp';
import React from 'react';
import { map } from 'lodash';

/*  Action types    */

export const SET_LANG = 'SET_LANG';
export const SET_UNIT = 'SET_UNIT';
export const SET_COURSE = 'SET_COURSE';
export const SET_LESSON = 'SET_LESSON';
export const SET_TOPIC = 'SET_TOPIC';
export const SET_SELECTED_ROW = 'SET_SELECTED_ROW';
export const SET_USER = 'SET_USER';
export const ADD_MESSAGE = 'ADD_MESSAGE';
export const CLOSE_MESSAGE = 'CLOSE_MESSAGE';

/*  Async action types   */

export const FETCH_EDITOR_CONTENT_SUCCESS = 'FETCH_EDITOR_CONTENT_SUCCESS';
export const FETCH_EDITOR_CONTENT_FAILURE = 'FETCH_EDITOR_CONTENT_FAILURE';
export const FETCH_EDITOR_CONTENT_REQUEST = 'FETCH_EDITOR_CONTENT_REQUEST';

export const FETCH_ATTACHMENTS_REQUEST = 'FETCH_ATTACHMENTS_REQUEST';
export const FETCH_ATTACHMENTS_FAILURE = 'FETCH_ATTACHMENTS_FAILURE';
export const FETCH_ATTACHMENTS_SUCCESS = 'FETCH_ATTACHMENTS_SUCCESS';

export const FETCH_ATTACHMENT_DATA_REQUEST = 'FETCH_ATTACHMENT_DATA_REQUEST';
export const FETCH_ATTACHMENT_DATA_FAILURE = 'FETCH_ATTACHMENT_DATA_FAILURE';
export const FETCH_ATTACHMENT_DATA_SUCCESS = 'FETCH_ATTACHMENT_DATA_SUCCESS';

export const FETCH_PAGE_DETAIL_REQUEST = 'FETCH_PAGE_DETAIL_REQUEST';
export const FETCH_PAGE_DETAIL_FAILURE = 'FETCH_PAGE_DETAIL_FAILURE';
export const FETCH_PAGE_DETAIL_SUCCESS = 'FETCH_PAGE_DETAIL_SUCCESS';

export const UPDATE_FIELD_REQUEST = 'UPDATE_FIELD_REQUEST';
export const UPDATE_FIELD_FAILURE = 'UPDATE_FIELD_FAILURE';
export const UPDATE_FIELD_SUCCESS = 'UPDATE_FIELD_SUCCESS';

export const DO_LOGIN_LESSON_REQUEST = 'DO_LOGIN_LESSON_REQUEST';
export const DO_LOGIN_LESSON_SUCCESS = 'DO_LOGIN_LESSON_SUCCESS';
export const DO_LOGIN_LESSON_FAILURE = 'DO_LOGIN_LESSON_FAILURE';

export const SET_SESSION = 'SET_SESSION';

/*  Other contstants    */

export const LangOptions = {
    EN: 1,
    ES: 2
};

export const DisplayFields = [
    {
        name: 'attach_id',
        label: 'ID',
        edit_display: true
    },{
        name: 'attach_tag',
        label: 'Tag',
        edit_display: false
    },{
        name: 'attach_desc',
        label: 'Description',
        edit_display: false
    }/*,{
        name: 'unit_desc',
        label: 'Unit',
        priority: 3,
        edit_display: false
    },{
        name: 'course_desc',
        label: 'Course',
        priority: 3,
        edit_display: false
    },{
        name: 'lesson_desc',
        label: 'Lesson',
        priority: 3,
        edit_display: false
    },{
        name: 'topic_desc',
        label: 'Topic',
        priority: 3,
        edit_display: false
    },{
        name: 'page_title',
        label: 'Page Title',
        priority: 3,
        edit_display: false
    }*/,{
        name: 'last_updated',
        label: 'Last Updated',
        priority: 3,
        displayFunc: (val) => {
            let d = new Date(val);
            let m = ((d.getMonth() < 10) ? '0' : '') + (d.getMonth()+1);
            let dy = ((d.getDate() < 10) ? '0' : '') + d.getDate();
            return d.getFullYear() + String.fromCharCode(8209) + m + String.fromCharCode(8209) + dy;
        },
        edit_display: false
    },{
        name: 'update_by',
        label: 'Updated By',
        edit_display: false
    }
]

function displayTextField(name, row) {
    return <RowTextField name={name} row={row} />
}

export const EditFields = [
    {
        name: 'attach_tag',
        label: 'Tag',
        readonly_hide: true,
        input_type: 'text'
    },{
        name: 'attach_desc',
        label: 'Description',
        readonly_hide: true,
        input_type: 'text'
    },{
        name: 'orig_format',
        label: 'Original Format',
        input_type: 'text'
    },{
        name: 'orig_name',
        label: 'Original Name',
        input_type: 'text'
    },{
        name: 'orig_path',
        label: 'Original Path',
        input_type: 'text'
    },{
        name: 'gdoc_url',
        label: 'GDoc URL',
        input_type: 'text'
    },{
        name: 'def_location_title',
        label: 'Link Text',
        input_type: 'text'
    },{
        name: 'content_notes',
        label: 'Content Notes',
        input_type: 'textarea'
    },{
        name: 'layout_notes',
        label: 'Layout Notes',
        input_type: 'textarea'
    },{
        name: 'production_notes',
        label: 'Production Notes',
        input_type: 'textarea'
    }
]

/*  Action creators */

export function setLang(lang) { 
    return { type: SET_LANG, lang };
}

export function setUnit(unit) {
    return function(dispatch) {
        localStorage.setItem('content_unit', unit);
        dispatch(setUnitStore(unit));
    }
}

export function setUnitStore(unit) {
    return { type: SET_UNIT, unit };
}

export function setCourse(course) {
    return function(dispatch) {
        localStorage.setItem('content_course', course);
        dispatch(setCourseStore(course));
    }
}

export function setCourseStore(course) {
    return { type: SET_COURSE, course };
}

export function setLesson(lesson) {
    return function(dispatch) {
        localStorage.setItem('content_lesson', lesson);
        dispatch(setLessonStore(lesson));
    }
}

export function setLessonStore(lesson) {
    return { type: SET_LESSON, lesson };
}

export function setTopic(topic) {
    return function(dispatch) {
        localStorage.setItem('content_topic', topic);
        dispatch(setTopicStore(topic));
    }
}

export function setTopicStore(topic) {
    return { type: SET_TOPIC, topic };
}

export function setUser(user) {
    return { type: SET_USER, user };
}

export function setSession(session_id, login_session_uid) {
    return {
        type: SET_SESSION,
        session_id,
        login_session_uid
    }
}

export function setSelectedRow(row_index) {
    return { type: SET_SELECTED_ROW, row_index };
}

export function addMessage(message_type, message) {
    return {
        type: ADD_MESSAGE,
        message_type,
        message
    }
}

export function closeMessage(index) {
    return {
        type: CLOSE_MESSAGE,
        index
    }
}


/*  Async actions   */

export function fetchEditorContentSuccess(content) {
    return {
        type: FETCH_EDITOR_CONTENT_SUCCESS,
        content
    }
}

export function fetchEditorContentFailure(ex) {
    return {
        type: FETCH_EDITOR_CONTENT_FAILURE,
        ex
    }
}

export function fetchEditorContentRequest() {
    return { type: FETCH_EDITOR_CONTENT_REQUEST };
}

export function fetchEditorContent() {
    return function(dispatch) {
        dispatch(fetchEditorContentRequest());

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_content_list/jsonp')
            .then(
                response => response.json(),
                ex => dispatch(fetchEditorContentFailure(ex))
            )
            .then(json => dispatch(fetchEditorContentSuccess(json.ResultSets[0])))
            .catch(ex => dispatch(fetchEditorContentFailure(ex)));
    }
}

function fetchAttachmentsSuccess(tag, lang, attachments) {
    return {
        type: FETCH_ATTACHMENTS_SUCCESS,
        tag,
        lang,
        attachments,
        receivedAt: Date.now()
    }
}

function fetchAttachmentsRequest(tag, lang) {
    return {
        type: FETCH_ATTACHMENTS_REQUEST,
        tag,
        lang
    }
}

function fetchAttachmentsFailure(ex) {
    return {
        type: FETCH_ATTACHMENTS_FAILURE,
        ex
    }
}

function fetchAttachments(tag, lang) {
    // console.debug('fetchPages', topicid, pageid, pattern);
    return function(dispatch) {
        dispatch(fetchAttachmentsRequest(tag, lang));
        console.debug('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_attach_data?content_tag=' + tag);
        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_attach_data?content_tag=' + tag, {
            timeout: 20000
        })
            .then(
                response => response.json(),
                ex => dispatch(fetchAttachmentsFailure(ex))
            )
            .then(json => dispatch(fetchAttachmentsSuccess(tag, lang, json.ResultSets[0])))
            .catch(ex => dispatch(fetchAttachmentsFailure(ex)));
    }
}

function shouldFetchAttachments(state, tag, lang) {
    console.debug('shouldFetchAttachments', tag, lang);
    if (!tag || !lang) return false;
    
    const attachments = state.attachments_by_tag;
    if (!attachments[lang]) return true;

    const lang_attachments = attachments[lang];
    if (!lang_attachments[tag]) return true;

    if (lang_attachments[tag].isFetching) {
        return false;
    } else {
        return (Date.now() - lang_attachments[tag].lastUpdated) > (1000 * 60 * 10); //   10 minutes old, refetch
    }

}

export function fetchAttachmentsIfNeeded(unit, course, lesson, topic, lang) {
    console.debug('fetchAttachmentsIfNeeded', unit, course, lesson, topic, lang);
    return (dispatch, getState) => {
        let tag;

        if (topic && topic != -1)
            tag = 'T' + topic;
        else if (lesson && topic == -1)
            tag = 'L' + lesson;
        else if (course && lesson == -1)
            tag = 'C' + course;
        else if (unit && course == -1)
            tag = 'U' + unit;

        if (shouldFetchAttachments(getState(), tag, lang)) {
            return dispatch(fetchAttachments(tag, lang));
        } else {
            return Promise.resolve();
        }
    }
}

function fetchAttachmentDataSuccess(attach_id, lang, attachment) {
    return {
        type: FETCH_ATTACHMENT_DATA_SUCCESS,
        attach_id,
        lang,
        attachment,
        receivedAt: Date.now()
    }
}

function fetchAttachmentDataRequest(attach_id, lang) {
    return {
        type: FETCH_ATTACHMENT_DATA_REQUEST,
        attach_id,
        lang
    }
}

function fetchAttachmentDataFailure(ex) {
    return {
        type: FETCH_ATTACHMENT_DATA_FAILURE,
        ex
    }
}

export function fetchAttachmentData(attach_id, lang) {
    // console.debug('fetchPages', topicid, pageid, pattern);
    return function(dispatch) {
        dispatch(fetchAttachmentDataRequest(attach_id, lang));
        console.debug('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_attach_data?attach_id=' + attach_id);
        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_attach_data?attach_id=' + attach_id, {
            timeout: 20000
        })
            .then(
                response => response.json(),
                ex => dispatch(fetchAttachmentDataFailure(ex))
            )
            .then(json => dispatch(fetchAttachmentDataSuccess(attach_id, lang, json.ResultSets[0][0])))
            // .catch(ex => dispatch(fetchAttachmentDataFailure(ex)));
    }
}

function doLoginLessonSuccess(session_id, login_session_uid, lesson_id, lang) {
    // console.debug('login_session_uid: ' + login_session_uid);
    // lang = lang.toUpperCase();
    return { 
        type: DO_LOGIN_LESSON_SUCCESS,
        session_id,
        login_session_uid,
        lesson_id,
        lang,
        receivedAt: Date.now()
    };
}

function doLoginLessonFailure(ex) {
    return {
        type: DO_LOGIN_LESSON_FAILURE,
        ex
    }
}

function doLoginLessonRequest() {
    return { type: DO_LOGIN_LESSON_REQUEST };
}


function shouldDoLoginLesson(state, lesson_id, lang) {
    console.debug('shouldDoLoginLesson: ' + lesson_id + ', ' + lang);
    if (!lesson_id) return false;
    const sessions = state.filters.sessions;
    // console.debug(sessions);
    if (!sessions[lang]) return true;
    const session = sessions[lang][lesson_id];
    console.debug(session);

    if (!session) {
        return true;
    } else if (session.isFetching) {
        return false
    } else {
        return (Date.now() - session.lastUpdated) > (1000 * 60 * 15); //   15 minutes old, refetch
    }
}


function doLoginLesson(lesson_id, lang) {
    return function(dispatch) {
        dispatch(doLoginLessonRequest());

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_init_review_session?lesson_id=' + lesson_id + '&lang_code=' + lang)
            .then(
                response => response.json(),
                ex => dispatch(doLoginLessonFailure(ex))
            )
            .then(json => dispatch(doLoginLessonSuccess(json.ResultSets[0][0].session_id, json.ResultSets[0][0].luid_str, lesson_id, lang)))
            // .catch(ex => dispatch(doLoginLessonFailure(ex)));
    }

}

export function doLoginIfNeeded(lesson_id, lang) {
    return (dispatch, getState) => {
        if (shouldDoLoginLesson(getState(), lesson_id, lang)) {
            // console.debug('doLogin');
            return dispatch(doLoginLesson(lesson_id, lang));
        } else {
            return Promise.resolve();
        }
    }
}

export function updateAttachFieldSuccess(attach_id, fieldName, fieldVal, lang, user) {
    return {
        type: UPDATE_FIELD_SUCCESS,
        attach_id,
        fieldName,
        fieldVal,
        lang,
        user
    }
}

export function updateAttachFieldRequest(attach_id, fieldName, fieldVal, user) {
    console.debug(arguments);
    return {
        type: UPDATE_FIELD_REQUEST,
        attach_id,
        fieldName,
        fieldVal,
        user
    }
}

export function updateAttachFieldFailure(attach_id, fieldName, ex) {
    return {
        type: UPDATE_FIELD_FAILURE,
        attach_id,
        fieldName,
        ex
    }
}

/*  Immediately update the field without having to post update which will wait for onBlur   */
export function immediateUpdateAttachField(attach_id, fieldName, fieldVal, lang, user) {
    return function(dispatch) {
        dispatch(updateAttachFieldSuccess(attach_id, fieldName, fieldVal, lang, user));
    }
}

export function updateAttachField(attach_id, fieldName, fieldVal, lang, user) {
    console.debug('updateAttachField', attach_id, fieldName, fieldVal, lang, user);
    return function(dispatch) {
        dispatch(updateAttachFieldRequest(attach_id, fieldName, fieldVal, user));
        // dispatch(updateAttachFieldSuccess(topic, pageid, fieldName, fieldVal));
        // let lang_code = invert(LangOptions)[lang];

        console.debug('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_attach_setval?attach_id=' + attach_id + '&varname=' + fieldName + '&varval=' + fieldVal + '&update_by=' + user);
        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_attach_setval?attach_id=' + attach_id + '&varname=' + fieldName + '&varval=' + fieldVal + '&update_by=' + user, { timeout: 20000 })
            .then(
                response => response.json(),
                ex => dispatch(updateAttachFieldFailure(attach_id, fieldName, ex))
            )
            .then(json => {
                // console.debug(json);
                // console.debug(json.ResultSets);
                // console.debug(json.ResultSets[0][0]);
                let status = json.ResultSets[0][0].status;
                // console.debug('status: ' + status);
                if (status < 100) {
                    dispatch(updateAttachFieldSuccess(attach_id, fieldName, fieldVal, lang, user))
                } else {
                    // console.debug('failed');
                    dispatch(updateAttachFieldFailure(attach_id, fieldName, json.ResultSets[0].statdesc))
                }

                
            })
            .catch(ex => dispatch(updateAttachFieldFailure(attach_id, fieldName, ex)));
    }
}

function fetchPageDetailSuccess(pageid, page_data) {
    return {
        type: FETCH_PAGE_DETAIL_SUCCESS,
        pageid,
        page_data
    }
}

function fetchPageDetailRequest(pageid) {
    return {
        type: FETCH_PAGE_DETAIL_REQUEST,
        pageid
    }
}

function fetchPageDetailFailure(pageid, ex) {
    return {
        type: FETCH_PAGE_DETAIL_FAILURE,
        pageid,
        ex
    }
}

export function fetchPageDetail(pageid) {
    return function(dispatch) {
        dispatch(fetchPageDetailRequest(pageid));
        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_page_details?pageid=' + pageid)
            .then(
                response => response.json(),
                ex => dispatch(fetchPageDetailFailure(pageid, ex))
            )
            .then(json => dispatch(fetchPageDetailSuccess(pageid, json.ResultSets[0][0])))
            .catch(ex => dispatch(fetchPageDetailFailure(pageid, ex)));
    }
}