import { Component } from 'react';

// import { invert } from 'lodash';

export default class ReadOnlyFields extends Component {
    render() {
        // let inverted_people = invert(this.props.people);

        return (
            <Container style={{overflow:'auto'}}>
                <Grid>
                    <Row>
                        {
                            this.props.readOnlyFields.map((field, ind) => {
                                if (this.props.row[field.name]) {
                                    return (
                                        <Col sm={3} key={field.name + "_" + this.props.index}>
                                            <BLabel>{(field.label) ? field.label : field.name}</BLabel>
                                            {this.props.row[field.name]}
                                        </Col>
                                    )
                                } else {
                                    return '';
                                }
                            })
                        }
                    </Row>
                </Grid>
            </Container>
        )
    }
}

