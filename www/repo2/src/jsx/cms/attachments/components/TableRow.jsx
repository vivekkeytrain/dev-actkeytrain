import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import { invert } from 'lodash';
// import { chain, filter, max, sortByOrder, find } from 'lodash'

// import { Link } from 'react-router';
import EditableFields from './EditableFields';
import ReadOnlyFields from './ReadOnlyFields';

import { LangOptions } from '../actions';

export default class TableHeader extends Component {
    onPageLinkClick(pageid, lang_id, e) {
        //  Don't want it to open the PageDetail
        e.stopPropagation();
        e.preventDefault();

        this.props.onPageLinkClick(pageid, lang_id);
    }

    renderPageLinks(row) {
        let pageID = row.pageid;
        let lang_code = invert(LangOptions)[row.lang_id];

        let url = 'http://dev.run.keytrain.com/objects2/' + this.props.courseTag + '/' + this.props.lessonTag + '/' + lang_code.toLowerCase() + '/html5_course.htm#load/' + this.props.enSession.session_id + '/' + this.props.enSession.login_session_uid + '/pageid/' + pageID;
        // console.debug(url);
        // let en_url = 'http://dev.run.keytrain.com/objects2/' + this.props.courseTag + '/' + this.props.lessonTag + '/en/html5_course.htm#load/' + this.props.enSession.session_id + '/' + this.props.enSession.login_session_uid + '/pageid/' + pageID;
        // let es_url = 'http://dev.run.keytrain.com/objects2/' + this.props.courseTag + '/' + this.props.lessonTag + '/es/html5_course.htm#load/' + this.props.esSession.session_id + '/' + this.props.esSession.login_session_uid + '/pageid/' + pageID;

        // let en_key = pageID + "_en";
        // let es_key = pageID + "_es";

        // console.debug(en_url);

        return <a href={url} target={"_lesson_" + lang_code.toLowerCase()} key={pageID + "_link"} onClick={this.onPageLinkClick.bind(this, pageID, row.lang_id)}>{lang_code.toUpperCase()}</a>

        // return [<a href={en_url} target="_lesson_en" key={en_key} onClick={this.onPageLinkClick.bind(this, pageID, LangOptions.EN)}>EN</a>, ' / ', <a href={es_url} target="_lesson_es" key={es_key} onClick={this.onPageLinkClick.bind(this, pageID, LangOptions.ES)}>ES</a>];
    }

    renderDisplayFields() {
        if (this.props.selectedRow == this.props.index) {
            return this.props.editViewDisplayFields.map((displayField, name) => {
                return this.renderCell(displayField, this.props.row);
            }, this);
        } else {
            return this.props.displayFields.map((displayField, ind) => {
                return this.renderCell(displayField, this.props.row);
            }, this);
        }
    }

    renderCell(displayField, row) {
        // console.debug(this);
        // let displayField = find(this.props.displayFields, {'name': name});
        // let displayField = this.props.displayFieldsHash[name];

        // console.debug(displayField);
        let cellData = row[displayField.name];
        if (typeof displayField.displayFunc === 'function') {
            let dfunc = displayField.displayFunc.bind(this, cellData, row);
            cellData = dfunc();
        }

        if (typeof cellData === 'boolean') {
            cellData = (cellData) ? <i className="glyphicon glyphicon-ok"></i> : <i className="glyphicon glyphicon-minus"></i>;
        }

        // console.debug('renderCell', name, cellData);
        return <td>{cellData}</td>;
    }

    render() {
        let classes = classNames({
            success: (this.props.selectedRow == this.props.index)
        });

        // console.debug(this.props);
        // console.debug((this.props.selectedRow == this.props.index));

        return(
            <tr onClick={this.props.onRowSelected.bind(this, this.props.index)} className={classes} ref={(ref) => this["row_" + this.props.index] = ref}>
                <td>{this.renderPageLinks(this.props.row)}</td>
                {
                    this.renderDisplayFields()
                    /*this.props.displayFields.map((field, ind) => {
                        return this.renderCell(field.name, this.props.row, this.props.selectedRow, this.props.index)
                    }, this)*/
                }

                <td colSpan={this.props.displayFields.length}>{(this.props.selectedRow == this.props.index) ? <EditableFields {...this.props} /> : <ReadOnlyFields {...this.props} />}</td>
            </tr>
        )

        // return (
        //     <tr onClick={this.props.onRowSelected.bind(this, this.props.index)} className={classes} ref={(ref) => this["row_" + this.props.index] = ref}>
        //         <td>{this.renderPageLinks(this.props.row.pageid)}</td>
        //         {
        //             this.props.displayFields.map((field, ind) => {
        //                 return <td>{this.renderCell(field.name, this.props.row)}</td>
        //             })
        //         }
        //         <td>{(this.props.selectedRow == this.props.index) ? <EditableFields {...this.props} /> : <ReadOnlyFields {...this.props} />}</td>
        //     </tr>
        // )
    }
}


