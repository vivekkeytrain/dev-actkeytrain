import { Component } from 'react';
import { LangOptions } from '../actions';

import { invert } from 'lodash';

export default class PageLink extends Component {

    render() {
        // console.debug(this.props);
        let lang_code = invert(LangOptions)[this.props.lang]
        let url = 'http://es.run.keytrain.com/objects/' + this.props.courseTag + '/' + this.props.lesson + '/' + lang_code + '/html5_course.htm#load/' + this.props.sessionId + '/' + this.props.loginSessionUID + '/pageid/' + this.props.pageID;
        // console.debug(url);
        
        let target="_lesson_" + lang_code;

        return <a href={url} target={target}>{lang_code}</a>
    }
}

