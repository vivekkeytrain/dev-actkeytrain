import { Component } from 'react';
import { map, indexOf } from 'lodash';

export default class PageFieldSelect extends Component {
    // state = {
    //     def_ind: -1
    // }

    handleImmediateFieldChange(val) {
        if (val == -1) return;
        this.props.onImmediateFieldChange(this.props.page.PageID, this.props.name, val);
    }

    onFieldChange(name, e) {

        // console.log('onFieldChange', e.target.value);
        // console.debug(this.props.page.PageID, name, e.target.value);
        // let def_ind = indexOf(this.props.set, e.target.value)
        
        this.setState({
            def_ind: e.target.value
        });

        let val = (e.target.value != -1) ? e.target.value : 0;
        // console.debug(e.target.value, val);
        this.props.onFieldChange(this.props.page.PageID, name, val);
    }

    componentWillMount() {
        // console.debug(this.props);
        let def_ind = this.props.page[this.props.name];
        // console.debug('name', this.props.name);
        // console.debug('def_ind', def_ind);
        // console.debug('defaultVal', this.props.defaultVal);
        if (!def_ind && this.props.defaultVal) {
            // console.debug('find default');
            def_ind = indexOf(this.props.set, this.props.defaultVal);
            // console.debug('def_ind now', def_ind);
            if (this.props.onImmediateFieldChange) {
                this.handleImmediateFieldChange(def_ind);
            }
        }

        this.setState({
            def_ind: def_ind
        });
    }

    // componentWillReceiveProps(props) {
    //     let def_ind = props.page[props.name];
    //     console.debug('name', props.name);
    //     console.debug('def_ind', def_ind);
    //     console.debug('defaultVal', props.defaultVal);
    //     if (!def_ind && props.defaultVal) {
    //         // console.debug('find default');
    //         def_ind = indexOf(props.set, props.defaultVal);
    //         console.debug('def_ind now', def_ind);
    //         if (props.onImmediateFieldChange) {
    //             // this.handleImmediateFieldChange(def_ind);
    //         }
    //     }

    //     this.setState({
    //         def_ind: def_ind
    //     });
    // }

    render() {

        

        // indexOf(this.props.set, )
        // console.debug(this.props.set);
        // console.debug(this.props.page[this.props.name]);
        // console.debug((this.props.defaultVal) ? this.props.defaultVal : this.props.page[this.props.name]);



        return (
            <select name={this.props.name} value={this.state.def_ind} onChange={this.onFieldChange.bind(this, this.props.name)} onBlur={this.onFieldChange.bind(this, this.props.name)}>
                <option value="-1"></option>
                {map(this.props.set, (stati, val) => 
                    <option value={val} key={val}>{stati}</option>
                )}
            </select>
        )
    }
}

PageFieldSelect.state = {
    def_ind: -1
}

