import { Component } from 'react';

import PageFieldSelect from './PageFieldSelect';
import PageTextField from './PageTextField';

// import { invert } from 'lodash';

export default class ReadOnlyFields extends Component {
    render() {
        // let inverted_people = invert(this.props.people);

        return (
            <Container style={{overflow:'auto'}}>
                <Grid>
                    <Row>
                        {(() => {
                            if (this.props.page.reviewed_by) {
                                return (
                                    <Col sm={3}>
                                        <BLabel>Reviewed By</BLabel>
                                        {this.props.page.reviewed_by}
                                    </Col>
                                )
                            }
                        })()}
                        {(() => {
                            if (this.props.page.status_val) {
                                return (
                                    <Col sm={3}>
                                        <BLabel>Status</BLabel>
                                        {this.props.stati[this.props.page.status_val]}
                                    </Col>
                                )
                            }
                        })()}
                        {(() => {
                            if (this.props.page.status_val > 1 && this.props.page.problem_bits) {
                                //  Only show the problem bits if status isn't perfect
                                return (
                                    <Col sm={3}>
                                        <BLabel>Problem</BLabel>
                                        {this.props.problemTypes[this.props.page.problem_bits]}
                                    </Col>
                                )
                            }
                        })()}
                        {(() => {
                            if (this.props.page.status_val > 1 && this.props.page.problem1_desc != '' && this.props.page.problem1_desc != null) {
                                //  Only show problem text field if problem_bits is set
                                return (
                                    <Col sm={3}>
                                        <BLabel>Problem Desc</BLabel>
                                        {this.props.page.problem1_desc}
                                    </Col>
                                );
                            } 
                        })()}
                        {(() => {
                            if (this.props.page.status_val > 1 && this.props.page.problem2_desc != '' && this.props.page.problem2_desc != null) {
                                //  Only show the second problem desc field if the first has data
                                return (
                                    <Col sm={3}>
                                        <BLabel>Problem Desc 2</BLabel>
                                        {this.props.page.problem2_desc}
                                    </Col>
                                );
                            } 
                        })()}
                        {(() => {
                            if (this.props.page.status_val > 1 && this.props.page.solution_desc != '' && this.props.page.solution_desc != null) {
                                //  Don't need the solution fields if we don't have a problem
                                return (
                                    <Col sm={3}>
                                        <BLabel>Solution</BLabel>
                                        {this.props.page.solution_desc}
                                    </Col>
                                )
                            }
                        })()}
                        {(() => {
                            if (this.props.page.status_val > 1 && this.props.page.details_desc) {
                                return (
                                    <Col sm={3}>
                                        <BLabel>Details</BLabel>
                                        {this.props.page.details_desc}
                                    </Col>
                                )
                            }
                        })()}
                        {(() => {
                            if (this.props.page.status_val > 1 && this.props.page.fix_desc) {
                                return (
                                    <Col sm={3}>
                                        <BLabel>Details</BLabel>
                                        {this.props.page.fix_desc}
                                    </Col>
                                )
                            }
                        })()}
                        {(() => {
                            if (this.props.page.status_val > 1 && this.props.page.assigned_to) {
                                //  Don't need the assigned_to and fixed_by if we don't have a problem
                                return (
                                    <Col sm={3}>
                                        <BLabel>Assigned To</BLabel>
                                        {this.props.page.assigned_to}
                                    </Col>
                                )
                            }
                        })()}
                        {(() => {
                            if (this.props.page.status_val > 1 && this.props.page.fixed_by) {
                                return (
                                    <Col sm={3}>
                                        <BLabel>Fixed By</BLabel>
                                        {this.props.page.fixed_by}
                                    </Col>
                                )
                            }
                        })()}
                    </Row>
                </Grid>
            </Container>
        )
    }
}

