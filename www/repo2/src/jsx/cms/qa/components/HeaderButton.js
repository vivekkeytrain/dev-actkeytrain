import { Component } from 'react';
// import classNames from 'classnames';
import { map } from 'lodash';

export default class HeaderButton extends Component {
    render() {
        // let classes = classNames({
        //     'btn': true,
        //     'btn-primary': true,
        //     'active': (this.props.val == this.props.active)
        // });

        return <Button sm outlined bsStyle='darkblue' active={this.props.val == this.props.active} onClick={() => this.props.clickHandler(this.props.val) }>{this.props.label}</Button>
        // return <button type="button" className={classes} onClick={() => this.props.clickHandler(this.props.val) }>{this.props.label}</button>
    }
}

