import { Component, PropTypes } from 'react';

export default class Filters extends Component {
    renderSelect(name, currentVal, options, changeHandler) {
        return (
            <label htmlFor={name}>
                <select name={name} value={currentVal}>
                    {options.map((option, index) =>
                        <option value={option}>{option}</option>
                    )}
                </select>
            </label>
        );
    }

    render() {
        return (
            <div className="row">
                {this.renderSelect('Lang', this.props.lang, this.props.langOptions, this.props.onUpdateLang)}
                {this.renderSelect('Mode', this.props.mode, this.props.modeFilters, this.props.onUpdateMode)}
            </div>
        )
    }
}

Filters.propTypes = {
    onUpdateLang: PropTypes.func.isRequired,
    onUpdateMode: PropTypes.func.isRequired,
    lang: PropTypes.oneOf([
        'EN', 'ES'
    ]).isRequired,
    mode: PropTypes.oneOf([
        'QA', 'REVIEW', 'NO_GO'
    ]).isRequired
}