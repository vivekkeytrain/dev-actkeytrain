import fetchJsonp from 'fetch-jsonp';
import { invert } from 'lodash';

/*  Action types    */

export const SET_LANG = 'SET_LANG';
export const SET_UNIT = 'SET_UNIT';
export const SET_COURSE = 'SET_COURSE';
export const SET_LESSON = 'SET_LESSON';
export const SET_TOPIC = 'SET_TOPIC';
export const SET_MODE = 'SET_MODE';
export const SET_USER = 'SET_USER';
export const SET_SELECTED_ROW = 'SET_SELECTED_ROW';
export const SET_PEOPLE_FILTER = 'SET_PEOPLE_FILTER';

/*  Async action types   */

export const FETCH_EDITOR_CONTENT_SUCCESS = 'FETCH_EDITOR_CONTENT_SUCCESS';
export const FETCH_EDITOR_CONTENT_FAILURE = 'FETCH_EDITOR_CONTENT_FAILURE';
export const FETCH_EDITOR_CONTENT_REQUEST = 'FETCH_EDITOR_CONTENT_REQUEST';

export const FETCH_PAGES_REQUEST = 'FETCH_PAGES_REQUEST';
export const FETCH_PAGES_FAILURE = 'FETCH_PAGES_FAILURE';
export const FETCH_PAGES_SUCCESS = 'FETCH_PAGES_SUCCESS';

export const UPDATE_FIELD_REQUEST = 'UPDATE_FIELD_REQUEST';
export const UPDATE_FIELD_FAILURE = 'UPDATE_FIELD_FAILURE';
export const UPDATE_FIELD_SUCCESS = 'UPDATE_FIELD_SUCCESS';

export const FETCH_STATUS_VALS_REQUEST = 'FETCH_STATUS_VALS_REQUEST';
export const FETCH_STATUS_VALS_FAILURE = 'FETCH_STATUS_VALS_FAILURE';
export const FETCH_STATUS_VALS_SUCCESS = 'FETCH_STATUS_VALS_SUCCESS';

export const FETCH_PROBLEM_BITS_REQUEST = 'FETCH_PROBLEM_BITS_REQUEST';
export const FETCH_PROBLEM_BITS_FAILURE = 'FETCH_PROBLEM_BITS_FAILURE';
export const FETCH_PROBLEM_BITS_SUCCESS = 'FETCH_PROBLEM_BITS_SUCCESS';

export const FETCH_PEOPLE_REQUEST = 'FETCH_PEOPLE_REQUEST';
export const FETCH_PEOPLE_FAILURE = 'FETCH_PEOPLE_FAILURE';
export const FETCH_PEOPLE_SUCCESS = 'FETCH_PEOPLE_SUCCESS';

export const DO_LOGIN_LESSON_REQUEST = 'DO_LOGIN_LESSON_REQUEST';
export const DO_LOGIN_LESSON_SUCCESS = 'DO_LOGIN_LESSON_SUCCESS';
export const DO_LOGIN_LESSON_FAILURE = 'DO_LOGIN_LESSON_FAILURE';

export const SET_SESSION = 'SET_SESSION';

/*  Other contstants    */

export const LangOptions = {
    EN: 1,
    ES: 2
};

export const ModeFilters = {
    REVIEW: 'Review',
    CORRECT: 'Correct',
    CHECK: 'Check'
};


/*  Action creators */

export function setLang(lang) { 
    return { type: SET_LANG, lang };
}

export function setUnit(unit) {
    return function(dispatch) {
        localStorage.setItem('content_unit', unit);
        dispatch(setUnitStore(unit));
    }
}

export function setUnitStore(unit) {
    return { type: SET_UNIT, unit };
}

export function setCourse(course) {
    return function(dispatch) {
        localStorage.setItem('content_course', course);
        dispatch(setCourseStore(course));
    }
}

export function setCourseStore(course) {
    return { type: SET_COURSE, course };
}

export function setLesson(lesson) {
    return function(dispatch) {
        localStorage.setItem('content_lesson', lesson);
        dispatch(setLessonStore(lesson));
    }
}

export function setLessonStore(lesson) {
    return { type: SET_LESSON, lesson };
}

export function setTopic(topic) {
    return function(dispatch) {
        localStorage.setItem('content_topic', topic);
        dispatch(setTopicStore(topic));
    }
}

export function setTopicStore(topic) {
    return { type: SET_TOPIC, topic };
}

export function setMode(mode) {
    return { type: SET_MODE, mode };
}

export function setUser(user) {
    return { type: SET_USER, user };
}

export function setSelectedRow(row_index) {
    return { type: SET_SELECTED_ROW, row_index };
}

export function setSession(session_id, login_session_uid) {
    return {
        type: SET_SESSION,
        session_id,
        login_session_uid
    }
}

export function setPeopleFilter(person) {
    return {
        type: SET_PEOPLE_FILTER,
        person
    }
}

// export function setTopicSlug(slug) {
//     return { type: SET_TOPIC_SLUG, slug };
// }

/*  Async actions   */

export function fetchEditorContentSuccess(content) {
    // console.debug('fetchEditorContentSuccess');
    // console.debug(content);
    return {
        type: FETCH_EDITOR_CONTENT_SUCCESS,
        content
    }
}

export function fetchEditorContentFailure(ex) {
    return {
        type: FETCH_EDITOR_CONTENT_FAILURE,
        ex
    }
}

export function fetchEditorContentRequest() {
    return { type: FETCH_EDITOR_CONTENT_REQUEST };
}

export function fetchEditorContent() {
    // console.log('fetchEditorContent');
    return function(dispatch) {
        dispatch(fetchEditorContentRequest());

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_content_list/jsonp')
            .then(
                response => response.json(),
                ex => dispatch(fetchEditorContentFailure(ex))
            )
            .then(json => dispatch(fetchEditorContentSuccess(json.ResultSets[0])))
            .catch(ex => dispatch(fetchEditorContentFailure(ex)));
    }
}

function fetchPagesSuccess(lang, tag, pages) {
    return {
        type: FETCH_PAGES_SUCCESS,
        lang,
        tag,
        pages,
        receivedAt: Date.now()
    }
}

function fetchPagesRequest(lang, tag) {
    return {
        type: FETCH_PAGES_REQUEST,
        lang,
        tag
    }
}

function fetchPagesFailure(ex) {
    return {
        type: FETCH_PAGES_FAILURE,
        ex
    }
}

function fetchPages(tag, lang) {
    // console.debug('fetchPages: ' + topic + ', '+ lang);
    return function(dispatch) {
        dispatch(fetchPagesRequest(lang, tag));
        let lang_code = invert(LangOptions)[lang];
        console.debug('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_qa_pages?content_tag=' + tag + '&lang_code=' + lang_code);
        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_qa_pages?content_tag=' + tag + '&lang_code=' + lang_code)
            .then(
                response => response.json(),
                ex => dispatch(fetchPagesFailure(ex))
            )
            .then(json => dispatch(fetchPagesSuccess(lang, tag, json.ResultSets[0])))
            .catch(ex => dispatch(fetchPagesFailure(ex)));
    }
}
/*
    pages_by_tag: {
        'EN': {
            T1007: {
                isFetching: false,
                items: [5,6,7,8],
                lastUpdated: 123456
            },
            L105: {
                isFetching: false,
                items: [...],
                lastUpdated: 123456
            }
        }
    }
    pages: {
        'EN': {
            5: {
                ...
            },
            6: {
                ...
            }
        }
        'ES': {
            ...
        }
    },
*/

function shouldFetchPages(state, tag, lang) {
    console.debug('shouldFetchPages', tag, lang);
    if (!tag || !lang) return false;
    
    const pages = state.pages_by_tag;
    if (!pages[lang]) return true;

    const lang_pages = pages[lang];
    if (!lang_pages[tag]) return true;

    if (lang_pages[tag].isFetching) {
        return false;
    } else {
        return (Date.now() - lang_pages[tag].lastUpdated) > (1000 * 60 * 10); //   10 minutes old, refetch
    }

}

export function fetchPagesIfNeeded(unit, course, lesson, topic, lang) {
    console.debug('fetchPagesIfNeeded', unit, course, lesson, topic, lang);
    return (dispatch, getState) => {
        let tag;
        if (topic && topic != -1)
            tag = 'T' + topic;
        else if (lesson && topic == -1)
            tag = 'L' + lesson;
        else if (course && lesson == -1)
            tag = 'C' + course;
        else if (unit && course == -1)
            tag = 'U' + unit;

        if (shouldFetchPages(getState(), tag, lang)) {
            console.debug('doing fetch');
            return dispatch(fetchPages(tag, lang));
        } else {
            return Promise.resolve();
        }
    }
}

export function updatePageFieldSuccess(pageid, fieldName, fieldVal, lang) {
    return {
        type: UPDATE_FIELD_SUCCESS,
        pageid,
        fieldName,
        fieldVal,
        lang
    }
}

export function updatePageFieldRequest(pageid, fieldName, fieldVal) {
    // console.debug(arguments);
    return {
        type: UPDATE_FIELD_REQUEST,
        pageid,
        fieldName,
        fieldVal
    }
}

export function updatePageFieldFailure(pageid, fieldName, ex) {
    return {
        type: UPDATE_FIELD_FAILURE,
        pageid,
        fieldName,
        ex
    }
}

/*  Immediately update the field without having to post update which will wait for onBlur   */
export function immediateUpdatePageField(pageid, fieldName, fieldVal, lang) {
    return function(dispatch) {
        dispatch(updatePageFieldSuccess(pageid, fieldName, fieldVal, lang));
    }
}

export function updatePageField(pageid, fieldName, fieldVal, lang) {
    // console.debug('updatePageField');
    return function(dispatch) {
        dispatch(updatePageFieldRequest(pageid, fieldName, fieldVal));
        // dispatch(updatePageFieldSuccess(topic, pageid, fieldName, fieldVal));
        let lang_code = invert(LangOptions)[lang].toLowerCase();

        console.debug('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_qa_setval?pageid=' + pageid + '&lang_code=' + lang_code + '&varname=' + fieldName + '&varval=' + fieldVal);
        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_qa_setval?pageid=' + pageid + '&lang_code=' + lang_code + '&varname=' + fieldName + '&varval=' + fieldVal, { timeout: 20000 })
            .then(
                response => response.json(),
                ex => dispatch(updatePageFieldFailure(pageid, fieldName, ex))
            )
            .then(json => {
                // console.debug(json);
                // console.debug(json.ResultSets);
                // console.debug(json.ResultSets[0][0]);
                let status = json.ResultSets[0][0].status;
                // console.debug('status: ' + status);
                if (status < 100) {
                    dispatch(updatePageFieldSuccess(pageid, fieldName, fieldVal, lang))
                } else {
                    // console.debug('failed');
                    dispatch(updatePageFieldFailure(pageid, fieldName, json.ResultSets[0].statdesc))
                }

                
            })
            .catch(ex => dispatch(updatePageFieldFailure(pageid, fieldName, ex)));
    }
}

export function fetchStatusValsSuccess(vals) {
    return {
        type: FETCH_STATUS_VALS_SUCCESS,
        vals: vals
    }
}

export function fetchStatusValsRequest() {
    return { type: FETCH_STATUS_VALS_REQUEST };
}

export function fetchStatusValsFailure(ex) {
    return {
        type: FETCH_STATUS_VALS_FAILURE,
        ex: ex
    }
}

export function fetchProblemBitsSuccess(vals) {
    return {
        type: FETCH_PROBLEM_BITS_SUCCESS,
        vals: vals
    }
}

export function fetchProblemBitsRequest() {
    return { type: FETCH_PROBLEM_BITS_REQUEST };
}

export function fetchProblemBitsFailure(ex) {
    return {
        type: FETCH_PROBLEM_BITS_FAILURE,
        ex: ex
    }
}

export function fetchGenStati(type) {
    switch (type) {
        case 'status_val':
            return function(dispatch) {
                dispatch(fetchStatusValsRequest());

                return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/gen.sp_get_stati?objname=qa&valname=status_val')
                    .then(
                        response => response.json(),
                        ex => dispatch(fetchStatusValsFailure(ex))
                    )
                    .then(json => dispatch(fetchStatusValsSuccess(json.ResultSets[0])))
                    .catch(ex => dispatch(fetchStatusValsFailure(ex)));
            }
        case 'problem_bits':
            return function(dispatch) {
                dispatch(fetchProblemBitsRequest());

                return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/gen.sp_get_stati?objname=qa&valname=problem_bits')
                    .then(
                        response => response.json(),
                        ex => dispatch(fetchProblemBitsFailure(ex))
                    )
                    .then(json => dispatch(fetchProblemBitsSuccess(json.ResultSets[0])))
                    .catch(ex => dispatch(fetchProblemBitsFailure(ex)));
            }
    }
}

export function fetchPeopleSuccess(people) {
    return {
        type: FETCH_PEOPLE_SUCCESS,
        people
    }
}

export function fetchPeopleRequest() {
    return { type: FETCH_PEOPLE_REQUEST };
}

export function fetchPeopleFailure(ex) {
    return {
        type: FETCH_PEOPLE_FAILURE,
        ex
    }
}

export function fetchPeople(lang) {
    return function(dispatch) {
        dispatch(fetchPeopleRequest());

        let lang_code = invert(LangOptions)[lang];

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_ui_users?context=qa&lang_code=' + lang_code)
            .then(
                response => response.json(),
                ex => dispatch(fetchPeopleFailure(ex))
            )
            .then(json => dispatch(fetchPeopleSuccess(json.ResultSets[0])))
            .catch(ex => dispatch(fetchPeopleFailure(ex)));
    }
}

function doLoginLessonSuccess(session_id, login_session_uid, lesson_id, lang) {
    // console.debug('login_session_uid: ' + login_session_uid);

    return { 
        type: DO_LOGIN_LESSON_SUCCESS,
        session_id,
        login_session_uid,
        lesson_id,
        lang,
        receivedAt: Date.now()
    };
}

function doLoginLessonFailure(ex) {
    return {
        type: DO_LOGIN_LESSON_FAILURE,
        ex
    }
}

function doLoginLessonRequest() {
    return { type: DO_LOGIN_LESSON_REQUEST };
}


function shouldDoLoginLesson(state, lesson_id, lang) {
    // console.debug('shouldDoLoginLesson: ' + lesson_id + ', ' + lang);
    if (!lesson_id) return false;
    const sessions = state.filters.sessions;
    // console.debug(sessions);
    if (!sessions[lang]) return true;
    const session = sessions[lang][lesson_id];
    // console.debug(session);

    if (!session) {
        return true;
    } else if (session.isFetching) {
        return false
    } else {
        return (Date.now() - session.lastUpdated) > (1000 * 60 * 15); //   15 minutes old, refetch
    }
}


function doLoginLesson(lesson_id, lang) {
    return function(dispatch) {
        dispatch(doLoginLessonRequest());

        let lang_code = invert(LangOptions)[lang];

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_init_review_session?lesson_id=' + lesson_id + '&lang_code=' + lang_code)
            .then(
                response => response.json(),
                ex => dispatch(doLoginLessonFailure(ex))
            )
            .then(json => dispatch(doLoginLessonSuccess(json.ResultSets[0][0].session_id, json.ResultSets[0][0].luid_str, lesson_id, lang)))
            .catch(ex => dispatch(doLoginLessonFailure(ex)));
    }
}

export function doLoginIfNeeded(lesson_id, lang) {
    return (dispatch, getState) => {
        if (shouldDoLoginLesson(getState(), lesson_id, lang)) {
            // console.debug('doLogin');
            return dispatch(doLoginLesson(lesson_id, lang));
        }
    }
}

// export function doScormInitSuccess(launch_data) {
//     return {
//         type: DO_SCORM_INIT_SUCCESS,
//         html5_path: launch_data.html5_path,
//         session_id: launch_data.session_id,
//         site_url: launch_data.site_url
//     }
// }

// export function doScormInitFailure(ex) {
//     return {
//         type: DO_SCORM_INIT_FAILURE,
//         ex
//     }
// }

// export function doScormInitRequest() {
//     return { type: DO_SCORM_INIT_REQUEST };
// }

// export function doScormInit() {
//     return function(dispatch) {
//         dispatch(doScormInitRequest());

//         return
//     }
// }