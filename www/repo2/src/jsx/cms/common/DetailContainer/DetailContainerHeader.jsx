import React, { Component, PropTypes } from 'react';

export default class DetailContainerHeader extends Component {
    render() {
        return (
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.closeDetailContainer.bind(this)}><span aria-hidden="true">×</span></button>
                <Grid>
                    <Row>
                        {this.props.children}
                    </Row>
                </Grid>
            </div>
        )
    }
}

// DetailContainerHeader.propTypes = {
//     closeDetailContainer: PropTypes.func.isRequired
// }