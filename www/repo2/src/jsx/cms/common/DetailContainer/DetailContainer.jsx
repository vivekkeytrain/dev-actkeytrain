import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

import { isUndefined } from 'lodash';

// import DetailContainerHeader from './DetailContainerHeader';
// import DetailContainerBody from './DetailContainerBody';

export default class DetailContainer extends Component {
    handleBackgroundClick(e) {
        if (e.target.id === 'modal-container') this.props.closeDetailContainer(e);
    }

    render() {
        let classObj = {'cms-modal-detail': true};

        if (!isUndefined(this.props.extraClass)) {
            classObj[this.props.extraClass] = true;
        }

        let classes = classNames(classObj);

        if (this.props.visible) {
            return (
                <div className={classes}>
                    <div className="modal-backdrop fade in"></div>
                    <div className="cms-modal-detail-modal" id="modal-container" onClick={this.handleBackgroundClick.bind(this)}>
                        <div className="cms-modal-detail">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            )
        } else {
            return <div />
        }
    }
}

// export { DetailContainerHeader, DetailContainerBody };

// DetailContainer.propTypes = {
//     extraClass: PropTypes.string,
//     closeDetailContainer: PropTypes.func.isRequired,
//     visible: PropTypes.bool.isRequired,
//     children: (props, propName, componentName) => {
//         let prop = props[propName];
//         let error;

//         if (prop[0].type !== DetailContainerHeader) return new Error(`First child of ${componentName} must be DetailContainerHeader`);

//         if (props[0].type !== DetailContainerBody) return new Error(`Second child of ${componentName} must be DetailContainerBody`);

//     }
// }

// DetailContainer.defaultProps = {
//     visible: false
// }
