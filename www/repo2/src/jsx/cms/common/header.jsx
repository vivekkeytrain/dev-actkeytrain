import { State, Navigation } from 'react-router';
import classNames from 'classnames';
import { Link } from 'react-router';
import $ from 'jquery';

class Brand extends React.Component {
  render() {
    return (
      <NavHeader {...this.props} className='hidden-xs'>
        <Link to='/'>
          <NavBrand tabIndex='-1'>
            <img src='/imgs/logo.png' alt='ACT Career Curriculum' width='203' height='30' />
          </NavBrand>
        </Link>
      </NavHeader>
    );
  }
}

var PageMenuItem = React.createClass({
  mixins: [State, Navigation],
  componentDidMount(){
    if (window.location.pathname == this.props.path){
      $('#pages_label').html(this.props.label);
    }    
  },
  render() {
    const ACTIVE = { color: 'red' }
    return (
      <MenuItem {...this.props}>
        <Link to={this.props.path} activeStyle={ACTIVE}>{this.props.label}</Link>
      </MenuItem>
    );
  }
});



var HeaderNavigation = React.createClass({
  mixins: [State, Navigation],

  itemSelect() {
    //console.log(this);
    //$(this).toggle();
  },

  render() {
    
    var hidemenu = false; //window.location.pathname == '/';

    var props = {
      ...this.props
      ,hidden: hidemenu
      ,className: classNames('text-center', this.props.className)
    };

    return (
      <NavContent {...props} >
        <Nav>
          <NavItem dropdown >
            <DropdownButton nav>
              <span id='pages_label'>Pages</span> <Icon bundle='fontello' glyph='down-open-mini' /> 
            </DropdownButton>
            <Menu alignRight ref='page-menu' id='notifications-menu' bsStyle='theme' onItemSelect={this.itemSelect}>
              <PageMenuItem path='/qa/' label='Quality Assurance' />
              <PageMenuItem path='/findpage/' label='Find Page' />
              <PageMenuItem path='/attachments/' label='Attachments' />
              <PageMenuItem path='/page-issues/' label='Page Issue Reports' />
              <PageMenuItem path='/svgmanager/' label='SVG Manager' />
            </Menu>
          </NavItem>
        </Nav>
      </NavContent>
    );
  }
});

export default class Header extends React.Component {
  componentDidMount() {
    var timer = window.setTimeout(() => {
      let nav = this._navheader.getDOMNode();
      $(nav).addClass('header-hidden');
    }, 5000);

    this.setState({
      timer
    })
  }

  componentWillUnmount() {
    window.clearTimeout(this.state.timer);
    this.setState({
      timer: null
    })
  }

  handleMouseOver() {
    let nav = this._navheader.getDOMNode();
    $(nav).removeClass('header-hidden');
  }

  handleMouseOut() {
    let nav = this._navheader.getDOMNode();
    $(nav).addClass('header-hidden');
  }

  render() {
    return (
      <Grid id='navbar' {...this.props}>
        <Row>
          <Col xs={12}>
            <NavBar fixedTop id='rubix-nav-header' ref={(r) => this._navheader = r} onMouseOver={this.handleMouseOver.bind(this)} onMouseOut={this.handleMouseOut.bind(this)}>
              <Container>
                <Row>
                  <Col xs={12}>
                    <Brand />
                    <HeaderNavigation />
                  </Col>
                </Row>
              </Container>
            </NavBar>
          </Col>
        </Row>
      </Grid>
    );
  }
}
