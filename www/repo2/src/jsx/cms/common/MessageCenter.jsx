import { Component, PropTypes } from 'react';
import classNames from 'classnames';

class Message extends Component {
    closeButtonClick(e) {
        e.preventDefault();
        this.props.onMessageClose(this.props.index);
    }

    render() {
        // console.debug(this.props);

        // let classes = classNames({
        //     'alert': true,
        //     'alert-success': (this.props.type === 'success'),
        //     'alert-info': (this.props.type === 'info'),
        //     'alert-warning': (this.props.type === 'warning'),
        //     'alert-danger': (this.props.type === 'danger'),
        //     'hidden': (!this.props.visible)
        // })

/*
            <Alert success={this.props.type === 'success'} info={this.props.type === 'info'} warning={this.props.type === 'warning'} danger={this.props.type === 'danger'} collapseBottom dismissible>{this.props.message}</Alert>
            */
        // return (
        //     <div role="alert" className={classes}>
        //         <div>
        //             <button role="button" type="button" className="close" onClick={this.closeButtonClick.bind(this)}>
        //                 <span aria-hidden="true">×</span>
        //                 <span className="sr-only">Close</span>
        //             </button>
        //             <span>{this.props.message}</span>
        //         </div>
        //     </div>


        // )

        let liClasses = classNames({
            'messenger-message-slot': true,
            'messenger-shown': (this.props.visible),
            'messenger-first': (this.props.isFirst),
            'messenger-last': (this.props.isLast)
        })

        let messageClasses = classNames({
            'messenger-message': true,
            'messenger-hidden': (!this.props.visible),
            'message': true,
            'alert': true,
            'info': (this.props.type === 'info'),
            'alert-info': (this.props.type === 'info'),
            'warning': (this.props.type === 'warning'),
            'alert-warning': (this.props.type === 'warning'),
            'error': (this.props.type === 'danger'),
            'alert-error':  (this.props.type === 'danger'),
            'success': (this.props.type === 'success'),
            'alert-success': (this.props.type === 'success')
        });

        return (
            <li className={liClasses}>
                <div className={messageClasses}>
                    <button type="button" className="messenger-close" data-dismiss="alert" onClick={this.closeButtonClick.bind(this)}>×</button>
                    <div className="messenger-message-inner">{this.props.message}</div>
                    <div className="messenger-spinner">
                        <span className="messenger-spinner-side messenger-spinner-side-left">
                            <span className="messenger-spinner-fill"></span>
                        </span>
                        <span className="messenger-spinner-side messenger-spinner-side-right">
                            <span className="messenger-spinner-fill"></span>
                        </span>
                    </div>
                </div>
            </li>
        )

    }
    
}

Message.propTypes = {
    message: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired
}

export default class MessageCenter extends Component {

    onMessageClose(message_index) {
        this.props.onMessageClose(message_index);
    }


    render() {
        // if (!this.props.children) return '';

        let classes = classNames({
            'messenger': true,
            'messenger-fixed': true,
            'messenger-on-top': true,
            // 'messenger-on-right': true,
            'messenger-theme-flat': true,
            'messenger-empty': (!this.props.children)
        })

        return (
            <ul className={classes}>
                {React.Children.map(this.props.children, (message) => {
                    return <Message type={message.props.type} message={message.props.children} index={message.props.index} visible={message.props.visible} key={"message_" + message.props.index} onMessageClose={this.onMessageClose.bind(this)} />
                }, this)}
            </ul>
        )

        // return (
        //     <div>
        //         {React.Children.map(this.props.children, (message) => {
        //             return <Message type={message.props.type} message={message.props.children} index={message.props.index} visible={message.props.visible} key={"message_" + message.props.index} onMessageClose={this.onMessageClose.bind(this)} />
        //         }, this)}
        //     </div>
        // );
    }
}


