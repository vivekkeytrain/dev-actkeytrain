import classNames from 'classnames';
import SidebarMixin from 'global/jsx/sidebar_component';

import Header from 'common/header';
import Sidebar from 'common/sidebar';
import Footer from 'common/footer';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import App from '../surveycomments/containers/App';
import surveyApp from '../surveycomments/reducers';
import { logger } from '../surveycomments/middleware';
import thunkMiddleware from 'redux-thunk';

let createStoreWithMiddleware = applyMiddleware(thunkMiddleware, logger)(createStore);
let store = createStoreWithMiddleware(surveyApp);

class Body extends React.Component {
  render() {
    return (
      <Provider store={store}>
        {() => <App />}
      </Provider>
    );
  }
}

@SidebarMixin
export default class extends React.Component {
  render() {
    let classes = classNames({
      'container-open': this.props.open
    });

    return (
      <Container id='container' className={classes}>
        <Header />
        <Sidebar />
        <Body />
        <Footer />
      </Container>
    );
  }
}



// import classNames from 'classnames';
// import SidebarMixin from 'global/jsx/sidebar_component';

// import Header from 'common/header';
// import Sidebar from 'common/sidebar';
// import Footer from 'common/footer';

// import $ from 'jquery';
// import { filter, forEach } from 'lodash';

// import Loading from 'surveycomments/components/Loading';
// import Answer from 'surveycomments/components/Answer';
// import Answers from 'surveycomments/components/Answers';
// import Comments from 'surveycomments/components/Comments';





// // var color = d3.scale.ordinal();
// // #003399



// var usefullness_stati, sentiment_stati, disposition_stati, category_stati, filtered_stati, exclusive_stati, additive_stati;
// usefullness_stati = sentiment_stati = disposition_stati = category_stati = {},
//     filtered_stati = 1,
//     exclusive_stati = [],
//     additive_stati = [];

// class Body extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             comments: []
//         }
//     }

//     fill_stati_obj(all_stati, prefix) {
//         // console.debug('fill_stati_obj: ' + prefix);
//         let prefix_re = new RegExp('^' + prefix + '_');
//         let tmp = filter(all_stati, function(status_obj) {
//             return status_obj.status_tag.match(prefix_re);
//         });

//         let out_obj = {}
//         forEach(tmp, function(el) {
//             out_obj[el.status_val] = el.status_desc;
//         });

//         return out_obj
//     }


//     get_stati_vals(valname, cb) {
//         // console.debug('get_stati_vals', valname);
//         $.ajax({
//             // url: 'http://es.run.keytrain.com/ajax/get_stati.asp',
//             url: 'http://dev.admapi.actkeytrain.com/dbwebapi/gen.sp_get_stati',
//             data: {
//                 'objname': 'log_tbl_session_rating',
//                 'valname': valname
//             },
//             dataType: 'jsonp',
//             context: this,
//             success: function(data) {
//                 // console.debug(data);
//                 // console.debug(this);
                
//                 data = data.ResultSets[0];
//                 // console.debug(data);
//                 cb.call(this, data);
//             }
//         });
//     }

//     componentDidMount() {
//         // console.debug('do ajax');
//         $.ajax({
//             //url: 'http://es.run.keytrain.com/ajax/get_stati.asp',
//             url: 'http://dev.admapi.actkeytrain.com/dbwebapi/gen.sp_get_stati',
//             data: {
//                 'objname': 'log_tbl_session_rating',
//                 'valname': 'comment_stati'
//             },
//             dataType: 'jsonp',
//             context: this,
//             success: function(data) {
//                 // console.debug(data);
//                 data = data.ResultSets[0];
//                 // console.debug(data);

//                 this.get_stati_vals.call(this, 'disp_stati', function(stati) {
//                     disposition_stati = this.fill_stati_obj(stati, 'disp');
//                     // console.debug(disposition_stati);

//                     this.get_stati_vals.call(this, 'useful_val', function(stati) {
//                         usefullness_stati = this.fill_stati_obj(stati, 'useful');

//                         this.get_stati_vals.call(this, 'custsent_val', function(stati) {
//                             sentiment_stati = this.fill_stati_obj(stati, 'custsent');

//                             this.get_stati_vals.call(this, 'cat_stati', function(stati) {
//                                 category_stati = this.fill_stati_obj(stati, 'cat');
//                                 this.get_questions();
//                             })
//                         })
//                     })
//                 })
//             }
//         });
//     }

//     get_questions() {
//         // console.debug(category_stati);
//         // console.debug(exclusive_stati);

//         $.ajax({
//             url: 'http://es.run.keytrain.com/cmiscorm/survey_questions.asp',
//             data: {'mode': 1},
//             dataType: 'jsonp',
//             context: this,
//             success: function(data) {
//                 // console.debug(data);
//                 var questions = {};
//                 var answers = {};
//                 _.each(data, function(obj, i) {
//                     questions[obj.id] = obj.question;
//                     answers[obj.id] = obj.answers;
//                 });

                
//                 this.show_comments(questions, answers);
//             },
//             error: function(a, b, c) {
//                 console.debug(a);
//                 console.debug(b);
//                 console.debug(c);
//             }
//         });
//     }

//     get_comments(disp_stati, cb) {
//         // console.debug('get_comments');
//         $.ajax({
//             // url: 'http://es.run.keytrain.com/ajax/survey_data.asp',
//             url: 'http://dev.admapi.actkeytrain.com/dbwebapi/dbo.rpt_sp_survey_data/jsonp',
//             data: {
//                 'disp_stati': disp_stati
//             },
//             headers: {
//                 'Accept': 'text/javascript'
//             },
//             dataType: 'jsonp',
//             context: this,
//             success:  function(survey_data) {
//                 // console.debug(survey_data);
//                 // var comments = _.filter(survey_data, function(post) {
//                 //     return post.comments != '';
//                 // }).reverse();
//                 var comments = survey_data.ResultSets[0];
//                 // console.debug(comments);
//                 cb.call(this, comments);
                
//             },
//             error: function() {
//                 console.debug(arguments);
//             }
//         })
//     }

//     show_comments(questions, answers) {
//         // console.debug('show_comments');

//         this.get_comments(1, function(comments) {
//             // console.debug('callback');
//             // React.render(, document.getElementById('comments'));
//             // console.log(comments);
//             this.setState({
//                 questions: questions,
//                 answers: answers,
//                 comments: comments
//             })
//         })
//     }

//     render() {
//         // console.log('render');
//         let content;
//         if (this.state.comments.length > 0) {
//             // console.log('got comments');
//             content = <Comments comments={this.state.comments} questions={this.state.questions} answers={this.state.answers} dispositionStati={disposition_stati} usefullnessStati={usefullness_stati} sentimentStati={sentiment_stati} categoryStati={category_stati} filteredStati={filtered_stati} exclusiveStati={exclusive_stati} additiveStati={additive_stati} />;
//         } else {
//             content = <h1>Wait for it ...</h1>
//         }

//         return (
//             <Container id='body'>
//                 <Grid>
//                     <Row>
//                         <Col sm={12}>
//                             <PanelContainer controlStyles='bg-blue fg-white'>
//                                 <PanelHeader className='bg-blue fg-white' style={{margin: 0}}>
//                                     <Grid>
//                                         <Row>
//                                             <Col xs={12}>
//                                                 <h3>Comments</h3>
//                                             </Col>
//                                         </Row>
//                                     </Grid>
//                                 </PanelHeader>
//                                 <PanelBody>
//                                     <Grid>
//                                         <Row>
//                                             <Col xs={12} style={{padding: 25}}>
//                 {content}
//             </Col>
//                                         </Row>
//                                     </Grid>
//                                 </PanelBody>
//                             </PanelContainer>
//                         </Col>
//                     </Row>
//                 </Grid>
//             </Container>
//         );
//     }
// }

// @SidebarMixin
// export default class extends React.Component {
//   render() {
//     let classes = classNames({
//       'container-open': this.props.open
//     });

//     return (
//       <Container id='container' className={classes}>
//         <Sidebar />
//         <Header />
//         <Body />
//         <Footer />
//       </Container>
//     );
//   }
// }
