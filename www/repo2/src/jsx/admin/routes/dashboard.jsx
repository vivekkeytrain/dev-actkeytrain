import classNames from 'classnames';
import SidebarMixin from 'global/jsx/sidebar_component';

import Header from 'common/header';
import Sidebar from 'common/sidebar';
import Footer from 'common/footer';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import App from '../dashboard/containers/App';
import dashboardApp from '../dashboard/reducers';
import { logger } from '../dashboard/middleware';
import thunkMiddleware from 'redux-thunk';

let createStoreWithMiddleware = applyMiddleware(thunkMiddleware, logger)(createStore);
let store = createStoreWithMiddleware(dashboardApp);

class Body extends React.Component {
  render() {
    // console.log('render');
    return (
      <Provider store={store}>
        {() => <App />}
      </Provider>
    );
  }
}

@SidebarMixin
export default class extends React.Component {
  render() {
    let classes = classNames({
      'container-open': this.props.open
    });

    return (
      <Container id='container' className={classes}>
        <Header />
        <Sidebar />
        <Body />
        <Footer />
      </Container>
    );
  }
}
