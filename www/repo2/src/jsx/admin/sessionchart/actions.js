import fetchJsonp from 'fetch-jsonp';

/*  Action types    */

export const SET_SESSION_TYPE = 'SET_SESSION_TYPE';
export const SET_DISPLAY_BY_FISCAL_YEAR = 'SET_DISPLAY_BY_FISCAL_YEAR';

/*  Async action types  */

export const FETCH_SESSION_DATA_REQUEST = 'FETCH_SESSION_DATA_REQUEST';
export const FETCH_SESSION_DATA_SUCCESS = 'FETCH_SESSION_DATA_SUCCESS';
export const FETCH_SESSION_DATA_FAILURE = 'FETCH_SESSION_DATA_FAILURE';

/*  Other constants */

export const START_DATE_DEFAULT = '2009-01-01'

export const SESSION_TYPES = {
    khours: 'Thousand Hours',
    ksessions: 'Thousand Sessions'
}

/*  Action creators */

export function setSessionType(session_type) {
    return {
        type: SET_SESSION_TYPE,
        session_type
    }
}

export function setDisplayByFiscalYear(display_by_fiscal_year) {
    return {
        type: SET_DISPLAY_BY_FISCAL_YEAR,
        display_by_fiscal_year
    }
}

function fetchSessionDataRequest(start_date, by_fiscal_year) {
    return { 
        type: FETCH_SESSION_DATA_REQUEST,
        start_date,
        by_fiscal_year
    };
}

function fetchSessionDataSuccess(start_date, by_fiscal_year, data) {
    // console.debug(data);
    return {
        type: FETCH_SESSION_DATA_SUCCESS,
        start_date,
        by_fiscal_year,
        data,
        receivedAt: Date.now()
    }
}

function fetchSessionDataFailure(ex) {
    return {
        type: FETCH_SESSION_DATA_FAILURE,
        ex
    }
}

export function fetchSessionData(start_date, by_fiscal_year=false) {
    return function(dispatch) {
        if (!start_date) start_date = START_DATE_DEFAULT;
        dispatch(fetchSessionDataRequest(start_date, by_fiscal_year));

        let url = 'http://dev.admapi.actkeytrain.com/dbwebapi/dbo.rpt_sp_session_data?start_date=' + start_date;

        if (by_fiscal_year) 
            url += '&by_fiscal_year=1';

        console.debug(url);

        return fetchJsonp(url, {
            timeout: 20000
        })
            .then(
                response => response.json(),
                ex => dispatch(fetchSessionDataFailure(ex))
            )
            .then(json => dispatch(fetchSessionDataSuccess(start_date, by_fiscal_year, json.ResultSets[0])))
            .catch(ex => dispatch(fetchSessionDataFailure(ex)));
    }
}