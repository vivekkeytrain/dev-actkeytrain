import React, { Component } from 'react';
import d3 from 'd3';
import tip from 'd3-tip';
import { chain, map, flatten } from 'lodash';

import { SESSION_TYPES } from '../actions';

import Bar from './Bar';

const margin = {top: 10, right: 35, bottom: 30, left: 60};
// const possible_colors = ["#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00","#758768", "#476B47", "#56989F", "#3E98D0", "#0073FF"];
const   possible_colors = ['#339966', '#FF6666', '#FFCC66', '#FF0066', '#003399', '#333366', '#3399CC'];

export default class Chart extends Component {
    constructor(props) {
        super(props);

        console.log('constructor', props);
        this.state = {
            x: null,
            y: null,
            xAxis: null,
            yAxis: null,
            width: props.width,
            height: props.height,
            color: null,
            toolTip: null
        }
    }

    componentDidMount() {
        console.log(this.props);
        let width = this.props.width;
        let height = this.props.height;

        let svg = d3.select('#svg_chart');
        let x = d3.scale.ordinal().rangeRoundBands([0, width], .1);
        console.log(x);
        let xAxis = d3.svg.axis().scale(x).orient('bottom');
        
        let y = d3.scale.linear().range([height, 0]);
        let yAxis = d3.svg.axis().scale(y).orient('left');

        let color = d3.scale.ordinal();
        
        
        let toolTip = tip()
            .attr('class', 'd3-tip');

        svg.call(toolTip);

        this.setState({
            x: x, 
            xAxis: xAxis,
            y: y,
            yAxis: yAxis,
            width: width,
            height: height,
            color: color,
            toolTip: toolTip,
            foo: 'bar'
        });


        console.log(this.setState);
        console.log(this.state);
    }

    componentWillUnmount() {
        d3.select('#svg_chart').selectAll(".tick").remove();
        d3.selectAll('.d3-tip').remove();
    }

    componentWillReceiveProps(props) {
        console.log(props);
        console.log(this.state);

        if (this.state && this.state.x && props.monthsRange.length) {
            let x = this.state.x;
            x.domain(props.monthsRange);

            let xAxis = this.state.xAxis;
            let svg = d3.select('#svg_chart');
            d3.select("#xaxis")
                .attr("transform", "translate(0," + this.state.height + ")")
                .call(xAxis);

            let y = this.state.y;
            y.domain([0,props.maxField]);

            let yAxis = this.state.yAxis;
            d3.select('#yaxis')
                .call(yAxis);

            let color = this.state.color;
            color.range(possible_colors.slice(0, props.years.length)).domain(props.years);
            // console.log(color.range());
        }
    }

    render() {
        console.log(this.props);
        console.log(this.state);

        let color = this.state.color;
        let x = this.state.x;
        let y = this.state.y;
        let session_data = this.props.sessionData;
        let year_count = this.props.years.length;

        let bars = [];
        if (x && y) {
            bars = chain(this.props.years).map((year, index) => {
                return map(session_data, (row, j) => {
                    if (row[index]) { 
                        // console.log(row[index].month);
                        // console.log(row[index].year);
                        return <Bar
                            width={x.rangeBand() / year_count}
                            height={this.props.height - y(row[index].val)}
                            offset={x(row[index].month) + (x.rangeBand() / year_count * index)}
                            availableHeight={this.props.height}
                            color={color(row[index].year)}
                            toolTip={this.state.toolTip}
                            year={row[index].year}
                            kval={row[index].val}
                            key={year + '_' + j}
                        />
                    }
                })
            }).flatten().value();
        }

        // console.log(bars);

        return (
            <svg width={this.state.width+margin.left+margin.right} height={this.state.height+margin.top+margin.bottom} style={{'float': 'left'}} id="svg_chart">
                <g transform={'translate(' + margin.left + ',' + margin.top + ')'} id="margin">
                    <g className="x axis" id="xaxis" />
                    <g className="y axis" id="yaxis">
                        <text transform='rotate(-90)' y='6' dy='.71em' style={{'textAnchor': 'end'}}>{SESSION_TYPES[this.props.sessionType]}</text>
                    </g>
                    {bars}
                </g>
            </svg>
        )
    }
}