import React, { Component } from 'react';

export default class Bar extends Component {
    constructor(props) {
        super(props);

        this.props = {
            width: 0,
            height: 0,
            offset: 0
        }
    }

    showTip(e) {
        // console.debug(this.props);
        let toolTip = this.props.toolTip;
        let text = this.props.year + ': ' + this.props.kval + ',000';
        toolTip.html(text).show(e.target);
    }

    hideTip(e) {
        let toolTip = this.props.toolTip;
        toolTip.hide(e.target);
    }

    render() {
        //onMouseOver={this.showTip.bind(this)} onMouseOut={this.hideTip.bind(this)}
        return (
            <rect fill={this.props.color} width={this.props.width} height={this.props.height} x={this.props.offset} y={this.props.availableHeight - this.props.height} onMouseOver={this.showTip.bind(this)} onMouseOut={this.hideTip.bind(this)} />
        );
    }
}

Bar.defaultProps = {
    width: 0,
    height: 0,
    offset: 0
}