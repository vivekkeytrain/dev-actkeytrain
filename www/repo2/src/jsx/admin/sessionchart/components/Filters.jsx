import React, { Component } from 'react';

import { map } from 'lodash';

export default class Filters extends Component {
    render() {
        console.log(this.props);
        return (
            <div>
                <select name="session_type" value={this.props.sessionType} onChange={this.props.onSessionTypeChange}>
                    {
                        map(this.props.sessionTypes, (v, k) => {
                            return <option value={k} key={k}>{v}</option>
                        })
                    }
                </select>

                <label>
                    Display by fiscal year:
                    <input type="checkbox" name="fiscal" checked={this.props.displayFiscal} onChange={this.props.onDisplayFiscalChange} />
                </label>
            </div>
        )
    }
}


