import { createSelector } from 'reselect';

import { chain, map, flatten, uniq, reverse, max, forEach, values } from 'lodash';

function selectFilteredSessionData(session_data, session_type, start_date, display_by_fiscal_year) {
    console.debug('selectFilteredSessionData', session_type, start_date, display_by_fiscal_year)
    if (!session_data[start_date]) return [];

    let key = display_by_fiscal_year ? 'fiscal' : 'calendar';
    if (!session_data[start_date][key]) return [];

    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    let data = {};
    forEach(session_data[start_date][key].data, (row) => {
        let m = row.m;
        if (display_by_fiscal_year) m = (m+9)%12;
        if (m==0) m = 12;
        console.debug('m', m);
        let month = months[m-1];
        let year = row.y;

        if (!data[month]) data[month] = [];

        data[month].push({
            month,
            year,
            val: (session_type == 'khours') ? row.thousand_hours : row.thousand_sessions,
            ksessions: row.thousand_sessions,
            khours: row.thousand_hours
        });
    });

    return values(data);
}

function selectMonthsRange(session_data) {
    return map(session_data, x => x[0].month);
}

// var years = _.uniq(_.flatten(_.map(data, function(d, k) {
//             return _.map(d, 'year');
//         }))).sort(function(a,b) { return a-b });
function selectYears(session_data) {
    return chain(session_data).map((d,k) => {
        return map(d, 'year');
    }).flatten().uniq().reverse().value();
}

// y.domain([0, d3.max(data, function(d) {
//             return d3.max(_.map(d, field));
//         })]);
function selectMaxField(session_data, session_type) {
    if (!session_data.length) return 0;

    console.debug('selectMaxField', session_data, session_type);
    let m = chain(session_data).map(d => map(d, session_type)).max(d => max(d)).max().value();

    console.debug(m);
    return m;
}

const filtersSelector = state => state.filters;
const sessionTypeSelector = state => state.filters.session_type;
const startDateSelector = state => state.filters.start_date;
const displayByFiscalYearSelector = state => state.filters.display_by_fiscal_year;
const sessionDataSelector = state => state.session_data;

const filteredSessionDataSelector = createSelector(
    sessionDataSelector,
    sessionTypeSelector,
    startDateSelector,
    displayByFiscalYearSelector,
    (session_data, session_type, start_date, display_by_fiscal_year) => {
        return selectFilteredSessionData(session_data, session_type, start_date, display_by_fiscal_year);
    }
);

const monthsRangeSelector = createSelector(
    filteredSessionDataSelector,
    (session_data) => {
        return selectMonthsRange(session_data);
    }
);

const yearsSelector = createSelector(
    filteredSessionDataSelector,
    (session_data) => {
        return selectYears(session_data);
    }
);

const maxFieldSelector = createSelector(
    filteredSessionDataSelector,
    sessionTypeSelector,
    (session_data, session_type) => {
        return selectMaxField(session_data, session_type);
    }
);

export const filteredContentSelector = createSelector(
    sessionTypeSelector,
    displayByFiscalYearSelector,
    filteredSessionDataSelector,
    monthsRangeSelector,
    yearsSelector,
    maxFieldSelector,
    (session_type, display_by_fiscal_year, session_data, months_range, years, max_field ) => {
        return { session_type, display_by_fiscal_year, session_data, months_range, years, max_field  }
    }
);