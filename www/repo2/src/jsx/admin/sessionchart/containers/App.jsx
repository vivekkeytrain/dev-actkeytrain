import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { 
    SESSION_TYPES, START_DATE_DEFAULT,
    setSessionType, setDisplayByFiscalYear,
    fetchSessionData
} from '../actions';

import { filteredContentSelector } from '../selectors';
import Chart from '../components/Chart';
import Filters from '../components/Filters';

class App extends Component {
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchSessionData());
    }

    handleSessionTypeChange(e) {
        const { dispatch } = this.props;
        console.debug(e.target.value);
        dispatch(setSessionType(e.target.value));
    }

    handleDisplayFiscalChange(e) {
        console.debug(e.target);
        console.debug(e.target.checked);
        const { dispatch } = this.props;
        dispatch(setDisplayByFiscalYear(e.target.checked));
        dispatch(fetchSessionData(START_DATE_DEFAULT, e.target.checked));
    }

    render() {
        //  Injected by connect() call:
        const { dispatch, session_type, display_by_fiscal_year, session_data, months_range, years, max_field } = this.props;

        console.debug(this.props);

        // <Chart questionIds={question_ids} answerVals={answer_vals} answerSumsArr={answer_sums_arr} answerSumsMax={answer_sums_max} />

        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PanelContainer controlStyles='bg-blue fg-white'>
                                <PanelHeader className='bg-blue fg-white' style={{margin: 0}}>
                                    <Grid>
                                        <Row>
                                            <Col xs={12}>
                                                <h3>Session Data Chart</h3>
                                                <p>This chart shows monthly usage over a period of years. This is useful for year-over-year comparison of total usage. However, this does not incorporate counts of sites or learners. For that, see the <Link to="/course-usage-reports/">Course Usage Reports</Link>.</p>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelHeader>
                                <PanelBody>
                                    <Grid>
                                        <Row>
                                            <Col xs={12} style={{padding: 25}}>
                                                <Filters 
                                                    sessionTypes={SESSION_TYPES}
                                                    sessionType={session_type}
                                                    displayFiscal={display_by_fiscal_year}
                                                    onSessionTypeChange={e => this.handleSessionTypeChange(e)}
                                                    onDisplayFiscalChange={e => this.handleDisplayFiscalChange(e)}
                                                    />
                                                <Chart 
                                                    sessionType={session_type}
                                                    sessionTypes={SESSION_TYPES}
                                                    sessionData={session_data}
                                                    monthsRange={months_range}
                                                    years={years}
                                                    maxField={max_field}
                                                    width={1200} 
                                                    height={700} 
                                                     />
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelBody>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        )
    }
}

export default connect(filteredContentSelector)(App);
