import fetch from 'isomorphic-fetch';
import Promise from 'promise-polyfill';

/*  Action types    */

export const INSERT_LOGIN = 'INSERT_LOGIN';
export const DELETE_LOGIN = 'DELETE_LOGIN';
export const INSERT_HISTORY = 'INSERT_HISTORY';
export const SET_HISTORY_TIMEFRAME = 'SET_HISTORY_TIMEFRAME';
export const SET_HISTORY_FETCH_TIMER = 'SET_HISTORY_FETCH_TIMER';

/*  Async action types  */

export const FETCH_LOGINS_SUCCESS = 'FETCH_LOGINS_SUCCESS';
export const FETCH_LOGINS_FAILURE = 'FETCH_LOGINS_FAILURE';
export const FETCH_LOGINS_REQUEST = 'FETCH_LOGINS_REQUEST';

export const FETCH_LOGINS_HISTORY_SUCCESS = 'FETCH_LOGINS_HISTORY_SUCCESS';
export const FETCH_LOGINS_HISTORY_FAILURE = 'FETCH_LOGINS_HISTORY_FAILURE';
export const FETCH_LOGINS_HISTORY_REQUEST = 'FETCH_LOGINS_HISTORY_REQUEST';

/*  Action creators */

export function insertLogin(student_id, pkg_id, statecode, insert_date) {
    return {
        type: INSERT_LOGIN,
        student_id,
        pkg_id,
        statecode,
        insert_date
    }
}

export function deleteLogin(student_id, pkg_id) {
    return {
        type: DELETE_LOGIN,
        student_id,
        pkg_id
    }
}

export function insertHistory(pkg_id, insert_date, count) {
    return {
        type: INSERT_HISTORY,
        pkg_id,
        insert_date,
        count
    }
}

export function setHistoryTimeframe(timeframe) {
    return {
        type: SET_HISTORY_TIMEFRAME,
        timeframe
    }
}

export function setHistoryFetchTimer(timer) {
    return {
        type: SET_HISTORY_FETCH_TIMER,
        timer
    }
}

/*  Async action createors  */

function fetchLoginsSuccess(logins) {
    return {
        type: FETCH_LOGINS_SUCCESS,
        logins
    }
}

function fetchLoginsFailure(ex) {
    return {
        type: FETCH_LOGINS_FAILURE,
        ex
    }
}

function fetchLoginsRequest() {
    return { type: FETCH_LOGINS_REQUEST }
}

export function fetchLogins() {
    return function(dispatch) {
        dispatch(fetchLoginsRequest());
        return fetch('/pgapi/logins/current', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': window.auth
            }
        })
            .then(
                response => response.json(),
                ex => dispatch(fetchLoginsFailure(ex))
            )
            .then(
                json => dispatch(fetchLoginsSuccess(json))
            )
            // .catch(
            //     ex => dispatch(fetchLoginsFailure(ex))
            // )
    }
}

function fetchLoginsHistorySuccess(history) {
    return {
        type: FETCH_LOGINS_HISTORY_SUCCESS,
        history
    }
}

function fetchLoginsHistoryFailure(ex) {
    return {
        type: FETCH_LOGINS_HISTORY_FAILURE,
        ex
    }
}

function fetchLoginsHistoryRequest() {
    return { type: FETCH_LOGINS_HISTORY_REQUEST }
}

export function fetchLoginsHistory(timeframe) {
    return function(dispatch) {
        dispatch(fetchLoginsHistoryRequest());

        let interval;
        switch(timeframe) {
            case 'hour':
                interval = '5seconds';
                // interval = '1minute';
                break;
            case 'day':
                interval = '10minutes';
                break;
            case 'week':
                interval = '1hour';
                break;
            case 'month': 
                interval = '6hours';
                break;
        }

        // console.debug('interval', interval);
        // console.debug('timeframe', timeframe);
        return fetch(`/pgapi/logins/history/${interval}/1${timeframe}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': window.auth
            }
        })
            .then(
                response => response.json(),
                ex => dispatch(fetchLoginsHistoryFailure(ex))
            )
            .then(
                json => dispatch(fetchLoginsHistorySuccess(json))
            )
            .catch(
                ex => dispatch(fetchLoginsHistoryFailure(ex))
            )
    }
}