import React, { Component, PropTypes } from 'react';
import { map } from 'lodash';
import classNames from 'classnames';

import StatePath from './StatePath';

export default class ColorLegend extends Component {
    //  http://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
    blendColors(c0, c1, p) {
        // console.debug('blendColors', p);
        var f=parseInt(c0.slice(1),16),t=parseInt(c1.slice(1),16),R1=f>>16,G1=f>>8&0x00FF,B1=f&0x0000FF,R2=t>>16,G2=t>>8&0x00FF,B2=t&0x0000FF;
        return "#"+(0x1000000+(Math.round((R2-R1)*p)+R1)*0x10000+(Math.round((G2-G1)*p)+G1)*0x100+(Math.round((B2-B1)*p)+B1)).toString(16).slice(1);
    }

    render() {
        console.debug(this.props);

        let bars = [<span>0</span>];
        for (let i=0; i<1; i+=0.01) {
            bars.push(<span className="legend-bar" style={{backgroundColor: this.blendColors('#BFEDCA', '#31AF4F', i)}}>{String.fromCharCode(160)}</span>)
        }
        bars.push(<span>{this.props.max}</span>);

        return (
            <Col xs={6} className="text-center">
                {bars}
            </Col>
        )
    }
}