import React, { Component, PropTypes } from 'react';
import { map } from 'lodash';
import classNames from 'classnames';

import StatePath from './StatePath';

export default class StatesMap extends Component {
    //  http://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
    // blendColors(c0, c1, p) {
    //     console.debug('blendColors', p);
    //     var f=parseInt(c0.slice(1),16),t=parseInt(c1.slice(1),16),R1=f>>16,G1=f>>8&0x00FF,B1=f&0x0000FF,R2=t>>16,G2=t>>8&0x00FF,B2=t&0x0000FF;
    //     return "#"+(0x1000000+(Math.round((R2-R1)*p)+R1)*0x10000+(Math.round((G2-G1)*p)+G1)*0x100+(Math.round((B2-B1)*p)+B1)).toString(16).slice(1);
    // }

    // getColor(count) {
    //     // console.debug(this.props.max, count);
    //     if (count == this.props.max) {
    //         return '#31AF4F';
    //     } else if (count > 0) {
    //         return this.blendColors('#BFEDCA', '#31AF4F', count/this.props.max);
    //     } else {
    //         return '#D3D3D3';
    //     }
    // }

    render() {
        console.debug(this.props);
        let { pkg_id } = this.props;

        return (
            <svg width="959" height="593" viewBox="0 0 959 593" id={`svg-${this.props.pkg_id}`} className="us-states">
                <title>Blank US states map.svg</title>
                <g id="outlines">
                    {map(this.props.states, (st, abbrev) => 
                        <StatePath st={st} abbrev={abbrev} pkg_id={pkg_id} max={this.props.max} />
                    )}
                </g>
                <path id="frames" fill="none" stroke="#A9A9A9" stroke-width="2" d="M215,493v55l36,45 M0,425h147l68,68h85l54,54v46"/>
                </svg>
        )
    }
}