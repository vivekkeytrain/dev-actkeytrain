import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import io from 'socket.io-client';
import { remove, uniq } from 'lodash';
// import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup';
import TransitionManager from 'react-transition-manager';
// import pg from 'pg';
import { 
    insertLogin, deleteLogin, insertHistory, fetchLogins, fetchLoginsHistory, setHistoryTimeframe,
    setHistoryFetchTimer
} from '../actions';

import { filteredContentSelector } from '../selectors';
import TimeframeLink from '../components/TimeframeLink';
import StatesMap from '../components/StatesMap';
import ColorLegend from '../components/ColorLegend';

//    "react": "shripadk/react-ssr-temp-fix",

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            kt_logins: [],
            cr_logins: [],
			announcements: []
        }
    }

    componentDidMount() {
        console.debug('componentDidMount')
        const { dispatch, timeframe } = this.props;

        dispatch(fetchLogins());
        dispatch(fetchLoginsHistory(timeframe));

        let student_id, pkg_id, insert_date, count, statecode;

        Pace.off();
        if (Pace && Pace.options && Pace.options.ajax) Pace.options.ajax.trackWebSockets = false;

        let socket = io.connect('/ktadmin');
        console.debug(socket);

        // let logins = this.state.logins || [];

        socket.on('connect', () => {
            console.debug('CONNECT CONNECT CONNECT');
        });

        socket.on('connected', () => {
            console.debug('ready for data');
            socket.emit('ready for data', {});
        });

        socket.on('insert', (data) => {
            console.debug('insert', data);
            // console.debug(data.message.payload.split('|'));
            [student_id, pkg_id, statecode, insert_date] = data.message.payload.split('|');
            // console.debug(student_id, pkg_id, statecode, insert_date);
            insert_date = (new Date(insert_date)).toISOString();
            dispatch(insertLogin(parseInt(student_id, 10), parseInt(pkg_id, 10), statecode, insert_date));
        });

        socket.on('delete', (data) => {
            console.debug('delete', data);
            [student_id, pkg_id, statecode, insert_date] = data.message.payload.split('|');
            dispatch(deleteLogin(parseInt(student_id, 10), parseInt(pkg_id, 10)));
        });

        socket.on('history_insert', (data) => {
            console.debug('history_insert', data);
            if (this.props.timeframe == 'hour') {
                [pkg_id, insert_date, count] = data.message.payload.split('|');
                //  Need to format insert_date like the others
                insert_date = (new Date(insert_date)).toISOString();
                // console.info(insert_date);
                dispatch(insertHistory(parseInt(pkg_id), insert_date, parseInt(count)));
            }
        });

        socket.on('disconnect', () => {
            console.info('DISCONNECTED');
        });

        socket.on('reconnect', () => {
            console.info('RECONNECT');
        });


        this.setHistoryFetchTimer();
        

    }

    componentDidUpdate(prevProps, prevState) {
		/*********fetch announcements from server starts here************/
		fetch('http://sdapi.actkeytrain.com/dbwebapi/dbo.api_sp_manage_announcements/json?mode=1')
            .then(result => result.json())
            .then(params => {
                console.log(params);
                this.setState({'announcements': params});
                console.log(this.state);
            });
		/*********fetch announcements from server ends here************/
        $('#ktspark').sparkline(this.props.history.kt.slice(-200), { height: '40px' });
        $('#crspark').sparkline(this.props.history.cr.slice(-200), { height: '40px' });
    }

    componentWillUnmout() {
        window.clearTimeout(this.props.timer);
    }

    setHistoryFetchTimer() {
        // console.debug('setHistoryFetchTimer');
        let delay;
        const { dispatch, timeframe } = this.props;

        if (timeframe == 'hour') return; // we listen for these inserts
        switch(timeframe) {
            case 'day':
                delay = 5 * 60 * 1000; // 5 minutes
                break;
            case 'week':
                delay = 1 * 60 * 60 * 1000; // 1 hour
                break;
            case 'month':
                delay = 6 * 60 * 60 * 1000; // 6 hours
                break;
        }
        console.debug(delay);

        let fetch = () => {
            dispatch(fetchLoginsHistory(timeframe));
            this.setHistoryFetchTimer();
        }
        let timeoutid = window.setTimeout(fetch, delay);
        dispatch(setHistoryFetchTimer(timeoutid));
    }

    render() {

        console.debug(this.props);
        console.debug(this.state); 

		
        let setTimeframe = (timeframe) => {
            // e.preventDefault();
            console.debug(this);
            console.debug(timeframe);
            const { dispatch } = this.props;

            dispatch(setHistoryTimeframe(timeframe));
            dispatch(fetchLoginsHistory(timeframe));
            this.setHistoryFetchTimer();
        }

        let kt_state_counts = this.props.orderedStates.kt.map((st) => {
            return <div className="state-count" key={`kt-${st.st}`}>{st.st}: {st.count}</div>
        });

        let cr_state_counts = this.props.orderedStates.cr.map((st) => {
            return <div className="state-count" key={`cr-${st.st}`}>{st.st}: {st.count}</div>
        });

        console.debug(kt_state_counts);

        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PanelContainer controlStyles='bg-blue fg-white'>
                                <PanelHeader className='bg-blue fg-white' style={{margin: 0}}>
                                    <Grid>
                                        <Row>
                                            <Col xs={12}>
                                                <h3>Announcements</h3>
                                                <p></p>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelHeader>
                                <PanelBody>
                                    <Grid>
                                        <Row>
                                            <Col xs={12}></Col>
											<div>{this.state.announcements}</div>
                                        </Row>
                                    </Grid>
                                </PanelBody>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        )
    }
}

// export default App;

export default connect(filteredContentSelector)(App);
