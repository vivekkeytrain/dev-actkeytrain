import React, { Component } from 'react';

import Chart from './Chart';
import Questions from './Questions';

export default class ChartContainer extends Component {
    render() {
        return (
            <div>
                <Chart />
                <Questions />
            </div>
        )
    }
}