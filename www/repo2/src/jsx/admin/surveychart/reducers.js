import { combineReducers } from 'redux';
import { invert } from 'lodash';
import { 
    LangOptions,
    SET_LANG,
    FETCH_QUESTIONS_REQUEST, FETCH_QUESTIONS_SUCCESS, FETCH_QUESTIONS_FAILURE, 
    FETCH_SURVEY_DATA_REQUEST, FETCH_SURVEY_DATA_SUCCESS, FETCH_SURVEY_DATA_FAILURE,
    FETCH_SURVEY_AVERAGE_DATA_REQUEST, FETCH_SURVEY_AVERAGE_DATA_SUCCESS, FETCH_SURVEY_AVERAGE_DATA_FAILURE
} from './actions';

function filters(state = {
    lang: ''
}, action) {
    switch (action.type) {
        case SET_LANG:
            return Object.assign({}, state, {
                lang: action.lang
            });
        default:
            return state;
    }
}

function questions(state = {
    isFetching: false,
    items: []
}, action) {
    switch (action.type) {
        case FETCH_QUESTIONS_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_QUESTIONS_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                items: action.questions
            });
        case FETCH_QUESTIONS_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

function survey_data(state = {
    isFetching: false,
    data: []
}, action) {
    switch (action.type) {
        case FETCH_SURVEY_DATA_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_SURVEY_DATA_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                data: action.data
            });
        case FETCH_SURVEY_DATA_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

function survey_average_data(state={}, action) {
    let lang_code = invert(LangOptions)[action.lang_id];
    lang_code = (lang_code) ? lang_code.toLowerCase() : 'all';
    switch (action.type) {
        case FETCH_SURVEY_AVERAGE_DATA_REQUEST:
        case FETCH_SURVEY_AVERAGE_DATA_SUCCESS:
        case FETCH_SURVEY_AVERAGE_DATA_FAILURE:
            return Object.assign({}, state, {
                [lang_code]: survey_average_data_by_lang(state[lang_code], action)
            });
        default:
            return state;
    }
}

function survey_average_data_by_lang(state = {
    isFetching: false,
    data: []
}, action) {
    switch (action.type) {
        case FETCH_SURVEY_AVERAGE_DATA_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_SURVEY_AVERAGE_DATA_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                data: action.data
            });
        case FETCH_SURVEY_AVERAGE_DATA_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}


const surveyApp = combineReducers({
    questions,
    survey_data,
    survey_average_data,
    filters
});

export default surveyApp;