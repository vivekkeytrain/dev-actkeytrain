import React, { Component, PropTypes } from 'react';
import { forEach, keys, difference, chain, includes } from 'lodash';

import { STAT_FIELDS } from '../actions';

export default class EventsStatsList extends Component {
    constructor(props) {
        // console.debug('constructor');
        // console.debug(props);
        super(props);

        this.state = {
            fields: []
        }
    }

    componentWillMount() {
        console.debug('componentWillMount');
        console.debug(this.props);

        let fields = this.state.fields;
        let stat_fields = keys(STAT_FIELDS);

        this.props.stats.forEach((stat) => {
            let unfilled_fields = chain(stat).keys().difference(fields).value();
            // console.debug(unfilled_fields);
            forEach(unfilled_fields, (field) => {
                // console.debug(this.props);
                // console.debug('stat[' + field + ']', stat[field]);
                if (includes(stat_fields, field) && (stat[field] !== null && stat[field] != '')) fields.push(field);
            }, this)
        });

        console.debug(fields);
        this.setState({
            fields: fields
        })
    }

    render() {
        console.debug(this.props);

        if (!this.props.stats.length) {
            return (
                <tr onClick={this.props.onRowSelected.bind(this)}>
                    <td colSpan="5" style={{textAlign: 'center'}}>
                        <em><strong>NO STATS</strong></em>
                    </td>
                </tr>
            )
        }

        return (
            <tr onClick={this.props.onRowSelected.bind(this)}>
                <td colSpan="5">
                    <Table>
                        <thead>
                            <tr>
                            {
                                this.state.fields.map((field) => {
                                    return <th key={"th_" + field}>{STAT_FIELDS[field]}</th>
                                })
                            }
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.props.stats.map((stat, ind) => {
                                    return (
                                    <tr key={'statrow_' + ind}>
                                    {
                                        this.state.fields.map((field) => {
                                            return <td key={"td_" + field}>{(field === 'stats_timestamp') ? this.props.formatTimestamp(stat[field]) : stat[field]}</td>
                                        }, this)
                                    }
                                    </tr>
                                    )
                                }, this)
                            }
                        </tbody>
                    </Table>
                </td>
            </tr>

        );
    }
}

