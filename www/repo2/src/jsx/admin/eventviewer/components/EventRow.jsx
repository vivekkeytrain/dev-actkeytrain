import React, { Component, PropTypes } from 'react';

// import EventStatsList from './EventStatsList';

export default class EventRow extends Component {

    
    render() {
        // console.debug(this.props);

        // if (this.props.event.event_id === this.props.selectedEvent) {
        //     row.push(
        //         <tr onClick={this.onRowSelected.bind(this)}>
        //             <td rowspan="5">
        //                 {
        //                     this.props.event.stats.map((stat, ind) => {
                                
        //                     }, this)
        //                 }
        //             </td>
        //         </tr>
        //     )
        // }

        // console.debug(row);
        return (
            <tr onClick={this.props.onRowSelected.bind(this, this.props.event)}>
                <td>{this.props.event.event_id}</td>
                <td>{this.props.event.event_type_tag}</td>
                <td>{this.props.formatTimestamp(this.props.event.event_timestamp)}</td>
                <td>{this.props.event.event_status}</td>
                <td>{this.props.event.event_statdesc}</td>
            </tr>
        )
    }
}