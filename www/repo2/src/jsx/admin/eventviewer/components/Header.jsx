import { Component, PropTypes } from 'react';

export default class Header extends Component {
    handleChange(changeHandler, e) {
        changeHandler(parseInt(e.target.value, 10));
    }

    handleTextChange(changeHandler, e) {
        changeHandler(e.target.value);
    }

    render() {
        console.debug(this.props);
        return (
            <Grid>
                <Row>
                    <Col>
                        <Grid>
                            <Row>
                                <Col xs={12} collapseLeft collapseRight>
                                    <Form>
                                        <Col xs={12} sm={3} collapseLeft >
                                            <Select control id="event_cat" onChange={this.handleChange.bind(this, this.props.onEventCatChange)} value={this.props.eventCat}>
                                                <option value="" >Select Event Category</option>
                                                <option value="-1"> -- ALL EVENT CATEGORIES -- </option>
                                                {this.props.eventCats.map((option, index) => 
                                                    <option key={index} value={option.event_cat_id}>{option.event_cat_desc}</option>
                                                )}
                                            </Select>
                                        </Col>

                                        <Col xs={12} sm={3} collapseLeft >
                                            <Select control id="event_type" onChange={this.handleChange.bind(this, this.props.onEventTypeChange)} value={this.props.eventType}>
                                                <option value="" >Select Event Type</option>
                                                <option value="-1"> -- ALL EVENT TYPES -- </option>
                                                {this.props.eventTypes.map((option, index) => 
                                                    <option key={index} value={option.event_type_id}>{option.event_type_desc}</option>
                                                )}
                                            </Select>
                                        </Col>

                                        <Col xs={12} sm={3} collapseLeft >
                                            <Select control id="event_status" onChange={this.handleChange.bind(this, this.props.onEventStatusChange)} value={this.props.eventStatus}>
                                                <option value="" >Select Event Status</option>
                                                <option value="-1"> -- ALL EVENT STATI -- </option>
                                                {this.props.eventStati.map((option, index) => 
                                                    <option key={index} value={option.event_status}>{option.event_statdesc}</option>
                                                )}
                                            </Select>
                                        </Col>

                                        <Col xs={12} sm={3} collapseLeft >
                                            <Input md className="floatlabel" type='text' placeholder="Start Date" name="start_date" value={this.props.startDate} onChange={this.handleTextChange.bind(this, this.props.onUpdateStartDate)} />
                                        </Col>

                                        <Col xs={12} sm={3} collapseLeft >
                                            <Input md className="floatlabel" type='text' placeholder="End Date" name="end_date" value={this.props.endDate} onChange={this.handleTextChange.bind(this, this.props.onUpdateEndDate)} />
                                        </Col>

                                        <Col xs={12} sm={3} collapseLeft >
                                            <Select control id="row_count" onChange={this.handleChange.bind(this, this.props.onUpdateRowCount)} value={this.props.rowCount}>
                                                <option value="">Select Row Count</option>
                                                <option value="10">10 Rows</option>
                                                <option value="20">20 Rows</option>
                                                <option value="50">50 Rows</option>
                                                <option value="100">100 Rows</option>
                                                <option value="200">200 Rows</option>
                                                <option value="500">500 Rows</option>
                                                <option value="1000">1000 Rows</option>
                                                <option value="5000">5000 Rows</option>
                                            </Select>
                                        </Col>

                                        <Col sm={3} collapseLeft>
                                            <Button bsStyle='blue' md onClick={this.props.onTextFilterFetch}>Filter</Button>
                                        </Col>
                                    </Form>
                                </Col>
                            </Row>
                        </Grid>
                    </Col>
                </Row>
            </Grid>
        )
    }
}

Header.propTypes = {
    onEventCatChange: PropTypes.func.isRequired,
    onEventTypeChange: PropTypes.func.isRequired,
    onEventStatusChange: PropTypes.func.isRequired,
    onUpdateStartDate: PropTypes.func.isRequired,
    onUpdateEndDate: PropTypes.func.isRequired,
    onUpdateRowCount: PropTypes.func.isRequired,
    onTextFilterFetch: PropTypes.func.isRequired
}