import { connect } from 'react-redux';

import {
    setSelectedEvent, setEventCat, setEventType, setEventStatus, setEndDate, setStartDate, setRowCount, setSortField,
    fetchEventTypes, fetchEvents
} from '../actions';

import { filteredContentSelector } from '../selectors';
import Header from '../components/Header';
import EventsList from '../components/EventsList';
import MessageCenter from 'common/MessageCenter';

class App extends React.Component {
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchEventTypes());
    }

    render() {
        const { dispatch, filters, selected_event, event_cat, event_cats, event_type, event_types, event_status, event_stati, start_date, end_date, row_count, messages, events, events_fetching } = this.props;
        console.debug(this.props);
        return (
            <Container id='body'>
            <Grid>
              <Row>
                <Col xs={12}>
                  {/* <PanelContainer plain > */}
                  <PanelContainer plain controlStyles='bg-green fg-white'>
                    <Panel style={{background: 'white'}}>
                        <PanelHeader className='bg-green fg-white' style={{margin: 0}}>
                            <Grid>
                                <Row>
                                    <Col xs={12}>
                                        <h3>Event Viewer</h3>
                                        <p>This is a log of system events. This is primarily for development and operations usage.</p>
                                    </Col>
                                </Row>
                            </Grid>
                        </PanelHeader>
                        <PanelBody>
                        <Grid>
                          <Row>
                            <Col xs={12} style={{padding: 25, overflow: 'auto'}}>
                                <MessageCenter onMessageClose={index => handleMessageClose(index)}>
                                    {messages.map((message, ind) => {
                                        return <div type={message.type} index={ind} visible={message.visible} key={"message_" + ind}>{message.message}</div>
                                    })}
                                </MessageCenter>

                                <Header 
                                    eventCat={event_cat}
                                    eventCats={event_cats}
                                    eventType={event_type}
                                    eventTypes={event_types}
                                    eventStatus={event_status}
                                    eventStati={event_stati}
                                    startDate={start_date}
                                    endDate={end_date}
                                    rowCount={row_count}
                                    
                                    onEventCatChange={event_cat => 
                                        dispatch(setEventCat(event_cat))
                                    }
                                    onEventTypeChange={event_type => 
                                        dispatch(setEventType(event_type))
                                    }
                                    onEventStatusChange={event_status => 
                                        dispatch(setEventStatus(event_status))
                                    }
                                    onUpdateEndDate={end_date => 
                                        dispatch(setEndDate(end_date))
                                    }
                                    onUpdateStartDate={start_date =>
                                        dispatch(setStartDate(start_date))
                                    }
                                    onUpdateRowCount={row_count =>
                                        dispatch(setRowCount(row_count))
                                    }
                                    onTextFilterFetch={() =>
                                        dispatch(fetchEvents(event_cat, event_type, start_date, end_date, row_count))
                                    }
                                    
                                />

                                <EventsList
                                    events={events}
                                    eventsFetching={events_fetching}
                                    selectedEvent={selected_event}
                                    sortField={filters.sort_field}
                                    sortDir={filters.sort_dir}
                                    onRowSelected={event_id =>
                                        dispatch(setSelectedEvent(event_id))
                                    }
                                    setSortField={(field_name, sort_dir) => 
                                        dispatch(setSortField(field_name, sort_dir))
                                    }
                                />

                            </Col>
                          </Row>
                        </Grid>
                      </PanelBody>
                    </Panel>
                  </PanelContainer>
                </Col>
              </Row>
            </Grid>
          </Container>
        )
    }
}
    
export default connect(filteredContentSelector)(App);