import React, { Component } from 'react';
import d3 from 'd3';

const margin = {top: 10, right: 0, bottom: 0, left: 5},
    width = 50 - margin.left - margin.right, 
    height = 63 - margin.top - margin.bottom;

var y = d3.scale.ordinal().rangeRoundBands([0, height], .1); 
var x = d3.scale.linear().range([width, 0]);

var xAxis = d3.svg.axis().scale(x).orient('bottom');
var yAxisLeft = d3.svg.axis().scale(y).orient("left");

export default class Answer extends Component {
    componentDidMount() {
        // console.debug('showAnswerBar');

        var svg = d3.select('#' + this.props.answerID)
            .append('svg')
                .attr('width', width + margin.left + margin.right)
                .attr('height', 3 + margin.top + margin.bottom)
            .append('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');


        var answer_val = this.props.answerVal;
        y.domain([0, 1]); // console.debug(x.range());
        x.domain([0, 6]);
        
        svg.selectAll('.answer')
            .data([answer_val])
        .enter().append('rect')
            .attr('height', 2 /*x.rangeBand()*/)
            .attr('x', function(d) {
                // console.debug(d);
                // // console.debug(questions);
                // console.debug(datum[d]);
                // console.debug(y(datum[d]));
                // return width-x(datum[d]);
                0;
            })
            .attr('width', function(d) {
                // console.debug(answer_val + ': ' + x(6 - answer_val));
                return width-x(answer_val == 0 || answer_val == 6 ? 0 : 6-answer_val);
            })
            .attr('y', function(d) {
                return 0;
                // return x(d) + (x.rangeBand() / question_ids.length)
            })
            .attr('class', 'answer-bar')
            .style('fill', '#003399');
    }

    render() {
        return (
            <div className="answer-block">
                <div className="col-md-8">{this.props.question}</div>
                <div className="col-md-2">{this.props.answer}</div>
                <div className="col-md-1" id={this.props.answerID}></div>
            </div>
        )
    }
}