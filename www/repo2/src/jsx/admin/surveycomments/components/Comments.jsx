import React, { Component } from 'react';
import classNames from 'classnames';

import { map } from 'lodash';

import Comment from './Comment';
import Answers from './Answers';

export default class Comments extends Component {
    componentDidMount() {
        // console.log(this.props);
        // this.setState({
        //     'comments': this.props.comments,
        //     'displayed_rows': slice_size
        // });

        // window.addEventListener('scroll', this.props.handleScroll.bind(this));

        this.checkLoadMore();
    }

    componentDidUpdate() {
        this.checkLoadMore();
    }

    checkLoadMore() {
        if (this.props.comments.length >= this.props.displayedRows && $('body').height() <= $(window).height()) {
            this.props.displayMoreComments();
        }
    }

    showHide(row_id, e) {
        // parent = $(e.target).parents('.comment-answers');
        // parent.toggleClass('rounded').removeClass('no-border');
        // parent.prev('.comment-answers').toggleClass('no-border');

        // $('#answers-' + row_id).toggle();
        // this.refs['answers-' + row_id].showAnswerBars();
        if (row_id == this.props.selected_row) 
            this.props.setSelectedRow(-1);
        else 
            this.props.setSelectedRow(row_id);
    }

    renderComments() {
        let questions = this.props.questions;
        let question_ids = this.props.questionIds;
        let answers = this.props.answers;
        let stati = this.props.stati;



        console.debug(this.props);
        let comments = map(this.props.comments, (comment, index) => {
            var classes = classNames({
                'row': true,
                'comment-answers': true,
                'rounded': (this.props.selectedRow == comment.row_id)
            });

            // console.debug(comment);
            return (
                <div className={classes} key={comment.row_id} onClick={this.showHide.bind(this, comment.row_id)}>
                    <div className='col-md-12' style={{'borderBottom': '1px dotted #CCCCCC', 'padding': '10px'}}>
                        <Comment datum={comment} questions={questions} questionIds={question_ids} stati={stati} updateRowCommentStatus={this.props.updateStati} key={"comment_" + comment.row_id} />
                        <Answers datum={comment} questions={questions} answers={answers} key={"answers_" + comment.row_id} selectedRow={this.props.selectedRow} />
                    </div>
                </div>
            );
        });

        return comments;
    }

    render() {
        let classes = classNames({
            'progress-bar': true,
            'active': true,
            'progress-bar-striped': true
        });

        let fetching;
        console.debug(this.props);
        if (this.props.commentsFetching) {
            fetching =  <div className={classes} style={{width: '100%'}}>
                            <span><strong>Fetching Data, Please Hold ...</strong></span>
                        </div>
        }

        return (
            <Container>
                {fetching}
                {this.renderComments()}
            </Container>
        )
    }
}