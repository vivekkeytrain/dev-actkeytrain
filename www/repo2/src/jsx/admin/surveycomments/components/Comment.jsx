import React, { Component } from 'react';

import classNames from 'classnames';
import d3 from 'd3';

import { map } from 'lodash'

const margin = {top: 10, right: 0, bottom: 0, left: 5},
    width = 50 - margin.left - margin.right, 
    height = 63 - margin.top - margin.bottom;

var y = d3.scale.ordinal().rangeRoundBands([0, height], .1); 
var x = d3.scale.linear().range([width, 0]);

var xAxis = d3.svg.axis().scale(x).orient('bottom');
var yAxisLeft = d3.svg.axis().scale(y).orient("left");

export default class Comment extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        // console.log('componentDidMount');
        // console.debug(this.props);
        var svg = d3.select('#row-' + this.props.datum.row_id)
            .append('svg')
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
            .append('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        // svg.append("g")
        //     .attr("class", "x axis")
        //     .attr("transform", "translate(0," + height + ")")
        //     .call(xAxis)
        //     // .selectAll('text')
        //     //     .style('text-anchor', 'end')
        //     //     .attr('dx', '-.8em')
        //     //     .attr('dy', '.15em')
        //     //     .attr('transform', 'rotate(-65)');

        // svg.append('g')
        //     .attr('class', 'y axis')
        //     .call(yAxisLeft)
        // .append('text')
        //     .attr('transform', 'rotate(-90)')
        //     .attr('y', 6)
        //     .attr('dy', '.71em')
        // console.log(svg);
        y.domain(this.props.questionIds); // console.log(x.range());
        x.domain([0, 6]);
        
        var questions = this.props.questions;
        var question_ids = this.props.questionIds;
        var datum = this.props.datum;
        // console.log(datum);
        // console.log(question_ids);
        svg.selectAll('.answer')
            .data(question_ids)
        .enter().append('rect')
            .attr('height', 2 /*x.rangeBand()*/)
            .attr('x', function(d) {
                // console.log(d);
                // // console.log(questions);
                // console.log(datum[d]);
                // console.log(y(datum[d]));
                // return width-x(datum[d]);
                0;
            })
            .attr('width', function(d) {
                // console.log(datum[d] + ': ' + x(6 - datum[d]));
                return width-x(datum[d] == 0 || datum[d] == 6 ? 0 : 6-datum[d]);
            })
            .attr('y', function(d) {
                return y(d) + 3 //x.rangeBand();
                // return x(d) + (x.rangeBand() / question_ids.length)
            })
            .attr('class', 'answer-bar')
            .style('fill', '#003399');




        // _.each(this.props.questions, function(q, id) {
        //     console.log(this.props.datum[id]);
        //     // console.log(q);
        // }, this)

        // this.props.handleDoneLoad(this.props.rowID);
    }

    setStatus(valname, stati, current_stati, e) {
        e.preventDefault();
        e.stopPropagation();
        $(e.target).blur();
        // console.log(this);

        console.debug(arguments);
        console.debug(this.props);
        this.props.updateRowCommentStatus(this.props.datum.row_id, this.props.datum.luid_str, valname, stati, current_stati);
    }

    // handleChange: function(e) {
    //     // e.preventDefault();
    //     // e.stopPropagation();
    //     console.log('handleChange');
    //     console.log('current: ' + this.props.datum.comment_stati);

    //     var stati = this.props.datum.comment_stati;
    //     if ((stati&4) == 4) {
    //         console.log('has status');
    //         // stati = stati^4;
    //         stati &= ~4;
    //     } else {
    //         console.log('no has status');
    //         stati = stati|4;
    //     }
    //     console.log('now: ' + stati);
    //     // this.props.updateRowCommentStatus(this.props.rowID, stati);
    //     this.setStatus(stati, e);
    // },

    noPropagate(e) {
        e.stopPropagation();
    }

    renderCategoryButtons() {
        return <div />
    }

    renderStatiButtons(valname, current_stati, avail_stati, grouped) {
        // console.debug('renderStatiButtons', valname, current_stati, avail_stati, grouped);
        /*
            avail_stati: {1: 'Low', 2: 'Medium', 3: 'High'}
        */
        // console.log(avail_stati);
        var buttons = map(avail_stati, (stati_obj, index) => {
            let stati = stati_obj.status_val;
            let name = stati_obj.status_desc;

            // console.log(valname + ', ' + current_stati + ', ' + stati);

            var classes = classNames({
                'btn': true,
                'btn-xs': true,
                'btn-outlined': true,
                'btn-default': (valname != 'disp_stati' || (valname == 'disp_stati' && stati == 1)),
                'btn-primary': (valname == 'disp_stati' && stati == 2),
                'btn-danger': (valname == 'disp_stati' && stati == 4),
                'btn-warning': (valname == 'disp_stati' && stati == 16),
                'active': ((current_stati&stati) == stati)
            });

            // var bsStyle;
            // if (valname != 'disp_stati' || (valname == 'disp_stati' && stati == 1)) {
            //     bsStyle = 'default';
            // } else if ((valname == 'disp_stati' && stati == 2)) {
            //     bsStyle = 'primary';
            // } else if ((valname == 'disp_stati' && stati == 4)) {
            //     bsStyle = 'danger';
            // } else if ((valname == 'disp_stati' && stati == 16)) {
            //     bsStyle = 'warning';
            // }

            var key = 'stati-' + stati;

            return <button type="button" className={classes} onClick={this.setStatus.bind(this, valname, stati, current_stati)} key={key}>{name}</button>
            // return <Button xs outlined bsStyle={bsStyle} active={(current_stati&stati) == stati} onClick={this.setStatus.bind(this, valname, stati)} key={key}>{name}</Button>
        });

        if (grouped) {
            buttons = <ButtonGroup>
                        {buttons}
                      </ButtonGroup>
        }
        // console.debug(buttons);
        return (
            <Col md={9}>
                {buttons}
            </Col>
        )
    }

    setSentiment(stati, e) {
        // console.log(arguments);
        this.noPropagate(e);
    }

    setUsefulness(stati, e) {
        this.noPropagate(e);
    }

    setDisposition(stati, e) {
        this.noPropagate(e);
    }

    setCategory(stati, e) {
        this.noPropagate(e);
    }

    render() {
        // console.log('stati: ' + this.props.datum.comment_stati)
        // console.log(this.props);
        var row_id = 'row-' + this.props.datum.row_id;
        // var answers_id = 'answers-' + this.props.datum.row_id;
        // var cx = React.addons.classSet;
        // var classes = cx({
        //     'row': true,
        //     'bg-success': ((this.props.datum.comment_stati&1) == 1),
        //     'bg-warning': ((this.props.datum.comment_stati&2) == 2)
        // });

        // var useless_checked = ((this.props.datum.comment_stati&4) == 4);
        // console.log('useless_checked: ' + useless_checked + ', ' + this.props.datum.comment_stati + ', ' + (this.props.datum.comment_stati&4));

        



        // var usefullness = 2,
        //     sentiment = 2,
        //     categories = 0,
        //     disposition = 0;
// console.log(this.props);
        return (
            <div className="row comment">
                <div className='col-md-6'>
                    <div className="row no-border"><div className='col-md-4'>{this.props.datum.datetime_str}</div></div>
                    <div className="row no-border"><div className='col-md-12'><strong>{this.props.datum.comments}</strong></div></div>
                    <div className="row no-border">
                        <div className="col-md-3">{this.props.datum.lesson_tag}</div>
                        <div className="col-md-2">Lang: {this.props.datum.lang_code}</div>
                        <div className="col-md-2">State: {this.props.datum.orgz_state}</div>
                        <div className="col-md-5">{this.props.datum.pkg_orgz_desc}</div>
                        {/*<div className="col-md-3">State: TN</div>
                        <div className="col-md-3">Course: Applied Mathematics 3</div>
                        <div className="col-md-3">Language: EN</div>*/}
                    </div>
                </div>
                <div className='col-md-1' id={row_id}></div>
                <div className='col-md-5'>
                    <div className="row stati">
                        <div className='col-md-3'><strong>Usefulness: </strong></div>
                        {this.renderStatiButtons('useful_val', this.props.datum.useful_val, this.props.stati.useful_val.items, true)}
                    </div>
                    <div className="row stati">
                        <div className='col-md-3'><strong>Sentiment: </strong></div>
                        {this.renderStatiButtons('custsent_val', this.props.datum.custsent_val, this.props.stati.custsent_val.items, true)}
                    </div>
                    <div className="row stati">
                        <div className='col-md-3'><strong>Categories: </strong></div>
                        {this.renderStatiButtons('cat_stati', this.props.datum.cat_stati, this.props.stati.cat_stati.items, false)}
                    </div>
                    <div className="row stati">
                        <div className='col-md-3'><strong>Disposition: </strong></div>
                        {this.renderStatiButtons('disp_stati', this.props.datum.disp_stati, this.props.stati.disp_stati.items, true)}
                    </div>
                </div>
                {/*}
                <div className="col-md-1"><button type="button" className="btn btn-success btn-xs" onClick={this.setStatus.bind(this, 1)}>Positive</button></div>
                <div className="col-md-1"><button type="button" className="btn btn-warning btn-xs" onClick={this.setStatus.bind(this, 2)}>Negative</button></div>
                <div className="col-md-1"><label><input type="checkbox" checked={useless_checked} onChange={this.handleChange} onClick={this.noPropagate} />Useless</label></div>
                */}
            </div>
        )
    }
}