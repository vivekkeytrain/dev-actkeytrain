import React, { Component } from 'react';
import LangFilter from '../../common/LangFilter'

import { map } from 'lodash';

export default class Filters extends Component {
    onUpdateMode(e) {
        this.props.onModeChange(e.target.value);
    }

    render() {
        console.log(this.props);

        return (
            <Row>
                <Col xs={12} sm={3}>
                    <LangFilter 
                        langs={this.props.langs} 
                        lang={this.props.lang}
                        onLangChange={this.props.onLangChange}
                    />
                </Col>

                <Col xs={12} sm={3}>
                    <Select control id="mode" onChange={this.onUpdateMode.bind(this)} value={this.props.mode}>
                        {map(this.props.modes, (val, key) => 
                            <option key={key} value={val.status_val}>{val.status_desc}</option>
                        )}
                    </Select>
                </Col>
            </Row>
        )
    }
}


