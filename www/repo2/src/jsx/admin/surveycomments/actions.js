import fetchJsonp from 'fetch-jsonp';

/*  Action types    */
export const SET_LANG = 'SET_LANG';
export const SET_MODE = 'SET_MODE';
export const SET_DISPLAYED_ROW_COUNT = 'SET_DISPLAYED_ROW_COUNT';
export const SET_SELECTED_ROW = 'SET_SELECTED_ROW';

/*  Async action types  */

export const FETCH_QUESTIONS_REQUEST = 'FETCH_QUESTIONS_REQUEST';
export const FETCH_QUESTIONS_SUCCESS = 'FETCH_QUESTIONS_SUCCESS';
export const FETCH_QUESTIONS_FAILURE = 'FETCH_QUESTIONS_FAILURE';

export const FETCH_SURVEY_DATA_REQUEST = 'FETCH_SURVEY_DATA_REQUEST';
export const FETCH_SURVEY_DATA_SUCCESS = 'FETCH_SURVEY_DATA_SUCCESS';
export const FETCH_SURVEY_DATA_FAILURE = 'FETCH_SURVEY_DATA_FAILURE';

export const FETCH_STATI_REQUEST = 'FETCH_STATI_REQUEST';
export const FETCH_STATI_SUCCESS = 'FETCH_STATI_SUCCESS';
export const FETCH_STATI_FAILURE = 'FETCH_STATI_FAILURE';

export const SET_STATI_REQUEST = 'SET_STATI_REQUEST';
export const SET_STATI_SUCCESS = 'SET_STATI_SUCCESS';
export const SET_STATI_FAILURE = 'SET_STATI_FAILURE';

import { invert } from 'lodash';

/*  Other contstants    */

export const LangOptions = {
    EN: 1,
    ES: 2
};

export const slice_size = 10;

/*  Action creators */

export function setLang(lang) {
    return { 
        type: SET_LANG,
        lang
    }
}

export function setMode(mode) {
    return {
        type: SET_MODE,
        mode
    }
}

export function setDisplayedRowCount(count) {
    return {
        type: SET_DISPLAYED_ROW_COUNT,
        count
    }
}

export function setSelectedRow(row_id) {
    return {
        type: SET_SELECTED_ROW,
        row_id
    }
}

function fetchQuestionsRequest() {
    return { type: FETCH_QUESTIONS_REQUEST };
}

function fetchQuestionsSuccess(questions) {
    return { 
        type: FETCH_QUESTIONS_SUCCESS,
        questions
    }
}

function fetchQuestionsFailure(ex) {
    return {
        type: FETCH_QUESTIONS_FAILURE,
        ex
    }
}

export function fetchQuestions() {
    return function(dispatch) {
        dispatch(fetchQuestionsRequest());

        return fetchJsonp('http://es.run.keytrain.com/cmiscorm/survey_questions.asp?mode=1')
            .then(
                response => response.json(),
                ex => dispatch(fetchQuestionsFailure(ex))
            )
            .then(json => dispatch(fetchQuestionsSuccess(json)))
            .catch(ex => dispatch(fetchQuestionsFailure(ex)));
    }
}

function fetchSurveyDataRequest(disp_stati, lang_id) {
    return { 
        type: FETCH_SURVEY_DATA_REQUEST,
        disp_stati,
        lang_id 
    };
}

function fetchSurveyDataSuccess(disp_stati, lang_id, data) {
    // console.debug('fetchSurveyDataSuccess', disp_stati, data);
    return {
        type: FETCH_SURVEY_DATA_SUCCESS,
        disp_stati,
        lang_id,
        data
    }
}

function fetchSurveyDataFailure(ex) {
    return {
        type: FETCH_SURVEY_DATA_FAILURE,
        ex
    }
}

export function fetchSurveyData(disp_stati, lang_id) {
    return function(dispatch) {
        dispatch(fetchSurveyDataRequest(disp_stati, lang_id));

        console.log(`http://dev.admapi.actkeytrain.com/dbwebapi/dbo.rpt_sp_survey_data?disp_stati=${disp_stati}&lang_id=${lang_id}`);
        return fetchJsonp(`http://dev.admapi.actkeytrain.com/dbwebapi/dbo.rpt_sp_survey_data?disp_stati=${disp_stati}&lang_id=${lang_id}`, {
            timeout: 20000
        })
            .then(
                response => response.json(),
                ex => dispatch(fetchSurveyDataFailure(ex))
            )
            .then(json => dispatch(fetchSurveyDataSuccess(disp_stati, lang_id, json.ResultSets[0])))
            // .catch(ex => dispatch(fetchSurveyDataFailure(ex)));
    }
}

function fetchStatiRequest(valname) {
    return { 
        type: FETCH_STATI_REQUEST,
        valname
    };
}

function fetchStatiSuccess(valname, data) {
    return {
        type: FETCH_STATI_SUCCESS,
        valname,
        data
    }
}

function fetchStatiFailure(valname, ex) {
    return {
        type: FETCH_STATI_FAILURE,
        valname,
        ex
    }
}

export function fetchStati(valname) {
    return function(dispatch) {
        dispatch(fetchStatiRequest(valname));

        return fetchJsonp('http://dev.admapi.actkeytrain.com/dbwebapi/gen.sp_get_stati?objname=log_tbl_session_rating&valname=' + valname)
            .then(
                response => response.json(),
                ex => dispatch(fetchStatiFailure(valname, ex))
            )
            .then(json => dispatch(fetchStatiSuccess(valname, json.ResultSets[0])))
            .catch(ex => dispatch(fetchStatiFailure(valname, ex)));
    }
}

function setStatiRequest(row_id, luid_str, stati_col, stati_val) {
    return {
        type: SET_STATI_REQUEST,
        row_id,
        luid_str,
        stati_col,
        stati_val
    }
}

function setStatiSuccess(row_id, luid_str, stati_col, stati_val, data) {
    return {
        type: SET_STATI_SUCCESS,
        row_id,
        luid_str,
        stati_col,
        stati_val,
        data
    }
}

function setStatiFailure(row_id, ex) {
    return {
        type: SET_STATI_FAILURE,
        row_id,
        ex
    }
}

export function setStati(row_id, luid_str, stati_col, stati_val, current_stati) {
    console.debug('setStati', row_id, luid_str, stati_col, stati_val, current_stati);
    return function(dispatch) {
        let new_stati = current_stati;
        if (stati_col == 'cat_stati') {
            //  Multiple stati allowed
            console.debug((current_stati&stati_val));
            if ((current_stati&stati_val) > 0) {
                //  Stati bit already set, unset it
                new_stati &= ~stati_val;
            } else {
                //  Stati bit not set, set it
                new_stati |= stati_val;
            }
            console.debug(new_stati);
        } else {
            //  Only single stati allowed
            if (current_stati == stati_val) {
                new_stati = 0;
            } else {
                new_stati = stati_val;
            }
        }

        dispatch(setStatiRequest(row_id, luid_str, stati_col, new_stati));

        return fetchJsonp('http://dev.admapi.actkeytrain.com/dbwebapi/dbo.api_sp_set_stati?row_id=' + row_id + '&luid_str=' + luid_str + '&stati_col=' + stati_col + '&stati_val=' + new_stati)
            .then(
                response => response.json(),
                ex => dispatch(setStatiFailure(row_id, ex))
            )
            .then(json => dispatch(setStatiSuccess(row_id, luid_str, stati_col, new_stati, json.ResultSets[0])))
            .catch(ex => dispatch(setStatiFailure(row_id, ex)));
    }
}