import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { invert } from 'lodash';
import { 
    LangOptions, CHART_HEIGHT, CHART_WIDTH, DisplayModes,
    setStartDate, setEndDate, setMode, setLang, setGranularity, setMetric, setMultiContent, setUnit, setCourse, closeMessage,
    fetchSessionData, fetchEditorContent
} from '../actions';

import { filteredContentSelector } from '../selectors';
import Chart from '../components/Chart';
import Filters from '../components/Filters';
import MessageCenter from 'common/MessageCenter';
// import LangFilter from '../../common/LangFilter';


class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            chartDataObj: {}
        }
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchEditorContent());
    }

    componentWillReceiveProps(props) {
        console.debug('componentWillReceiveProps', props);
        let modeObj, chartDataObj, dataObj, xTicks, x, ys,
            mode = props.mode,
            unit = props.unit,
            course = props.course,
            lang = props.lang;

        if (mode) {
            console.debug('mode', mode);
            if (mode.indexOf('Usage by Course') == 0) {
                if (unit) {
                    if (lang) {
                        let lang_code = invert(LangOptions)[lang];
                        if (course) {
                            //  lang and course
                            modeObj = props[DisplayModes[mode]['lang']['course']];
                        } else {
                            //  lang, no course
                            modeObj = props[DisplayModes[mode]['lang']['all']];
                        }

                        if (modeObj) {
                            dataObj = modeObj.data[lang_code];
                            xTicks = modeObj.xTicks[lang_code];
                            x = modeObj.xs[lang_code];
                            ys = modeObj.ys[lang_code];
                        }
                    } else {
                        if (course) {
                            //  No lang, course
                            modeObj = props[DisplayModes[mode]['course']];
                        } else {
                            //  No lang, no course
                            modeObj = props[DisplayModes[mode]['all']];
                        }

                        if (modeObj) {
                            dataObj = modeObj.data;
                            xTicks = modeObj.xTicks;
                            x = modeObj.x;
                            ys = modeObj.ys;
                        }
                        
                    }
                }
            } else {
                if (lang) {
                    console.debug('lang', lang);
                    let lang_code = invert(LangOptions)[lang];
                    modeObj = props[DisplayModes[mode]['lang']];

                    if (modeObj) {
                        dataObj = modeObj.data[lang_code];
                        xTicks = modeObj.xTicks[lang_code];
                        x = modeObj.xs[lang_code];
                        ys = modeObj.ys[lang_code];
                    }
                } else {
                    modeObj = props[DisplayModes[mode]['all']];
                    console.debug(modeObj);

                    if (modeObj) {
                        dataObj = modeObj.data;
                        xTicks = modeObj.xTicks;
                        x = modeObj.x;
                        ys = modeObj.ys;
                    }
                }
            }

            chartDataObj = {
                dataObj,
                xObj: x,
                xTicks,
                ysObj: ys
            }
        }

        this.setState({
            chartDataObj
        })
    }

    render() {
        //  Injected by connect() call:
        const { dispatch, units, courses, content_hierarchy, filters, lang, mode, granularity, metric, start_date, end_date, content, unit, course, lesson, messages, session_data_totals, session_data_totals_victory, session_lang_data_totals, session_course_data_totals, session_unit_data_totals, session_course_lang_data_totals, session_unit_lang_data_totals, session_data_lang_totals_victory, session_data_metric_totals_victory, session_data_metric_lang_totals_victory, session_data_unit_totals_victory, filters_are_dirty, is_fetching } = this.props;

        console.debug(this.props);
        console.debug(this.state);

        

/*
export const DisplayModes = {
    'Total Usage by Language': {
        'all': 'session_data_totals_victory',
        'lang': 'session_data_lang_totals_victory'
    },
    'Total Usage by Metric': {
        'all': 'session_data_metric_totals_victory',
        'lang': 'session_data_metric_lang_totals_victory'
    },
    'Total Usage By Unit/Course': {
        'all': 'session_data_unit_totals_victory',
        'course': 'session_data_unit_course_totals_victory',
        'lang': {
            'all': 'session_data_lang_unit_totals_victory',
            'course': 'session_data_lang_unit_course_totals_victory'
        }
    }
}


componentWillReceiveProps(props) {
        console.debug(this.state);
        console.debug(props);

        let dataObj, xTicks, x, ys;

        let modeObj = DisplayModes[props.mode];
        console.debug(modeObj);
        if (props.mode) {
            if (props.lang) {
                let lang_code = invert(LangOptions)[props.lang];
                modeObj = props[modeObj['lang']];
                
                if (!modeObj) return;

                dataObj = modeObj.data[lang_code];
                xTicks = modeObj.xTicks[lang_code];
                x = modeObj.xs[lang_code];
                ys = modeObj.ys[lang_code];
            } else {
                modeObj = props[modeObj['all']];

                if (!modeObj) return;

                dataObj = modeObj.data;
                xTicks = modeObj.xTicks;
                x = modeObj.x;
                ys = modeObj.ys;
            }
        }

        console.debug(dataObj, xTicks, x, ys);

        this.setState({
            dataObj: dataObj,
            xObj: x,
            ysObj: ys,
            xTicks: xTicks
        });
    }

    sessionDataTotalsVictory={session_data_totals_victory}
                                                    sessionDataLangTotalsVictory={session_data_lang_totals_victory}
                                                    sessionDataTotals={session_data_totals}
                                                    sessionLangDataTotals={session_lang_data_totals}
                                                    sessionCourseDataTotals={session_course_data_totals}
                                                    sessionUnitDataTotals={session_unit_data_totals}
                                                    sessionCourseLangDataTotals={session_course_lang_data_totals}
                                                    sessionUnitLangDataTotals={session_unit_lang_data_totals}
                                                    sessionDataMetricTotalsVictory={session_data_metric_totals_victory}
                                                    sessionDataMetricLangTotalsVictory={session_data_metric_lang_totals_victory}
                                                    sessionDataUnitTotalsVictory={session_data_unit_totals_victory}

*/


        // <Chart questionIds={question_ids} answerVals={answer_vals} answerSumsArr={answer_sums_arr} answerSumsMax={answer_sums_max} />

        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PanelContainer controlStyles='bg-blue fg-white'>
                                <PanelHeader className='bg-blue fg-white' style={{margin: 0}}>
                                    <Grid>
                                        <Row>
                                            <Col xs={12}>
                                                <h3>Course Usage Reports</h3>
                                                <p>
                                                    This report shows course usage in several different ways. Choose the basic report type from the "Select Display Mode" list. Choose a date range and select Monthly or Weekly.
                                                </p>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelHeader>
                                <PanelBody>
                                    <Grid>
                                        <Row>
                                            <Col xs={12} style={{padding: 25}}>
                                                <MessageCenter onMessageClose={index => dispatch(closeMessage(index))}>
                                                    {messages.map((message, ind) => {
                                                        return <div type={message.type} index={ind} visible={message.visible} key={"message_" + ind}>{message.message}</div>
                                                    })}
                                                </MessageCenter>
                                                <Filters 
                                                    langs={LangOptions}
                                                    lang={lang}
                                                    startDate={start_date}
                                                    endDate={end_date}
                                                    mode={mode}
                                                    granularity={granularity}
                                                    metric={metric}
                                                    multiContent={filters.content_arr}
                                                    units={units}
                                                    unit={unit}
                                                    courses={courses}
                                                    course={course}
                                                    contentHierarchy={content_hierarchy}
                                                    filtersAreDirty={filters_are_dirty}
                                                    isFetching={is_fetching}
                                                    onUpdateStartDate={start_date => dispatch(setStartDate(start_date))}
                                                    onUpdateEndDate={end_date => dispatch(setEndDate(end_date))}
                                                    onUpdateDisplayMode={mode => dispatch(setMode(mode))}
                                                    onUpdateMetric={metric => dispatch(setMetric(metric))}
                                                    onUpdateGranularity={granularity => dispatch(setGranularity(granularity))}
                                                    onUpdateMultiContent={content_arr => dispatch(setMultiContent(content_arr))}
                                                    onTextFilterFetch={() => dispatch(fetchSessionData(start_date, end_date, granularity))}
                                                    onUpdateUnit={(unit) => dispatch(setUnit(unit))}
                                                    onUpdateCourse={(course) => dispatch(setCourse(course))}
                                                    onLangChange={(lang_id) => {
                                                        dispatch(setLang(lang_id))
                                                    }}
                                                    />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs={12}>
                                                <Chart 
                                                    mode={mode}
                                                    lang={lang}
                                                    metric={metric}
                                                    chartDataObj={this.state.chartDataObj}
                                                    width={CHART_WIDTH} 
                                                    height={CHART_HEIGHT} 
                                                     />
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelBody>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        )
    }
}

export default connect(filteredContentSelector)(App);
