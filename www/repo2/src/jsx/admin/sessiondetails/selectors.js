import { createSelector } from 'reselect';
import d3 from 'd3';

import { LangOptions, CHART_HEIGHT, CHART_WIDTH, TOTALS_COLORS, DATA_FIELDS, isUTCDate, urlFormatDate } from './actions';

import { map, chain, forEach, filter, flatten, invert, find, reduce, max, min, range, includes, remove, keys, values, isNaN } from 'lodash';

//  http://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
function shadeColor(color, percent) {   
    var f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
    return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

function formatDateRange(start_date, end_date, with_year=false) {
    // console.debug('formatDateRange', start_date, end_date)
    if (!isUTCDate(start_date)) {
        start_date = urlFormatDate(start_date);
        if (!start_date) return 'LOADING ...';
    }   
    start_date = new Date(start_date + 'T12:00:00');

    if (!isUTCDate(end_date)) {
        end_date = urlFormatDate(end_date);
        if (!end_date) return 'LOADING ...';
    }
    end_date = new Date(end_date + 'T12:00:00');

    if (start_date == 'Invalid Date' || end_date == 'Invalid Date') return 'LOADING ...';

    // console.debug(start_date);
    // console.debug(end_date);

    // console.debug(start_date);
    if (isNaN(start_date.getMonth()) || isNaN(end_date.getMonth())) return 'LOADING ...';

    

    let sd = `${start_date.getMonth()+1}/${start_date.getDate()}`
    let ed = `${end_date.getMonth()+1}/${end_date.getDate()}`

    if (with_year) {
        sd += '/' + (''+start_date.getFullYear()).substring(2);
        ed += '/' + (''+end_date.getFullYear()).substring(2);
    }

    return `${sd} - ${ed}`;
}

/*  Get a unique set of units   */
function selectUniqueUnits(content, tags) {
    return map(tags, (tag) => {
        let match = find(content, {'unit_tag': tag});

        return {
            id: match.unit_id,
            tag: match.unit_tag,
            desc: match.unit_desc,
            seq: match.unit_seq,
            display: match.unit_display
        }
    });
}

/*  Select a unique set of unit tags that will be used to create a unique set of units  */
function selectUnitTags(content) {
    return chain(content)
        .map('unit_tag')
        .uniq()
        .value();
}


function selectUniqueCourses(content, tags) {
    // console.debug('selectUniqueCourses', content, tags);
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'course_tag': tag});

            return {
                id: match.course_id,
                tag: match.course_tag,
                desc: match.course_desc,
                seq: match.course_seq,
                display: match.course_display
            }
        })
        .value();
}

function selectCourseTags(content, unit) {
    // console.debug('selectCourseTags', content, unit);
    return chain(content)
        .filter({'unit_id': unit})
        .map('course_tag')
        .uniq()
        .value();
}

export function selectCourseTag(course, courses) {
    if (!course || !courses || course == -1) return '';
    // console.debug('selectCourseTag', course, courses);
    return chain(courses).find({'id': course}).value().tag;
}


/*  Content filtered by current selected state */
function selectContent(content, unit) {
    // console.debug(content);
    // console.debug(unit, typeof unit);
    // console.debug([unit, course, lesson, topic]);
    let filtered_content = {
        units: content,
        courses: []
    }
    if (unit != '')
        filtered_content.courses = filter(content, {'unit_id': unit});

    // console.debug(filtered_content.courses);

    return filtered_content;
}

function selectContentHierarchy(content, unit_tags) {
    // console.debug('selectContentHierarchy', content, unit_tags);
    return map(unit_tags, (tag) => {
        let match = find(content, {'unit_tag': tag});
        console.debug(match);

        let course_tags = selectCourseTags(content, match.unit_id);
        console.debug(course_tags);
        let courses = selectUniqueCourses(content, course_tags);
        console.debug(courses);

        return {
            id: match.unit_id,
            tag: match.unit_tag,
            desc: match.unit_desc,
            seq: match.unit_seq,
            display: match.unit_display,
            courses
        }
    });
}

function selectFilteredSessionData(session_data, session_data_segments, start_date, end_date, granularity) {
    // console.debug(session_data);
    // console.debug(session_data_segments);
    let key = `${urlFormatDate(start_date)}-${urlFormatDate(end_date)}`;
    if (!session_data_segments[granularity] || !session_data_segments[granularity][key]) return [];
    // console.debug(`session_data_segments[${granularity}][${key}]`);
    // console.debug(session_data_segments[granularity][key]);

    return chain(session_data_segments[granularity][key]).map((obj) => {
        if (!session_data[`${obj.start_date}-${obj.end_date}`]) return [];
        // console.debug(session_data[`${obj.start_date}-${obj.end_date}`].data);
        return {
            start_date: obj.start_date,
            end_date: obj.end_date,
            data: session_data[`${obj.start_date}-${obj.end_date}`].data
        }
    }).value();

}

function selectSessionDataTotals(session_data) {
    //  After session data has been filtered down based upon date we find the row that is null for unit/course/lang, this is the rollup
    // console.debug(session_data);
    return map(session_data, (segment) => {
        return {
            start_date: segment.start_date,
            end_date: segment.end_date,
            data: find(segment.data, (row) => {
                return (!row.unit_id && !row.course_id && !row.lang_id);
            })
        }
    });
}

function selectSessionsDataTotalsVictory(session_data_totals) {
    /*  { data: ..., dataAttributes: ..., labels: ... } */

    //  Determine the scale maxes 
    let maxes = [],
        ys = [];
    
    // console.debug(session_data_totals);
    maxes = map(keys(DATA_FIELDS), (field) => {
        return reduce(session_data_totals, (memo, obj, ind) => {
            // console.debug(field, memo, obj.data[field])
            return max([memo, obj.data ? obj.data[field] : 0]);
        }, 0)
    });

    ys = map(keys(DATA_FIELDS), (field, ind) => {
        return {
            label: DATA_FIELDS[field],
            scale: d3.scale.linear().range([CHART_HEIGHT, 0]).domain([0, maxes[ind]]).nice()
        }
    })


    let x = d3.scale.ordinal().rangeRoundBands([50, CHART_WIDTH-50], .1).domain(range(session_data_totals.length));

    let data = chain(session_data_totals).map((obj, ind) => {
        // console.debug(obj);
        // console.debug(ind, x(ind), (x(ind) + (x.rangeBand() / session_data_totals.length)));
        return map(keys(DATA_FIELDS), (field, i) => {
            // console.debug(obj.data[field], ys[i].domain(), ys[i](obj.data[field]));
            if (obj.data && obj.data[field])
                return {
                    x: (x(ind) + (x.rangeBand() / keys(DATA_FIELDS).length * i)),
                    y: ys[i].scale(obj.data[field]),
                    fill: TOTALS_COLORS[i],
                    width: x.rangeBand() / keys(DATA_FIELDS).length,
                    label: `${formatDateRange(obj.start_date, obj.end_date)}: ${DATA_FIELDS[field]}: ${obj.data[field]}`
                };
            else
                return {
                    x: (x(ind) + (x.rangeBand() / keys(DATA_FIELDS).length * i)),
                    y: ys[i].scale(0),
                    fill: TOTALS_COLORS[i],
                    width: x.rangeBand() / keys(DATA_FIELDS).length,
                    label: `${formatDateRange(obj.start_date, obj.end_date)}: ${DATA_FIELDS[field]}: 0`
                };
        })
    }).flatten().value();

    let start_date = reduce(session_data_totals, (memo, obj, ind) => {
        let d = new Date(obj.start_date + 'T12:00:00');
        return d < memo ? d : memo;
    }, new Date('2070-01-01'));
    let end_date = reduce(session_data_totals, (memo, obj, ind) => {
        let d = new Date(obj.end_date + 'T12:00:00');
        return d > memo ? d : memo;
    }, new Date('1970-01-01'));
    console.debug(start_date, end_date);
    let xTicks = map(session_data_totals, (obj, ind) => {
        let label = formatDateRange(obj.start_date, obj.end_date, (start_date.getFullYear() != end_date.getFullYear()));
        return label;
    });

    console.debug(xTicks);


    return {
        data,
        xTicks,
        x,
        ys
    }
}

function selectSessionLangDataTotals(session_data) {
    let retObj = {};
    forEach(session_data, (segment) => {
        let lang_rows = filter(segment.data, (row) => {
            return (!row.unit_id && !row.course_id && row.lang_id);
        });

        // console.debug(segment);
        // console.debug(lang_rows);

        forEach(lang_rows, (row) => {
            let lang_code = invert(LangOptions)[row.lang_id];
            let dataObj = {
                start_date: segment.start_date,
                end_date: segment.end_date,
                data: row
            };
            if (retObj[lang_code]) {
                retObj[lang_code].push(dataObj);
            } else {
                retObj[lang_code] = [dataObj];
            }
        });

        //  Now fill with empty data if need be
        forEach(values(LangOptions), (lang_id) => {
            // console.debug(`find ${lang_id}`);
            if (!find(lang_rows, {'lang_id': lang_id})) {
                // console.debug('not found');
                let lang_code = invert(LangOptions)[lang_id];
                let row = {
                    lang_id,
                    course_id: null,
                    unit_id: null
                };

                forEach(keys(DATA_FIELDS), (field) => {
                    row[field] = 0
                });

                let dataObj = {
                    start_date: segment.start_date,
                    end_date: segment.end_date,
                    data: row

                };

                if (retObj[lang_code]) {
                    retObj[lang_code].push(dataObj);
                } else {
                    retObj[lang_code] = [dataObj];
                }
            }
        });

        // console.debug(retObj);

    });

    return retObj;
}

function selectSessionLangDataTotalsVictory(session_lang_data_totals) {
    let maxes = {},
        ys = {},
        data = {},
        xTicks = {},
        xs = {};

    forEach(session_lang_data_totals, (obj, lang) => {
        // console.debug(obj);
        maxes[lang] = map(keys(DATA_FIELDS), (field) => {
            return reduce(obj, (memo, obj, ind) => {
                // console.debug(obj.data, field);
                return max([memo, obj.data[field]]);
            }, 0)
        });

        ys[lang] = map(keys(DATA_FIELDS), (field, ind) => {
            return {
                label: DATA_FIELDS[field],
                scale: d3.scale.linear().range([CHART_HEIGHT, 0]).domain([0, maxes[lang][ind]]).nice()
            }
        });

        // console.debug(obj);
        let x = d3.scale.ordinal().rangeRoundBands([50, CHART_WIDTH-50], .1).domain(range(obj.length))
        xs[lang] = x;
        // console.debug(x.domain());

        data[lang] = chain(obj).map((lang_data, ind) => {
            // console.debug(lang_data);
            return map(keys(DATA_FIELDS), (field, i) => {
                // console.debug(lang_data);
                // console.debug(field, lang_data.data[field]);
                if (lang_data.data && lang_data.data[field]) {
                    return {
                        x: (x(ind) + (x.rangeBand() / keys(DATA_FIELDS).length * i)),
                        y: ys[lang][i].scale(lang_data.data[field]),
                        fill: TOTALS_COLORS[i],
                        width: x.rangeBand() / keys(DATA_FIELDS).length,
                        label: `${formatDateRange(lang_data.start_date, lang_data.end_date)}: ${DATA_FIELDS[field]}: ${lang_data.data[field]}`
                    }
                } else
                    return {
                        x: (x(ind) + (x.rangeBand() / keys(DATA_FIELDS).length * i)),
                        y: ys[lang][i].scale(0),
                        fill: TOTALS_COLORS[i],
                        width: x.rangeBand() / keys(DATA_FIELDS).length,
                        label: `${formatDateRange(lang_data.start_date, lang_data.end_date)}: ${DATA_FIELDS[field]}: 0`
                    };
            })
        }).flatten().value();;

        let lang_xticks = [];
        let start_date = reduce(obj, (memo, obj, ind) => {
            let d = new Date(obj.start_date + 'T12:00:00');
            return d < memo ? d : memo;
        }, new Date('2070-01-01'));
        let end_date = reduce(obj, (memo, obj, ind) => {
            let d = new Date(obj.end_date + 'T12:00:00');
            return d > memo ? d : memo;
        }, new Date('1970-01-01'));

        forEach(obj, (lang_data, ind) => {
            let label = formatDateRange(lang_data.start_date, lang_data.end_date, (start_date.getFullYear() != end_date.getFullYear()));
            if (!includes(lang_xticks, label)) lang_xticks.push(label);
        });
        xTicks[lang] = lang_xticks;
        console.debug(xTicks);
    });

    /*
let start_date = reduce(session_data_totals, (memo, obj, ind) => {
        let d = new Date(obj.start_date + 'T12:00:00');
        return d < memo ? d : memo;
    }, new Date('2070-01-01'));
    let end_date = reduce(session_data_totals, (memo, obj, ind) => {
        let d = new Date(obj.end_date + 'T12:00:00');
        return d > memo ? d : memo;
    }, new Date('1970-01-01'));
    let xTicks = map(session_data_totals, (obj, ind) => {
        let label = formatDateRange(obj.start_date, obj.end_date, (start_date.getFullYear() != end_date.getFullYear()));
        return label;
    });

    */

    // console.debug(maxes);
    // console.debug(ys);

    return {
        data,
        xTicks,
        xs,
        ys
    }
}

function selectSessionsDataMetricTotalsVictory(session_lang_data_totals, metric) {
    console.debug('selectSessionsDataMetricTotalsVictory', metric);

    let ys = [],
        data = [],
        xTicks = [],
        xs = [];

    let i = 0;
    forEach(session_lang_data_totals, (obj, lang) => {
        let metric_max = reduce(obj, (memo, obj, ind) => {
            // console.debug(memo, obj.data, obj.data[metric]);
            return max([memo, obj.data ? obj.data[metric] : 0]);
        }, 0);
        // console.debug(metric_max);

        ys.push({
            label: lang,
            scale: d3.scale.linear().range([CHART_HEIGHT, 0]).domain([0, metric_max]).nice()
        });

        let x = d3.scale.ordinal().rangeRoundBands([50, CHART_WIDTH-50], .1).domain(range(obj.length))
        xs.push(x);

        data = data.concat(chain(obj).map((lang_data, ind) => {
            // console.debug(lang_data);
            // console.debug(metric, lang_data.data[metric], ys[i].scale(lang_data.data[metric]));
            if (lang_data.data && lang_data.data[metric]) {
                return {
                    x: (x(ind) + (x.rangeBand() / keys(LangOptions).length * i)),
                    y: ys[i].scale(lang_data.data[metric]),
                    fill: TOTALS_COLORS[i],
                    width: x.rangeBand() / keys(LangOptions).length,
                    label: `${formatDateRange(lang_data.start_date, lang_data.end_date)} : ${lang.toUpperCase()}: ${DATA_FIELDS[metric]}: ${lang_data.data[metric]}`
                }
            } else {
                return {
                    x: (x(ind) + (x.rangeBand() / keys(LangOptions).length * i)),
                    y: ys[i].scale(0),
                    fill: TOTALS_COLORS[i],
                    width: x.rangeBand() / keys(LangOptions).length,
                    label: `${formatDateRange(lang_data.start_date, lang_data.end_date)} : ${lang.toUpperCase()}: ${DATA_FIELDS[metric]}: 0`
                };
            }
        }).flatten().value());

        let start_date = reduce(obj, (memo, obj, ind) => {
            let d = new Date(obj.start_date + 'T12:00:00');
            return d < memo ? d : memo;
        }, new Date('2070-01-01'));
        let end_date = reduce(obj, (memo, obj, ind) => {
            let d = new Date(obj.end_date + 'T12:00:00');
            return d > memo ? d : memo;
        }, new Date('1970-01-01'));

        forEach(obj, (lang_data, lang) => {
            let label = formatDateRange(lang_data.start_date, lang_data.end_date, (start_date.getFullYear() != end_date.getFullYear()));
            if (!includes(xTicks, label)) xTicks.push(label)
        });

        i++;
    });


    return {
        data,
        xTicks,
        xs,
        ys
    }
}

function selectSessionDataMetricLangTotalsVictory(session_lang_data_totals, metric) {
    console.debug('selectSessionDataMetricLangTotalsVictory', metric);

    let ys = {},
        data = {},
        xTicks = {},
        xs = {};

    let i = 0;
    forEach(session_lang_data_totals, (obj, lang) => {
        let metric_max = reduce(obj, (memo, obj, ind) => {
            return max([memo, obj.data ? obj.data[metric] : 0]);
        }, 0);

        ys[lang] = [{
            label: lang,
            scale: d3.scale.linear().range([CHART_HEIGHT, 0]).domain([0, metric_max]).nice()
        }];

        let x = d3.scale.ordinal().rangeRoundBands([50, CHART_WIDTH-50], .1).domain(range(obj.length))
        xs[lang] = x;

        data[lang] = chain(obj).map((lang_data, ind) => {
            // console.debug(lang_data);
            if (lang_data.data && lang_data.data[metric]) {
                return {
                    x: x(ind),
                    y: ys[lang][0].scale(lang_data.data[metric]),
                    fill: TOTALS_COLORS[i],
                    width: x.rangeBand(),
                    label: `${formatDateRange(lang_data.start_date, lang_data.end_date)} : ${lang.toUpperCase()}: ${DATA_FIELDS[metric]}: ${lang_data.data[metric]}`
                }
            } else {
                return {
                    x: x(ind),
                    y: ys[lang][0].scale(0),
                    fill: TOTALS_COLORS[i],
                    width: x.rangeBand(),
                    label: `${formatDateRange(lang_data.start_date, lang_data.end_date)} : ${lang.toUpperCase()}: ${DATA_FIELDS[metric]}: 0`
                };
            }
        }).flatten().value();;

        let lang_xticks = [];
        let start_date = reduce(obj, (memo, obj, ind) => {
            let d = new Date(obj.start_date + 'T12:00:00');
            return d < memo ? d : memo;
        }, new Date('2070-01-01'));
        let end_date = reduce(obj, (memo, obj, ind) => {
            let d = new Date(obj.end_date + 'T12:00:00');
            return d > memo ? d : memo;
        }, new Date('1970-01-01'));

        forEach(obj, (lang_data, lang) => {
            let label = formatDateRange(lang_data.start_date, lang_data.end_date, (start_date.getFullYear() != end_date.getFullYear()));
            if (!includes(lang_xticks, label)) lang_xticks.push(label);
        });
        xTicks[lang] = lang_xticks;

        i++;
    });


    return {
        data,
        xTicks,
        xs,
        ys
    }
}

function selectSessionsDataUnitTotalsVictory(session_data_unit_totals, unit) {
    console.debug('selectSessionsDataUnitTotalsVictory', unit);
    let maxes = [],
        ys = [],
        session_data_totals = session_data_unit_totals[unit];

    console.debug(session_data_totals);

    if (!unit || !session_data_totals) 
        return {
            data: [],
            xTicks: [],
            x: null,
            ys: []
        }
    
    console.debug(session_data_totals);
    maxes = map(keys(DATA_FIELDS), (field) => {
        return reduce(session_data_totals, (memo, obj, ind) => {
            return max([memo, obj.data ? obj.data[field] : 0]);
        }, 0)
    });

    ys = map(keys(DATA_FIELDS), (field, ind) => {
        return {
            label: DATA_FIELDS[field],
            scale: d3.scale.linear().range([CHART_HEIGHT, 0]).domain([0, maxes[ind]]).nice()
        }
    })


    let x = d3.scale.ordinal().rangeRoundBands([50, CHART_WIDTH-50], .1).domain(range(session_data_totals.length));

    let data = chain(session_data_totals).map((obj, ind) => {
        return map(keys(DATA_FIELDS), (field, i) => {
            if (obj.data && obj.data[field])
                return {
                    x: (x(ind) + (x.rangeBand() / keys(DATA_FIELDS).length * i)),
                    y: ys[i].scale(obj.data[field]),
                    fill: TOTALS_COLORS[i],
                    width: x.rangeBand() / keys(DATA_FIELDS).length,
                    label: `${formatDateRange(obj.start_date, obj.end_date)}: ${DATA_FIELDS[field]}: ${obj.data[field]}`
                };
            else
                return {
                    x: (x(ind) + (x.rangeBand() / keys(DATA_FIELDS).length * i)),
                    y: ys[i].scale(0),
                    fill: TOTALS_COLORS[i],
                    width: x.rangeBand() / keys(DATA_FIELDS).length,
                    label: `${formatDateRange(obj.start_date, obj.end_date)}: ${DATA_FIELDS[field]}: 0`
                };
        })
    }).flatten().value();

    let start_date = reduce(session_data_totals, (memo, obj, ind) => {
        let d = new Date(obj.start_date + 'T12:00:00');
        return d < memo ? d : memo;
    }, new Date('2070-01-01'));
    let end_date = reduce(session_data_totals, (memo, obj, ind) => {
        let d = new Date(obj.end_date + 'T12:00:00');
        return d > memo ? d : memo;
    }, new Date('1970-01-01'));

    let xTicks = map(session_data_totals, (obj, ind) => {
        let label = formatDateRange(obj.start_date, obj.end_date, (start_date.getFullYear() != end_date.getFullYear()));
        return label;
    });

    return {
        data,
        xTicks,
        x,
        ys
    }
}

function selectSessionLangDataUnitTotalsVictory(session_lang_data_totals, unit) {
    console.debug('selectSessionLangDataUnitTotalsVictory', session_lang_data_totals, unit);
    let maxes = {},
        ys = {},
        data = {},
        xTicks = {},
        xs = {};

    if (unit) {
        forEach(session_lang_data_totals, (obj, lang) => {
            console.debug(obj);
            console.debug(obj[unit]);
            if (obj[unit]) {
                maxes[lang] = map(keys(DATA_FIELDS), (field) => {
                    return reduce(obj[unit], (memo, obj, ind) => {
                        return max([memo, obj.data[field]]);
                    }, 0)
                });

                ys[lang] = map(keys(DATA_FIELDS), (field, ind) => {
                    return {
                        label: DATA_FIELDS[field],
                        scale: d3.scale.linear().range([CHART_HEIGHT, 0]).domain([0, maxes[lang][ind]]).nice()
                    }
                });

                console.debug(obj[unit]);
                let x = d3.scale.ordinal().rangeRoundBands([50, CHART_WIDTH-50], .1).domain(range(obj[unit].length))
                xs[lang] = x;

                data[lang] = chain(obj[unit]).map((lang_data, ind) => {
                    // console.debug(lang_data);
                    return map(keys(DATA_FIELDS), (field, i) => {
                        // console.debug(lang_data);
                        // console.debug(field, lang_data.data[field]);
                        if (lang_data.data && lang_data.data[field]) {
                            return {
                                x: (x(ind) + (x.rangeBand() / keys(DATA_FIELDS).length * i)),
                                y: ys[lang][i].scale(lang_data.data[field]),
                                fill: TOTALS_COLORS[i],
                                width: x.rangeBand() / keys(DATA_FIELDS).length,
                                label: `${formatDateRange(lang_data.start_date, lang_data.end_date)}: ${DATA_FIELDS[field]}: ${lang_data.data[field]}`
                            }
                        } else
                            return {
                                x: (x(ind) + (x.rangeBand() / keys(DATA_FIELDS).length * i)),
                                y: ys[lang][i].scale(0),
                                fill: TOTALS_COLORS[i],
                                width: x.rangeBand() / keys(DATA_FIELDS).length,
                                label: `${formatDateRange(lang_data.start_date, lang_data.end_date)}: ${DATA_FIELDS[field]}: 0`
                            };
                    })
                }).flatten().value();;

                let lang_xticks = [];
                let start_date = reduce(obj[unit], (memo, obj, ind) => {
                    let d = new Date(obj.start_date + 'T12:00:00');
                    return d < memo ? d : memo;
                }, new Date('2070-01-01'));
                let end_date = reduce(obj[unit], (memo, obj, ind) => {
                    let d = new Date(obj.end_date + 'T12:00:00');
                    return d > memo ? d : memo;
                }, new Date('1970-01-01'));

                forEach(obj[unit], (lang_data, lang) => {
                    let label = formatDateRange(lang_data.start_date, lang_data.end_date, (start_date.getFullYear() != end_date.getFullYear()));
                    if (!includes(lang_xticks, label)) lang_xticks.push(label);
                });
                xTicks[lang] = lang_xticks;
            }
        });
    }

    

    // console.debug(maxes);
    // console.debug(ys);

    return {
        data,
        xTicks,
        xs,
        ys
    }
}


function selectSessionCourseDataTotals(session_data, editor_content) {
    let retObj = {};
    forEach(session_data, (segment) => {
        let course_rows = filter(segment.data, (row) => {
            return (!row.unit_id && !row.lang_id && row.course_id);
        });

        forEach(course_rows, (row) => {
            let course = find(editor_content, (content_row) => {
                return content_row.course_id === row.course_id;
            });
            let course_desc = course ? course.course_desc : 'UNKNOWN';
            let dataObj = {
                course: course_desc,
                start_date: segment.start_date,
                end_date: segment.end_date,
                data: row
            };
            if (retObj[row.course_id]) {
                retObj[row.course_id].push(dataObj);
            } else {
                retObj[row.course_id] = [dataObj];
            }
        });
    });
    return retObj;
}

function selectSessionsDataCourseTotalsVictory(session_data_course_totals, course) {
    console.debug('selectSessionsDataCourseTotalsVictory', course);
    let maxes = [],
        ys = [],
        session_data_totals = session_data_course_totals[course];

    console.debug(session_data_totals);

    if (!course || !session_data_totals) 
        return {
            data: [],
            xTicks: [],
            x: null,
            ys: []
        }
    
    console.debug(session_data_totals);
    maxes = map(keys(DATA_FIELDS), (field) => {
        return reduce(session_data_totals, (memo, obj, ind) => {
            return max([memo, obj.data ? obj.data[field] : 0]);
        }, 0)
    });

    ys = map(keys(DATA_FIELDS), (field, ind) => {
        return {
            label: DATA_FIELDS[field],
            scale: d3.scale.linear().range([CHART_HEIGHT, 0]).domain([0, maxes[ind]]).nice()
        }
    })


    let x = d3.scale.ordinal().rangeRoundBands([50, CHART_WIDTH-50], .1).domain(range(session_data_totals.length));

    let data = chain(session_data_totals).map((obj, ind) => {
        return map(keys(DATA_FIELDS), (field, i) => {
            if (obj.data && obj.data[field])
                return {
                    x: (x(ind) + (x.rangeBand() / keys(DATA_FIELDS).length * i)),
                    y: ys[i].scale(obj.data[field]),
                    fill: TOTALS_COLORS[i],
                    width: x.rangeBand() / keys(DATA_FIELDS).length,
                    label: `${formatDateRange(obj.start_date, obj.end_date)}: ${DATA_FIELDS[field]}: ${obj.data[field]}`
                };
            else
                return {};
        })
    }).flatten().value();

    let start_date = reduce(session_data_totals, (memo, obj, ind) => {
        let d = new Date(obj.start_date + 'T12:00:00');
        return d < memo ? d : memo;
    }, new Date('2070-01-01'));
    let end_date = reduce(session_data_totals, (memo, obj, ind) => {
        let d = new Date(obj.end_date + 'T12:00:00');
        return d > memo ? d : memo;
    }, new Date('1970-01-01'));

    let xTicks = map(session_data_totals, (obj, ind) => {
        let label = formatDateRange(obj.start_date, obj.end_date, (start_date.getFullYear() != end_date.getFullYear()));
        return label;
    });

    return {
        data,
        xTicks,
        x,
        ys
    }
}

function selectSessionCourseLangDataTotals(session_data, editor_content) {
    let retObj = {};

    forEach(session_data, (segment) => {
        let course_lang_rows = filter(segment.data, (row) => {
            return (row.unit_id && row.course_id && row.lang_id); // we have rows with unit_id == null but unit/course are bound so those can be ignored 
        });

        forEach(course_lang_rows, (row) => {
            let lang_code = invert(LangOptions)[row.lang_id];
            let course = find(editor_content, (content_row) => {
                return content_row.course_id === row.course_id;
            });
            let course_desc = course ? course.course_desc : 'UNKNOWN';
            let dataObj = {
                course: course_desc,
                start_date: segment.start_date,
                end_date: segment.end_date,
                data: row
            };

            if (retObj[lang_code]) {
                if (retObj[lang_code][row.course_id]) {
                    retObj[lang_code][row.course_id].push(dataObj);
                } else {
                    retObj[lang_code][row.course_id] = [dataObj];
                }
            } else {
                retObj[lang_code] = {
                    [row.course_id]: [dataObj]
                }
            }
        });
    });

    return retObj;
}

function selectSessionLangDataCourseTotalsVictory(session_lang_data_totals, course) {
    console.debug('selectSessionLangDataCourseTotalsVictory', session_lang_data_totals, course);
    let maxes = {},
        ys = {},
        data = {},
        xTicks = {},
        xs = {};

    if (course) {
        forEach(session_lang_data_totals, (obj, lang) => {
            console.debug(obj);
            console.debug(obj[course]);
            maxes[lang] = map(keys(DATA_FIELDS), (field) => {
                return reduce(obj[course], (memo, obj, ind) => {
                    return max([memo, obj.data[field]]);
                }, 0)
            });

            ys[lang] = map(keys(DATA_FIELDS), (field, ind) => {
                return {
                    label: DATA_FIELDS[field],
                    scale: d3.scale.linear().range([CHART_HEIGHT, 0]).domain([0, maxes[lang][ind]]).nice()
                }
            });

            console.debug(obj[course]);
            let x = d3.scale.ordinal().rangeRoundBands([50, CHART_WIDTH-50], .1).domain(range(obj[course].length))
            xs[lang] = x;

            data[lang] = chain(obj[course]).map((lang_data, ind) => {
                // console.debug(lang_data);
                return map(keys(DATA_FIELDS), (field, i) => {
                    // console.debug(lang_data);
                    // console.debug(field, lang_data.data[field]);
                    if (lang_data.data && lang_data.data[field]) {
                        return {
                            x: (x(ind) + (x.rangeBand() / keys(DATA_FIELDS).length * i)),
                            y: ys[lang][i].scale(lang_data.data[field]),
                            fill: TOTALS_COLORS[i],
                            width: x.rangeBand() / keys(DATA_FIELDS).length,
                            label: `${formatDateRange(lang_data.start_date, lang_data.end_date)}: ${DATA_FIELDS[field]}: ${lang_data.data[field]}`
                        }
                    } else
                        return {};
                })
            }).flatten().value();;

            let lang_xticks = [];
            let start_date = reduce(obj, (memo, obj, ind) => {
                let d = new Date(obj.start_date + 'T12:00:00');
                return d < memo ? d : memo;
            }, new Date('2070-01-01'));
            let end_date = reduce(obj, (memo, obj, ind) => {
                let d = new Date(obj.end_date + 'T12:00:00');
                return d > memo ? d : memo;
            }, new Date('1970-01-01'));

            forEach(obj[course], (lang_data, lang) => {
                let label = formatDateRange(lang_data.start_date, lang_data.end_date, (start_date.getFullYear() != end_date.getFullYear()));
                if (!includes(lang_xticks, label)) lang_xticks.push(label);
            });
            xTicks[lang] = lang_xticks;
        });
    }

    

    // console.debug(maxes);
    // console.debug(ys);

    return {
        data,
        xTicks,
        xs,
        ys
    }
}

function selectSessionUnitDataTotals(session_data, editor_content) {
    console.debug('selectSessionUnitDataTotals', session_data, editor_content);
    let retObj = {};
    forEach(session_data, (segment) => {
        let unit_rows = filter(segment.data, (row) => {
            return (!row.course_id && !row.lang_id && row.unit_id);
        });

        forEach(unit_rows, (row) => {
            let unit = find(editor_content, (content_row) => {
                return content_row.unit_id === row.unit_id;
            });
            let unit_desc = unit ? unit.unit_desc : 'UNKNOWN';
            let dataObj = {
                unit: unit_desc,
                start_date: segment.start_date,
                end_date: segment.end_date,
                data: row
            }
            if (retObj[row.unit_id]) {
                retObj[row.unit_id].push(dataObj);
            } else {
                retObj[row.unit_id] = [dataObj];
            }
        });
    });
    console.debug(retObj);
    return retObj;
}

function selectSessionUnitLangDataTotals(session_data, editor_content) {
    let retObj = {};

    forEach(session_data, (segment) => {
        let unit_lang_rows = filter(segment.data, (row) => {
            return (row.unit_id && row.lang_id && !row.course_id); 
        });

        // console.debug(unit_lang_rows);

        forEach(unit_lang_rows, (row) => {
            let lang_code = invert(LangOptions)[row.lang_id];
            let unit = find(editor_content, (content_row) => {
                return content_row.unit_id === row.unit_id;
            });
            let unit_desc = unit ? unit.unit_desc : 'UNKNOWN';
            let dataObj = {
                unit: unit_desc,
                start_date: segment.start_date,
                end_date: segment.end_date,
                data: row
            };

            if (retObj[lang_code]) {
                if (retObj[lang_code][row.unit_id]) {
                    retObj[lang_code][row.unit_id].push(dataObj);
                } else {
                    retObj[lang_code][row.unit_id] = [dataObj];
                }
            } else {
                retObj[lang_code] = {
                    [row.unit_id]: [dataObj]
                }
            }
        });


        //  Now fill with empty data if need be
        forEach(values(LangOptions), (lang_id) => {
            // console.debug(`find ${lang_id}`);
            if (!find(unit_lang_rows, {'lang_id': lang_id})) {
                // console.debug('not found');
                let lang_code = invert(LangOptions)[lang_id];
                let data_row = {
                    lang_id,
                    course_id: null,
                    unit_id: null
                };

                forEach(keys(DATA_FIELDS), (field) => {
                    data_row[field] = 0
                });

                let dataObj = {
                    start_date: segment.start_date,
                    end_date: segment.end_date,
                    data: data_row
                };

                forEach(unit_lang_rows, (row) => {
                    if (retObj[lang_code]) {
                        if (retObj[lang_code][row.unit_id]) {
                            retObj[lang_code][row.unit_id].push(dataObj);
                        } else {
                            retObj[lang_code][row.unit_id] = [dataObj];
                        }
                    } else {
                        retObj[lang_code] = {
                            [row.unit_id]: [dataObj]
                        }
                    }
                });
            }
        });
    });

    return retObj;
}

function selectSessionsDataContentMetricTotals(session_data, editor_content, content_hierarchy) {
    let retObj = {};

    function upsert(content_tag, dataObj) {
        if (retObj[content_tag]) {
            retObj[content_tag].push(dataObj);
        } else {
            retObj[content_tag] = [dataObj];
        }
    }

    forEach(session_data, (segment) => {
        // console.debug(segment);
        forEach(content_hierarchy, (unit_row) => {
            let courses = unit_row.courses;
            // console.debug(courses);
            // delete unit_row.courses;
            let unit_data = find(segment.data, {'unit_id': unit_row.id, 'course_id': null, 'lang_id': null});
            // console.debug(unit_data);

            let dataObj = {
                desc: unit_row.desc,
                start_date: segment.start_date,
                end_date: segment.end_date,
                data: unit_data
            };

            let content_tag = 'u' + unit_row.id;
            upsert(content_tag, dataObj);

            forEach(courses, (course_row) => {
                let course_data = find(segment.data, {'unit_id': unit_row.id, 'course_id': course_row.id, 'lang_id': null});

                // console.debug(course_row);
                dataObj = {
                    desc: course_row.desc,
                    start_date: segment.start_date,
                    end_date: segment.end_date,
                    data: course_data
                }

                content_tag = 'c' + course_row.id;
                upsert(content_tag, dataObj);
            })
        })
    });

    return retObj;
}

function selectSessionsDataContentMetricLangTotals(session_data, editor_content, content_hierarchy) {
    console.debug('selectSessionsDataContentMetricLangTotals', session_data, editor_content, content_hierarchy);
    let retObj = {};

    function upsert(lang_code, content_tag, dataObj) {
        if (retObj[lang_code]) {
            if (retObj[lang_code][content_tag]) {
                retObj[lang_code][content_tag].push(dataObj);
            } else {
                retObj[lang_code][content_tag] = [dataObj];
            }
        } else {
            retObj[lang_code] = {
                [content_tag]: [dataObj]
            }
        }
    }

    forEach(session_data, (segment) => {
        // console.debug(segment);
        forEach(content_hierarchy, (unit_row) => {
            let courses = unit_row.courses;

            let unit_lang_rows = filter(segment.data, (row) => {
                return (row.unit_id == unit_row.id && !row.course_id && row.lang_id);
            });
            // console.debug(unit_lang_rows);

            forEach(unit_lang_rows, (row) => {
                let lang_code = invert(LangOptions)[row.lang_id];

                let dataObj = {
                    desc: unit_row.desc,
                    start_date: segment.start_date,
                    end_date: segment.end_date,
                    data: row
                }

                let content_tag = 'u' + row.unit_id;
                upsert(lang_code, content_tag, dataObj);
            });

            forEach(courses, (course_row) => {
                let course_lang_rows = filter(segment.data, (row) => {
                    return (row.unit_id == unit_row.id && row.course_id == course_row.id && row.lang_id);
                });

                forEach(course_lang_rows, (row) => {
                    let lang_code = invert(LangOptions)[row.lang_id];

                    let dataObj = {
                        desc: course_row.desc,
                        start_date: segment.start_date,
                        end_date: segment.end_date,
                        data: row
                    }

                    let content_tag = 'c' + row.course_id;
                    upsert(lang_code, content_tag, dataObj);
                });
            });
        })
    });

    return retObj;

    /*
    forEach(session_data, (segment) => {
        let unit_lang_rows = filter(segment.data, (row) => {
            return (row.unit_id && row.lang_id && !row.course_id); 
        });

        // console.debug(unit_lang_rows);

        forEach(unit_lang_rows, (row) => {
            let lang_code = invert(LangOptions)[row.lang_id];
            let unit = find(editor_content, (content_row) => {
                return content_row.unit_id === row.unit_id;
            });
            let unit_desc = unit ? unit.unit_desc : 'UNKNOWN';
            let dataObj = {
                unit: unit_desc,
                start_date: segment.start_date,
                end_date: segment.end_date,
                data: row
            };

            if (retObj[lang_code]) {
                if (retObj[lang_code][row.unit_id]) {
                    retObj[lang_code][row.unit_id].push(dataObj);
                } else {
                    retObj[lang_code][row.unit_id] = [dataObj];
                }
            } else {
                retObj[lang_code] = {
                    [row.unit_id]: [dataObj]
                }
            }
        });


        //  Now fill with empty data if need be
        forEach(values(LangOptions), (lang_id) => {
            // console.debug(`find ${lang_id}`);
            if (!find(unit_lang_rows, {'lang_id': lang_id})) {
                // console.debug('not found');
                let lang_code = invert(LangOptions)[lang_id];
                let data_row = {
                    lang_id,
                    course_id: null,
                    unit_id: null
                };

                forEach(keys(DATA_FIELDS), (field) => {
                    data_row[field] = 0
                });

                let dataObj = {
                    start_date: segment.start_date,
                    end_date: segment.end_date,
                    data: data_row
                };

                forEach(unit_lang_rows, (row) => {
                    if (retObj[lang_code]) {
                        if (retObj[lang_code][row.unit_id]) {
                            retObj[lang_code][row.unit_id].push(dataObj);
                        } else {
                            retObj[lang_code][row.unit_id] = [dataObj];
                        }
                    } else {
                        retObj[lang_code] = {
                            [row.unit_id]: [dataObj]
                        }
                    }
                });
            }
        });
    });

    */
}

function selectSessionsDataContentMetricTotalsVictory(session_content_metric_data, multi_content, metric) {
    console.debug('selectSessionsDataContentMetricTotalsVictory', session_content_metric_data, multi_content, metric);
    if (!keys(session_content_metric_data).length || !multi_content.length || !metric) {
        console.debug('empty, return');
        return {
            data: [],
            xTicks: [],
            ys: [],
            xs: []
        }
    }

    let data = [], 
        xTicks = [], 
        xs = [], 
        ysObj = {
            'units': {
                label: `Units - ${DATA_FIELDS[metric]}`,
                max: 0
            }, 
            'courses': {
                label: `Courses - ${DATA_FIELDS[metric]}`,
                max: 0
            }
        }; //  Y-axes will be units / courses
        

    forEach(multi_content, (content_tag) => {
        let content_type = content_tag.indexOf('u') == 0 ? 'units' : 'courses';
        let content_rows = session_content_metric_data[content_tag];

        let content_max = reduce(content_rows, (memo, obj, ind) => {
            let row_val = obj.data ? obj.data[metric] : 0;
            return memo > row_val ? memo : row_val;
        }, ysObj[content_type].max);

        ysObj[content_type].max = content_max;
    });

    console.debug(ysObj);

    //  Now that we've got the max vals we can determine the scales
    forEach(multi_content, (content_tag, multi_i) => {
        let content_type = content_tag.indexOf('u') == 0 ? 'units' : 'courses';
        let content_rows = session_content_metric_data[content_tag];

        let x = d3.scale.ordinal().rangeRoundBands([50, CHART_WIDTH-50], .1).domain(range(content_rows.length));
        xs.push(x);

        if (!ysObj[content_type].scale) ysObj[content_type].scale = d3.scale.linear().range([CHART_HEIGHT, 0]).domain([0, ysObj[content_type].max]).nice()

        let y = ysObj[content_type].scale;

        let start_date = reduce(content_rows, (memo, obj, ind) => {
            let d = new Date(obj.start_date + 'T12:00:00');
            return d < memo ? d : memo;
        }, new Date('2070-01-01'));
        let end_date = reduce(content_rows, (memo, obj, ind) => {
            let d = new Date(obj.end_date + 'T12:00:00');
            return d > memo ? d : memo;
        }, new Date('1970-01-01'));

        forEach(content_rows, (row, i) => {
            console.debug(row);
            let metric_val = (row.data && row.data[metric]) ? row.data[metric] : 0;
            let label = (row.data && row.start_date) ? `${formatDateRange(row.start_date, row.end_date)} : ${row.desc} : ${metric_val}` : 'LOADING ...'

            let fill;
            if (multi_i > TOTALS_COLORS.length-1) {
                let multiplier = Math.floor(multi_i / TOTALS_COLORS.length);
                let mod = multi_i % TOTALS_COLORS.length;

                fill = shadeColor(TOTALS_COLORS[mod], -.2 * multiplier);
            } else {
                fill = TOTALS_COLORS[multi_i];
            }


            data.push({
                x: x(i) + (x.rangeBand() / multi_content.length * multi_i),
                y: y(metric_val),
                fill,
                width: x.rangeBand() / multi_content.length,
                label
            })

            // data = data.concat(chain(row).map((content_obj, ind) => {
            //     let metric_val = (content_obj.data && content_obj.data[metric]) ? content_obj.data[metric] : 0
            //     return {
            //         x: x(ind) + (x.rangeBand() / multi_content.length * multi_i),
            //         y: y(metric_val),
            //         fill: TOTALS_COLORS[multi_i],
            //         width: x.rangeBand() / multi_content.length,
            //         label: `${formatDateRange(row.start_date, row.end_date)} : ${row.desc} : ${metric_val}`
            //     }
            // }).flatten().value());

            label = formatDateRange(row.start_date, row.end_date, (start_date.getFullYear() != end_date.getFullYear()));
            if (!includes(xTicks, label)) xTicks.push(label);
        });


        

        // forEach(content_rows, (content_obj, ind) => {
        //     console.debug(content_obj);
            
        // });
    });

    let ys = chain(ysObj).map((yObj, content_type) => {
        return yObj.scale ? yObj : null
    }).compact().value();

    return {
        data,
        xTicks,
        xs,
        ys
    }
}

function selectSessionsDataContentMetricTotalsLangVictory(session_content_metric_lang_data, multi_content, metric) {
    console.debug('selectSessionsDataContentMetricTotalsLangVictory', session_content_metric_lang_data, multi_content, metric);
    if (!keys(session_content_metric_lang_data).length || !multi_content.length || !metric) {
        console.debug('empty, return');
        return {
            data: [],
            xTicks: [],
            ys: [],
            xs: []
        }
    }

    let data = {}, 
        xTicks = {}, 
        xs = {}, 
        ysObj = {
            'units': {
                label: `Units - ${DATA_FIELDS[metric]}`,
                max: 0
            }, 
            'courses': {
                label: `Courses - ${DATA_FIELDS[metric]}`,
                max: 0
            }
        }, //  Y-axes will be units / courses
        ys = {};
    
    console.debug(JSON.stringify(ysObj));

    forEach(session_content_metric_lang_data, (obj, lang) => {
        console.debug(ysObj);
        console.debug(JSON.stringify(ysObj));
        // ys[lang] = Object.assign({}, ysObj);
        //  Assigns by reference otherwise, which causes problems
        ys[lang] = {};
        forEach(ysObj, (obj, content_type) => {
            ys[lang][content_type] = Object.assign({}, obj);
        })
        data[lang] = [];
        xTicks[lang] = [];

        console.debug(`ys[${lang}]`, ys[lang]);
        forEach(multi_content, (content_tag) => {
            let content_type = content_tag.indexOf('u') == 0 ? 'units' : 'courses';
            let content_rows = session_content_metric_lang_data[lang][content_tag];
            console.debug(`ys[${lang}][${content_type}]`, ys[lang][content_type]);
            console.debug(content_rows);

            let content_max = reduce(content_rows, (memo, obj, ind) => {
                let row_val = obj.data ? obj.data[metric] : 0;
                return memo > row_val ? memo : row_val;
            }, ys[lang][content_type].max);
            console.debug(JSON.stringify(ysObj));

            console.debug(content_max);

            ys[lang][content_type].max = content_max;
            console.debug(JSON.stringify(ysObj));
        });

        console.debug(ys);

        console.debug(JSON.stringify(ysObj));
        forEach(multi_content, (content_tag, multi_i) => {
            let content_type = content_tag.indexOf('u') == 0 ? 'units' : 'courses';
            let content_rows = session_content_metric_lang_data[lang][content_tag];
            console.debug(`[${lang}][${content_tag}]`);

            if (content_rows) {
                let x = d3.scale.ordinal().rangeRoundBands([50, CHART_WIDTH-50], .1).domain(range(content_rows.length));

                if (!ys[lang][content_type].scale) ys[lang][content_type].scale = d3.scale.linear().range([CHART_HEIGHT, 0]).domain([0, ys[lang][content_type].max]).nice();

                let y = ys[lang][content_type].scale;

                let start_date = reduce(content_rows, (memo, obj, ind) => {
                    let d = new Date(obj.start_date + 'T12:00:00');
                    return d < memo ? d : memo;
                }, new Date('2070-01-01'));
                let end_date = reduce(content_rows, (memo, obj, ind) => {
                    let d = new Date(obj.end_date + 'T12:00:00');
                    return d > memo ? d : memo;
                }, new Date('1970-01-01'));

                forEach(content_rows, (row, i) => {
                    let metric_val = (row.data && row.data[metric]) ? row.data[metric] : 0;
                    let label = (row.data && row.start_date) ? `${formatDateRange(row.start_date, row.end_date)} : ${row.desc} : ${metric_val}` : 'LOADING ...'

                    let fill;
                    if (multi_i > TOTALS_COLORS.length-1) {
                        let multiplier = Math.floor(multi_i / TOTALS_COLORS.length);
                        let mod = multi_i % TOTALS_COLORS.length;

                        fill = shadeColor(TOTALS_COLORS[mod], -.2 * multiplier);
                    } else {
                        fill = TOTALS_COLORS[multi_i];
                    }

                    data[lang].push({
                        x: x(i) + (x.rangeBand() / multi_content.length * multi_i),
                        y: y(metric_val),
                        fill,
                        width: x.rangeBand() / multi_content.length,
                        label
                    });

                    label = formatDateRange(row.start_date, row.end_date, (start_date.getFullYear() != end_date.getFullYear()));
                    if (!includes(xTicks[lang], label)) xTicks[lang].push(label);
                });

                console.debug(data);
            }

            
        });

        ys[lang] = chain(ys[lang]).map((yObj, content_type) => {
            return yObj.scale ? yObj : null;
        }).compact().value();
    });

    return {
        data,
        xTicks,
        xs,
        ys
    }
}

function selectFiltersAreDirty(start_date, end_date, granularity, session_data_segments, session_data) {
    // console.debug('selectFiltersAreDirty', start_date, end_date, granularity, session_data_segments, session_data);
    let key = `${urlFormatDate(start_date)}-${urlFormatDate(end_date)}`;
    // console.debug(key);
    let is_dirty = !(session_data_segments && session_data_segments[granularity] && session_data_segments[granularity][key]);

    if (!is_dirty) {
        //  Check to see if it's actually dirty or we're fetching
        // console.debug(session_data_segments[granularity][key]);
        forEach(session_data_segments[granularity][key], (obj, ind) => {
            let segment_key = `${urlFormatDate(obj.start_date)}-${urlFormatDate(obj.end_date)}`;
            // console.debug(segment_key);
            // console.debug(obj);
            // console.debug(session_data[segment_key]);
            if (!session_data[segment_key] || !session_data[segment_key].data || session_data[segment_key].data.length == 0 || session_data[segment_key].isFetching) // don't have data yet, not dirty
                is_dirty = true;
            return !is_dirty; // A false value will break the loop, which is what we want
        })
    }

    console.debug(is_dirty);
    return is_dirty;
}

function selectIsFetching(start_date, end_date, granularity, session_data_segments, session_data) {
    console.debug('selectIsFetching', start_date, end_date, granularity, session_data_segments, session_data);
    let key = `${urlFormatDate(start_date)}-${urlFormatDate(end_date)}`;
    if (!session_data_segments || !session_data_segments[granularity] || !session_data_segments[granularity][key]) return false;

    let is_fetching = false;
    forEach(session_data_segments[granularity][key], (obj, ind) => {
        let segment_key = `${urlFormatDate(obj.start_date)}-${urlFormatDate(obj.end_date)}`;
        // console.debug(session_data[segment_key]);
        if (session_data[segment_key])
            is_fetching = session_data[segment_key].isFetching;
        // else
        //     is_fetching = true; //  If we don't yet have session_data but we ought, it means we're fetching 

        return !is_fetching; // If we're fetching on any we can break out of the loop
    });

    console.debug(is_fetching);
    return is_fetching;
}


const filtersSelector = state => state.filters;
const langSelector = state => state.filters.lang;
const modeSelector = state => state.filters.mode;
const startDateSelector = state => state.filters.start_date;
const endDateSelector = state => state.filters.end_date;
const granularitySelector = state => state.filters.granularity;
const metricSelector = state => state.filters.metric;
const multiContentSelector = state => state.filters.content_arr;

const contentSelector = state => state.editor_content.content;
const unitSelector = state => state.filters.unit;
const courseSelector = state => state.filters.course;

const messagesSelector = state => state.filters.messages;

const sessionDataSelector = state => state.session_data;
const sessionDataSegmentsSelector = state => state.session_data_segments;

const contentDataSelector = createSelector(
    contentSelector,
    unitSelector,
    (content, unit) => {
        return selectContent(content, unit);
    }
);

const unitTagsSelector = createSelector(
    contentSelector,
    (content) => {
        return selectUnitTags(content);
    }
);

const unitsSelector = createSelector(
    [contentSelector, unitTagsSelector],
    (content, tags) => {
        return selectUniqueUnits(content, tags);
    }
);

const courseTagsSelector = createSelector(
    [contentDataSelector, unitSelector],
    (content, unit) => {
        return selectCourseTags(content.courses, unit);
    }
);

const coursesSelector = createSelector(
    [contentDataSelector, courseTagsSelector],
    (content, tags) => {
        return selectUniqueCourses(content.courses, tags);
    }
);

/*  Selector to get the course tag of the currently selected course_id */
const courseTagSelector = createSelector(
    [courseSelector, coursesSelector],
    (course, courses) => {
        return selectCourseTag(course, courses);
    }
);

const contentHierarchySelector = createSelector(
    [contentSelector, unitTagsSelector],
    (content, unit_tags) => {
        return selectContentHierarchy(content, unit_tags);
    }
);

const filteredSessionDataSelector = createSelector(
    sessionDataSelector,
    sessionDataSegmentsSelector,
    startDateSelector,
    endDateSelector,
    granularitySelector,
    (session_data, session_data_segments, start_date, end_date, granularity) => {
        return selectFilteredSessionData(session_data, session_data_segments, start_date, end_date, granularity);
    }
);

const sessionDataTotalsSelector = createSelector(
    filteredSessionDataSelector,
    (session_data) => {
        return selectSessionDataTotals(session_data);
    }
);

const sessionDataTotalsVictorySelector = createSelector(
    sessionDataTotalsSelector,
    (session_data_totals) => {
        return selectSessionsDataTotalsVictory(session_data_totals);
    }
);

const sessionDataLangTotalsSelector = createSelector(
    filteredSessionDataSelector,
    (session_data) => {
        return selectSessionLangDataTotals(session_data);
    }
);

const sessionDataLangTotalsVictorySelector = createSelector(
    sessionDataLangTotalsSelector,
    (session_lang_data_totals) => {
        return selectSessionLangDataTotalsVictory(session_lang_data_totals);
    }
);

const sessionDataMetricTotalsVictorySelector = createSelector(
    sessionDataLangTotalsSelector,
    metricSelector,
    (session_lang_data_totals, metric) => {
        return selectSessionsDataMetricTotalsVictory(session_lang_data_totals, metric);
    }
);

const sessionDataMetricLangTotalsVictorySelector = createSelector(
    sessionDataLangTotalsSelector,
    metricSelector,
    (session_lang_data_totals, metric) => {
        return selectSessionDataMetricLangTotalsVictory(session_lang_data_totals, metric);
    }
);

const sessionDataCourseTotalsSelector = createSelector(
    filteredSessionDataSelector,
    contentSelector,
    (session_data, editor_content) => {
        return selectSessionCourseDataTotals(session_data, editor_content);
    }
);

const sessionDataUnitTotalsSelector = createSelector(
    filteredSessionDataSelector,
    contentSelector,
    (session_data, editor_content) => {
        return selectSessionUnitDataTotals(session_data, editor_content);
    }
);

const sessionDataUnitTotalsVictorySelector = createSelector(
    sessionDataUnitTotalsSelector,
    unitSelector,
    (session_data_unit_totals, unit) => {
        return selectSessionsDataUnitTotalsVictory(session_data_unit_totals, unit);
    }
);

const sessionDataCourseLangTotalsSelector = createSelector(
    filteredSessionDataSelector,
    contentSelector,
    (session_data, editor_content) => {
        return selectSessionCourseLangDataTotals(session_data, editor_content);
    }
);

const sessionDataUnitLangTotalsSelector = createSelector(
    filteredSessionDataSelector,
    contentSelector,
    (session_data, editor_content) => {
        return selectSessionUnitLangDataTotals(session_data, editor_content);
    }
);

const sessionDataUnitLangTotalsVictorySelector = createSelector(
    sessionDataUnitLangTotalsSelector,
    unitSelector,
    (session_lang_data, unit) => {
        return selectSessionLangDataUnitTotalsVictory(session_lang_data, unit);
    }
);

const sessionDataCourseTotalsVictorySelector = createSelector(
    sessionDataCourseTotalsSelector,
    courseSelector,
    (session_course_data, course) => {
        return selectSessionsDataCourseTotalsVictory(session_course_data, course);
    }
);

const sessionDataCourseLangTotalsVictorySelector = createSelector(
    sessionDataCourseLangTotalsSelector,
    courseSelector,
    (session_lang_data, course) => {
        return selectSessionLangDataCourseTotalsVictory(session_lang_data, course);
    }
);

const sessionDataContentMetricTotalsSelector = createSelector(
    filteredSessionDataSelector,
    contentSelector,
    contentHierarchySelector,
    (session_data, editor_content, content_hierarchy) => {
        return selectSessionsDataContentMetricTotals(session_data, editor_content, content_hierarchy);
    }
);

const sessionDataContentMetricLangTotalsSelector = createSelector(
    filteredSessionDataSelector,
    contentSelector,
    contentHierarchySelector,
    (session_data, editor_content, content_hierarchy) => {
        return selectSessionsDataContentMetricLangTotals(session_data, editor_content, content_hierarchy);
    }
);

const sessionDataContentMetricTotalsVictorySelector = createSelector(
    sessionDataContentMetricTotalsSelector,
    multiContentSelector,
    metricSelector,
    (session_content_metric_data, multi_content, metric) => {
        return selectSessionsDataContentMetricTotalsVictory(session_content_metric_data, multi_content, metric);
    }
);

const sessionDataContentMetricTotalsLangVictorySelector = createSelector(
    sessionDataContentMetricLangTotalsSelector,
    multiContentSelector,
    metricSelector,
    (session_content_metric_lang_data, multi_content, metric) => {
        return selectSessionsDataContentMetricTotalsLangVictory(session_content_metric_lang_data, multi_content, metric);
    }
);

const filtersAreDirtySelector = createSelector(
    startDateSelector,
    endDateSelector,
    granularitySelector,
    sessionDataSegmentsSelector,
    sessionDataSelector,
    (start_date, end_date, granularity, session_data_segments, session_data) => {
        return selectFiltersAreDirty(start_date, end_date, granularity, session_data_segments, session_data);
    }
);

const isFetchingSelector = createSelector(
    startDateSelector,
    endDateSelector,
    granularitySelector,
    sessionDataSegmentsSelector,
    sessionDataSelector,
    (start_date, end_date, granularity, session_data_segments, session_data) => {
        return selectIsFetching(start_date, end_date, granularity, session_data_segments, session_data);
    }
);

export const filteredContentSelector = createSelector(
    contentDataSelector,
    unitsSelector,
    coursesSelector,
    courseTagSelector,
    contentHierarchySelector,
    filtersSelector,
    langSelector,
    modeSelector,
    granularitySelector,
    metricSelector,
    startDateSelector,
    endDateSelector,
    unitSelector,
    courseSelector,
    messagesSelector,
    sessionDataSelector,
    filteredSessionDataSelector,
    sessionDataTotalsSelector,
    sessionDataLangTotalsSelector,
    sessionDataCourseTotalsSelector,
    sessionDataUnitTotalsSelector,
    sessionDataCourseLangTotalsSelector,
    sessionDataUnitLangTotalsSelector,
    sessionDataTotalsVictorySelector,
    sessionDataLangTotalsVictorySelector,
    sessionDataMetricTotalsVictorySelector,
    sessionDataMetricLangTotalsVictorySelector,
    sessionDataUnitTotalsVictorySelector,
    sessionDataUnitLangTotalsVictorySelector,
    sessionDataCourseTotalsVictorySelector,
    sessionDataCourseLangTotalsVictorySelector,
    sessionDataContentMetricTotalsSelector,
    sessionDataContentMetricTotalsVictorySelector,
    sessionDataContentMetricLangTotalsSelector,
    sessionDataContentMetricTotalsLangVictorySelector,
    filtersAreDirtySelector,
    isFetchingSelector,
    (content, units, courses, course_tag, content_hierarchy, filters, lang, mode, granularity, metric, start_date, end_date, unit, course, messages, session_data, filtered_session_data, session_data_totals, session_lang_data_totals, session_course_data_totals, session_unit_data_totals, session_course_lang_data_totals, session_unit_lang_data_totals, session_data_totals_victory, session_data_lang_totals_victory, session_data_metric_totals_victory, session_data_metric_lang_totals_victory, session_data_unit_totals_victory, session_data_unit_lang_totals_victory, session_data_unit_course_totals_victory, session_data_lang_unit_course_totals_victory, session_data_content_metric_totals, session_data_content_metric_totals_victory, session_data_content_metric_lang_totals, session_data_content_metric_totals_lang_victory, filters_are_dirty, is_fetching) => {
        return {
            content, units, courses, course_tag, content_hierarchy, filters, lang, mode, granularity, metric, start_date, end_date, unit, course, messages, session_data, filtered_session_data, session_data_totals, session_lang_data_totals, session_course_data_totals, session_unit_data_totals, session_course_lang_data_totals, session_unit_lang_data_totals, session_data_totals_victory, session_data_lang_totals_victory, session_data_metric_totals_victory, session_data_metric_lang_totals_victory, session_data_unit_totals_victory, session_data_unit_lang_totals_victory, session_data_unit_course_totals_victory, session_data_lang_unit_course_totals_victory, session_data_content_metric_totals, session_data_content_metric_totals_victory, session_data_content_metric_lang_totals, session_data_content_metric_totals_lang_victory, filters_are_dirty, is_fetching
        }
    }
)