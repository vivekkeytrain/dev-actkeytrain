import React, { Component } from 'react';
import d3 from 'd3';
import tip from 'd3-tip';

import { forEach, map, flatten, compact } from 'lodash';

const   margin = {top: 10, right: 35, bottom: 30, left: 60};
const   possible_colors = ['#339966', '#FF6666', '#FFCC66', '#FF0066', '#003399', '#333366', '#3399CC'];

//     width = 1100 - margin.left - margin.right,
//     height = 700 - margin.top - margin.bottom;


// let y = d3.scale.linear().range([height, 0]);

// let xAxis = d3.svg.axis().scale(x).orient('bottom');
// let yAxisLeft = d3.svg.axis().scale(y).orient("left");

// class ToolTip extends Component {
//     render() {
//         let text = (this.props.index == 0) ? <i>SKIPPED</i> : <div style={{'text-align': center, 'width': '100%'}}>{this.props.questionAnswers}
//         return (
//             <div className="d3-tip n" style={{'position': 'absolute', 'opacity': 0}}>

//             </div>
//         )
//     }
// }

class Bar extends Component {
    constructor(props) {
        super(props);

        this.props = {
            width: 0,
            height: 0,
            offset: 0
        }
    }

    showTip(e) {
        let toolTip = this.props.toolTip;
        let avg = this.props.answerAvgs[this.props.question][this.props.index]

        let text = ((this.props.index == 0) ? '<i>SKIPPED</i>' : this.props.questionAnswers[this.props.question][this.props.index-1]) + '<div style="text-align: center; width: 100%">' + this.props.sum + ' (' + Math.round(avg * 1000) / 10 + '%)</div>';
        toolTip.html(text).show(e.target);
    }

    hideTip(e) {
        let toolTip = this.props.toolTip;
        toolTip.hide(e.target);
    }

    render() {
        //onMouseOver={this.showTip.bind(this)} onMouseOut={this.hideTip.bind(this)}
        return (
            <rect fill={this.props.color} width={this.props.width} height={this.props.height} x={this.props.offset} y={this.props.availableHeight - this.props.height} onMouseOver={this.showTip.bind(this)} onMouseOut={this.hideTip.bind(this)} />
        );
    }
}

Bar.defaultProps = {
    width: 0,
    height: 0,
    offset: 0
}

export default class Chart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            x: null,
            y: null,
            xAxis: null,
            yAxis: null,
            width: props.width,
            height: props.height,
            color: null,
            toolTip: null
        }
    }

    componentDidMount() {
        let width = this.props.width;
        let height = this.props.height;

        let svg = d3.select('#svg_chart');
        let x = d3.scale.ordinal().rangeRoundBands([0, width], .1);
        let xAxis = d3.svg.axis().scale(x).orient('bottom');
        
        let y = d3.scale.linear().range([height, 0]);
        let yAxis = d3.svg.axis().scale(y).orient('left');

        let color = d3.scale.ordinal();
        color.range(possible_colors).domain(this.props.answerVals);
        
        let toolTip = tip()
            .attr('class', 'd3-tip');

        svg.call(toolTip);

        this.setState({
            x: x, 
            xAxis: xAxis,
            y: y,
            yAxis: yAxis,
            width: width,
            height: height,
            color: color,
            toolTip: toolTip
        });
    }

    componentWillReceiveProps(props) {
        // console.debug(props);

        let x = this.state.x;
        x.domain(props.questionIds);

        let xAxis = this.state.xAxis;
        let svg = d3.select('#svg_chart');
        d3.select("#xaxis")
            .attr("transform", "translate(0," + this.state.height + ")")
            .call(xAxis);

        let y = this.state.y;
        y.domain([0,props.answerSumsMax]);

        let yAxis = this.state.yAxis;
        d3.select('#yaxis')
            .call(yAxis);

        

    }

    render() {
        // console.debug(this.props);
        // console.debug(this.state);

        let color = this.state.color;
        let x = this.state.x;
        let y = this.state.y;
        let answer_vals = this.props.answerVals;
        let answer_sums_arr = this.props.answerSumsArr;

        // let bars = compact(flatten(map(answer_sums_arr, (sums_obj) => {
        //     return map(answer_vals, (val, i) => {
        //         if (sums_obj.sums[i]) {
        //             return <Bar 
        //                 width={x.rangeBand() / answer_vals.length} 
        //                 height={this.props.height - y(sums_obj.sums[i])}
        //                 offset={x(sums_obj.question) + (x.rangeBand() / answer_vals.length * i)}
        //                 availableHeight={this.props.height}
        //                 color={color(i)}
        //                 toolTip={this.state.toolTip}
        //                 question={sums_obj.question}
        //                 index={i}
        //                 sum={sums_obj.sums[i]}
        //                 questionAnswers={this.props.questionAnswers}
        //                 answerAvgs={this.props.answerAvgs}
        //             />
        //         }
        //     }, this);
        // }, this)));
        let bars = '';



        // let bars = compact(flatten(map(this.props.answerVals, (val, i) => {
        //     let x = this.state.x;
        //     let y = this.state.y;
        //     let answer_vals = this.props.answerVals;
        //     let answer_sums_arr = this.props.answerSumsArr;

        //     return map(answer_sums_arr, (sums_obj) => {
        //         // console.debug(sums_obj);
        //         // console.debug('sums_obj.sums['+i+']', sums_obj.sums[i]);
        //         // console.debug(color(i));
        //         if (sums_obj.sums[i]) {
        //             return <Bar 
        //                 width={x.rangeBand() / answer_vals.length} 
        //                 height={this.props.height - y(sums_obj.sums[i])}
        //                 offset={x(sums_obj.question) + (x.rangeBand() / answer_vals.length * i)}
        //                 availableHeight={this.props.height}
        //                 color={color(i)}
        //                 toolTip={this.state.toolTip}
        //                 question={sums_obj.question}
        //                 index={i}
        //                 sum={sums_obj.sums[i]}
        //                 questionAnswers={this.props.questionAnswers}
        //                 answerAvgs={this.props.answerAvgs}
        //             />
        //         } else {
        //             return '';
        //         }
        //     }, this);

        //     // 
        // }, this)));

        // console.debug(bars);

        return (
            <svg width={this.state.width+margin.left+margin.right} height={this.state.height+margin.top+margin.bottom} style={{'float': 'left'}} id="svg_chart">
                <g transform={'translate(' + margin.left + ',' + margin.top + ')'} id="margin">
                    <g className="x axis" id="xaxis" />
                    <g className="y axis" id="yaxis">
                        <text transform='rotate(-90)' y='6' dy='.71em' style={{'textAnchor': 'end'}}>Count</text>
                    </g>
                    {bars}
                </g>
            </svg>
        )
    }

    // constructor(props) {
    //     super(props);

    //     this.state = {
    //         svg: null
    //     }
    // }

    // componentWillReceiveProps(props) {
    //     console.debug(props);
    // }

    // componentDidMount() {
    //     console.debug(this.props);
    //     let svg = d3.select('#chart').append('svg')
    //             .attr('width', width + margin.left + margin.right)
    //             .attr('height', height + margin.top + margin.bottom)
    //             .style('float', 'left')
    //         .append('g')
    //             .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        

    //     // $('svg').after($('<div />').attr('id', 'questions'));

        

    //     svg.append("g")
    //         .attr("class", "x axis")
    //         .attr("transform", "translate(0," + height + ")")
    //         .call(xAxis);

    //     svg.append('g')
    //         .attr('class', 'y axis')
    //         .call(yAxisLeft)
    //     .append('text')
    //         .attr('transform', 'rotate(-90)')
    //         .attr('y', 6)
    //         .attr('dy', '.71em')
    //         .style('text-anchor', 'end')
    //         .text('Count');

    //     console.debug(svg);
    //     this.setState({
    //         svg: svg
    //     })

        
    // }

    // render() {
    //     console.debug('render');
    //     if (this.state && this.state.svg) {
    //         let svg = this.state.svg;

    //         svg.selectAll("*").remove();

    //         let color = d3.scale.ordinal();

    //         let possible_colors = ['#339966', '#FF6666', '#FFCC66', '#FF0066', '#003399', '#333366', '#3399CC'];

    //         x.domain(this.props.questionIds);
    //         color.range(possible_colors).domain(this.props.answerVals);

    //         y.domain([0, this.props.answerSumsMax]);

    //         forEach(this.props.answerVals, (val, i) => {
    //             console.debug(val);
    //             // for (var i=0; i<question_vals.length; i++) {
    //                     // var q = questions[val];

    //             let tip_func = function(i) {
    //                 return function(d) {
    //                     // console.debug(d);
    //                     return ((i == 0) ? '<i>SKIPPED</i>' : d.counts.answers[i-1]) + '<div style="text-align: center; width: 100%">' + d.counts.sums[i] + ' (' + Math.round(d.counts.avgs[i] * 1000) / 10 + '%)</div>';
    //                     // return d.counts.sums[i];
    //                 }
    //             }

    //             // let tip = d3.tip()
    //             //     .attr('class', 'd3-tip')
    //             //     .html(tip_func(i));

    //             // svg.call(tip);

    //             svg.selectAll('.answer-' + i)
    //                 .data(this.props.answerSumsArr)
    //             .enter().append('rect')
    //                 .attr('width', x.rangeBand() / this.props.answerVals.length)
    //                 .attr('y', function(d) {
    //                     // console.debug(d.counts.sums[i]);
    //                     return y(d.counts.sums[i]);
    //                 })
    //                 .attr('height', function(d) {
    //                     return height - y(d.counts.sums[i]);
    //                 })
    //                 .attr('x', function(d) {
    //                     // console.debug(d);
    //                     // console.debug(d.question);
    //                     // console.debug(x(d.question));
    //                     return x(d.question) + (x.rangeBand() / this.props.answerVals.length * i);
    //                     // return x(question_objs[d.question]) + (x.rangeBand() / answer_vals.length * i);
    //                 })
    //                 .attr('class', 'answer-' + i)
    //                 .style('fill', function(d) {
    //                     return color(i);
    //                 })
    //                 // .on('mouseover', tip.show)
    //                 // .on('mouseout', tip.hide);
    //         });
    //     }

    //     return (<div id="chart" />)
    // }
}