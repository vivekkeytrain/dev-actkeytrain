import fetchJsonp from 'fetch-jsonp';
// import 'babel-polyfill';

/*  Action types    */

export const SET_START_DATE = 'SET_START_DATE';
export const SET_END_DATE = 'SET_END_DATE';
export const SET_GRANULARITY = 'SET_GRANULARITY';
export const SET_LANG = 'SET_LANG';
export const SET_MODE = 'SET_MODE';
export const SET_METRIC = 'SET_METRIC';
export const SET_MULTI_CONTENT = 'SET_MULTI_CONTENT';

export const SET_UNIT = 'SET_UNIT';
export const SET_COURSE = 'SET_COURSE';
export const SET_LESSON = 'SET_LESSON';

export const ADD_MESSAGE = 'ADD_MESSAGE';
export const CLOSE_MESSAGE = 'CLOSE_MESSAGE';

/*  Async action types  */

export const FETCH_SESSION_DATA_REQUEST = 'FETCH_SESSION_DATA_REQUEST';
export const FETCH_SESSION_DATA_SUCCESS = 'FETCH_SESSION_DATA_SUCCESS';
export const FETCH_SESSION_DATA_FAILURE = 'FETCH_SESSION_DATA_FAILURE';

export const FETCH_EDITOR_CONTENT_SUCCESS = 'FETCH_EDITOR_CONTENT_SUCCESS';
export const FETCH_EDITOR_CONTENT_FAILURE = 'FETCH_EDITOR_CONTENT_FAILURE';
export const FETCH_EDITOR_CONTENT_REQUEST = 'FETCH_EDITOR_CONTENT_REQUEST';

/*  Other contstants    */

export const START_DATE_DEFAULT = '2015-09-01';

export const LangOptions = {
    EN: 1,
    ES: 2
};

export const GranularityOptions = [
    'Weekly',
    'Monthly',
    'Full Range'
];

export const DisplayModes = {
    'Total Usage by Language - three total usage metrics over time, with an available breakdown by language': {
        'all': 'session_data_totals_victory',
        'lang': 'session_data_lang_totals_victory'
    },
    'Total Usage by Metric - total usage for a single metric over time, with comparison by language': {
        'all': 'session_data_metric_totals_victory',
        'lang': 'session_data_metric_lang_totals_victory'
    },
    'Usage by Course - three total usage metrics over time for a single chosen course or entire unit': {
        'all': 'session_data_unit_totals_victory',
        'course': 'session_data_unit_course_totals_victory',
        'lang': {
            'all': 'session_data_unit_lang_totals_victory',
            'course': 'session_data_lang_unit_course_totals_victory'
        }
    },
    'Course Metric Comparison - usage for a single metric over time with comparison of multiple courses or units': {
        'all': 'session_data_content_metric_totals_victory',
        'lang': 'session_data_content_metric_totals_lang_victory'
    }
}

// export const DisplayModes = [
//     'Totals',
//     'Languages',
//     'Units',
//     'Courses',
//     'Courses by Languages'

//     // 'Day / Hour',
//     // 'Month / Day of Week',
//     // 'Year / Month',
//     // 'Course / Levels',
//     // 'Course -> Lesson / Topic'
// ];

export const CHART_WIDTH = 1200;
export const CHART_HEIGHT = 700;

export const TOTALS_COLORS = ['#F15854', '#5DA5DA', '#60BD68', '#FAA43A', '#F17CB0', '#B2912F', '#B276B2', '#DECF3F', '#4D4D4D'];
// export const TOTALS_COLORS = ['#0000FF', '#00FF00', '#FF0000', '#000000'];

// export const DATA_FIELDS = ['student_count', 'orgz_count', 'time_seconds'];
export const DATA_FIELDS = {
    'student_count' : '# Students',
    'orgz_count': '# Orgs',
    'time_seconds': '# Hours'
}


/*  Action creators     */

export function setStartDate(start_date) {
    return {
        type: SET_START_DATE,
        start_date
    }
}

export function setEndDate(end_date) {
    return {
        type: SET_END_DATE,
        end_date
    }
}

export function setGranularity(granularity) {
    return {
        type: SET_GRANULARITY,
        granularity
    }
}

export function setLang(lang) { 
    return {
        type: SET_LANG,
        lang
    }
}

export function setMode(mode) {
    return {
        type: SET_MODE,
        mode
    }
}

export function setMetric(metric) {
    return {
        type: SET_METRIC,
        metric
    }
}

export function setMultiContent(content_arr) {
    return {
        type: SET_MULTI_CONTENT,
        content_arr
    }
}

export function setUnit(unit) {
    return { type: SET_UNIT, unit };
}

export function setCourse(course) {
    return { type: SET_COURSE, course };
}

export function addMessage(message_type, message) {
    return {
        type: ADD_MESSAGE,
        message_type,
        message
    }
}

export function closeMessage(index) {
    return {
        type: CLOSE_MESSAGE,
        index
    }
}

/*  Async actions   */

function fetchSessionDataRequest(segment_start_date, segment_end_date, filter_start_date, filter_end_date, granularity) {
    return {
        type: FETCH_SESSION_DATA_REQUEST,
        segment_start_date,
        segment_end_date,
        filter_start_date,
        filter_end_date,
        granularity
    }
}

function fetchSessionDataSuccess(start_date, end_date, data) {
    return {
        type: FETCH_SESSION_DATA_SUCCESS,
        start_date,
        end_date,
        data,
        receivedAt: Date.now()
    }
}

function fetchSessionDataFailure(ex, start_date, end_date) {
    // console.error('fetchSessionDataFailure', ex, start_date, end_date);
    return {
        type: FETCH_SESSION_DATA_FAILURE,
        start_date,
        end_date,
        ex
    }
}

export function isUTCDate(date_str) {
    if (!date_str) return false;
    if (typeof date_str.getMonth === 'function') return false; //Already a date
    return date_str.match(/^\d{4}-\d{2}-\d{2}$/);
}

function getDateSegments(start_date, end_date, granularity) {
    // console.debug('getDateSegments', start_date, end_date, granularity);
    if (isUTCDate(start_date)) start_date += 'T12:00:00'; // Otherwise we're behind by a day
    if (isUTCDate(end_date)) end_date += 'T12:00:00';
    let loop_start_date = new Date(start_date);
    end_date = new Date(end_date);
    if (granularity === 'Weekly') {
        loop_start_date.setDate(loop_start_date.getDate() - loop_start_date.getDay() + 1);    // Week starts on Monday
        do {
            let loop_end_date = new Date(loop_start_date);
            loop_end_date.setDate(loop_end_date.getDate() + 7);
            yield [loop_start_date, loop_end_date];
            loop_start_date = loop_end_date;
        } while (loop_start_date < end_date);
    } else if (granularity === 'Monthly') {
        loop_start_date.setDate(1);
        do {
            let loop_end_date = new Date(loop_start_date);
            loop_end_date.setMonth(loop_end_date.getMonth() + 1) //
            let next_loop_start_date = new Date(loop_end_date);
            loop_end_date.setDate(0); // Day 0 == last day of previous month
            yield [loop_start_date, loop_end_date];
            loop_start_date = next_loop_start_date;
        } while (loop_start_date < end_date);
    } else if (granularity == 'Full Range') {
        // console.debug('Full Range');
        // console.debug(loop_start_date, end_date);
        yield [loop_start_date, end_date];
    }
}

export function urlFormatDate(d) {
    if (isUTCDate(d)) d += 'T12:00:00';
    d = new Date(d);
    if (d == 'Invalid Date') return '';
    let m = (d.getMonth()+1 < 10 ? '0' : '') + (d.getMonth()+1);
    let dt = (d.getDate() < 10 ? '0' : '') + d.getDate();
    return `${d.getFullYear()}-${m}-${dt}`;
}

export function fetchSessionData(start_date, end_date, granularity) {
    return function(dispatch) {
        if (!start_date) start_date = START_DATE_DEFAULT;
        
        let it = getDateSegments(start_date, end_date, granularity);

        let date_segments = it.next();
        do {
            // console.debug(date_segments);
            let [loop_start_date, loop_end_date] = date_segments.value;
            dispatch(fetchSessionDataRequest(urlFormatDate(loop_start_date), urlFormatDate(loop_end_date), start_date, end_date, granularity));
            let url = `http://dev.admapi.actkeytrain.com/dbwebapi/dbo.rpt_sp_content_usage_summary?startdate=${urlFormatDate(loop_start_date)}&enddate=${urlFormatDate(loop_end_date)}`;
            // console.debug(url);

            fetchJsonp(url, {
                timeout: 200000
            })
                .then(
                    response => response.json(),
                    ex => dispatch(fetchSessionDataFailure(ex, urlFormatDate(loop_start_date), urlFormatDate(loop_end_date)))
                )
                .then(json => dispatch(fetchSessionDataSuccess(urlFormatDate(loop_start_date), urlFormatDate(loop_end_date), json.ResultSets[0])))
                .catch(ex => dispatch(fetchSessionDataFailure(ex, urlFormatDate(loop_start_date), urlFormatDate(loop_end_date))));

            date_segments = it.next();
        } while (!date_segments.done);
        
    }
}

function fetchEditorContentSuccess(content) {
    return {
        type: FETCH_EDITOR_CONTENT_SUCCESS,
        content
    }
}

function fetchEditorContentFailure(ex) {
    return {
        type: FETCH_EDITOR_CONTENT_FAILURE,
        ex
    }
}

function fetchEditorContentRequest() {
    return { type: FETCH_EDITOR_CONTENT_REQUEST };
}

export function fetchEditorContent() {
    return function(dispatch) {
        dispatch(fetchEditorContentRequest());

        return fetchJsonp('http://dev.admapi.actkeytrain.com/dbwebapi/dbo.rpt_sp_get_content_list/?content_char=L')
            .then(
                response => response.json(),
                ex => dispatch(fetchEditorContentFailure(ex))
            )
            .then(json => dispatch(fetchEditorContentSuccess(json.ResultSets[0])))
            .catch(ex => dispatch(fetchEditorContentFailure(ex)));
    }
}