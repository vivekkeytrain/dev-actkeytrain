import React, { Component, PropTypes } from 'react';

import classNames from 'classnames';

export default class StatePath extends Component {
    constructor(props) {
        super(props);

        this.state = {
            add: false,
            subtract: false,
            timer: null
        }
    }

    componentWillReceiveProps(nextProps) {
        // console.debug('componentWillReceiveProps', nextProps, this.props);
        if (!nextProps.st.initial) {
            // console.info(nextProps.st, this.props.st);
            if (nextProps.st.count > this.props.st.count) {
                this.setState({
                    add: true
                });
                this.fireTimer();
            } else if (nextProps.st.count < this.props.st.count) {
                this.setState({
                    subtract: true
                });
                this.fireTimer();
            }
        }
    }

    fireTimer() {
        console.debug('fireTimer state: ', this.state);
        if (this.state.timer == null) {
            console.debug('fireTimer', this.props.abbrev);
            let unset = () => {
                this.setState({
                    add: false,
                    subtract: false,
                    timer: null
                })
            };

            let timer = window.setTimeout(unset, 3000);
            this.setState({
                timer
            })
        }
    }

    //  http://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
    blendColors(c0, c1, p) {
        // console.debug('blendColors', p);
        var f=parseInt(c0.slice(1),16),t=parseInt(c1.slice(1),16),R1=f>>16,G1=f>>8&0x00FF,B1=f&0x0000FF,R2=t>>16,G2=t>>8&0x00FF,B2=t&0x0000FF;
        return "#"+(0x1000000+(Math.round((R2-R1)*p)+R1)*0x10000+(Math.round((G2-G1)*p)+G1)*0x100+(Math.round((B2-B1)*p)+B1)).toString(16).slice(1);
    }

    getColor(count) {
        // console.debug(this.props.max, count);
        if (count == this.props.max) {
            return '#31AF4F';
        } else if (count > 0) {
            return this.blendColors('#BFEDCA', '#31AF4F', count/this.props.max);
        } else {
            return '#E0E0E0';
        }
    }

    render() {
        let classes = classNames({
            'us-state': true,
            'add show': this.state.add,
            'subtract show': this.state.subtract
        })

        return (
            <path className={classes} fill={this.getColor(this.props.st.count)} d={this.props.st.path} id={`${this.props.abbrev}-${this.props.pkg_id}`} />
        )
    }
}