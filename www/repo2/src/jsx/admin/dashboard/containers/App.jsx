import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import io from 'socket.io-client';
import { remove, uniq } from 'lodash';
// import ReactCSSTransitionGroup from 'react/lib/ReactCSSTransitionGroup';
import TransitionManager from 'react-transition-manager';
// import pg from 'pg';
import { 
    insertLogin, deleteLogin, insertHistory, fetchLogins, fetchLoginsHistory, setHistoryTimeframe,
    setHistoryFetchTimer
} from '../actions';

import { filteredContentSelector } from '../selectors';
import TimeframeLink from '../components/TimeframeLink';
import StatesMap from '../components/StatesMap';
import ColorLegend from '../components/ColorLegend';

//    "react": "shripadk/react-ssr-temp-fix",

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            kt_logins: [],
            cr_logins: []
        }
    }

    // processLoginInserts(payload) {
    //     console.debug('insert', payload);
    //     let student_id, pkg_id;
    //     [student_id, pkg_id] = payload.split('|');

    //     if (pkg_id == 1) { // KT Login
    //         let kt_logins = uniq(this.state.kt_logins.concat([student_id]));
    //         this.setState({kt_logins});
    //     } else if (pkg_id == 2) { // CR Login
    //         let cr_logins = uniq(this.state.cr_logins.concat([student_id]));
    //         this.setState({cr_logins});
    //     }
    // }

    // processLoginDeletes(payload) {
    //     console.debug('delete', payload);
    //     let deleted_student_id, pkg_id;
    //     [deleted_student_id, pkg_id] = payload.split('|');

    //     if (pkg_id == 1) { // KT Login
    //         let kt_logins = this.state.kt_logins.slice();
    //         remove(kt_logins, (student_id) => {
    //             return student_id == deleted_student_id;
    //         });

    //         this.setState({kt_logins: uniq(kt_logins)});
    //     } else if (pkg_id == 2) { // CR Login
    //         let cr_logins = this.state.cr_logins.slice();

    //         remove(cr_logins, (student_id) => {
    //             return student_id == deleted_student_id;
    //         });
    //         this.setState({cr_logins: uniq(cr_logins)});
    //     }
    // }

    componentDidMount() {
        console.debug('componentDidMount')
        const { dispatch, timeframe } = this.props;

        dispatch(fetchLogins());
        dispatch(fetchLoginsHistory(timeframe));

        let student_id, pkg_id, insert_date, count, statecode;

        Pace.off();
        if (Pace && Pace.options && Pace.options.ajax) Pace.options.ajax.trackWebSockets = false;

        let socket = io.connect('/ktadmin');
        console.debug(socket);

        // let logins = this.state.logins || [];

        socket.on('connect', () => {
            console.debug('CONNECT CONNECT CONNECT');
        });

        socket.on('connected', () => {
            console.debug('ready for data');
            socket.emit('ready for data', {});
        });

        // socket.on('initial', (data) => {
        //     dispatch(insertLogin(data.message.payload.split('|')))
        //     // this.processLoginInserts(data.message.payload);
        //     // console.info('initial', data.message.payload);
        //     // let logins = this.state.logins.concat([data.message.payload]);
        //     // this.setState({logins: uniq(logins)});

        //     // logins = this.state.logins || [];
        //     // logins.push(data.message.payload);
        //     // this.setState({
        //     //     logins
        //     // });
        //     // console.info(logins);
        // });

        socket.on('insert', (data) => {
            console.debug('insert', data);
            // console.debug(data.message.payload.split('|'));
            [student_id, pkg_id, statecode, insert_date] = data.message.payload.split('|');
            // console.debug(student_id, pkg_id, statecode, insert_date);
            insert_date = (new Date(insert_date)).toISOString();
            dispatch(insertLogin(parseInt(student_id, 10), parseInt(pkg_id, 10), statecode, insert_date));
            // this.processLoginInserts(data.message.payload);
            // console.info('insert', data.message.payload);
            // let logins = this.state.logins.concat([data.message.payload]);
            // this.setState({logins: uniq(logins)});
            // logins.push(data.message.payload);
            // this.setState({
            //     logins
            // })
        });

        socket.on('delete', (data) => {
            console.debug('delete', data);
            [student_id, pkg_id, statecode, insert_date] = data.message.payload.split('|');
            dispatch(deleteLogin(parseInt(student_id, 10), parseInt(pkg_id, 10)));
            // this.processLoginDeletes(data.message.payload);
            // console.info('delete', data.message.payload);
            // let logins = this.state.logins.slice();

            // remove(logins, (student_id) => {
            //     return student_id == data.message.payload;
            // });
            // this.setState({logins: uniq(logins)});
        });

        socket.on('history_insert', (data) => {
            console.debug('history_insert', data);
            //  We only do listening inserts when we're at the 5 second granularity,
            //  otherwise we're using setTimeout to fetch 
            if (this.props.timeframe == 'hour') {
                [pkg_id, insert_date, count] = data.message.payload.split('|');
                //  Need to format insert_date like the others
                insert_date = (new Date(insert_date)).toISOString();
                // console.info(insert_date);
                dispatch(insertHistory(parseInt(pkg_id), insert_date, parseInt(count)));
            }
        });

        socket.on('disconnect', () => {
            console.info('DISCONNECTED');
        });

        socket.on('reconnect', () => {
            console.info('RECONNECT');
        });

        // console.info('SET STATE SET STATE SET STATE SET STATE');
        // console.info(logins);

        this.setHistoryFetchTimer();
        

    }

    componentDidUpdate(prevProps, prevState) {
        // console.debug('componentDidUpdate');
        // console.debug(this.props.history);

        $('#ktspark').sparkline(this.props.history.kt.slice(-200), { height: '40px' });
        $('#crspark').sparkline(this.props.history.cr.slice(-200), { height: '40px' });
    }

    componentWillUnmout() {
        window.clearTimeout(this.props.timer);
    }

    setHistoryFetchTimer() {
        // console.debug('setHistoryFetchTimer');
        let delay;
        const { dispatch, timeframe } = this.props;

        if (timeframe == 'hour') return; // we listen for these inserts
        switch(timeframe) {
            case 'day':
                delay = 5 * 60 * 1000; // 5 minutes
                break;
            case 'week':
                delay = 1 * 60 * 60 * 1000; // 1 hour
                break;
            case 'month':
                delay = 6 * 60 * 60 * 1000; // 6 hours
                break;
        }
        console.debug(delay);

        let fetch = () => {
            dispatch(fetchLoginsHistory(timeframe));
            this.setHistoryFetchTimer();
        }
        let timeoutid = window.setTimeout(fetch, delay);
        dispatch(setHistoryFetchTimer(timeoutid));
    }

    render() {
        //  Injected by connect() call:
        // const { dispatch } = this.props;

        console.debug(this.props);
        console.debug(this.state); 

        // let logins = this.state.logins.map((student_id) => {
        //     return <div key={student_id}>{student_id}</div>
        // });

        // let kt_logins = this.props.logins.kt.map((student_id) => {
        //     return <div className="student-id" key={student_id}>{student_id}</div>
        // });

        // let cr_logins = this.props.logins.cr.map((student_id) => {
        //     return <div className="student-id" key={student_id}>{student_id}</div>
        // });

        let setTimeframe = (timeframe) => {
            // e.preventDefault();
            console.debug(this);
            console.debug(timeframe);
            const { dispatch } = this.props;

            dispatch(setHistoryTimeframe(timeframe));
            dispatch(fetchLoginsHistory(timeframe));
            this.setHistoryFetchTimer();
        }

        let kt_state_counts = this.props.orderedStates.kt.map((st) => {
            return <div className="state-count" key={`kt-${st.st}`}>{st.st}: {st.count}</div>
        });

        let cr_state_counts = this.props.orderedStates.cr.map((st) => {
            return <div className="state-count" key={`cr-${st.st}`}>{st.st}: {st.count}</div>
        });

        console.debug(kt_state_counts);

        // logins = (logins.length > 0) ? logins : <div key="solo" />

        // console.debug(logins);

        // <Chart questionIds={question_ids} answerVals={answer_vals} answerSumsArr={answer_sums_arr} answerSumsMax={answer_sums_max} />

        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PanelContainer controlStyles='bg-blue fg-white'>
                                <PanelHeader className='bg-blue fg-white' style={{margin: 0}}>
                                    <Grid>
                                        <Row>
                                            <Col xs={12}>
                                                <h3>Announcements</h3>
                                                <p></p>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelHeader>
                                <PanelBody>
                                    <Grid>
                                        <Row>
                                            <Col xs={12}>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Subject</th>
                                                            <th>Short Body</th>
                                                            <th>Long Body</th>
                                                            <th>Date Added</th>
                                                            <th>Is Active</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Levels 1 and 2 to be Reorganized</td>
                                                            <td>We will soon be moving our Pre-WorkKeys lessons for Levels 1 and 2 of Reading, Math and Locating to the existing Reading, Math and Locating courses, including pretests.  For more information, see the <strong><em>Levels 1 and 2</em></strong> document on the support page (click <strong>Support</strong> in the upper right corner).</td>
                                                            <td>Long Body text</td>
                                                            <td>47:25.0</td>
                                                            <td>Active</td>
                                                            <td><a href="#" >Edit</a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>What is new</td>
                                                            <td>Signing up students is faster now!  When creating a student, you can jump directly to assigning lessons or adding the student to a class.  Assigning lessons is faster, with one click to "Auto" assign the pretest in a skill.  And the new "My Reports" menu keeps a list of recent class or school reports.</td>
                                                            <td>Long Body text</td>
                                                            <td>42:20.0</td>
                                                            <td>Active</td>
                                                            <td><a href="#" >Edit</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelBody>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        )
    }
}

// export default App;

export default connect(filteredContentSelector)(App);
