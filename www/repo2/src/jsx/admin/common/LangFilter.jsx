import { Component, PropTypes } from 'react';

import { map, forOwn } from 'lodash';

export default class LangFilter extends Component {
    handleChange(e) {
        this.props.onLangChange(parseInt(e.target.value, 10));
    }

    render() {
        return (
            <Form>
                <Col xs={12} sm={12} collapseLeft >
                    <Select control id="lang" onChange={this.handleChange.bind(this)} value={this.props.lang}>
                        <option value="" >Select Language</option>
                        {map(this.props.langs, (val, key) => 
                            <option key={val} value={val}>{key}</option>
                        )}
                    </Select>
                </Col>
            </Form>
        )
    }
}

