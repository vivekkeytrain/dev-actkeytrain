import DetailContainer from './DetailContainer';
import DetailContainerHeader from './DetailContainerHeader';
import DetailContainerBody from './DetailContainerBody';

export { DetailContainer, DetailContainerHeader, DetailContainerBody };