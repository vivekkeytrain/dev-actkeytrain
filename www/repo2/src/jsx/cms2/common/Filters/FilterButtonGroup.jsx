import React, { Component, PropTypes } from 'react';

export default class FilterGroupButton extends Component {
    onButtonClick(button_val, e) {
        this.props.onButtonClick(button_val);
        e.target.blur();
    }

    render() {
        return (
            <Button bsStyle='blue' md active={(this.props.buttonVal == this.props.groupVal)} onClick={this.onButtonClick.bind(this, this.props.buttonVal)}>{this.props.children}</Button>
        );
    }
} 