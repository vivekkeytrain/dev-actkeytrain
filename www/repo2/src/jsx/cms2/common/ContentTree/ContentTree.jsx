import { Component, PropTypes } from 'react';

import ContentTreeSelect from './ContentTreeSelect';

export default class ContentTree extends Component {
    handleChange(type, val) {
        this.props.onContentTreeChange(type, val);
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col>
                        <Grid>
                            <Row>
                                <Col xs={12} collapseLeft collapseRight>
                                    <Form>
                                        <ContentTreeSelect label={'Unit'} options={this.props.contentTreeOptions.units} currentVal={this.props.contentTreeVals.unit} changeHandler={this.handleChange.bind(this, 'unit')} />
                                        <ContentTreeSelect label={'Course'} options={this.props.contentTreeOptions.courses} currentVal={this.props.contentTreeVals.course} changeHandler={this.handleChange.bind(this, 'course')} />
                                        <ContentTreeSelect label={'Lesson'} options={this.props.contentTreeOptions.lessons} currentVal={this.props.contentTreeVals.lesson} changeHandler={this.handleChange.bind(this, 'lesson')} />
                                        <ContentTreeSelect label={'Topic'} options={this.props.contentTreeOptions.topics} currentVal={this.props.contentTreeVals.topic} changeHandler={this.handleChange.bind(this, 'topic')} /> 
                                    </Form>
                                </Col>
                            </Row>
                        </Grid>
                    </Col>
                </Row>
            </Grid>
        )
    }
}

ContentTree.propTypes = {
    contentTreeOptions: PropTypes.object.isRequired,
    contentTreeVals: PropTypes.object.isRequired,
    onContentTreeChange: PropTypes.func.isRequired
}