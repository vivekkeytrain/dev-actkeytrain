import { Component, PropTypes } from 'react';

import ContentTreeOption from './ContentTreeOption';

export default class ContentTreeSelect extends Component {
    handleChange(clickHandler, e) {
        this.props.changeHandler(parseInt(e.target.value, 10));
    }

    render() {
        let disabled = (this.props.options.length == 0);
        return (

                <Col xs={12} sm={3} collapseLeft >
                    <Select control id={this.props.label} onChange={this.handleChange.bind(this, this.props.changeHandler)} value={this.props.currentVal} disabled={disabled} >
                        <option value="" >Select {this.props.label}</option>
                        {(this.props.label !== 'Unit') ? <option value="-1"> -- ALL -- </option> : ''}
                        {this.props.options.map((option, index) => 
                            <ContentTreeOption option={option} index={index} label={this.props.label} key={index} />
                        )}
                    </Select>
                </Col>
            
        )
    }
}

