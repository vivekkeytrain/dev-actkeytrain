import { Component, PropTypes } from 'react';

import classNames from 'classnames';
import { isUndefined } from 'lodash';

export default class SortTableCell extends Component {
    render() {
        let cell_data = this.props.dataRow[this.props.displayField.name];

        if (typeof this.props.displayField.displayFunc === 'function') {
            let dfunc = this.props.displayField.displayFunc.bind(this, cell_data, this.props, this.props.index);
            cell_data = dfunc();
        }

        return <td key={this.props.index} style={!isUndefined(this.props.displayField.style) ? this.props.displayField.style : {}}>{cell_data}</td>
    }
}

SortTableCell.propTypes = {
    dataRow: PropTypes.object.isRequired,
    displayField: PropTypes.shape({
        name: PropTypes.string.isRequired,
        label: PropTypes.string,
        isOn: PropTypes.bool,
        sortable: PropTypes.bool,
        displayFunc: PropTypes.func,
        style: PropTypes.object
    }).isRequired,
    index: PropTypes.number.isRequired
}