import { Component, PropTypes } from 'react';

import SortHeader from './SortHeader';

export default class SortTableHeader extends Component {
    render() {
        return (
            <thead>
                <tr>
                    {
                        this.props.displayFields.map((field, ind) => {
                            return <SortHeader displayField={field} sortField={this.props.sortField} sortDir={this.props.sortDir} key={ind} setSortField={this.props.setSortField} />
                        })
                    }
                </tr>
            </thead>
        )
    }
}

SortTableHeader.propTypes = {
    displayFields: PropTypes.arrayOf(PropTypes.object).isRequired,
    sortField: PropTypes.string,
    sortDir: PropTypes.oneOf([
        'asc', 'desc'
    ]),
    setSortField: PropTypes.func.isRequired
}

SortTableHeader.defaultProps = {
    sortDir: 'asc'
}
