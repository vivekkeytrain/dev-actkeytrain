import React, { Component, PropTypes } from 'react';

import classNames from 'classnames';

export default class SortHeader extends Component {
    handleSortHeaderClick(e) {
        e.preventDefault();

        if (this.props.displayField.sortable === false) return; // Not a sortable field, do nothing

        let sort_dir = (this.props.sortField == this.props.displayField.name && this.props.sortDir == 'asc') ? 'desc' : 'asc';
        this.props.setSortField(this.props.displayField.name, sort_dir);
    }

    render() {
        // console.debug(this.props);
        let classes = classNames({
            'sortable-head': true,
            'sortable-descending': (this.props.sortField == this.props.displayField.name && this.props.sortDir == 'desc'),
            'sortable-ascending': (this.props.sortField == this.props.displayField.name && this.props.sortDir == 'asc')
        });

        let label;
        if (this.props.displayField.sortable !== false) {
            label = <button onClick={this.handleSortHeaderClick.bind(this)}>{this.props.displayField.label ? this.props.displayField.label : this.props.displayField.name}</button>;
        } else {
            label = <span style={{padding: ".9em 1.6em .7em .6em", display:"inline-block"}}>{this.props.displayField.label ? this.props.displayField.label : this.props.displayField.name}</span>
        }

        return (
            <th data-sortable-col={this.props.displayField.sortable !== false} className={classes}>
                {label}
            </th>
        )
    }
} 

SortHeader.propTypes = {
    displayField: PropTypes.shape({
        name: PropTypes.string.isRequired,
        label: PropTypes.string,
        isOn: PropTypes.bool,
        sortable: PropTypes.bool,
        displayFunc: PropTypes.func,
        style: PropTypes.object
    }).isRequired,
    sortField: PropTypes.string,
    sortDir: PropTypes.oneOf([
        'asc', 'desc'
    ]),
    setSortField: PropTypes.func.isRequired
}

SortHeader.defaultProps = {
    isOn: true,
    sortable: true,
    sortDir: 'asc'
}