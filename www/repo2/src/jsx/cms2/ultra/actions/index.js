import fetchJsonp from 'fetch-jsonp';
import React from 'react';

/*  Action types    */

/*  Common  */
export const ADD_MESSAGE = 'ADD_MESSAGE';
export const CLOSE_MESSAGE = 'CLOSE_MESSAGE';

export const ADD_MESSAGE = 'ADD_MESSAGE';
export const CLOSE_MESSAGE = 'CLOSE_MESSAGE';

export const UPDATE_DISPLAY_FIELD = 'UPDATE_DISPLAY_FIELD';
export const LOAD_DISPLAY_FIELDS = 'LOAD_DISPLAY_FIELDS';
export const SET_SORT_FIELD = 'SET_SORT_FIELD';
export const SET_HIGHLIGHTED_ROW = 'SET_HIGHLIGHTED_ROW';

export const SET_SESSION = 'SET_SESSION';

export const SET_LANG = 'SET_LANG';
export const SET_UNIT = 'SET_UNIT';
export const SET_COURSE = 'SET_COURSE';
export const SET_LESSON = 'SET_LESSON';
export const SET_TOPIC = 'SET_TOPIC';

/*  Find Page   */
export const SET_PAGEID = 'SET_PAGEID';
export const SET_PATTERN = 'SET_PATTERN';
export const SET_PATTERN_FILTER = 'SET_PATTERN_FILTER';

export const SET_EDIT_MODE = 'SET_EDIT_MODE';
export const SET_EDIT_ID = 'SET_EDIT_ID';

export const SET_CHUNK_MESSAGE = 'SET_CHUNK_MESSAGE';
export const REMOVE_CHUNK_MESSAGE = 'REMOVE_CHUNK_MESSAGE';

export const SET_FILTER_STATUS = 'SET_FILTER_STATUS';

/*  Async action types  */

/*  Common  */
export const DO_LOGIN_LESSON_REQUEST = 'DO_LOGIN_LESSON_REQUEST';
export const DO_LOGIN_LESSON_SUCCESS = 'DO_LOGIN_LESSON_SUCCESS';
export const DO_LOGIN_LESSON_FAILURE = 'DO_LOGIN_LESSON_FAILURE';

export const FETCH_EDITOR_CONTENT_SUCCESS = 'FETCH_EDITOR_CONTENT_SUCCESS';
export const FETCH_EDITOR_CONTENT_FAILURE = 'FETCH_EDITOR_CONTENT_FAILURE';
export const FETCH_EDITOR_CONTENT_REQUEST = 'FETCH_EDITOR_CONTENT_REQUEST';



/*  Page Issues */
export const FETCH_PAGE_ISSUES_REQUEST = 'FETCH_PAGE_ISSUES_REQUEST';
export const FETCH_PAGE_ISSUES_SUCCESS = 'FETCH_PAGE_ISSUES_SUCCESS';
export const FETCH_PAGE_ISSUES_FAILURE = 'FETCH_PAGE_ISSUES_FAILURE';

/*  Find Page   */
export const FETCH_PAGES_REQUEST = 'FETCH_PAGES_REQUEST';
export const FETCH_PAGES_FAILURE = 'FETCH_PAGES_FAILURE';
export const FETCH_PAGES_SUCCESS = 'FETCH_PAGES_SUCCESS';

export const FETCH_PAGE_DETAIL_REQUEST = 'FETCH_PAGE_DETAIL_REQUEST';
export const FETCH_PAGE_DETAIL_FAILURE = 'FETCH_PAGE_DETAIL_FAILURE';
export const FETCH_PAGE_DETAIL_SUCCESS = 'FETCH_PAGE_DETAIL_SUCCESS';

export const FETCH_PAGE_CHUNKS_REQUEST = 'FETCH_PAGE_CHUNKS_REQUEST';
export const FETCH_PAGE_CHUNKS_FAILURE = 'FETCH_PAGE_CHUNKS_FAILURE';
export const FETCH_PAGE_CHUNKS_SUCCESS = 'FETCH_PAGE_CHUNKS_SUCCESS';

export const UPDATE_FIELD_REQUEST = 'UPDATE_FIELD_REQUEST';
export const UPDATE_FIELD_FAILURE = 'UPDATE_FIELD_FAILURE';
export const UPDATE_FIELD_SUCCESS = 'UPDATE_FIELD_SUCCESS';

export const SET_CHUNK_VAL_REQUEST = 'SET_CHUNK_VAL_REQUEST';
export const SET_CHUNK_VAL_SUCCESS = 'SET_CHUNK_VAL_SUCCESS';
export const SET_CHUNK_VAL_FAILURE = 'SET_CHUNK_VAL_FAILURE';

export const DO_PAGE_CHUNK_ACTION_REQUEST = 'DO_PAGE_CHUNK_ACTION_REQUEST';
export const DO_PAGE_CHUNK_ACTION_SUCCESS = 'DO_PAGE_CHUNK_ACTION_SUCCESS';
export const DO_PAGE_CHUNK_ACTION_FAILURE = 'DO_PAGE_CHUNK_ACTION_FAILURE';

export const FETCH_PAGE_QA_CONTENT_REQUEST = 'FETCH_PAGE_QA_CONTENT_REQUEST';
export const FETCH_PAGE_QA_CONTENT_SUCCESS = 'FETCH_PAGE_QA_CONTENT_SUCCESS';
export const FETCH_PAGE_QA_CONTENT_FAILURE = 'FETCH_PAGE_QA_CONTENT_FAILURE';

export const FETCH_APP_CONSTANTS_SUCCESS = 'FETCH_APP_CONSTANTS_SUCCESS';
export const FETCH_APP_CONSTANTS_FAILURE = 'FETCH_APP_CONSTANTS_FAILURE';
export const FETCH_APP_CONSTANTS_REQUEST = 'FETCH_APP_CONSTANTS_REQUEST';

export const FIX_PAGE_REQUEST = 'FIX_PAGE_REQUEST';
export const FIX_PAGE_SUCCESS = 'FIX_PAGE_SUCCESS';
export const FIX_PAGE_FAILURE = 'FIX_PAGE_FAILURE';


/*  Action creators */

/*  Common  */
export function addMessage(message_type, message, position='top-center') {
    return {
        type: ADD_MESSAGE,
        message_type,
        message,
        position
    }
}

export function closeMessage(index) {
    return {
        type: CLOSE_MESSAGE,
        index
    }
}

export function updateDisplayField(field_name, isOn) {
    return {
        type: UPDATE_DISPLAY_FIELD,
        field_name,
        isOn
    }
}

export function updateLocalDisplayField(field_name, isOn) {
    return function(dispatch) {
        localStorage.setItem('display_field_' + field_name, isOn ? 'true' : 'false');
        dispatch(updateDisplayField(field_name, isOn));
    }
}

export function loadDisplayFields() {
    let localDisplayFields = [];
    DisplayFields.forEach((field, ind) => {
        let isOn = localStorage.getItem('display_field_' + field.name) ? localStorage.getItem('display_field_' + field.name) === 'true' : field.isOn;
        localDisplayFields[ind] = Object.assign({}, field, {
            isOn: isOn
        });
    });


    return function(dispatch) {
        map(localDisplayFields, (field, index) => {
            dispatch(updateLocalDisplayField(field.name, field.isOn))
        });
    }
}

export function setSortField(field_name, sort_dir) {
    return {
        type: SET_SORT_FIELD,
        field_name,
        sort_dir
    }
}

export function setHighlightedRow(index) {
    return {
        type: SET_HIGHLIGHTED_ROW,
        index
    }
}

export function setSession(session_id, login_session_uid) {
    return {
        type: SET_SESSION,
        session_id,
        login_session_uid
    }
}

export function setLang(lang) { 
    return { type: SET_LANG, lang };
}

export function setUnit(unit) {
    return function(dispatch) {
        localStorage.setItem('content_unit', unit);
        dispatch(setUnitStore(unit));
    }
}

export function setUnitStore(unit) {
    return { type: SET_UNIT, unit };
}

export function setCourse(course) {
    return function(dispatch) {
        localStorage.setItem('content_course', course);
        dispatch(setCourseStore(course));
    }
}

export function setCourseStore(course) {
    return { type: SET_COURSE, course };
}

export function setLesson(lesson) {
    return function(dispatch) {
        localStorage.setItem('content_lesson', lesson);
        dispatch(setLessonStore(lesson));
    }
}

export function setLessonStore(lesson) {
    return { type: SET_LESSON, lesson };
}

export function setTopic(topic) {
    return function(dispatch) {
        localStorage.setItem('content_topic', topic);
        dispatch(setTopicStore(topic));
    }
}

export function setTopicStore(topic) {
    return { type: SET_TOPIC, topic };
}

/*  Find Page   */
export function setPage(page) {
    return { type: SET_PAGE, page };
}

export function setPageID(pageid) {
    return { type: SET_PAGEID, pageid };
}

export function setPattern(pattern) {
    return { type: SET_PATTERN, pattern };
}

export function setPatternFilter(pattern) {
    return { type: SET_PATTERN_FILTER, pattern };
}

export function setPasteboard(chunk, cut=false) {
    return {
        type: SET_PASTEBOARD,
        chunk,
        cut
    }
}

export function setEditMode(mode) {
    return {
        type: SET_EDIT_MODE,
        mode
    }
}

export function setFilterStatus(stati) {
    return {
        type: SET_FILTER_STATUS,
        stati
    }
}

export function setEditID(pageid) {
    return {
        type: SET_EDIT_ID,
        pageid
    }
}

/*  Async action creators   */

/*  Common  */
function doLoginLessonSuccess(session_id, login_session_uid, lesson_id, lang) {
    // console.debug('doLoginLessonSuccess', session_id, login_session_uid, lesson_id, lang);
    // console.debug('login_session_uid: ' + login_session_uid);
    // lang = lang.toUpperCase();
    return { 
        type: DO_LOGIN_LESSON_SUCCESS,
        session_id,
        login_session_uid,
        lesson_id,
        lang,
        receivedAt: Date.now()
    };
}

function doLoginLessonFailure(ex) {
    return {
        type: DO_LOGIN_LESSON_FAILURE,
        ex
    }
}

function doLoginLessonRequest() {
    return { type: DO_LOGIN_LESSON_REQUEST };
}


function shouldDoLoginLesson(state, lesson_id, lang) {
    // console.debug('shouldDoLoginLesson: ' + lesson_id + ', ' + lang);
    if (!lesson_id || lesson_id == -1) return false;
    const sessions = state.filters.sessions;
    // console.debug(sessions);
    if (!sessions[lang]) return true;
    const session = sessions[lang][lesson_id];
    // console.debug(session);

    if (!session) {
        return true;
    } else if (session.isFetching) {
        return false
    } else {
        return (Date.now() - session.lastUpdated) > (1000 * 60 * 15); //   15 minutes old, refetch
    }
}


function doLoginLesson(lesson_id, lang) {
    return function(dispatch) {
        dispatch(doLoginLessonRequest());

        let lang_code = invert(LangOptions)[lang];

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_init_review_session?lesson_id=' + lesson_id + '&lang_code=' + lang_code, { timeout: 20000 })
            .then(
                response => response.json(),
                ex => dispatch(doLoginLessonFailure(ex))
            )
            .then(json => dispatch(doLoginLessonSuccess(json.ResultSets[0][0].session_id, json.ResultSets[0][0].luid_str, lesson_id, lang)))
            .catch(ex => dispatch(doLoginLessonFailure(ex)));
    }

}

export function doLoginIfNeeded(lesson_id, lang) {
    // console.debug('doLoginIfNeeded', lesson_id, lang);
    return (dispatch, getState) => {
        if (shouldDoLoginLesson(getState(), lesson_id, lang)) {
            // console.debug('doLogin');
            return dispatch(doLoginLesson(lesson_id, lang));
        } else {
            return Promise.resolve();
        }
    }
}

function fetchEditorContentSuccess(content) {
    return {
        type: FETCH_EDITOR_CONTENT_SUCCESS,
        content
    }
}

function fetchEditorContentFailure(ex) {
    return {
        type: FETCH_EDITOR_CONTENT_FAILURE,
        ex
    }
}

function fetchEditorContentRequest() {
    return { type: FETCH_EDITOR_CONTENT_REQUEST };
}

export function fetchEditorContent() {
    return function(dispatch) {
        dispatch(fetchEditorContentRequest());

        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_content_list/jsonp', {
                timeout: 20000
            })
            .then(
                response => response.json(),
                ex => dispatch(fetchEditorContentFailure(ex))
            )
            .then(json => dispatch(fetchEditorContentSuccess(json.ResultSets[0])))
            .catch(ex => dispatch(fetchEditorContentFailure(ex)));
    }
}


/*  Page Issues */
function fetchPageIssuesSuccess(page_issues) {
    return {
        type: FETCH_PAGE_ISSUES_SUCCESS,
        page_issues,
        receivedAt: Date.now()
    }
}

function fetchPageIssuesFailure(ex) {
    return {
        type: FETCH_PAGE_ISSUES_FAILURE,
        ex
    }
}

function fetchPageIssuesRequest() {
    return {
        type: FETCH_PAGE_ISSUES_REQUEST
    }
}

export function fetchPageIssues() {
    return function(dispatch) {
        dispatch(fetchPageIssuesRequest());

        return fetchJsonp(`${editor_api_url}dbo.api_sp_get_page_issues`)
            .then(
                response => response.json(),
                ex => dispatch(fetchPageIssuesFailure(ex))
            )
            .then(json => dispatch(fetchPageIssuesSuccess(json.ResultSets[0])))
            .catch(ex => dispatch(fetchPageIssuesFailure(ex)));
    }
}



/*  Find Page   */
function fetchPageDetailSuccess(pageid, page_data) {
    return {
        type: FETCH_PAGE_DETAIL_SUCCESS,
        pageid,
        page_data
    }
}

function fetchPageDetailRequest(pageid) {
    return {
        type: FETCH_PAGE_DETAIL_REQUEST,
        pageid
    }
}

function fetchPageDetailFailure(pageid, ex) {
    return {
        type: FETCH_PAGE_DETAIL_FAILURE,
        pageid,
        ex
    }
}

export function fetchPageDetail(pageid) {
    return function(dispatch) {
        dispatch(fetchPageDetailRequest(pageid));
        return fetchJsonp('http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_page_details?pageid=' + pageid)
            .then(
                response => response.json(),
                ex => dispatch(fetchPageDetailFailure(pageid, ex))
            )
            .then(json => dispatch(fetchPageDetailSuccess(pageid, json.ResultSets[0])))
            .catch(ex => dispatch(fetchPageDetailFailure(pageid, ex)));
    }
}

function fetchPageChunksSuccess(pageid, lang, chunks) {
    return {
        type: FETCH_PAGE_CHUNKS_SUCCESS,
        pageid,
        lang,
        chunks
    }
}

function fetchPageChunksRequest(pageid, lang) {
    return {
        type: FETCH_PAGE_CHUNKS_REQUEST,
        pageid, 
        lang
    }
}

function fetchPageChunksFailure(pageid, lang, ex) {
    return {
        type: FETCH_PAGE_CHUNKS_FAILURE,
        pageid,
        lang,
        ex
    }
}

function fetchPageChunks(pageid, lang) {
    return function(dispatch) {
        dispatch(fetchPageChunksRequest(pageid, lang));
        console.debug(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_page_chunks?pageid=${pageid}&lang_id=${lang}`);
        return fetchJsonp(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_page_chunks?pageid=${pageid}&lang_id=${lang}`)
            .then(
                response => response.json(),
                ex => dispatch(fetchPageChunksFailure(pageid, lang, ex))
            )
            .then(json => {
                console.debug(json);
                dispatch(fetchPageChunksSuccess(pageid, lang, json.ResultSets[0]));
            })
            .catch(ex => dispatch(fetchPageChunksFailure(pageid, lang, ex)));
    }
}

function shouldFetchPageChunks(state, pageid, lang) {
    if (!(pageid && lang)) return false;

    const pages = state.pages;
    if (!(pages[lang] && pages[lang][pageid] && pages[lang][pageid].chunks)) return true;

    return false;
}

export function fetchPageChunksIfNeeded(pageid, lang) {
    console.debug('fetchPageChunksIfNeeded', pageid, lang);
    return (dispatch, getState) => {
        if (shouldFetchPageChunks(getState(), pageid, lang)) {
            return dispatch(fetchPageChunks(pageid, lang));
        } else {
            return Promise.resolve();
        }
    }
}

function fetchPagesSuccess(tag, lang, pattern, pages) {
    return {
        type: FETCH_PAGES_SUCCESS,
        tag,
        lang,
        pattern,
        pages,
        receivedAt: Date.now()
    }
}

function fetchPagesRequest(tag, lang, pattern) {
    return {
        type: FETCH_PAGES_REQUEST,
        tag,
        lang,
        pattern
    }
}

function fetchPagesFailure(ex) {
    return {
        type: FETCH_PAGES_FAILURE,
        ex
    }
}

function fetchPages(tag, lang, pattern='', stati=0) {
    console.debug('fetchPages', tag, lang, pattern);
    return function(dispatch) {
        dispatch(fetchPagesRequest(`${tag}_${stati}`, lang, pattern));
        console.debug(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_page_data?content_tag=${tag}&pattern=${pattern}&qa_status_val=${stati}`);
        return fetchJsonp(`http://dev.edapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_page_data?content_tag=${tag}&pattern=${pattern}&qa_status_val=${stati}`, {
            timeout: 20000
        })
            .then(
                response => response.json(),
                ex => dispatch(fetchPagesFailure(ex))
            )
            .then(json => dispatch(fetchPagesSuccess(`${tag}_${stati}`, lang, pattern, json.ResultSets[0])))
            .catch(ex => dispatch(fetchPagesFailure(ex)));
    }
}

function shouldFetchPages(state, content_tag, lang, pattern, stati) {
    console.debug('shouldFetchPages', content_tag, lang, pattern, stati);
    if (!(content_tag && lang) && !(content_tag && pattern && lang)) return false;
    
    const pages = state.pages_by_tag;
    if (!pages[lang]) return true;

    const lang_pages = pages[lang];
    console.debug(lang_pages);

    let tag = `${content_tag}_${stati}`
    console.debug('tag', tag);
    if (content_tag && !lang_pages[tag]) return true;
    if (pattern && !lang_pages['PATTERN_' + pattern]) return true;

    const tag_pages = (tag) ? lang_pages[tag] : lang_pages['PATTERN_' + pattern];
    if (tag_pages.isFetching) {
        return true;
    } else {
        return (Date.now() - lang_pages[tag].lastUpdated) > (1000 * 60 * 10); //   10 minutes old, refetch
    }
}

export function fetchPagesIfNeeded(unit, course, lesson, topic, lang, pageid, pattern, stati=0) {
    console.debug('fetchPagesIfNeeded', unit, course, lesson, topic, lang, pageid, pattern, stati);
    return (dispatch, getState) => {
        let tag;

        if (pageid) 
            tag = 'P' + pageid;
        else if (topic && topic != -1)
            tag = 'T' + topic;
        else if (lesson && topic == -1)
            tag = 'L' + lesson;
        else if (course && lesson == -1)
            tag = 'C' + course;
        else if (unit && course == -1)
            tag = 'U' + unit;

        console.debug(tag);
        if (shouldFetchPages(getState(), tag, lang, pattern, stati)) {
            console.debug('fetching');
            return dispatch(fetchPages(tag, lang, pattern, stati));
        } else {
            console.debug('not fetching');
            return Promise.resolve();
        }
    }
}