import { connect } from 'react-redux';

import {

} from '../actions';

import { filteredContentSelector } from '../selectors';
import SortTable from '../../common/SortTable/SortTable';
import { DetailContainer, DetailContainerHeader, DetailContainerBody } from '../../common/DetailContainer';

class App extends React.Component {
    componentDidMount() {
        const { dispatch } = this.props;
    }

    render() {
        const { dispatch } = this.props;

        let title = 'The Ultimate Interface';
        let description = '42';

        return (
            <Container id='body'>
            <Grid>
                <Row>
                    <Col xs={12}>
                        <PanelContainer plain controlStyles='bg-green fg-white'>
                            <Panel style={{background: 'white'}}>
                                <PanelHeader className='bg-green fg-white' style={{margin: 0}}>
                                    <Grid>
                                        <Row>
                                            <Col xs={12}>
                                                <h3>{title}</h3>
                                                <p>{description}</p>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelHeader>
                            </Panel>
                        </PanelContainer>
                        <PanelBody>
                            <Grid>
                                <Col sx={12} style={{padding: 25, overflow: 'auto'}}>
                                    <SortTable
                                        data={table_data}
                                        dataFetching={data_fetching}
                                        sortField={sort_field}
                                        sortDir={sort_dir}
                                        setSortField={(field_name, sort_dir) => 
                                            dispatch(setSortField(field_name, sort_dir))
                                        }
                                        handlePageLinkClick={(issue, lesson_id, lang) =>
                                            handlePageLinkClick(issue, lesson_id, lang)
                                        }
                                        onUpdateDisplayField={(field_name, isOn) => 
                                            dispatch(updateLocalDisplayField(field_name, isOn))
                                        }
                                        />
                                    <DetailContainer
                                        visible={container_visible}
                                        closeDetailContainer={(e) => {

                                        }}>
                                        <DetailContainerHeader
                                            closeDetailContainer={(e)=> {

                                            }}
                                            >
                                            <Col xs={12}>{container_header_children}</Col>
                                        </DetailContainerHeader>
                                        <DetailContainerBody>
                                            <Col xs={12}>{container_body_children}</Col>
                                        </DetailContainerBody>
                                    </DetailContainer>
                                </Col>
                            </Grid>
                        </PanelBody>
                    </Col>
                </Row>
            </Grid>
            </Container>
        );
    }
}

export default connect(filteredContentSelector)(App);