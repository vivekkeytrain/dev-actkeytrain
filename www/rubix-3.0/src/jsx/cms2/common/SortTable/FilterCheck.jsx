import React, { Component, PropTypes } from 'react';

export default class FilterCheck extends Component {
    onUpdateDisplayField(name, e) {
        this.props.onUpdateDisplayField(name, e.target.checked);
    }

    render() {
        let ref = 'checkbox_' + this.props.field.name;
        return (
            <Checkbox name={this.props.field.name} ref={ref} onChange={this.onUpdateDisplayField.bind(this, this.props.field.name)} checked={this.props.field.isOn}>
                {(this.props.field.label) ? this.props.field.label : this.props.field.name}
            </Checkbox>
        );
    }
} 



