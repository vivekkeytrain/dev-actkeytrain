import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

import SortTableHeader from './SortTableHeader';
import SortTableRow from './SortTableRow';
import FilterCheck from './FilterCheck';

export default class SortTable extends Component {
    render() {
        let row_count = (this.props.data) ? this.props.data.length : 0;

        let disp_table;
        if (this.props.issuesFetching) {
            disp_table = <div className={classes} style={{width: '100%'}}>
                            <span><strong>Fetching Data, Please Hold ...</strong></span>
                         </div>
        } else {
            disp_table = <div>
                <Row>
                    <Col xs={6}>
                        <Dropdown>
                            <DropdownButton md bsStyle='blue'>
                              <span><Icon bundle='fontello' glyph='cog-5'/></span> <Caret/>
                            </DropdownButton>
                            <Menu bsStyle='blue' style={{paddingLeft: '10px'}}>
                                {this.props.displayFields.map((field, index) =>
                                    <FilterCheck onUpdateDisplayField={this.props.onUpdateDisplayField} field={field} key={index} />
                                )}
                            </Menu>
                        </Dropdown>
                    </Col>
                    <Col xs={2} className="pull-right">
                        Row Count: {row_count}
                    </Col>
                </Row>
                <table
                    key='dataTable'
                    ref='dataTable'
                    className='tablesaw tablesaw-sortable tablesaw-stack table-striped table-bordered'>
                    <thead>
                        <SortTableHeader displayFields={this.props.displayFields} sortField={this.props.sortField} sortDir={this.props.sortDir} setSortField={this.props.setSortField} />
                    </thead>
                    <tbody>
                        {
                            this.props.data.map((data_row, index) => 
                                <SortTableRow {...this.props} dataRow={data_row} index={index} highlighted={(this.props.highlightedRow == index)} key={index} ref={(ref) => this['tableRow_' + index] = ref} />
                            )
                        }
                    </tbody>
                </table>
            </div>
        }

        return disp_table;
    }
}

SortTable.propTypes = {
    data: PropTypes.array.isRequired,
    dataFetching: PropTypes.bool,
    displayFields: PropTypes.arrayOf(PropTypes.object).isRequired,
    highlightedRow: PropTypes.number,
    sortField: PropTypes.string,
    sortDir: PropTypes.oneOf([
        'asc', 'desc'
    ]),
    setSortField: PropTypes.func.isRequired,
    onUpdateDisplayField: PropTypes.func.isRequired
}

SortTable.defaultProps = {
    data: [],
    dataFetching: false,
    sortDir: 'asc',
    highlightedRow: -1
}
