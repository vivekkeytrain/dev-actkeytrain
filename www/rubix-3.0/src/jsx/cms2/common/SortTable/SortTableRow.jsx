import { Component, PropTypes } from 'react';

import classNames from 'classnames';
import { isUndefined } from 'lodash';

import SortTableCell from './SortTableCell';

export default class SortTableRow extends Component {
    setFocus(index, e) {
        this.props.setHighlightedRow(index);
    }

    render() {
        // console.debug(this.props);
        let classes = classNames({
            warning: (this.props.selectedRow == this.props.index),
            highlighted: this.props.highlighted
        });

        return (
            <tr onClick={this.setFocus.bind(this, this.props.index)} className={classes} ref={(ref) => this[`row_${this.props.index}`] = ref}>
                {
                    this.props.displayFields.map((field, ind) => {
                        return <SortTableCell dataRow={this.props.dataRow} displayField={field} index={ind} key={ind} />
                    })
                }
            </tr>
        )
    }
}

SortTableRow.propTypes = {
    dataRow: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired,
    displayFields: PropTypes.array.isRequired,
    selectedRow: PropTypes.number,
    highlighted: PropTypes.bool,
    setHighlightedRow: PropTypes.func
}

SortTableRow.defaultProps = {
    highlighted: false,
    selectedRow: -1
}
