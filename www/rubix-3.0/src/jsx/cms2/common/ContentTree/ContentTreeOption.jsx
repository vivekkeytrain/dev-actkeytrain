import { Component, PropTypes } from 'react';

export default class ContentTreeOption extends Component {
    render() {
        let key = this.props.label + '_' + this.props.index;

        return (
            <option key={key} value={this.props.option.id}>{this.props.option.display}</option>
        )
    }
}

