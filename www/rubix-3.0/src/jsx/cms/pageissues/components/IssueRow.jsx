import { Component, PropTypes } from 'react';
import PageLinks from './PageLinks';

import classNames from 'classnames';
import { isUndefined } from 'lodash';

export default class IssueRow extends Component {
    renderCell(displayField) {
        // console.debug(this.props);
        let cell_data = this.props.issue[displayField.name];

        if (typeof displayField.displayFunc === 'function') {
            let dfunc = displayField.displayFunc.bind(this, cell_data, this.props, this.props.index);
            cell_data = dfunc();
        }

        return cell_data;
    }

    setFocus(index, e) {
        this.props.setHighlightedRow(index);
    }

    render() {
        // console.debug(this.props);
        let classes = classNames({
            warning: (this.props.selectedRow == this.props.index),
            highlighted: this.props.highlighted
        });

        return (
            <tr onClick={this.setFocus.bind(this, this.props.index)} className={classes} ref={(ref) => this[`row_${this.props.index}`] = ref}>
                {
                    this.props.displayFieldsOn.map((field, ind) => {
                        return <td key={ind} style={!isUndefined(field.style) ? field.style : {}}>{this.renderCell(field)}</td>
                    })
                }
            </tr>
        )
    }
}