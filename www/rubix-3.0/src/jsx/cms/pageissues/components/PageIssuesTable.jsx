import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

import IssueTableHeader from './IssueTableHeader';
import IssueRow from './IssueRow';

export default class PageIssuesTable extends Component {
    render() {
        console.debug(this.props);
        let row_count = (this.props.pageIssues) ? this.props.pageIssues.length : 0;

        let disp_table;
        if (this.props.issuesFetching) {
            disp_table = <div className='progress-bar active progress-bar-striped' style={{width: '100%'}}>
                            <span><strong>Fetching Data, Please Hold ...</strong></span>
                         </div>
        } else {
            disp_table = <div>
                <div>Row Count: {row_count}</div>
                <table
                    className='tablesaw tablesaw-sortable tablesaw-stack table-striped table-bordered'>
                    <thead>
                        <IssueTableHeader displayFieldsOn={this.props.displayFieldsOn} sortField={this.props.sortField} sortDir={this.props.sortDir} setSortField={this.props.setSortField} />
                    </thead>
                    <tbody>
                        {
                            this.props.pageIssues.map((issue, index) =>
                                <IssueRow {...this.props} issue={issue} index={index} key={index} ref={(ref) => this['issueRow_' + index] = ref} highlighted={(this.props.highlightedRow == index)} />
                            )
                        }
                    </tbody>
                </table>
            </div>
        }

        return disp_table;
    }
}