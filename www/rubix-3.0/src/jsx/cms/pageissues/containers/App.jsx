import { connect } from 'react-redux';

import {
    fetchPageIssues, closeMessage, updateLocalDisplayField, setSortField, doLoginIfNeeded, fetchEditorContent
} from '../actions';

import { filteredContentSelector } from '../selectors';
import Filters from '../components/Filters';
// import PageIssuesTable from '../components/PageIssuesTable';
import SortTable from '../../../cms2/common/SortTable/SortTable';
import MessageCenter from 'common/MessageCenter';

import { DetailContainer, DetailContainerHeader, DetailContainerBody } from '../../../cms2/common/DetailContainer';

class App extends React.Component {
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchPageIssues());

        dispatch(fetchEditorContent());

        // dispatch(doLoginIfNeeded(lesson, LangOptions.EN));
        // dispatch(doLoginIfNeeded(lesson, LangOptions.ES));
    }

    render() {
        const { dispatch, page_issues, issues_fetching, sort_field, sort_dir, highlighted_row, filtered_issues, messages, displayFields, displayFieldsOn, units, courses, lessons, topics, sessions } = this.props;
        console.debug(this.props);

        let handleMessageClose = (index) => {
            dispatch(closeMessage(index));
        }

        let handlePageLinkClick = (issue, lesson_id, lang) => {
            console.debug('handlePageLinkClick', issue, lesson_id, lang);
            dispatch(doLoginIfNeeded(lesson_id, lang))
                .then(() => {
                    console.debug(sessions);
                    console.debug(sessions[lang]);
                    console.debug(sessions[lang][lesson_id]);
                    // console.debug(issue);

                    let course_tag = courses[issue.course_id].tag;
                    let lesson_tag = lessons[issue.lesson_id].tag;
                    let session_id = sessions[lang][lesson_id].session_uid;
                    let login_session_uid = sessions[lang][lesson_id].login_session_uid;

                    console.debug(course_tag);

                    let url = `http://es.run.keytrain.com/objects/${course_tag}/${lesson_tag}/${lang}/html5_course.htm#load/${session_id}/${login_session_uid}/pageid/${issue.pageID}`;

                    console.debug(url);

                    window.open(url, `lesson_${lang}`);
                    // let url = 'http://es.run.keytrain.com/objects/' + this.props.courseTag + '/' + this.props.lessonTag + '/' + lang_code + '/html5_course.htm#load/' + this.props.sessionId + '/' + this.props.loginSessionUID + '/pageid/' + this.props.pageID;
                });
        }

        /*
        <PageIssuesTable
                                    pageIssues={filtered_issues}
                                    issuesFetching={issues_fetching}
                                    displayFields={displayFields}
                                    displayFieldsOn={this.props.displayFieldsOn}
                                    sortField={sort_field}
                                    sortDir={sort_dir}
                                    units={units}
                                    courses={courses}
                                    lessons={lessons}
                                    topics={topics}
                                    setSortField={(field_name, sort_dir) => 
                                        dispatch(setSortField(field_name, sort_dir))
                                    }
                                    handlePageLinkClick={(issue, lesson_id, lang) => {
                                        handlePageLinkClick(issue, lesson_id, lang);
                                    }}
                                />
                                */


/*

                                    */                                

        return (
            <Container id='body'>
            <Grid>
              <Row>
                <Col xs={12}>
                  <PanelContainer plain controlStyles='bg-green fg-white'>
                    <Panel style={{background: 'white'}}>
                        <PanelHeader className='bg-green fg-white' style={{margin: 0}}>
                            <Grid>
                                <Row>
                                    <Col xs={12}>
                                        <h3>Page Issue Reports</h3>
                                        <p>This is a log of page issues.</p>
                                    </Col>
                                </Row>
                            </Grid>
                        </PanelHeader>
                        <PanelBody>
                        <Grid>
                          <Row>
                            <Col xs={12} style={{padding: 25, overflow: 'auto'}}>
                                <SortTable
                                    data={filtered_issues}
                                    dataFetching={issues_fetching}
                                    displayFields={displayFieldsOn}
                                    sortField={sort_field}
                                    sortDir={sort_dir}
                                    setSortField={(field_name, sort_dir) => 
                                        dispatch(setSortField(field_name, sort_dir))
                                    }
                                    handlePageLinkClick={(issue, lesson_id, lang) => {
                                        handlePageLinkClick(issue, lesson_id, lang);
                                    }}
                                    onUpdateDisplayField={(field_name, isOn) =>
                                        dispatch(updateLocalDisplayField(field_name, isOn))
                                    }
                                    />
                                <DetailContainer 
                                    visible={true}
                                    closeDetailContainer={(e) => {
                                        console.debug('close');
                                    }}>
                                    <DetailContainerHeader
                                        closeDetailContainer={(e) => {
                                            console.debug('close');
                                        }}
                                        >
                                        <Col xs={12}>Some children here</Col>
                                    </DetailContainerHeader>
                                    <DetailContainerBody>
                                        <Col xs={12}>Body children here</Col>
                                    </DetailContainerBody>
                                </DetailContainer>
                            </Col>
                          </Row>
                        </Grid>
                      </PanelBody>
                      <MessageCenter 
                        onMessageClose={(index) => 
                            handleMessageClose(index)
                        }>
                        {messages.map((message, ind) => {
                            return <div type={message.type} index={ind} visible={message.visible} key={"message_" + ind}>{message.message}</div>
                        })}
                      </MessageCenter>
                    </Panel>
                  </PanelContainer>
                </Col>
              </Row>
            </Grid>
          </Container>
        )
    }
}
    
export default connect(filteredContentSelector)(App);