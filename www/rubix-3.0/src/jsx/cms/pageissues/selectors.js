import { createSelector } from 'reselect';
import { orderBy, sortBy, filter, chain, map, find } from 'lodash';

function selectFilteredIssues(issues, sort_field, sort_dir) {
    if (sort_field) {
        return orderBy(issues, [sort_field], [sort_dir]);
    } else {
        return sortBy(issues, 'pageID');
    }
}

function selectDisplayFieldsOn(displayFields) {
    return filter(displayFields, {'isOn': true});
}

/*  Get a unique set of units   */
// function selectUniqueUnits(content, tags) {
//     return map(tags, (tag) => {
//         let match = find(content, {'unit_tag': tag});

//         return {
//             id: match.unit_id,
//             tag: match.unit_tag,
//             desc: match.unit_desc,
//             seq: match.unit_seq,
//             display: match.unit_display
//         }
//     });
// }

function selectUniqueUnits(content) {
    //  Unique array of unit_ids
    let unit_ids = chain(content)
        .map('unit_id')
        .uniq()
        .value();

    /*
    Map over unit ids, 
    find matching object in content,
    create hash mapped on unit_id
    */
    return chain(unit_ids)
        .map(unit_id => {
            let unit = find(content, {'unit_id': unit_id});

            return [unit_id, {
                id: unit.unit_id,
                tag: unit.unit_tag,
                desc: unit.unit_desc,
                seq: unit.unit_seq,
                display: unit.unit_display
            }]
        })
        .fromPairs()
        .value();
}

function selectUniqueCourses(content) {
    let course_ids = chain(content)
        .map('course_id')
        .uniq()
        .value();

    return chain(course_ids)
        .map(course_id => {
            let course = find(content, {'course_id': course_id})
            return [course_id, {
                id: course.course_id,
                tag: course.course_tag,
                desc: course.course_desc,
                seq: course.course_seq,
                display: course.course_display,
                unit_id: course.unit_id,
                unit_tag: course.unit_tag,
                unit_desc: course.unit_desc,
                unit_seq: course.unit_seq,
                unit_display: course.unit_display
            }]
        })
        .fromPairs()
        .value();
}

function selectUniqueLessons(content) {
    let lesson_ids = chain(content)
        .map('lesson_id')
        .uniq()
        .value();

    return chain(lesson_ids)
        .map(lesson_id => {
            let lesson = find(content, {'lesson_id': lesson_id});
            return [lesson_id, {
                id: lesson.lesson_id,
                tag: lesson.lesson_tag,
                desc: lesson.lesson_desc,
                seq: lesson.lesson_seq,
                display: lesson.lesson_display,
                unit_id: lesson.unit_id,
                unit_tag: lesson.unit_tag,
                unit_desc: lesson.unit_desc,
                unit_seq: lesson.unit_seq,
                unit_display: lesson.unit_display,
                course_id: lesson.course_id,
                course_tag: lesson.course_tag,
                course_desc: lesson.course_desc,
                course_seq: lesson.course_seq,
                course_display: lesson.course_display
            }]
        })
        .fromPairs()
        .value();
}

function selectUniqueTopics(content) {
    let topic_ids = chain(content)
        .map('topic_id')
        .uniq()
        .value();

    return chain(topic_ids)
        .map(topic_id => {
            let topic = find(content, {'topic_id': topic_id})
            return [topic_id, {
                id: topic.topic_id,
                tag: topic.topic_tag,
                desc: topic.topic_desc,
                seq: topic.topic_seq,
                display: topic.topic_display,
                unit_id: topic.unit_id,
                unit_tag: topic.unit_tag,
                unit_desc: topic.unit_desc,
                unit_seq: topic.unit_seq,
                unit_display: topic.unit_display,
                course_id: topic.course_id,
                course_tag: topic.course_tag,
                course_desc: topic.course_desc,
                course_seq: topic.course_seq,
                course_display: topic.course_display,
                lesson_id: topic.lesson_id,
                lesson_tag: topic.lesson_tag,
                lesson_desc: topic.lesson_desc,
                lesson_seq: topic.lesson_seq,
                lesson_display: topic.lesson_display
            }]
        })
        .fromPairs()
        .value();
}

/*  Select a unique set of unit tags that will be used to create a unique set of units  */
// function selectUnitTags(content) {
//     return chain(content)
//         .map('unit_tag')
//         .uniq()
//         .value();
// }


// function selectUniqueCourses(content, tags) {
//     // console.debug('selectUniqueCourses', content, tags);
//     return chain(tags)
//         .map((tag) => {
//             let match = find(content, {'course_tag': tag});

//             return {
//                 id: match.course_id,
//                 tag: match.course_tag,
//                 desc: match.course_desc,
//                 seq: match.course_seq,
//                 display: match.course_display
//             }
//         })
//         .value();
// }

// function selectCourseTags(content, unit) {
//     // console.debug('selectCourseTags', content, unit);
//     return chain(content)
//         .filter({'unit_id': unit})
//         .map('course_tag')
//         .uniq()
//         .value();
// }

// function selectUniqueLessons(content, tags) {
//     return chain(tags)
//         .map((tag) => {
//             let match = find(content, {'lesson_tag': tag});

//             return {
//                 id: match.lesson_id,
//                 tag: match.lesson_tag,
//                 desc: match.lesson_desc,
//                 seq: match.lesson_seq,
//                 display: match.lesson_display
//             }
//         })
//         .value();
// }

// function selectLessonTags(content, course) {
//     return chain(content)
//         .filter({'course_id': course})
//         .map('lesson_tag')
//         .uniq()
//         .value();
// }

// function selectUniqueTopics(content, tags) {
//     return chain(tags)
//         .map((tag) => {
//             let match = find(content, {'topic_tag': tag});

//             return {
//                 id: match.topic_id,
//                 tag: match.topic_tag,
//                 desc: match.topic_desc,
//                 seq: match.topic_seq,
//                 display: match.topic_display
//             }
//         })
//         .value();
// }

// function selectTopicTags(content, lesson) {
//     return chain(content)
//         .filter({'lesson_id': lesson})
//         .map('topic_tag')
//         .uniq()
//         .value();
// }

// /*  Content filtered by current selected state */
// function selectContent(content, unit, course, lesson, topic) {
//     // console.debug(content);
//     // console.debug([unit, course, lesson, topic]);
//     let filtered_content = {
//         units: content,
//         courses: [],
//         lessons: [],
//         topics: []
//     }
//     if (unit != '')
//         filtered_content.courses = filter(content, {'unit_id': unit});
    
//     if (course != '') 
//         filtered_content.lessons = filter(content, {'course_id': course});

//     if (lesson != '') 
//         filtered_content.topics = filter(content, {'lesson_id': lesson});

//     // console.debug(filtered_content);

//     return filtered_content;
// }

export function selectCourseTag(course, courses) {
    if (!course || !courses || course == -1) return '';
    // console.debug('selectCourseTag', course, courses);
    return chain(courses).find({'id': course}).value().tag;
}

export function selectLessonTag(lesson, lessons) {
    if (!lesson || !lessons || lesson == -1) return '';

    return chain(lessons).find({'id': lesson}).value().tag;
}

const issuesSelector = state => state.page_issues.issues;
const fetchingSelector = state => state.page_issues.isFetching;
const sortFieldSelector = state => state.filters.sort_field;
const sortDirSelector = state => state.filters.sort_dir;
const highlightedRowSelector = state => state.filters.highlighted_row;
const messagesSelector = state => state.filters.messages;
const displayFieldsSelector = state => state.filters.displayFields;
const contentSelector = state => state.editor_content.content;
const unitSelector = state => state.filters.unit;
const courseSelector = state => state.filters.course;
const lessonSelector = state => state.filters.lesson;
const topicSelector = state => state.filters.topic;
const sessionsSelector = state => state.filters.sessions;

const filteredIssuesSelector = createSelector(
    issuesSelector,
    sortFieldSelector,
    sortDirSelector,
    (issues, sort_field, sort_dir) => {
        return selectFilteredIssues(issues, sort_field, sort_dir);
    }
)

const displayFieldsOnSelector = createSelector(
    displayFieldsSelector,
    (displayFields) => {
        return selectDisplayFieldsOn(displayFields);
    }
);

const contentDataSelector = createSelector(
    contentSelector,
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    (content, unit, course, lesson, topic) => {
        return selectContent(content, unit, course, lesson, topic);
    }
);

// const unitTagsSelector = createSelector(
//     contentSelector,
//     (content) => {
//         return selectUnitTags(content);
//     }
// );

// const unitsSelector = createSelector(
//     [contentSelector, unitTagsSelector],
//     (content, tags) => {
//         return selectUniqueUnits(content, tags);
//     }
// );

// const courseTagsSelector = createSelector(
//     [contentDataSelector, unitSelector],
//     (content, unit) => {
//         return selectCourseTags(content.courses, unit);
//     }
// );

// const coursesSelector = createSelector(
//     [contentDataSelector, courseTagsSelector],
//     (content, tags) => {
//         return selectUniqueCourses(content.courses, tags);
//     }
// );

/*  Selector to get the course tag of the currently selected course_id */
// const courseTagSelector = createSelector(
//     [courseSelector, coursesSelector],
//     (course, courses) => {
//         return selectCourseTag(course, courses);
//     }
// );

// const lessonTagsSelector = createSelector(
//     [contentDataSelector, courseSelector],
//     (content, course) => {
//         return selectLessonTags(content.lessons, course);
//     }
// );

// const lessonsSelector = createSelector(
//     [contentDataSelector, lessonTagsSelector],
//     (content, tags) => {
//         return selectUniqueLessons(content.lessons, tags);
//     }
// );

// const lessonTagSelector = createSelector(
//     [lessonSelector, lessonsSelector],
//     (lesson, lessons) => {
//         return selectLessonTag(lesson, lessons);
//     }
// );

// const topicTagsSelector = createSelector(
//     [contentDataSelector, lessonSelector],
//     (content, lesson) => {
//         return selectTopicTags(content.topics, lesson);
//     }
// );

// const topicsSelector = createSelector(
//     [contentDataSelector, topicTagsSelector],
//     (content, tags) => {
//         return selectUniqueTopics(content.topics, tags);
//     }
// );

const unitsSelector = createSelector(
    [contentSelector],
    (content) => {
        return selectUniqueUnits(content)
    }
);

const coursesSelector = createSelector(
    [contentSelector],
    (content) => {
        return selectUniqueCourses(content);
    }
);

const lessonsSelector = createSelector(
    [contentSelector],
    (content) => {
        return selectUniqueLessons(content);
    }
);

const topicsSelector = createSelector(
    [contentSelector],
    (content) => {
        return selectUniqueTopics(content);
    }
);

export const filteredContentSelector = createSelector(
    issuesSelector,
    fetchingSelector,
    sortFieldSelector,
    sortDirSelector,
    highlightedRowSelector,
    filteredIssuesSelector,
    messagesSelector,
    displayFieldsSelector,
    displayFieldsOnSelector,
    unitsSelector,
    coursesSelector,
    lessonsSelector,
    topicsSelector,
    sessionsSelector,
    (page_issues, issues_fetching, sort_field, sort_dir, highlighted_row, filtered_issues, messages, displayFields, displayFieldsOn, units, courses, lessons, topics, sessions) => {
        return {
            page_issues,
            issues_fetching,
            sort_field,
            sort_dir,
            highlighted_row,
            filtered_issues,
            messages,
            displayFields, 
            displayFieldsOn,
            units,
            courses,
            lessons,
            topics,
            sessions
        }
    }
);