import classNames from 'classnames';
import SidebarMixin from 'global/jsx/sidebar_component';

import Header from 'common/header';
import Sidebar from 'common/sidebar';
import Footer from 'common/footer';

import { Link } from 'react-router';

import LoremIpsum from 'global/jsx/loremipsum';


var DashboardPanel = React.createClass({

  render() {

    return (

      <PanelContainer plain controlStyles={classNames('fg-white', this.props.classes)} {...this.props}>
        <Link to={this.props.path}>
        <Panel style={{background: 'white'}}>
          <PanelHeader className={classNames('text-center', this.props.classes)}>
            <Grid>
              <Row>
                <Col xs={12} className='fg-white'>
                  <h3>{this.props.label}</h3>
                </Col>
              </Row>
            </Grid>
          </PanelHeader>
          <PanelBody>
            <Grid>
              <Row>
                <Col xs={12}>
                <div>
                  <p>
                    {this.props.children}
                  </p>
                </div>
                </Col>
              </Row>
            </Grid>
          </PanelBody>
        </Panel>
        </Link>
      </PanelContainer>

    );
  }
});


class Body extends React.Component {
  render() {

    return (

      <Container id='body'>
        <Grid>

          <Row>
            <Col sm={4} smCollapseRight>
              <DashboardPanel 
                label='Find Page'
                path='/findpage/'
                classes='bg-blue'>
                <LoremIpsum query='4s' />
              </DashboardPanel>
            </Col>

            <Col sm={4} smCollapseRight>
              <DashboardPanel 
                label='Quality Assurance'
                path='/qa/'
                classes='bg-green'>
                <LoremIpsum query='4s' />
              </DashboardPanel>
            </Col>

            <Col sm={4} >
              <DashboardPanel 
                label='Attachments'
                path='/attachments/'
                classes='bg-red'>
                <LoremIpsum query='4s' />
              </DashboardPanel>
            </Col>

          </Row>

          <Row>
            <Col sm={4} smCollapseRight>
              <DashboardPanel 
                label='Page Issue Reports'
                path='/page-issues/'
                classes='bg-yellow'>
                <LoremIpsum query='4s' />
              </DashboardPanel>
            </Col>

            <Col sm={4} smCollapseRight>
              <DashboardPanel 
                label='SVG Manager'
                path='/svgmanager/'
                classes='bg-blue'>
                <LoremIpsum query='4s' />
              </DashboardPanel>
            </Col>

          </Row>

        </Grid>
      </Container>


    );
  }
}

@SidebarMixin
export default class extends React.Component {
  render() {
    var classes = classNames({
      'container-open': this.props.open
    });

    return (
      <Container id='container' className={classes}>
        <Header />
        <Sidebar />
        <Body />
        <Footer />
      </Container>
    );
  }
}
