import { createSelector } from 'reselect';
import { chain, uniq, filter, map, find, merge, includes, sortBy, orderBy, cloneDeep, forEach, isUndefined, isArray, findIndex, forOwn } from 'lodash';

import { LangOptions } from './actions';

/*  Get a unique set of units   */
function selectUniqueUnits(content, tags) {
    return map(tags, (tag) => {
        let match = find(content, {'unit_tag': tag});

        return {
            id: match.unit_id,
            tag: match.unit_tag,
            desc: match.unit_desc,
            seq: match.unit_seq,
            display: match.unit_display
        }
    });
}

/*  Select a unique set of unit tags that will be used to create a unique set of units  */
function selectUnitTags(content) {
    return chain(content)
        .map('unit_tag')
        .uniq()
        .value();
}


function selectUniqueCourses(content, tags) {
    // console.debug('selectUniqueCourses', content, tags);
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'course_tag': tag});

            return {
                id: match.course_id,
                tag: match.course_tag,
                desc: match.course_desc,
                seq: match.course_seq,
                display: match.course_display
            }
        })
        .value();
}

function selectCourseTags(content, unit) {
    // console.debug('selectCourseTags', content, unit);
    return chain(content)
        .filter({'unit_id': unit})
        .map('course_tag')
        .uniq()
        .value();
}

function selectUniqueLessons(content, tags) {
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'lesson_tag': tag});

            return {
                id: match.lesson_id,
                tag: match.lesson_tag,
                desc: match.lesson_desc,
                seq: match.lesson_seq,
                display: match.lesson_display
            }
        })
        .value();
}

function selectLessonTags(content, course) {
    return chain(content)
        .filter({'course_id': course})
        .map('lesson_tag')
        .uniq()
        .value();
}

function selectUniqueTopics(content, tags) {
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'topic_tag': tag});

            return {
                id: match.topic_id,
                tag: match.topic_tag,
                desc: match.topic_desc,
                seq: match.topic_seq,
                display: match.topic_display
            }
        })
        .value();
}

function selectTopicTags(content, lesson) {
    return chain(content)
        .filter({'lesson_id': lesson})
        .map('topic_tag')
        .uniq()
        .value();
}

/*  Content filtered by current selected state */
function selectContent(content, unit, course, lesson, topic) {
    // console.debug(content);
    // console.debug([unit, course, lesson, topic]);
    let filtered_content = {
        units: content,
        courses: [],
        lessons: [],
        topics: []
    }
    if (unit != '')
        filtered_content.courses = filter(content, {'unit_id': unit});
    
    if (course != '') 
        filtered_content.lessons = filter(content, {'course_id': course});

    if (lesson != '') 
        filtered_content.topics = filter(content, {'lesson_id': lesson});

    // console.debug(filtered_content);

    return filtered_content;
}

// function selectPages(pages, topicid, pageid, pattern) {
//     // console.debug('selectPages', topicid, pageid, pattern);
//     if (topicid) {
//         if (!pages.topics || !pages.topics[topicid]) return [];

//         return chain(pages.topics[topicid].items)
//             .values()
//             .sortBy('Pageno')
//             .value();
//     } else if (pageid) {
//         if (!pages.pages || !pages.pages[pageid]) return [];

//         return chain(pages.pages[pageid].items)
//             .values()
//             .sortBy('Pageno')
//             .value();
//     } else if (pattern) {
//         if (!pages.patterns || !pages.patterns[pattern]) return [];

//         return chain(pages.patterns[pattern].items)
//             .values()
//             .sortBy('Pageno')
//             .value();
//     }

//     return [];
// }

/*
pages_by_tag: {
        'EN': {
            T1007: {
                isFetching: false,
                items: {
                    ALL: [1,2,3,5,6,7,8]
                    PATTERN_<pattern>: [5,6,7,8],
                    PATTERN_<pattern2>: [1,2,3],
                    UPDATED_DATE: [1,2]
                }
                lastUpdated: 123545
            },
            L105: {
                isFetching: false,
                items: [...],
                lastUpdated: 123455
            }
        }
    }
*/

function selectContentTag(unit, course, lesson, topic, pageid, filter_stati) {
    // console.debug('selectContentTag', unit, course, lesson, topic);
    let tag;
    if (pageid)
        tag = 'P' + pageid;
    else if (topic && topic != -1)
        tag = 'T' + topic;
    else if (lesson && topic == -1)
        tag = 'L' + lesson;
    else if (course && lesson == -1)
        tag = 'C' + course;
    else if (unit && course == -1)
        tag = 'U' + unit;

    tag = `${tag}_${filter_stati}`;

    return tag;
}

function getTagPages(pages, pages_by_tag, lang, tag, pattern, updated_date, sort_field, sort_dir) {
    console.log('getTagPages', lang, tag, pattern, updated_date);
    if (!(pages[lang] && pages_by_tag[lang] && pages_by_tag[lang][tag])) {
        return [];
    } else {
        let page_items = pages_by_tag[lang][tag].items;
        // console.debug(page_items);
        // let pattern_tag = (pattern) ? 'PATTERN_' + pattern : 'ALL';
        let pattern_tag = 'ALL';
        if (pattern) 
            pattern_tag = `PATTERN_${pattern}`;
        else if (updated_date)
            pattern_tag = `UPDATED_DATE_${updated_date}`;
        // console.debug(pattern_tag);
        let page_ids = page_items[pattern_tag];
        // let page_ids = (pattern) ? page_items['PATTERN_' + pattern] : page_items['ALL'];
        // console.debug(page_ids);
        let page_objs = pages[lang];
        // console.debug(page_objs);
        let out_pages = map(page_ids, (pageid) => {
            return page_objs[pageid]    //  Turn pageids into actual page objects
        });
        // console.debug(out_pages);
        if (sort_field) {
            out_pages = orderBy(out_pages, [sort_field], [sort_dir]);
        } else {
            out_pages = sortBy(out_pages, 'page_number');
        }

        

        // console.debug('out_pages');
        // console.debug(out_pages);
        return out_pages;
    }
}

function selectFilteredPages(pages, pages_by_tag, lang, unit, course, lesson, topic, pattern, updated_date, pageid, sort_field, sort_dir, filter_stati) {
    console.debug('selectFilteredPages', lang, unit, course, lesson, topic, pattern, updated_date, pageid, sort_field, sort_dir);
    if (pageid && pages[lang] && pages[lang][pageid]) {
        return [pages[lang][pageid]];
    } else if (topic && topic !== -1) {
        return selectPagesByTag(pages, pages_by_tag, lang, 'T', unit, course, lesson, topic, pattern, updated_date, sort_field, sort_dir, filter_stati);
    } else if (lesson && lesson !== -1) {
        return selectPagesByTag(pages, pages_by_tag, lang, 'L', unit, course, lesson, -1, pattern, updated_date, sort_field, sort_dir, filter_stati);
    } else if (course && course !== -1) {
        return selectPagesByTag(pages, pages_by_tag, lang, 'C', unit, course, -1, -1, pattern, updated_date, sort_field, sort_dir, filter_stati);
    } else if (unit) {
        return selectPagesByTag(pages, pages_by_tag, lang, 'U', unit, -1, -1, -1, pattern, updated_date, sort_field, sort_dir, filter_stati);
    } else {
        return [];
    }

    // if (pageid && all_pages[lang] && all_pages[lang][pageid]) {
    //         // console.debug(all_pages[lang][pageid]);
    //         pages = [all_pages[lang][pageid]];
    //     } else if (topic && topic !== -1 && topic_pages) {
    //         pages = topic_pages;
    //     } else if (lesson && lesson !== -1 && lesson_pages) {
    //         pages = lesson_pages;
    //     } else if (course && course !== -1 && course_pages) {
    //         pages = course_pages;
    //     } else if (unit && unit_pages) {
    //         pages = unit_pages;
    //     } else {
    //         pages = [];
    //     }
}

function selectPagesByTag(pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, pattern, updated_date, sort_field, sort_dir, filter_stati) {
    console.log('selectPagesByTag', lang, tag_type, unit, course, lesson, topic, pattern, updated_date);

    //  Only return pages if 'ALL' (-1) is selected
    // console.debug(tag_type, course);
    if (tag_type === 'U' && course !== -1) return [];
    // console.debug(tag_type, lesson);
    if (tag_type === 'C' && lesson !== -1) return [];
    // console.debug(tag_type, topic);
    if (tag_type === 'L' && topic !== -1) return [];

    let tag = tag_type;
    if (tag_type === 'U') tag += unit;
    if (tag_type === 'C') tag += course;
    if (tag_type === 'L') tag += lesson;
    if (tag_type === 'T') tag += topic;
    // console.debug('tag', tag);
    tag = `${tag}_${filter_stati}`

    return getTagPages(pages, pages_by_tag, lang, tag, pattern, updated_date, sort_field, sort_dir);
}

function selectPagesIsFetching(pages_by_tag, lang, unit, course, lesson, topic, filter_stati) {
    // console.debug('selectPagesIsFetching', pages_by_tag, lang, unit, course, lesson, topic);
    if (!pages_by_tag[lang]) return false;

    const lang_pages = pages_by_tag[lang];

    let tag;
    if (topic && topic != -1)
        tag = 'T' + topic;
    else if (lesson && topic == -1)
        tag = 'L' + lesson;
    else if (course && lesson == -1)
        tag = 'C' + course;
    else if (unit && course == -1)
        tag = 'U' + unit;

    tag = `${tag}_${filter_stati}`;

    if (!lang_pages[tag]) return false;

    // console.debug(tag);
    // console.debug(lang_pages[tag]);

    return lang_pages[tag].isFetching;
}

function selectEverythingPage(page_everythings, edit_id) {
    console.debug('selectEverythingPage', page_everythings, edit_id);
    if (!page_everythings || !page_everythings[edit_id]) return null;

    let out = cloneDeep(page_everythings[edit_id]);

    //  Create an answers array
    let answers = [];
    console.debug('MCSelectedAnswer', out.MCSelectedAnswer);
    if (out.MCSelectedAnswer) {
        let charcode = 65;
        console.debug(charcode, `MCDistractor${String.fromCharCode(charcode)}`, out[`MCDistractor${String.fromCharCode(charcode)}`])
        while (out[`MCDistractor${String.fromCharCode(charcode)}`]) {
            console.debug(charcode, `MCDistractor${String.fromCharCode(charcode)}`, out[`MCDistractor${String.fromCharCode(charcode)}`])
            let answer = {
                distractor: out[`MCDistractor${String.fromCharCode(charcode)}`],
                feedback: out[`MCFeedback${String.fromCharCode(charcode)}`],
                isCorrect: (parseInt(out.MCSelectedAnswer, 10)-1) === answers.length
            };

            answers.push(answer);
            charcode++;
        }
    }
    out.Answers = answers

    //  Coallesce questions
    out.Question = out.MCQuestion || out.FIBQuestion;

    //  Turn boolean strings into actual booleans
    forOwn(out, (val, key) => {
        if (val === 'True') out[key] = true;
        if (val === 'False') out[key] = false;
    });

    if (out.UseImage) {
        out.image_path = `http://es.run.keytrain.com/objects/${out.course_tag}/${out.lesson_tag}/en/images/image_${out.pageid}.jpg`;
    }

    return out;
}

function selectDetailPage(pages, page_details, lang, edit_id) {
    // console.debug('selectDetailPage', pages, edit_id);

    if (!pages[lang]) return null;
    if (!pages[lang][edit_id]) return null;

    let out = cloneDeep(pages[lang][edit_id]);
    if (page_details[lang] && page_details[lang][edit_id]) {
        out = Object.assign({}, out, page_details[lang][edit_id]);
    }

    return out;
}

function chunksToChildren(in_chunks, parent='') {
    // console.debug('chunksToChildren', in_chunks, parent);
    /*
        An array of chunks with these fieldNames

        [
            'custom>>>incorrectFeedback',
            'custom>>>question',
            'custom>>>sources>>>0',
            'custom>>>sources>>>1',
        ]

        will become an array of chunks with these chunkvars and children

        [
            'custom': [
                'incorrectFeedback',
                'question',
                'sources': [
                    '0',
                    '1'
                ]
            ]
        ]
    */
    // let chunks = Object.assign({}, in_chunks);
    let chunks = cloneDeep(in_chunks);
    // console.debug('cloned chunks', chunks);
    let out_chunks = [];
    while (chunks.length > 0) {
        // console.debug('chunks left', chunks.length);
        let chunk = chunks.shift();
        // console.debug(chunk);
        if (!isUndefined(chunk.chunkvar) && chunk.chunkvar.indexOf('>>>') !== -1) {
            //  Multi-level chunk, probably custom
            let chunkvar = chunk.display || chunk.chunkvar;
            // console.debug('^' + parent.replace('[', '\\[').replace(']', '\\]') + '>>>');
            chunkvar = chunkvar.replace(new RegExp('^' + parent.replace(/\[/g, '\\[').replace(/\]/g, '\\]') + '>>>'), '');
            // console.debug('chunkvar', chunkvar);
            let pieces = chunkvar.split('>>>');
            let top = pieces.shift();
            let parent_top = (parent) ? `${parent}>>>${top}` : top; 
            let out_chunk = {
                chunkvar: chunk.chunkvar
            };


            //  Do we have a parent chunk yet, if not need to create dummy and make others children of it
            // console.debug(`${top} != ${parent}: ${parent_top}`);
            if (top != parent) {
                //  Now find all of the children of this parent and recurse
                let children = [];
                // console.debug(chunk);




                // let display = chunks ? chunk.display || chunk.chunkvar : null;
                let display = chunks.length > 0 ? chunks[0].display || chunks[0].chunkvar : null;
                // console.debug(display, parent_top);
                while (!isUndefined(display) && chunks.length > 0 && display.indexOf(parent_top + '>>>') === 0) {
                    // console.debug('add child', parent_top);
                    children.push(chunks.shift());
                    // chunk = chunks.shift();
                    display = chunks.length > 0 ? chunks[0].display || chunks[0].chunkvar : null;

                    // if (display !== null) {
                    //     display = display.replace(/\[.+\]/g, '');
                    // }
                    // console.debug(display, parent_top);
                    // console.debug('children', map(children, 'chunkvar'));
                    // console.debug('chunks', map(chunks, 'chunkvar'));
                    // console.debug(chunk);
                }




                // console.debug('last chunk', chunk);
                // console.debug(children.length);
                // console.debug(map(children, 'chunkvar'));
                if (children.length > 0) {
                    // console.debug(`${parent}>>>${top}`)
                    // console.debug('add chunk', chunk);
                    children.unshift(chunk); // Add the first child to the array
                    out_chunk.chunkval = chunksToChildren(children, parent_top);
                } else {
                    out_chunk = chunk; //  No children, just use the chunk
                }
                out_chunk.display = top.replace(/\[.+\]/g, '');
                // out_chunk.chunkvar = chunk.chunkvar

            }
            out_chunks.push(out_chunk);

            // chunk.displayvar = pieces.shift();
            // let children = [],
            //     childchunk, 
            //     childvar;
            // do {
            //     childchunk = chunks.shift();
            // } while (childchunk.chunkvar.indexOf(childvar + '>>>') === 0); //   Starts with the same parent
        } else {
            out_chunks.push(chunk);
        }
    }
    // console.debug(out_chunks);
    return out_chunks;
}

function setChunkMessage(chunks, chunkvar, message) {
    // console.debug('setChunkMessage', chunks, chunkvar, message);
    return map(chunks, (chunk, chunk_ind) => {
        if (isArray(chunk.chunkval)) {
            chunk.chunkval = setChunkMessage(chunk.chunkval, chunkvar, message)
        } else if (chunk.chunkvar === chunkvar) {
            console.debug('SET CHUNK MESSAGE', chunk, message);
            chunk = Object.assign({}, chunk, {
                message
            })
            console.debug(chunk);
        } /*else {
            chunk = Object.assign({}, chunk, {
                message: {}
            })
        }*/
        return chunk;
    })
}

//  Separated out the chunk_messages so all chunks don't have to be reselected on each message change
function selectDetailPages(page_details, chunk_messages, state) {
    console.debug('selectDetailPages', page_details, chunk_messages, state);
    // assign(page_details, chunk_messages);

    let out = cloneDeep(page_details);
    forEach(chunk_messages, (lang_obj, lang_id) => {
        forEach(lang_obj, (page_obj, page_id) => {
            forEach(page_obj.chunks, (chunk, message_chunk_ind) => {
                let page_detail_ind = findIndex(out, {'lang_id': parseInt(lang_id), 'PageID': parseInt(page_id)});
                console.debug(out, lang_id, page_id);
                console.debug(page_detail_ind);

                //  Now that we've got the page, find the chunk
                //  Could have multiple chunks with the same chunkvar
                //  Have to search deep and find the one with chunkval that isn't array

                if (out[page_detail_ind] && out[page_detail_ind].chunks) {
                    // let chunk_ind = findIndex(page_details[page_detail_ind].chunks, {'chunkvar', chunk.chunkvar});
                    // if (chunk_ind !== -1) {
                    //     page_details[page_detail_ind].chunks[chunk_ind]
                    // }
                    // console.debug(chunk);
                    // page_details[page_detail_ind].chunks = setChunkMessage(page_details[page_detail_ind].chunks, chunk.chunkvar, chunk.message);
                    out[page_detail_ind].chunks = Object.assign({}, out[page_detail_ind].chunks, setChunkMessage(page_details[page_detail_ind].chunks, chunk.chunkvar, chunk.message));
                    console.debug(out[page_detail_ind].chunks);
                } else {
                    console.debug('no match', page_detail_ind);
                }

                // if (page_details[lang_id] && page_details[lang_id][page_id] && page_details[lang_id][page_id].chunks) {
                //     let chunk_ind = findIndex(page_details[lang_id][page_id].chunks, {'chunkvar': chunk.chunkvar});
                //     if (chunk_ind !== -1) {
                //         page_details[lang_id][page_id].chunks[chunk_ind].message = chunk.message;
                //     }
                // }
            })
        })
    })

    console.debug(out);
    return out;
}

function selectDetailPagesChunks(pages, page_details, edit_id) {
    console.debug('selectDetailPagesChunks', pages, page_details, edit_id);
    console.debug(LangOptions);
    return map(LangOptions, (v, k) => {
        console.debug(v, k);
        console.debug(pages[v]);
        if (pages[v]) console.debug(pages[v][edit_id]);
        console.debug(page_details[v]);
        if (page_details[v]) console.debug(page_details[v][edit_id])
        if (!(pages[v] && pages[v][edit_id]) && !(page_details[v] && page_details[v][edit_id])) return null;

        let this_page = pages[v] ? pages[v][edit_id] : {};
        
        let p = Object.assign({}, this_page, {
            lang_id: v,
            // chunks: (this_page.chunks && this_page.chunks.length > 0) ? chunksToChildren(this_page.chunks) : this_page.chunks
        });

        console.debug(page_details);
        if (page_details[v] && page_details[v][edit_id]) {
            console.debug(page_details[v][edit_id]);
            p = Object.assign({}, p, page_details[v][edit_id])
        }
        console.debug(p);


        // console.debug(p.chunks);
        if (p.chunks) {
            /*  
                We're going to assume that the English chunks are the canonical. So, if we have a non-english 
                page with chunks that don't match we will fill it out with empty chunks
            */
            console.debug('length', p.chunks.length);
            if (v !== LangOptions.EN) {
                //  Non-english page
                let en_page = (page_details && page_details[LangOptions.EN] && page_details[LangOptions.EN][edit_id]) ? page_details[LangOptions.EN][edit_id] : [];
                if (en_page && p.chunks && en_page.chunks && p.chunks.length != en_page.chunks.length) {
                    console.debug(p.chunks.length, en_page.chunks.length);
                    console.debug(p.chunks, en_page.chunks);
                    //  Chunk mismatch
                    //  Loop through english chunks, check for match in non-english, insert if needed
                    forEach(en_page.chunks, (chunk, ind) => {
                        if (!find(p.chunks, (pchunk) => {
                                return (pchunk.chunkvar == chunk.chunkvar);
                            })) {
                            // console.debug('mismatch', chunk);
                            p.chunks = [
                                ...p.chunks.slice(0, ind),
                                Object.assign({}, chunk, {
                                    chunkval: '',
                                    lang_id: v,
                                    chunk_count_all: 0
                                }),
                                ...p.chunks.slice(ind+1)
                            ];
                        }
                    })
                    console.debug(p.chunks);
                }
            }


            // if (p.chunks.length === 0) {
            //     //  No chunks in this return
            //     console.debug('lang', v);
            //     if (v !== LangOptions.EN) {
            //         //  Non-english page
            //         console.debug('en chunks', page_details[LangOptions.EN][edit_id].chunks);
            //         console.debug(page_details[LangOptions.EN]);
            //         console.debug(page_details[LangOptions.EN][edit_id]);
            //         if (page_details[LangOptions.EN] && page_details[LangOptions.EN][edit_id] && page_details[LangOptions.EN][edit_id].chunks) {
            //             //  English page exists with chunks
            //             p.chunks = map(page_details[LangOptions.EN][edit_id].chunks, (chunk) => {
            //                 return Object.assign({}, chunk, {
            //                     chunkval: '',
            //                     lang_id: v
            //                 })
            //             });
            //             console.debug(p.chunks);
            //         }
            //     }
            // }

            // console.error('HERE HERE HERE');
            // console.debug(p.chunks);
            p.chunks = chunksToChildren(p.chunks);
        }


        // chunksToChildren(this_page.chunks);
        // p.lang_id = v;

        // console.debug(p);
        // Fix up the chunks
        // if (p.chunks && p.chunks.length > 0) {
        //     p.chunks = chunksToChildren(p.chunks);
        //     console.debug(p);
        // }

    //     let out = cloneDeep(pages[lang][edit_id]);
    // if (page_details[lang] && page_details[lang][edit_id]) {
    //     out = Object.assign({}, out, page_details);
    // }

        return p;
    });
}

function selectSelectedPageID(pages_by_tag, tag, pattern, updated_date, selected_row) {
    console.debug('selectSelectedPageID', pages_by_tag, tag, pattern, selected_row);
    if (!(pages_by_tag && tag && selected_row != -1)) return null;

    let lang_pages = pages_by_tag[1];
    // console.debug(lang_pages);
    if (!lang_pages) return null;

    let tag_pages = lang_pages[tag];
    // console.debug(tag_pages);
    if (!tag_pages) return null;

    // let pattern_tag = (pattern) ? 'PATTERN_' + pattern : 'ALL';
    let pattern_tag = 'ALL';
    if (pattern)
        pattern_tag = `PATTERN_${pattern}`;
    else if (updated_date)
        pattern_tag = `UPDATED_DATE_${updated_date}`;
    let pattern_pages = tag_pages.items[pattern_tag];
    if (!pattern_pages) return null;

    console.debug(pattern_pages[selected_row]);
    return pattern_pages[selected_row];
}

// function selectLessonPages(pages, pages_by_tag, lang, lesson, topic, pattern) {
//     console.log('selectLessonPages', lang, lesson, topic, pattern);
//     //  If they haven't selected "ALL" as the topic then they don't get lesson pages
//     if (topic !== -1) return [];

//     let tag = 'L' + lesson;
//     let tag_pages = getTagPages(pages, pages_by_tag, lang, tag, pattern);
//     // console.debug(tag_pages);

//     if (!tag_pages) return [];

//     // if (mode == ModeFilters.CORRECT) {
//     //     // console.debug('correct mode');
//     //     //  Pages that have a problem and status isn't perfect
//     //     tag_pages = filter(tag_pages, (page) => {
//     //         // console.debug(page);
//     //         return page.status_val > 1 && page.problem_bits > 0;
//     //     });
//     // } else if (mode == ModeFilters.CHECK) {
//     //     // console.debug('check mode');
//     //     //  Pages with a fixed_by and status isn't perfect
//     //     tag_pages = filter(tag_pages, (page) => {
//     //         // console.debug(page.fixed_by);
//     //         return page.status_val > 1 && page.fixed_by != null;
//     //     });
//     // }

//     // console.debug(pages);
//     return tag_pages;
// }

// function selectTopicPages(pages, pages_by_tag, lang, topic, pattern) {
//     // console.debug('selectTopicPages', lang, topic, pattern);

//     //  If it's the "ALL" topic then we don't actually have topic pages, we have lesson pages
//     if (topic == -1) return [];

//     //  If we haven't picked a topic or a lang then we don't have pages
//     let tag = 'T' + topic;
//     let tag_pages = getTagPages(pages, pages_by_tag, lang, tag, pattern);

//     if (!tag_pages) return [];

//         // let pages = chain(topic_pages[lang][topic].items)
//         //     .values()
//         //     .sortBy('page_number')
//         //     .value();
//     // console.debug(tag_pages);

//     // console.debug('mode', mode);

//     // if (mode == ModeFilters.REVIEW) {
//     //     //  In the future perhaps this will be pages with no status
//     //     // console.debug('review mode');
//     // } else if (mode == ModeFilters.CORRECT) {
//     //     // console.debug('correct mode');
//     //     //  Pages that have a problem and status isn't perfect
//     //     tag_pages = filter(tag_pages, (page) => {
//     //         return page.status_val > 1 && page.problem_bits > 0;
//     //     });
//     // } else if (mode == ModeFilters.CHECK) {
//     //     // console.debug('check mode');
//     //     //  Pages with a fixed_by and status isn't perfect
//     //     tag_pages = filter(tag_pages, (page) => {
//     //         // console.debug(page.fixed_by);
//     //         return page.status_val > 1 && page.fixed_by != null;
//     //     });
//     // }

//     // console.debug(pages);
//     return tag_pages;

//     // return (topic_pages[topic]) ? topic_pages[topic].items : {};
// }

export function selectCourseTag(course, courses) {
    if (!course || !courses || course == -1) return '';
    // console.debug('selectCourseTag', course, courses);
    return chain(courses).find({'id': course}).value().tag;
}

export function selectLessonTag(lesson, lessons) {
    if (!lesson || !lessons || lesson == -1) return '';

    return chain(lessons).find({'id': lesson}).value().tag;
}

function selectSession(sessions, lang) {
    // console.debug('selectSession', lang);
    // console.debug(sessions);
    if (!sessions[lang]) return {};

    return sessions[lang] || {};
}

function selectDisplayFieldsOn(displayFields) {
    return filter(displayFields, {'isOn': true});
}

const contentSelector = state => state.editor_content.content;
const unitSelector = state => state.filters.unit;
const courseSelector = state => state.filters.course;
const lessonSelector = state => state.filters.lesson;
const topicSelector = state => state.filters.topic;
const pageSelector = state => state.filters.page; //    This is the filter text box
const pageIDSelector = state => state.filters.pageid; //    This is the actual filter value
const patternSelector = state => state.filters.pattern; //  This is the filter text box
const patternFilterSelector = state => state.filters.pattern_filter; //  This is the actual filter value
const updatedDateSelector = state => state.filters.updated_date;
const updatedDateFilterSelector = state => state.filters.updated_date_filter;
const selectedRowSelector = state => state.filters.selectedRow;
const selectedAttachmentSelector = state => state.filters.selectedAttachment;
const selectedRowTopSelector = state => state.filters.rowTop;
// const widthsSelector = state => state.filters.widths;
const editModeSelector = state => state.filters.edit_mode;
const editIDSelector = state => state.filters.edit_id;
const sortFieldSelector = state => state.filters.sort_field;
const sortDirSelector = state => state.filters.sort_dir;
const highlightedRowSelector = state => state.filters.highlighted_row;

const sessionsSelector = state => state.filters.sessions;

const displayFieldsSelector = state => state.filters.displayFields;
const messagesSelector = state => state.filters.messages;

const langSelector = state => state.filters.lang;
const pasteboardSelector = state => state.filters.pasteboard;
const pasteboardCutSelector = state => state.filters.pasteboard_cut;
// const modeSelector = state => state.filters.mode;
const filterStatiSelector = state => state.filters.stati;
const filtersSelector = state => state.filters;

const pagesSelector = state => state.pages;
const tagPagesSelector = state => state.pages_by_tag;
const pageDetailsSelector = state => state.page_details;
const pageEverythingsSelector = state => state.page_everythings;
const chunkMessagesSelector = state => state.chunk_messages;

const constantsSelector = state => state.constants;

const stateSelector = state => state;

const contentDataSelector = createSelector(
    contentSelector,
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    (content, unit, course, lesson, topic) => {
        return selectContent(content, unit, course, lesson, topic);
    }
);

const unitTagsSelector = createSelector(
    contentSelector,
    (content) => {
        return selectUnitTags(content);
    }
);

const unitsSelector = createSelector(
    [contentSelector, unitTagsSelector],
    (content, tags) => {
        return selectUniqueUnits(content, tags);
    }
);

const courseTagsSelector = createSelector(
    [contentDataSelector, unitSelector],
    (content, unit) => {
        return selectCourseTags(content.courses, unit);
    }
);

const coursesSelector = createSelector(
    [contentDataSelector, courseTagsSelector],
    (content, tags) => {
        return selectUniqueCourses(content.courses, tags);
    }
);

/*  Selector to get the course tag of the currently selected course_id */
const courseTagSelector = createSelector(
    [courseSelector, coursesSelector],
    (course, courses) => {
        return selectCourseTag(course, courses);
    }
);

const lessonTagsSelector = createSelector(
    [contentDataSelector, courseSelector],
    (content, course) => {
        return selectLessonTags(content.lessons, course);
    }
);

const lessonsSelector = createSelector(
    [contentDataSelector, lessonTagsSelector],
    (content, tags) => {
        return selectUniqueLessons(content.lessons, tags);
    }
);

const lessonTagSelector = createSelector(
    [lessonSelector, lessonsSelector],
    (lesson, lessons) => {
        return selectLessonTag(lesson, lessons);
    }
);

const topicTagsSelector = createSelector(
    [contentDataSelector, lessonSelector],
    (content, lesson) => {
        return selectTopicTags(content.topics, lesson);
    }
);

const topicsSelector = createSelector(
    [contentDataSelector, topicTagsSelector],
    (content, tags) => {
        return selectUniqueTopics(content.topics, tags);
    }
);

const displayFieldsOnSelector = createSelector(
    displayFieldsSelector,
    (displayFields) => {
        return selectDisplayFieldsOn(displayFields);
    }
);

// const filteredPagesSelector = createSelector(
//     pagesSelector,
//     topicSelector,
//     pageIDSelector,
//     patternSelector,
//     (pages, topicid, pageid, pattern) => {
//         return selectPages(pages, topicid, pageid, pattern);
//     }
// );

// selectPagesByTag(pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, pattern)

const contentTagSelector = createSelector(
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    pageIDSelector,
    filterStatiSelector,
    (unit, course, lesson, topic, pageid, filter_stati) => {
        return selectContentTag(unit, course, lesson, topic, pageid, filter_stati);
    }
);

const filteredPagesSelector = createSelector(
    pagesSelector,
    tagPagesSelector,
    langSelector,
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    patternFilterSelector,
    updatedDateFilterSelector,
    pageIDSelector,
    sortFieldSelector,
    sortDirSelector,
    filterStatiSelector,
    (pages, pages_by_tag, lang, unit, course, lesson, topic, pattern, updated_date, page_id, sort_field, sort_dir, filter_stati) => {
        return selectFilteredPages(pages, pages_by_tag, lang, unit, course, lesson, topic, pattern, updated_date, page_id, sort_field, sort_dir, filter_stati);
    }
)

const unitPagesSelector = createSelector(
    pagesSelector,
    tagPagesSelector,
    langSelector,
    () => 'U',
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    patternFilterSelector,
    updatedDateFilterSelector,
    sortFieldSelector,
    sortDirSelector,
    filterStatiSelector,
    (pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, pattern, udpated_date, sort_field, sort_dir, filter_stati) => {
        return selectPagesByTag(pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, pattern, udpated_date, sort_field, sort_dir, filter_stati)
    }
);

const coursePagesSelector = createSelector(
    pagesSelector,
    tagPagesSelector,
    langSelector,
    () => 'C',
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    patternFilterSelector,
    updatedDateFilterSelector,
    sortFieldSelector,
    sortDirSelector,
    filterStatiSelector,
    (pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, pattern, updated_date, sort_field, sort_dir, filter_stati) => {
        return selectPagesByTag(pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, pattern, updated_date, sort_field, sort_dir, filter_stati)
    }
);

const lessonPagesSelector = createSelector(
    pagesSelector,
    tagPagesSelector,
    langSelector,
    () => 'L',
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    patternFilterSelector,
    updatedDateFilterSelector,
    sortFieldSelector,
    sortDirSelector,
    filterStatiSelector,
    (pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, pattern, updated_date, sort_field, sort_dir, filter_stati) => {
        return selectPagesByTag(pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, pattern, updated_date, sort_field, sort_dir, filter_stati)
    }
);

const topicPagesSelector = createSelector(
    pagesSelector,
    tagPagesSelector,
    langSelector,
    () => 'T',
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    patternFilterSelector,
    updatedDateFilterSelector,
    sortFieldSelector,
    sortDirSelector,
    filterStatiSelector,
    (pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, pattern, updated_date, sort_field, sort_dir, filter_stati) => {
        return selectPagesByTag(pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, pattern, updated_date, sort_field, sort_dir, filter_stati)
    }
);

const pagesIsFetchingSelector = createSelector(
    tagPagesSelector,
    langSelector,
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    filterStatiSelector,
    (pages_by_tag, lang, unit, course, lesson, topic, filter_stati) => {
        return selectPagesIsFetching(pages_by_tag, lang, unit, course, lesson, topic, filter_stati);
    }
);

const selectedPageIDSelector = createSelector(
    tagPagesSelector,
    contentTagSelector,
    patternSelector,
    updatedDateFilterSelector,
    selectedRowSelector,
    (pages_by_tag, tag, pattern, updated_date, selected_row) => {
        return selectSelectedPageID(pages_by_tag, tag, pattern, updated_date, selected_row);
    }
);

const detailPageSelector = createSelector(
    pagesSelector,
    pageDetailsSelector,
    langSelector,
    selectedPageIDSelector,
    (pages, page_details, lang, edit_id) => {
        return selectDetailPage(pages, page_details, lang, edit_id);
    }
)

const detailPagesChunksSelector = createSelector(
    pagesSelector,
    pageDetailsSelector,
    selectedPageIDSelector,
    (pages, page_details, edit_id) => {
        return selectDetailPagesChunks(pages, page_details, edit_id);
    }
)

const detailPagesSelector = createSelector(
    detailPagesChunksSelector,
    chunkMessagesSelector,
    stateSelector,
    (page_details, chunk_messages, state) => {
        return selectDetailPages(page_details, chunk_messages, state);
    }
)

const everythingPageSelector = createSelector(
    pageEverythingsSelector,
    selectedPageIDSelector,
    (page_everythings, edit_id) => {
        return selectEverythingPage(page_everythings, edit_id);
    }
)

// const topicPagesSelector = createSelector(
//     pagesSelector,
//     tagPagesSelector,
//     langSelector,
//     topicSelector,
//     patternSelector,
//     (pages, tagPages, lang, topic, pattern) => {
//         return selectTopicPages(pages, tagPages, lang, topic, pattern);
//     }
// );



// const lessonPagesSelector = createSelector(
//     pagesSelector,
//     tagPagesSelector,
//     langSelector,
//     lessonSelector,
//     topicSelector,
//     patternSelector,
//     (pages, tagPages, lang, lesson, topic, pattern) => {
//         return selectLessonPages(pages, tagPages, lang, lesson, topic, pattern)
//     })

const enSessionSelector = createSelector(
    sessionsSelector,
    () => LangOptions.EN,
    (sessions, lang) => {
        return selectSession(sessions, lang);
    }
)

const esSessionSelector = createSelector(
    sessionsSelector,
    () => LangOptions.ES,
    (sessions, lang) => {
        return selectSession(sessions, lang);
    }
)

export const filteredContentSelector = createSelector(
    contentDataSelector,
    unitSelector,
    unitsSelector,
    courseSelector,
    coursesSelector,
    courseTagSelector,
    lessonSelector,
    lessonsSelector,
    lessonTagSelector,
    topicSelector,
    topicsSelector,
    langSelector,
    pageSelector,
    pageIDSelector,
    patternSelector,
    patternFilterSelector,
    updatedDateSelector,
    updatedDateFilterSelector,
    displayFieldsSelector,
    displayFieldsOnSelector,
    // filteredPagesSelector,
    pagesSelector,
    filteredPagesSelector,
    // unitPagesSelector,
    // coursePagesSelector,
    // lessonPagesSelector,
    // topicPagesSelector,
    pagesIsFetchingSelector,
    constantsSelector,
    selectedRowSelector,
    selectedAttachmentSelector,
    selectedRowTopSelector,
    sessionsSelector,
    enSessionSelector,
    esSessionSelector,
    editModeSelector,
    editIDSelector,
    detailPageSelector,
    detailPagesSelector,
    sortFieldSelector,
    sortDirSelector,
    messagesSelector,
    selectedPageIDSelector,
    highlightedRowSelector,
    pasteboardSelector,
    pasteboardCutSelector,
    filterStatiSelector,
    filtersSelector,
    everythingPageSelector,
    (content, unit, units, course, courses, course_tag, lesson, lessons, lesson_tag, topic, topics, lang, page, pageid, pattern, patternFilter, updated_date, updated_date_filter, displayFields, displayFieldsOn, all_pages, filtered_pages, /*unit_pages, course_pages, lesson_pages, topic_pages,*/ pages_fetching, constants, selectedRow, selectedAttach, rowTop, all_sessions, enSession, esSession, edit_mode, edit_id, detailPage, detailPages, sort_field, sort_dir, messages, selected_page_id, highlighted_row, pasteboard, pasteboard_cut, filter_stati, filters, everything_page) => {
        return {
            content,
            unit,
            units,
            course,
            courses,
            course_tag,
            lesson,
            lessons,
            lesson_tag,
            topic,
            topics,
            lang,
            page,
            pageid,
            pattern,
            patternFilter,
            updated_date,
            updated_date_filter,
            displayFields,
            displayFieldsOn,
            all_pages,
            filtered_pages,
            // unit_pages,
            // course_pages,
            // lesson_pages,
            // topic_pages,
            pages_fetching,
            constants,
            selectedRow,
            selectedAttach,
            rowTop,
            all_sessions,
            enSession,
            esSession,
            edit_mode,
            edit_id, 
            detailPage,
            detailPages,
            sort_field, 
            sort_dir, 
            messages,
            selected_page_id,
            highlighted_row,
            pasteboard,
            pasteboard_cut,
            filter_stati, 
            filters,
            everything_page
        }
    }
);


