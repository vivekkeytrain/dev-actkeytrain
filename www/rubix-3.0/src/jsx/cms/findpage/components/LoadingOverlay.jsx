import React, { Component, PropTypes } from 'react';

import classNames from 'classnames';

export default class LoadingOverlay extends Component {
    constructor(props) {
        super(props);

        this.props = {
            en: false,
            es: false
        }
    }

    render() {
        // let cols = [];
        // if (this.props.en) {
        //     cols.push(  <Col sm={6} smOffset={3}>
        //                     <Well noMargin className="text-center bg-primary">Loading...</Well>
        //                 </Col>);
        // } else {
        //     cols.push()
        // }

        let classes = classNames({
            'overlay': true,
            'left': this.props.en,
            'right': this.props.es
        });

        return (
            <div className={classes}>
                <div className="vertical-alignment-helper">
                    <div className="vertical-align-center">
                        <Grid>
                            <Row>
                                <Col sm={6} smOffset={3}>
                                    <Well noMargin className="text-center bg-primary">Loading...</Well>
                                </Col>
                            </Row>
                        </Grid>
                    </div>
                </div>
            </div>
        )
    }
}

// LoadingOverlay.propTypes = {
//     en: PropTypes.bool.isRequired,
//     es: PropTypes.bool.isRequired
// }