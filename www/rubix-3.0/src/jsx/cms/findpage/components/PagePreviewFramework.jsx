import React, { Component, PropTypes } from 'react';

import PagePreviewQuestionBlock from './PagePreviewQuestionBlock';
import PagePreviewImageBlock from './PagePreviewImageBlock';
import WYSIWYGField from './WYSIWYGField';
import ClickInputField from './ClickInputField';

import { forEach } from 'lodash';

export default class PagePreviewFramework extends Component {

    // closeWYSIWYG(e) {
    //     console.debug('closeWYSIWYG', e.target);
    //     if ($(e.target).parents('.trumbowyg-box').size() == 0) {
    //         forEach(this.refs, (r) => {
    //             console.debug(r);
    //             $(r._wysiwygfield.getDOMNode()).trigger('tbwblur').trumbowyg('destroy');
    //             $(r._wysiwygfield.getDOMNode()).css({'height': auto});
    //         })
    //     }
    // }

    blurAll(e) {
        forEach(this.refs, (r, k) => {
            if (r._textarea) {
                console.debug(r._textarea);
                r.onBlur();
            }
        })

    }

    dangerousHTML(str) {
        return {
            __html: str
        }
    }

    updateFieldVal(field_name, field_val) {
        console.debug('updateFieldVal', field_name, field_val);
        const { everythingPage } = this.props;

        this.props.updateEverythingField(everythingPage.pageid, field_name, field_val);
    }

    render() {
        const { everythingPage } = this.props;

        if (!everythingPage) return <div />

        let full_width = everythingPage.page_type_id == 0 && !everythingPage.UseImage;

        let hint = <div />
        if (everythingPage.Hint) {
            hint = <div className="inline-modal">
                    <label>
                        <i className="icon-fontello-lightbulb"></i>Hint
                    </label>
                    <ClickInputField value={everythingPage.Hint} updateFieldVal={(val) => { console.debug('updateFieldVal') }} ref="questionfield" />
                   </div>
        }

        let solution_steps = <div />
        if (everythingPage.SolutionSteps) {
            solution_steps = <div className="inline-modal">
                                <label>
                                    <i className="icon-fontello-list-numbered"></i>
                                    Solution Steps
                                </label>
                                <p dangerouslySetInnerHTML={this.dangerousHTML(everythingPage.SolutionSteps)}></p>
                            </div>
        }

        /*
        <div className="pagetext" dangerouslySetInnerHTML={this.dangerousHTML(everythingPage.page_text)} />
        <div className="problemtext" dangerouslySetInnerHTML={this.dangerousHTML(everythingPage.problem_text)} />
        */

        return (
            <Row onClick={this.blurAll.bind(this)}>
                <Col xs={full_width ? 12 : 7}>
                    <WYSIWYGField className="pagetext" updateFieldVal={this.updateFieldVal.bind(this, 'page_text')} message={everythingPage.messages.page_text} label="" value={everythingPage.page_text} />
                    
                    {everythingPage.problem_text ? <WYSIWYGField className="problemtext" updateFieldVal={this.updateFieldVal.bind(this, 'problem_text')} message={everythingPage.messages.problem_text} label="" value={everythingPage.problem_text} /> : '' }
                    <div id="extras">
                        <p className="calculator"><i className="rubix-icon icon-fontello-calculator"></i>Calculator {everythingPage.Calculator ? 'On' : 'Off'}</p>
                        {hint}
                        {solution_steps}
                    </div>
                </Col>
                <PagePreviewQuestionBlock everythingPage={everythingPage} updateFieldVal={this.updateFieldVal.bind(this)} />
                <PagePreviewImageBlock everythingPage={everythingPage} updateFieldVal={this.updateFieldVal.bind(this)} />
            </Row>
        )
    }
} 
