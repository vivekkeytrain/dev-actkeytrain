import React, { Component, PropTypes } from 'react';

import { map, isArray, isUndefined } from 'lodash';
import { EditModes } from '../actions';

import classNames from 'classnames';
// import PagePreview from './PagePreview';
// import PageEdit from './PageEdit';

export default class ChunkLabel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            copying: false,
            cut: false
        }
    }

    componentDidUpdate(prevProps, prevState) {
        // console.debug('componentDidUpdate', this.state);
        if (this.state.copying) {
            //  Do the actual copy
            this._copytext.getDOMNode().select();
            console.debug(this._copytext.getDOMNode());
            console.debug(this._copytext.getDOMNode().value);

            try {
                var successful = document.execCommand('copy');
                console.debug('copy successful', successful);
                if (successful) {
                    this.props.setChunkMessage(this.state.cut ? 'Cut' : 'Copied', 'info', this.props.chunk.pageid, this.props.chunk.chunkvar, this.props.chunk.lang_id);

                } else {
                    this.props.setChunkMessage('Failed to copy', 'error', this.props.chunk.pageid, this.props.chunk.chunkvar, this.props.chunk.lang_id);
                }
            } catch (err) {
                this.props.setChunkMessage('Failed to copy', 'error', this.props.chunk.pageid, this.props.chunk.chunkvar, this.props.chunk.lang_id);
            }
            setTimeout(() => {
                this.props.removeChunkMessage(this.props.chunk.pageid, this.props.chunk.chunkvar, this.props.chunk.lang_id);
            }, 1500);

            this.setState({
                copying: false,
                cut: false
            })
        } /*else if (this.state.cut) {
            console.debug('cutting');
            console.debug(this.props.pasteboard, this.props.chunk);

            if (!this.props.pasteboard || this.props.pasteboard.row_id !== this.props.chunk.row_id) {
                //  Some other chunk is getting cut
                this.setState({
                    cut: false
                });
                console.debug('no longer cut');
                // this.removeHighlight(this.props.chunk);
            } else if (!this.props.chunk.message || this.props.chunk.message.type !== 'cut') {
                console.debug(this.props.chunk.message);
                //  Gotta look to see if it's already set or we end up with infinite loop
                this.highlightChunk(this.props.chunk, 'cut');
            }
        }*/
    }

    padDatePart(p) {
        return (p<10) ? `0${p}` : p;
    }

    formatDate(date_str) {
        let d = new Date(date_str);
        let mon = this.padDatePart(d.getMonth()+1);
        let day = this.padDatePart(d.getDay());
        let hour = this.padDatePart(d.getHours());
        let min = this.padDatePart(d.getMinutes());

        return `${d.getFullYear()}-${mon}-${day} ${hour}:${min}`
    }

    highlightChunk(chunk, type) {
        this.props.setChunkMessage('', type, chunk.pageid, chunk.chunkvar, chunk.lang_id);
    }

    removeHighlight(chunk) {
        console.debug(chunk);
        if (chunk.message && !chunk.message.message) // Don't remove it if it has the 'Copied' message, settimeout will remove
            this.props.removeChunkMessage(chunk.pageid, chunk.chunkvar, chunk.lang_id);
            //  Don't want to lose the 'cut' message
            if (this.state.cut)
                this.highlightChunk(chunk, 'cut');
    }

    removeCutHighlight(chunk) {
        if (chunk.message.type != 'cut') // Don't remove the cut message until the pasteboard is cleared
            this.props.removeChunkMessage(chunk.pageid, chunk.chunkvar, chunk.lang_id);
    }

    dangerousHTML(str) {
        // console.debug(str);
        if (!str) str = '&nbsp;';
        return {
            __html: str
        }
    }

    copyChunk(chunk) {
        //  The actual copy takes place in componentDidUpdate.
        //  We do this because we have to insert a hidden textarea in order to select
        //  the text for copying. This has to happen after DOM render, so in componentDidUpdate.
        //  Setting the copying state to true forces the textarea to render.
        this.setState({
            copying: true
        });

        this.props.setPasteboard(false);
    }

    pasteChunk(chunk) {
        console.debug('PASTE CHUNK', this.props.pasteboard);
        if (!this.props.pasteboard) return;

        // this.props.updateFieldVal(this.props.pasteboard.chunkval);
        if (this.props.pasteboardCut) {
            this.props.doPageChunkAction(this.props.pasteboard, this.props.chunk, 'cut_paste');
            this.setState({
                cut: false
            });
            this.props.setPasteboard(null, false);
        }
        else
            this.props.doPageChunkAction(this.props.pasteboard, this.props.chunk, 'copy_paste');
    }

    cutChunk(chunk) {
        console.debug('cutChunk');
        this.props.setPasteboard(true);
        console.debug(this.props.pasteboard);
        this.setState({
            cut: true
        });
        this.highlightChunk(this.props.chunk, 'cut');
    }

    emptyChunk(chunk) {
        // this.props.updateFieldVal('');
        this.props.doPageChunkAction(this.props.chunk, null, 'set_blank');
    }

    luckyChunk(chunk) {
        this.copyChunk(chunk);
        this.props.doPageChunkAction(this.props.chunk, null, 'lucky');
    }
    
    render() {
        // console.debug(this.props);
        // console.debug('message: ' + this.props.chunk.message);

        let style = {
            paddingLeft: this.props.indent + 'px',
            // borderLeft: (this.props.indent) ? '1px solid black': 'none'
        }
//style={{display: 'none', visible: false, width: 0, height: 0}}
        let textarea = this.state.copying ? <textarea  value={this.props.chunk.chunkval} readOnly={true} ref={(r) => this._copytext = r}/> : <span />;

        let classes = classNames({
            'message': (!isUndefined(this.props.chunk.message) && !isUndefined(this.props.chunk.message.message)),
            'message-success': (this.props.chunk.message && this.props.chunk.message.type == 'success'),
            'message-danger': (this.props.chunk.message && this.props.chunk.message.type == 'error'),
            'message-warning': (this.props.chunk.message && this.props.chunk.message.type == 'warning'),
            'message-info': (this.props.chunk.message && this.props.chunk.message.type == 'info'),
            'message-cut': (this.props.chunk.message && this.props.chunk.message.type == 'cut'),
            'message-readonly': (this.props.chunk.message && this.props.chunk.message.type == 'readonly')
        });

        let message;
        if (!isUndefined(this.props.chunk.message) && !isUndefined(this.props.chunk.message.message) && this.props.chunk.message.message)
            message = <span className={classes} style={{margin: 0, padding: '2px 10px', fontWeight: 'bold'}}>{this.props.chunk.message.message}</span>
        else
            message = <label>{this.props.chunk.chunk_count_all>1 ? `[ ${this.props.chunk.chunk_count_all} occurrences ]` : ""}</label>

        let pasteClasses = classNames({
            'glyphicon': true,
            'glyphicon-paste': true,
            'disabled': !this.props.pasteboard
        });

        let tools = [];
        if (!isArray(this.props.chunk.chunkval)) {
            tools.push(<i className="glyphicon glyphicon-erase" data-toggle="tooltip" data-placement="top" title="Empty" style={{paddingRight:"15px"}} onMouseOver={this.highlightChunk.bind(this, this.props.chunk, 'error')} onMouseOut={this.removeHighlight.bind(this, this.props.chunk)} onClick={this.emptyChunk.bind(this, this.props.chunk)}></i>);

            if (!this.props.readonly) {
                tools.push(<i className="glyphicon glyphicon-scissors" data-toggle="tooltip" data-placement="top" title="Cut" style={{paddingRight:"5px"}} onMouseOver={this.highlightChunk.bind(this, this.props.chunk, 'info')} onMouseOut={this.removeCutHighlight.bind(this, this.props.chunk)} onClick={this.cutChunk.bind(this, this.props.chunk)}></i>);
                tools.push(<i className="glyphicon glyphicon-copy" onMouseOver={this.highlightChunk.bind(this, this.props.chunk, 'info')} onMouseOut={this.removeHighlight.bind(this, this.props.chunk)} onClick={this.copyChunk.bind(this, this.props.chunk)} data-toggle="tooltip" data-placement="top" title="Copy" style={{paddingRight:"10px"}}></i>);
                tools.push(<i className={pasteClasses} data-toggle="tooltip" data-placement="top" title="Paste" style={{paddingRight:"15px"}} onMouseOver={this.highlightChunk.bind(this, this.props.chunk, 'info')} onMouseOut={this.removeHighlight.bind(this, this.props.chunk)} onClick={this.pasteChunk.bind(this, this.props.chunk)}></i>);
                tools.push(<i className="glyphicon glyphicon-flash" data-toggle="tooltip" data-placement="top" title="Feeling Lucky" style={{paddingRight:"15px"}} onMouseOver={this.highlightChunk.bind(this, this.props.chunk, 'info')} onMouseOut={this.removeHighlight.bind(this, this.props.chunk)} onClick={this.luckyChunk.bind(this, this.props.chunk)}></i>);
            }
        }


        return (
            <div>
            <Col xs={4} style={style}>
                <label data-toggle="tooltip" 
                    data-placement="top" 
                    title={`${this.props.chunk.update_by}: ${this.formatDate(this.props.chunk.update_date)}`}
                >{this.props.chunk.display || this.props.chunk.chunkvar}</label>
                <span className="clipboard">
                    {tools}
                </span>
                {textarea}
            </Col>
            <Col xs={2}>
                {message}
            </Col>
            </div>
        )
    }
} 
