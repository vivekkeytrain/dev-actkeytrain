import React, { Component, PropTypes } from 'react';

import { map, isUndefined } from 'lodash';
import { EditModes } from '../actions';

import classNames from 'classnames';

// import ReactCSSTransitionGroup from 'React.addons.ReactCSSTransitionGroup'


// import PagePreview from './PagePreview';
// import PageEdit from './PageEdit';

export default class ChunkField extends Component {
    constructor(props) {
        super(props);

        this.props = {
            readonly: false
        }

        this.state = {};

        // this.state = {
        //     open: false
        // }
    }

    // componentWillMount() {
    //     console.error('componentWillMount');
    // }

    componentWillUnmount() {
        $(this._chunkfield.getDOMNode()).trumbowyg().off('tbwblur').off('tbwclose');
        $(this._chunkfield.getDOMNode()).trumbowyg('destroy');
    }

    componentDidUpdate(prevProps, nextProps) {
        // console.log('componentDidUpdate', prevProps, nextProps);
        $(this._chunkfield.getDOMNode()).css({height: 'auto'});
    }

    displayWYSIWYG(e) {
        e.stopPropagation();

        // console.debug(this.state);
        // if (this.state.open) return;

        // console.debug('displayWYSIWYG', this.props);
        // console.debug($(this._chunkfield.getDOMNode()));
        // if (!this.props.readonly) {
        if ($(this._chunkfield.getDOMNode()).html() == '&nbsp;') $(this._chunkfield.getDOMNode()).html('');
        $(this._chunkfield.getDOMNode()).trumbowyg({
            autogrow: false,
            semantic: false,
            // closable: true,
            fullscreenable: false,
            removeformatPasted: true,
            btns: [
                'viewHTML',
                '|', ['bold', 'italic', 'underline', 'link'],
                // '|', 'btnGrp-design',
                '|', 'link',
                '|', 'btnGrp-lists',
                '|', 'close'
            ]
        }).trumbowyg()
        .on('tbwclose', () => {
            console.debug('close ' + this.props.chunk.chunkvar);
            // this.setState({
            //     open: false
            // });
            // if ($(this._chunkfield.getDOMNode()).html() == '') $(this._chunkfield.getDOMNode()).html('&nbsp;');
            if ($(this._chunkfield.getDOMNode()).trumbowyg('html') === '') $(this._chunkfield.getDOMNode()).html('&nbsp;');
        })
        .on('tbwblur', () => {
            // this.setState({
            //     open: false
            // });
            console.debug('blur ' + this.props.chunk.chunkvar);
            console.debug(this._chunkfield.getDOMNode())
            console.debug($(this._chunkfield.getDOMNode()).trumbowyg('html'));
            //  Only update if we're not read-only
            if (this.props.readonly) {
                $(this._chunkfield.getDOMNode()).html(this.props.chunk.chunkval);
                this.props.removeChunkMessage(this.props.chunk.pageid, this.props.chunk.chunkvar, this.props.chunk.lang_id);
            } else if ($(this._chunkfield.getDOMNode()).trumbowyg('html') && $(this._chunkfield.getDOMNode()).trumbowyg('html') !== '') { //  Sometimes we get false for this, not sure why
                this.props.updateFieldVal($(this._chunkfield.getDOMNode()).trumbowyg('html'));
            }

            if ($(this._chunkfield.getDOMNode()).trumbowyg('html').trim() === '') {
                console.debug('empty');
                $(this._chunkfield.getDOMNode()).trumbowyg('html', '&nbsp;');
            }

            $(this._chunkfield.getDOMNode()).css({height: 'auto'});
            console.debug(this._chunkfield.getDOMNode())
            // alert('');
            $(this._chunkfield.getDOMNode()).trumbowyg().off('tbwblur').off('tbwclose');

            
            // $(this._chunkfield.getDOMNode()).trumbowyg('destroy');
        })

        if (this.props.readonly) {
            this.props.setChunkMessage('READ ONLY', 'readonly', this.props.chunk.pageid, this.props.chunk.chunkvar, this.props.chunk.lang_id);
        }
            
            // .on('tbwinit', () => {
            //     // console.error('INIT SET STATE');
            //     console.debug(this);
            //     this.setState({
            //         open: true
            //     });
            // })

            // console.error('SET STATE OPEN');
            // this.setState({
            //     open: true
            // })
            // .on('tbwclose', () => {
            //     console.error('close ' + this.props.chunk.chunkvar);
            //     this.props.updateFieldVal($(this._chunkfield.getDOMNode()).trumbowyg('html'));
            //     console.log(this._chunkfield.getDOMNode())
            //     $(this._chunkfield.getDOMNode()).css({height: 'auto'});
            //     $(this._chunkfield.getDOMNode()).trumbowyg().off('tbwblur').off('tbwclose');
            // });
        // }
    }

    dangerousHTML(str) {
        // console.debug(str);
        if (!str) str = '&nbsp;';
        return {
            __html: str
        }
    }

    

    render() {
        // console.debug(this.props.chunk.chunkval);

        let style = {
            paddingLeft: this.props.indent + 'px',
            // borderLeft: (this.props.indent) ? '1px solid black': 'none'
        }



        let classes = classNames({
            'message': (!isUndefined(this.props.chunk.message) && !isUndefined(this.props.chunk.message.message)),
            'message-success': (this.props.chunk.message && this.props.chunk.message.type == 'success'),
            'message-danger': (this.props.chunk.message && this.props.chunk.message.type == 'error'),
            'message-warning': (this.props.chunk.message && this.props.chunk.message.type == 'warning'),
            'message-info': (this.props.chunk.message && this.props.chunk.message.type == 'info'),
            'message-cut': (this.props.chunk.message && this.props.chunk.message.type == 'cut'),
            'message-readonly': (this.props.chunk.message && this.props.chunk.message.type == 'readonly')
        });

        return (
            <Col xs={6} className={classes} style={style} onClick={this.displayWYSIWYG.bind(this)} >
                <div className={this.props.chunk.display || this.props.chunk.chunkvar} dangerouslySetInnerHTML={this.dangerousHTML(this.props.chunk.chunkval)} ref={(r) => this._chunkfield = r}></div>
            </Col>
        )
    }
} 

ChunkField.defaultProps = {
    readonly: false
}