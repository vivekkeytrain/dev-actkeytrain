import React, { Component } from 'react';
import classNames from 'classnames';
import { map, filter, difference, keys } from 'lodash';

import PageDetail from './PageDetail';

export default class PageDetails extends Component {
    componentDidMount() {
        let parent = $(React.findDOMNode(this)).parents('.col-xs-12').first();
        parent.css({
            'height': parent.height() + $(React.findDOMNode(this)).height() + 'px'
        });
    }

    componentWillUnmount() {
        let parent = $(React.findDOMNode(this)).parents('.col-xs-12').first();
        parent.css({
            'height': 'auto'
        });
    }

    render() {
        let classes = classNames({
            'row': true,
            'show': (this.props.selectedRow != null),
            'hidden': (this.props.selectedRow == null)
        });

        let styles = {
            position: 'absolute',
            zIndex: 100,
            border: '1px solid black',
            width: '98%',
            top: this.props.rowTop + 'px',
            left: '3%',
            background: '#CCCCCC'
        }

        // let displayDetailFields = filter(this.props.detailFields, {'display': true});
        // let dbFields = keys(this.props.selectedRow);
        // let definedFields = map(this.props.detailFields, 'name');

        // //  DB fields not defined in displayFields array. Will get dumped raw
        // let rawFields = difference(dbFields, definedFields);

        // let details = map(displayDetailFields, (field) => {
        //     let val = this.props.selectedRow[field.name];
        //     if (typeof field.displayFunc === 'function') {
        //         val = field.displayFunc(val);
        //     }
        //     return <PageDetail name={field.label ? field.label : field.name} val={val} key={field.name} />
        // });

        // rawFields.forEach((rawField) => {
        //     // console.debug(rawField);
        //     details.push(<PageDetail name={rawField} val={this.props.selectedRow[rawField]} key={rawField} />)
        // })

        let details = map(this.props.selectedRow.chunks, (chunk) => {
            let val = chunk.chunkval;
            let name = chunk.chunkvar;

            return <PageDetail name={name} val={val} key={name} />
        });

        console.debug(details);

        return (
            <div className={classes} style={styles}>
                {details}
            </div>
        )
    }
}

