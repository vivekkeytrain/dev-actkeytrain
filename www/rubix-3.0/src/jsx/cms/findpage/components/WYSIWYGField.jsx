import React, { Component, PropTypes } from 'react';

import classNames from 'classnames';
import { isUndefined } from 'lodash';

export default class WYSIWYGField extends Component {

    componentWillUnmount() {
        $(this._wysiwygfield.getDOMNode()).trumbowyg().off('tbwblur').off('tbwclose');
        $(this._wysiwygfield.getDOMNode()).trumbowyg('destroy');
    }

    compoentDidUpdate(prevProps, nextProps) {
        $(this._wysiwygfield.getDOMNode()).css({height: 'auto'});
    }

    displayWYSIWYG(e) {
        console.debug('displayWYSIWYG', e.target);
        e.stopPropagation();

        if ($(e.target).parents('.trumbowyg-box').size() != 0) return; // already displaying WYSIWYG

        if ($(this._wysiwygfield.getDOMNode()).html() == '&nbsp;') $(this._wysiwygfield.getDOMNode()).html('');
        $(this._wysiwygfield.getDOMNode()).trumbowyg({
            autogrow: false,
            semantic: false,
            fullscreenable: false,
            removeformatPasted: true,
            btns: [
                'viewHTML',
                '|', ['bold', 'italic', 'underline', 'link'],
                '|', 'link',
                '|', 'btnGrp-lists',
                '|', 'close'
            ]
        }).trumbowyg()
        .on('tbwclose', () => {
            console.debug('tbwclose');
            if ($(this._wysiwygfield.getDOMNode()).trumbowyg('html') === '')
                $(this._wysiwygfield.getDOMNode()).html('');
        })
        .on('tbwblur', () => {
            console.debug('tbwblur');
            if ($(this._wysiwygfield.getDOMNode()).trumbowyg('html') && $(this._wysiwygfield.getDOMNode()).trumbowyg('html') !== '') {
                console.debug('go updateFieldVal');
                this.props.updateFieldVal($(this._wysiwygfield.getDOMNode()).trumbowyg('html'));
            }

            if ($(this._wysiwygfield.getDOMNode()).trumbowyg('html').trim() === '') {
                $(this._wysiwygfield.getDOMNode()).trumbowyg('html', '&nbsp;');
            }

            $(this._wysiwygfield.getDOMNode()).css({height: 'auto'});
            $(this._wysiwygfield.getDOMNode()).trumbowyg().off('tbwblur').off('tbwclose');
        });
    }

    dangerousHTML(str) {
        // console.debug(str);
        if (!str) str = '&nbsp;';
        return {
            __html: str
        }
    }

    render() {
        let classes_obj = {
            'message': (!isUndefined(this.props.message) && !isUndefined(this.props.message.message)),
            'message-success': (this.props.message && this.props.message.type == 'success'),
            'message-danger': (this.props.message && this.props.message.type == 'error'),
            'message-warning': (this.props.message && this.props.message.type == 'warning'),
            'message-info': (this.props.message && this.props.message.type == 'info'),
            'message-cut': (this.props.message && this.props.message.type == 'cut'),
            'message-readonly': (this.props.message && this.props.message.type == 'readonly')
        };

        let classes = classNames(classes_obj);

        let message;
        if (!isUndefined(this.props.message) && !isUndefined(this.props.message.message) && this.props.message.message)
            message = <span className={classes} style={{margin: 0, padding: '2px 10px', fontWeight: 'bold'}}>{this.props.message.message}</span>

        classes_obj[this.props.className] = true;
        classes_obj.message = false;

        console.debug(classes_obj);

        classes = classNames(classes_obj);

        return (
            <div>
                <Row>
                    <Col xs={8}>
                        <label>{this.props.label}</label>
                    </Col>
                    <Col xs={4}>
                        {message}
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} className={classes} onClick={this.displayWYSIWYG.bind(this)}>
                        <div dangerouslySetInnerHTML={this.dangerousHTML(this.props.value)} ref={(r) => this._wysiwygfield = r} />
                    </Col>
                </Row>
            </div>
        )
    }
} 
