import React, { Component, PropTypes } from 'react';

export default class FilterSelect extends Component {
    // constructor(props) {
    //     console.debug('constructor', props);
    //     super(props);

    //     this.state = {
    //         checked: props.checked === true
    //     }
    // }

    // componentDidMount() {
    //     // console.debug('didMount', this.props.field);
    //     let ref = 'checkbox_' + this.props.field.name;
    //     this.refs[ref].checked = this.props.field.isOn;
    //     console.debug(this.props.field.name, this.props.field.isOn);
    // }

    onUpdateDisplayField(name, e) {
        // console.debug(this);
        this.props.onUpdateDisplayField(name, e.target.checked);
    }

    // componentWillReceiveProps(props) {
    //     // console.debug('componentWillReceiveProps');
    //     console.debug(props.field);
    //     let ref = 'checkbox_' + props.field.name;
    //     this.refs[ref].checked = props.field.isOn;

    // }

    // componentDidUpdate(prevProps, prevState) {
    //     // console.debug(prevProps);
    //     // console.debug(prevState);
    //     let ref = 'checkbox_' + this.props.field.name;
    //     console.debug(ref, this.refs[ref].checked);
    //     this.refs[ref].checked = this.props.field.isOn;
    //     console.debug(this.refs[ref].checked, this.refs[ref].isChecked());
    //     console.debug(this.props.field.name, this.props.field.isOn);
    // }


  handleSelection(itemprops) {
    // access any property attached to MenuItem child component.
    // ex: itemprops.keyaction === 'another-action' if MenuItem
    // with "Another action" is clicked.
    var value = itemprops.children;
    alert(value);
    if(itemprops.keyaction === 'another-action')
      alert('You clicked another-action');
  }


    render() {
        // console.debug(this.props.field);
        // console.debug('render');
        let ref = 'checkbox_' + this.props.field.name;
        return (
            
                <Checkbox name={this.props.field.name} ref={ref} onChange={this.onUpdateDisplayField.bind(this, this.props.field.name)} checked={this.props.field.isOn}>
                    {(this.props.field.label) ? this.props.field.label : this.props.field.name}
                </Checkbox>
            
        );
    }
} 



