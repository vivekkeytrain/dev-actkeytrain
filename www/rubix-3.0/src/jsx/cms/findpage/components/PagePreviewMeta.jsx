import React, { Component, PropTypes } from 'react';

export default class PagePreviewMeta extends Component {

    format_date(date_str) {
        let d = new Date(date_str);

        return `${d.toLocaleDateString()} ${d.toLocaleTimeString()}`;
    }

    render() {
        const { everythingPage } = this.props;

        if (!everythingPage) return <div />

        console.debug(everythingPage.changes);

        return (
            <div className="meta-info">
                <Row>
                    <Col xs={3}><label>Created:</label> {this.format_date(everythingPage.insert_date)}</Col>
                    <Col xs={3}><label>Updated:</label> {this.format_date(everythingPage.update_date)} by {everythingPage.LastEditedBy}</Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <Row><Col xs={12}><label>Change Log</label></Col></Row>
                        <Row>
                            <Col xs={2}><label>Update Date</label></Col>
                            <Col xs={3}><label>Update Var</label></Col>
                            <Col xs={5}><label>Update Desc</label></Col>
                            <Col xs={2}><label>Updated By</label></Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                            {everythingPage.changes.map((row) => {
                                return (
                                <Row>
                                    <Col xs={2}>{row.update_datestr}</Col>
                                    <Col xs={3}>{row.update_var}</Col>
                                    <Col xs={5}>{row.update_ui_desc}</Col>
                                    <Col xs={2}>{row.update_by}</Col>
                                </Row>
                                )
                            })}
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        )
    }
} 
