import React, { Component, PropTypes } from 'react';
import { filter } from 'lodash';

import FilterCheck from './FilterCheck';
import FilterText from './FilterText';
import FilterButton from './FilterButton';

export default class Filters extends Component {
    
    componentDidMount() {
        
        $('input.floatlabel').floatlabel({
          slideInput: false,
          labelStartTop: '5px',
          labelEndTop: '-2px'
        });

        let last_week = new Date();
        last_week.setDate(last_week.getDate() - 7);

        let dtp = this.refs.datetimepicker.getDOMNode();

        $(dtp).datetimepicker({
            // defaultDate: `${last_week.getMonth()+1}/${last_week.getDate()}/${last_week.getFullYear()}`
        });
        $(this.refs.calendaricon.getDOMNode()).removeClass('icon-2x'); // annoyingly being added by datepicker

        $(dtp).on('dp.change', (e) => {
            console.debug('date change', e.date.format());
            this.props.onUpdateTextFilter('updated_date', e.date.format().slice(0,-6)); // trim time zone
            // this.props.onTextFilterFetch();
        });

        $(dtp).on('dp.hide', (e) => {
            console.debug('hide', e.date.format());
            this.props.onTextFilterFetch();
        })

        // $(dtp).on('dp.show', (e) => {
        //     console.debug($(dtp).data('DateTimePicker').getDate());
        //     $(dtp).data('DateTimePicker').setDate(last_week);
        // })
    }

    onUpdateDateChange(date) {
        console.debug('onUpdateDateChange', date);
    }

    render() {
        // console.debug(filter(this.props.displayFields, { 'isOn': true }));
        // console.debug(FilterSelect);

        return (
            <Row>

                <Col sm={12}>

                    <Grid>
                    <Row>
                        <Col sm={1} collapseLeft>

                          <Dropdown>
                            <DropdownButton md bsStyle='blue'>
                              <span><Icon bundle='fontello' glyph='cog-5'/></span> <Caret/>
                            </DropdownButton>
                            <Menu bsStyle='blue' onItemSelect={this.handleSelection} style={{paddingLeft: '10px'}}>

                                {this.props.displayFields.map((field, index) =>
                                    <FilterCheck onUpdateDisplayField={this.props.onUpdateDisplayField} field={field} key={index} />
                                )}

                            </Menu>
                          </Dropdown>

                        </Col>
                        <Col sm={1} collapseLeft>
                        <FilterText name="pageid" label="PageID" val={this.props.page} onUpdateTextFilter={this.props.onUpdateTextFilter} onTextFilterFetch={this.props.onTextFilterFetch} key="pageid" />
                        </Col>
                        <Col sm={3} collapseLeft>
                        <FilterText name="pattern" label="Pattern" val={this.props.pattern} onUpdateTextFilter={this.props.onUpdateTextFilter} onTextFilterFetch={this.props.onTextFilterFetch} key="pattern" />
                        </Col>
                        <Col sm={3} collapseLeft>
                            <InputGroup className='date' ref='datetimepicker'>
                                <input className="form-control floatlabel" type='text' name='updateDate' placeholder='Updated Since' />
                                <InputGroupAddon>
                                    <span className="rubix-icon icon-fontello-calendar fontello icon-fontello-calendar-alt" ref='calendaricon' />
                                </InputGroupAddon>
                            </InputGroup>
                            {/*<div>
                                <div id='datetimepicker1-parent' className='datetimepicker-inline' />
                            </div>*/}
                        </Col>
                        <Col sm={3} collapseLeft>
                            <ButtonGroup md>
                                <FilterButton statiVal={0} filterStati={this.props.filterStati} onButtonClick={this.props.onFilterStatusChange}>All</FilterButton>
                                <FilterButton statiVal={2} filterStati={this.props.filterStati} onButtonClick={this.props.onFilterStatusChange}>Liveable</FilterButton>
                                <FilterButton statiVal={4} filterStati={this.props.filterStati} onButtonClick={this.props.onFilterStatusChange}>No-Go</FilterButton>
                            </ButtonGroup>
                        </Col>
                        <Col sm={1} collapseLeft>
                            <Button bsStyle='danger' md onClick={this.props.onTextFilterFetch} className='pull-right'>Fetch</Button>
                        </Col>
                    </Row>
                    </Grid>

                </Col>



            </Row>
        )
    }
} 