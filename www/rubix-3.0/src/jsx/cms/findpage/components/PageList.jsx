import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import { filter, find } from 'lodash'


import PageDetails from './PageDetails';
import SortHeader from './SortHeader';
import PageRow from './PageRow';
import PageTableHeader from './PageTableHeader';
// import $ from 'jquery';

import { loadWidths, LangOptions, EditModes } from '../actions';

import { Link } from 'react-router';

export default class PageList extends Component {
    // constructor(props) {
    //     // console.debug('constructor');
    //     // console.debug(props);
    //     super(props);

    //     this.state = {
    //         rows: [],
    //         sortField: '',
    //         sortDir: 'asc'
    //     }
    // }


    // componentDidMount() {
    //     document.body.addEventListener('click', () => this.hideDetails());


    // }

    // componentDidUnmount() {
    //     document.body.removeEventListener('click', () => this.hideDetails());
    // }

    componentDidUpdate() {
        this.props.refreshSession();
    }

    // componentWillReceiveProps(props) {
    //     console.debug('componentWillReceiveProps');
    //     console.debug(props);
    //     let rows = Object.assign([], props.pages);

    //     if (this.state && this.state.sortField) {
    //         rows = sortByOrder(rows, [this.state.sortField], [this.state.sortDir]);
    //     }

    //     if (props.pages) {
    //         this.setState({
    //             rows: rows
    //         })
    //     }


    //     // console.debug(this.state);
    // }

    // componentDidUpdate() {
    //     // console.debug('componentDidUpdate');

    //     //  First kill the buttons
    //     $('th[data-sortable-col] > button').contents().unwrap();

    //     //  Then recreate them
    //     $('.tablesaw').sortable.prototype._init.call($('.tablesaw'));

    //     //  For some reason we get double buttons sometimes. Take care of that.
    //     $('th[data-sortable-col] > button > button').contents().unwrap();

    // }

    // hideDetails() {
    //     if (this.props && this.props.selectedRow)
    //         this.props.onRowUnselect();
    // }

    handleRowClick(row_index, row_data, e) {
        // let target = $(e.target).is('tr') ? $(e.target) : $(e.target).parents('tr').first();
        // // console.debug(target);
        // // console.debug(target.offset());
        // // console.debug($('#body').offset());
        // // console.debug(target.height());
        // // console.debug($(target).offset().top - $('#body').offset().top - $(target).height());

        // let dataTable = React.findDOMNode(this.refs.dataTable);
        // this.props.onRowSelect(row_data, $(target).offset().top - $('#body').offset().top);// - $(target).height());
        console.debug(row_data)
        this.props.onRowSelect(row_index, row_data);
    }

    handlePreviewClick(pageid, e) {
        e.preventDefault();
        this.props.setEditID(pageid);
        this.props.setEditMode(EditModes.PREVIEW);
    }

    


    onAttachClick(val, row, e) {
        e.stopPropagation();
        e.preventDefault();
        console.debug('onAttachClick');
        console.debug(arguments);

    }

    onPageLinkClick(pageid, lang_id, e) {
        //  Don't want it to open the PageDetail
        e.stopPropagation();
        e.preventDefault();

        this.props.onPageLinkClick(pageid, lang_id);
    }

    renderQALink(rowData) {
        let pageID = rowData.pageID;
        let query = {
            unit_id: rowData.unit_id,
            course_id: rowData.course_id,
            lesson_id: rowData.lesson_id,
            topic_id: rowData.topic_id,
            page_id: pageID
        }
        return <Link to="/qa/" query={query} >QA</Link>
    }

    renderPageLinks(pageID) {
        let en_url = 'http://dev.run.keytrain.com/objects2/' + this.props.courseTag + '/' + this.props.lessonTag + '/en/html5_course.htm#load/' + this.props.enSession.session_id + '/' + this.props.enSession.login_session_uid + '/pageid/' + pageID;
        let es_url = 'http://dev.run.keytrain.com/objects2/' + this.props.courseTag + '/' + this.props.lessonTag + '/es/html5_course.htm#load/' + this.props.esSession.session_id + '/' + this.props.esSession.login_session_uid + '/pageid/' + pageID;

        let en_key = pageID + "_en";
        let es_key = pageID + "_es";

        return [<a href={en_url} target="_lesson_en" key={en_key} onClick={this.onPageLinkClick.bind(this, pageID, LangOptions.EN)}>EN</a>, ' / ', <a href={es_url} target="_lesson_es" key={es_key} onClick={this.onPageLinkClick.bind(this, pageID, LangOptions.ES)}>ES</a>];
    }

    setHighlightedRow(index) {
        // this.props.setHighlightedRow(index);
    }

    render() {
        console.debug(this.props);
        let row_count = (this.props.pages) ? this.props.pages.length : 0;
        let pageDetails;

        let classes = classNames({
            'progress-bar': true,
            'active': true,
            'progress-bar-striped': true
        })

        // console.debug('selectedRow', this.props.selectedRow);
        // if (this.props.selectedRow) {
        //     pageDetails = <PageDetails {...this.props} key="pageDetails" />
        // }
        // console.debug(pageDetails);

        let disp_table;
        if (this.props.pagesFetching) {
            disp_table = <div className={classes} style={{width: '100%'}}>
                            <span><strong>Fetching Data, Please Hold ...</strong></span>
                         </div>
        } else {
            //return <th data-sortable-col key={'th_' + ind}>{field.label ? field.label : field.name}</th>
            //onClick={this.handleRowClick.bind(this, ind, row)}
            disp_table = <div>
                <div>Row Count: {row_count}</div>
                <table
                    key='dataTable'
                    ref='dataTable'
                    className='tablesaw tablesaw-sortable tablesaw-stack table-striped table-bordered'>
                    <thead>
                        <PageTableHeader displayFields={this.props.displayFieldsOn} sortField={this.props.sortField} sortDir={this.props.sortDir} setSortField={this.props.setSortField} />
                    </thead>
                    <tbody>
                        {
                            this.props.pages.map((page, index) => 
                                <PageRow {...this.props} page={page} index={index} key={index} ref={(ref) => this['pageRow_' + index] = ref} highlighted={(this.props.highlightedRow == index)} />
                            )
                        }
                    </tbody>
                </table>
            </div>
        }
        // console.debug(this.props.pagesFetching);
        // console.debug(fetching);
//(e, rowIndex, rowData) => this.props.onRowSelect(rowData)
        // console.debug(this.state.rows);

        return disp_table;
    }
}

