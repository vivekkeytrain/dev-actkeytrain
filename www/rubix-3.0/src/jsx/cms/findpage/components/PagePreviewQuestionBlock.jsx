import React, { Component, PropTypes } from 'react';

import ClickInputField from './ClickInputField';

import { forEach } from 'lodash';

export default class PagePreviewQuestionBlock extends Component {

    blurAll(e) {
        if (e.target.tagName === 'TEXTAREA') return;
        forEach(this.refs, (r, k) => {
            // console.debug(k);
            if (r._textarea) {
                r.onBlur();
            }
        })

    }

    render() {
        const { everythingPage } = this.props;

        if (everythingPage.UseImage) return <div />

        let fibanswers = <div />
        if (everythingPage.FIBAnswers) {
            fibanswers =    <div className="fib-answers">
                                <label>Answers</label>
                                <div className="answer"><ClickInputField value={everythingPage.FIBAnswers} updateFieldVal={(val) => { console.debug('updateFieldVal') }} ref="questionfield" /></div>
                                <div className="alert alert-success"><ClickInputField value={everythingPage.FIBFeedbackCorrect} updateFieldVal={(val) => { console.debug('updateFieldVal') }} ref="questionfield" /></div>
                                <div className="alert alert-danger"><ClickInputField value={everythingPage.FIBFeedbackIncorrect} updateFieldVal={(val) => { console.debug('updateFieldVal') }} ref="questionfield" /></div>
                            </div>
        } 

        return (
            <Col xs={5} className="question-box panel panel-primary" onClick={this.blurAll.bind(this)}>
                <div className="panel-heading">
                    <h3 className="panel-title">Try it!</h3>
                </div>
                <div className="panel-body">
                    <p className="question"><ClickInputField value={everythingPage.Question} updateFieldVal={(val) => { this.props.updateFieldVal('question', val) }} ref="questionfield" /></p>
                    <div className="answers">
                        {everythingPage.Answers.map((answer, i) => {
                            return  <div>
                                        <div className={`answer ${answer.isCorrect ? 'correct' : ''}`}><i className="rubix-icon icon-simple-line-icons-check"></i><ClickInputField value={answer.distractor} updateFieldVal={(val) => { this.props.updateFieldVal(`MCDistractor${String.fromCharCode(i+65)}`, val) }} ref={`mcd${String.fromCharCode(i+97)}`} /></div>
                                        <div className={`alert ${answer.isCorrect ? 'correct alert-success' : 'incorrect alert-danger'}`}><ClickInputField value={answer.feedback} updateFieldVal={(val) => { this.props.updateFieldVal(`MCFeedback${String.fromCharCode(i+65)}`, val) }} ref={`mcf${String.fromCharCode(i+97)}`} /></div>
                                    </div>
                        })}
                        {fibanswers}
                    </div>
                </div>
            </Col>
        )
    }
} 