import React, { Component, PropTypes } from 'react';

import { map, forEach, isArray, concat, filter, zip } from 'lodash';
import { EditModes, LangOptions } from '../actions';

import ChunkLabel from './ChunkLabel';
import ChunkField from './ChunkField';
import PageLinks from './PageLinks';
import StatiButtonGroup from './StatiButtonGroup';
import LoadingOverlay from './LoadingOverlay';

import io from 'socket.io-client';
// import PagePreview from './PagePreview';
// import PageEdit from './PageEdit';

export default class PageDetailView extends Component {

    constructor(props) {
        super(props);

        // this.state = {
        //     pasteboard: null
        // }
    }

    componentDidUpdate(prevProps, prevState) {
        // console.info('componentDidUpdate', this.state, this.props);

        // if (Pace && Pace.options && Pace.options.ajax) Pace.options.ajax.trackWebSockets = false;

        // if (this.props.detailPage && !this.state.socket) { 
        //     //   Only create the socket if we haven't done so yet and we actually have a detail page to display
        //     let socket = io('/chunks');

        //     socket.on('connect', () => {
        //         console.info('CONNECT CONNECT CONNECT')
        //     });

        //     socket.on('socket', (data) => {
        //         console.info('SOCKET DATA', data);
        //     });

        //     socket.on('chunks', (data) => {
        //         console.info('CHUNKS DATA', data);
        //     });

        //     socket.on('disconnect', () => {
        //         console.info('DISCONNECTED');
        //     });

        //     socket.on('event', (data) => {
        //         console.info('GENERIC EVENT', data);
        //     });

        //     socket.on('update', (data) => {
        //         console.info('chunks updated', data);
        //     })

        //     this.setState({
        //         socket: socket,
        //         'foo': 'bar'
        //     });

        //     console.info('state set', this.state);
        // } else if (!this.props.detailPage && this.state.socket) {
        //     //  If we have removed the detail page we want to remove the socket connection
        //     console.info('we updated and have socket, disconnect', this.state, this.props);
        //     this.state.socket.disconnect();
        //     this.setState({
        //         socket: null
        //     })

        //     if (Pace && Pace.options && Pace.options.ajax) Pace.options.ajax.trackWebSockets = true;
        // }
    }

    componentWillUnmount() {
        // console.info('unmount disconnect');
        // this.state.socket.disconnect();
        // this.setState({
        //     socket: null
        // })

        // if (Pace && Pace.options && Pace.options.ajax) Pace.options.ajax.trackWebSockets = true;
        this.props.setPasteboard(null);
    }

    closeWindow() {
        this.props.setEditMode(EditModes.LIST);
        this.props.setPasteboard(null);
        // this.props.onRowSelect(-1, null);
    }

    handleBackgroundClick(e) {
        if (e.target.id === 'modal-container') this.closeWindow();
    }

    updateChunkVal(chunk, new_chunkval) {
        // console.debug(arguments);
        //setChunkVal={(pageid, lang_id, chunkvar, chunkval)

        //  Strip the leading and trailing <p> </p>. We don't need or want those
        new_chunkval = new_chunkval.replace(/^<p>/, '').replace(/<\/p>$/, '');

        if (new_chunkval != chunk.chunkval) {
            this.props.setChunkVal(chunk.pageid, chunk.lang_id, chunk.chunkvar, new_chunkval, chunk.row_id);
        } else {
            console.debug('no change')
        }
    }

    dangerousHTML(str) {
        return {
            __html: str
        }
    }

    changeSelectedRow(increment) {
        // console.debug(this.props);
        this.props.changeSelectedRow(increment);
        // let new_selected_index = this.props.selectedRow + increment;

        // if (new_selected_index < 0) new_selected_index = this.props.pages.length-1;
        // if (new_selected_index >= this.props.pages.length) new_selected_index = 0;

        // this.props.onRowSelect(new_selected_index, this.props.pages[new_selected_index]);
    }

    statiSelect(field_name, lang_page, exclusive, field_val, e) {
        // console.debug(arguments);
        // console.debug(this.props);

        // let current_stati = this.props.detailPages[lang_id].qa_fields[field_name];
        let current_stati = lang_page.qa_fields[field_name];
        // console.debug(this.props.detailPages[lang_id]);
        if (!exclusive) {
            // console.debug(current_stati);
            // console.debug(current_stati, field_val,  current_stati&field_val);
            if ((current_stati&field_val) > 0) {
                //  Stati bit already set, unset it
                field_val = current_stati & ~field_val;
            } else {
                //  Stati bit not set, set it
                field_val = current_stati | field_val;
            }
        }

        this.props.onUpdatePageField(lang_page.PageID, field_name, field_val, lang_page.lang_id);
    }

    updateField(field_name, lang_page, e) {
        console.debug('updateField', field_name, lang_page, e.target.value);
        this.props.onUpdatePageField(lang_page.PageID, field_name, e.target.value, lang_page.lang_id);
    }

    setPasteboard(chunk, cut) {
        console.debug('setPasteboard', chunk, this.props.pasteboard, this.props.pasteboardCut);
        //  If we have a chunk set as 'cut', remove it 
        if (this.props.pasteboardCut) {
            // console.error('REMOVE REMOVE REMOVE REMOVE REMOVE');
            this.props.removeChunkMessage(this.props.pasteboard.pageid, this.props.pasteboard.chunkvar, this.props.pasteboard.lang_id);
            // console.debug(this.refs);
            // console.debug(`_chunkField_${this.props.pasteboard.chunkvar}_${this.props.pasteboard.lang_id}`)
            // console.debug(this.refs[`_chunkField_${this.props.pasteboard.chunkvar}_${this.props.pasteboard.lang_id}`]);
            // this.refs[`_chunkField_${this.props.pasteboard.chunkvar}_${this.props.pasteboard.lang_id}`].setState({
            //     cut: false
            // })
            // alert('');

            // ref={`_chunkField_${chunk.chunkvar}_{$chunk.lang_id}`}

            // this.props.setChunkMessage('Blah blah blah', 'info', this.props.pasteboard.pageid, this.props.pasteboard.chunkvar, this.props.pasteboard.lang_id);
        }

        this.props.setPasteboard(chunk, cut);
    }

    closeWYSIWYG(e) {
        // console.debug('closeWYSIWYG', e.target, $(e.target).parents('.trumbowyg-box'));

        // console.debug(this);

        if ($(e.target).parents('.trumbowyg-box').size() == 0) {
            //  We're not inside a wysiwyg box
            //  So let's close any that are open

            // console.debug($(e.target).parents('.trumbowyg-box').size())

            forEach(this.refs, (r) => {
                // console.debug(r._chunkfield.getDOMNode());
                // console.debug(r.state);
                // console.debug(r._chunkfield.getDOMNode());
                // if (r.state.open)
                    $(r._chunkfield.getDOMNode()).trigger('tbwblur').trumbowyg('destroy');
                    $(r._chunkfield.getDOMNode()).css({'height': 'auto'});
            })
        }
    }

    doPageChunkAction(src_chunk, dest_chunk, chunk_action) {
        console.debug('doPageChunkAction', src_chunk, dest_chunk, chunk_action);
        this.props.doPageChunkAction(this.props.detailPage.PageID, src_chunk.lang_id, src_chunk, dest_chunk, chunk_action);
    }

    doFixPage(optflags) {
        this.props.doFixPage(this.props.detailPage.PageID, optflags);
    }

    // renderChunk(chunk, indent=0) {
    //     // console.debug('renderChunk', chunk, indent);
    //     if (isArray(chunk.chunkval)) {
    //         let out = [];
    //         forEach(chunk.chunkval, (c) => {
    //             out = concat(out, this.renderChunk(c, indent+20));
    //         });
    //         return out;
    //     } else {
    //         return [<ChunkLabel chunk={chunk} indent={indent} />, <ChunkField chunk={chunk} updateFieldVal={this.updateChunkVal.bind(this, chunk)} readonly={(chunk.lang_id == 1)} indent={indent} ref={`_chunkField_${chunk.chunkvar}_{$chunk.lang_id}`} />]
    //     }
    // }

    // renderChunk2(chunk_arr, indent=0) {
    //     // console.debug('renderChunk2', chunk_arr, indent);
    //     let labels = map(chunk_arr, (chunk) => {
    //         // console.debug(chunk)
    //         // console.debug(chunk.chunkvar);
    //         return <ChunkLabel chunk={chunk} indent={indent} />
    //     });

    //     let fields = [];
    //     if (isArray(chunk_arr[0].chunkval)) {
    //         let nested = this.renderChunk2(map(chunk_arr, 'chunkval'), indent+20);
    //         fields.push(<Row>{nested}</Row>)
    //     }

    //     forEach(chunk_arr, (chunk, ind) => {
    //         // console.debug(chunk.chunkvar, chunk.chunkval);
    //         if (!isArray(chunk.chunkval)) {
    //             fields.push(<ChunkField chunk={chunk} updateFieldVal={this.updateChunkVal.bind(this, chunk)} readonly={(chunk.lang_id == 1)} indent={indent} />);
    //         }
    //     })

    //     // console.debug(labels);
    //     // console.debug(fields);
    //     return (
    //         <div>
    //             <Row>{labels}</Row>
    //             <Row>{fields}</Row>
    //         </div>
    //     )
    // }

    renderChunk3(chunk, indent=25) {
        // console.debug('renderChunk3', chunk, indent);
        let out_arr = [];

        out_arr.push(<ChunkLabel chunk={chunk} indent={indent} pasteboard={this.props.pasteboard} pasteboardCut={this.props.pasteboardCut} readonly={(chunk.lang_id == 1)} setChunkMessage={this.props.setChunkMessage} removeChunkMessage={this.props.removeChunkMessage} updateFieldVal={this.updateChunkVal.bind(this, chunk)} setPasteboard={this.setPasteboard.bind(this, chunk)} doPageChunkAction={this.doPageChunkAction.bind(this)} />);

        

        if (isArray(chunk.chunkval)) {
            forEach(chunk.chunkval, (c) => {
                out_arr = concat(out_arr, this.renderChunk3(c, indent+25));
            });
        } else {
            out_arr.push(<ChunkField chunk={chunk} setChunkMessage={this.props.setChunkMessage} removeChunkMessage={this.props.removeChunkMessage} updateFieldVal={this.updateChunkVal.bind(this, chunk)} readonly={(chunk.lang_id == 1)} indent={indent} ref={`_chunkField_${chunk.chunkvar}_${chunk.lang_id}`} />)
        }

        return out_arr;
    }

    render() {
        console.debug(this.props);

        // console.debug(this.props.detailPages);

        if (this.props.editMode !== EditModes.PREVIEW) return <div />;

        console.debug('PREVIEW PREVIEW PREVIEW PREVIEW PREVIEW PREVIEW');

        let details = [], status, problem;
        if (this.props.detailPages && this.props.detailPages[0] && this.props.detailPages[0].chunks) {
            console.debug('here');
            //  One row per chunk
            // details = map(this.props.detailPages[0].chunks, (c, ind) => {
            //     let labels = [],
            //         fields = [];

            //     //  One column per language
            //     forEach(this.props.detailPages, (lang_page) => {
            //         // console.debug(lang_page);
            //         if (lang_page && lang_page.chunks) {
            //             let chunk = lang_page.chunks[ind];
            //             labels.push(<ChunkLabel chunk={chunk} />);
            //             fields.push(<ChunkField chunk={chunk} updateFieldVal={this.updateChunkVal.bind(this, chunk)} readonly={(chunk.lang_id == 1)} />)
            //         }
                    
            //     })

            //     return (
            //         <div key={ind}>
            //             <Row>
            //                 {labels}
            //             </Row>
            //             <Row>
            //                 {fields}
            //             </Row>
            //         </div>
            //     );
            // });
            // console.debug(details);

            // console.debug(this.props.detailPages)
            // let combined = zip(this.props.detailPages[0].chunks, this.props.detailPages[1].chunks);

            // forEach(combined, (c) => {

            // })

            // // details = <div />;
            let chunk_arr = [];
            // forEach(this.props.detailPages, (lang_page) => {
            //     if (lang_page && lang_page.chunks) {
            //         forEach(lang_page.chunks, (chunk) => {
            //             chunk_arr = concat(chunk_arr, this.renderChunk(chunk));
            //         })
            //         // chunk_arr = concat(chunk_arr, )
            //     }
            // })

            // forEach(this.props.detailPages[0].chunks, (c, ind) => {
            //     forEach(this.props.detailPages, (lang_page) => {
            //         if (lang_page && lang_page.chunks) {
            //             let chunk = lang_page.chunks[ind];
            //             chunk_arr = concat(chunk_arr, this.renderChunk(chunk));
            //         }
            //     })
            // })

            // let chunk_arr = map(this.props.detailPages, (lang_page) => {
            //     if (lang_page && lang_page.chunks) {
            //         return map(lang_page.chunks, (chunk) => {
            //             return this.renderChunk(chunk);
            //         }, this)
            //     } else {
            //         return '';
            //     }
            // }, this);

            forEach(this.props.detailPages, (page) => {
                let page_chunks_arr = [];
                if (page) {
                    forEach(page.chunks, (c, ind) => {
                        page_chunks_arr = concat(page_chunks_arr, this.renderChunk3(c));
                    })
                }
                chunk_arr.push(page_chunks_arr);
            })


            // forEach(this.props.detailPages[0].chunks, (c, ind) => {
            //     let these_chunks = map(this.props.detailPages, (page) => {
            //         return page.chunks[ind];
            //     })
            //     chunk_arr = concat(chunk_arr, this.renderChunk2(these_chunks));
            // })

            // console.debug(zip(...chunk_arr));

            details = map(zip(...chunk_arr), (row, ind) => {
                return (
                    <Row key={ind}>
                        {row.map((col) => {
                            // console.debug(col);
                            return col
                        })}
                    </Row>
                )
            })
            // details = chunk_arr;

            // let labels = filter(chunk_arr, (arr, i) => {
            //     return i%2 == 0;
            // });

            // let fields = filter(chunk_arr, (arr, i) => {
            //     return i%2 != 0;
            // });

            // console.debug(labels);
            // while (labels.length > 0) {
            //     //  For each lang
            //     let row_labels = [],
            //         row_fields = [];
            //     forEach(this.props.detailPages, (lang_page, i) => {
            //         row_labels.push(labels.shift());
            //         row_fields.push(fields.shift());
            //     });

            //     details.push(
            //         <div key={labels.length}>
            //             <Row>{row_labels}</Row>
            //             <Row>{row_fields}</Row>
            //         </div>
            //     )
            // }
            // console.debug(details);


            //  Status, Problem, Problem Desc, Problem Desc 2

            //  For each language
            let status_labels = [], status_fields = [],
                problem_labels = [[], [], []], problem_fields = [[], [], []];

            forEach(this.props.detailPages, (lang_page) => {
                let labels = [],
                    fields = [];

                if (lang_page && lang_page.qa_fields) {
                    // let lang_id = lang_page.lang_id;
                    // console.debug(lang_page);
                    // console.debug(this);
                    status_labels.push(<Col xs={6}><label>Status</label></Col>);
                    status_fields.push(<Col xs={6}><StatiButtonGroup currentVal={lang_page.qa_fields.status_val} buttonSelected={this.statiSelect.bind(this, 'status_val', lang_page, true)} bits={this.props.constants.qa_status_val} grouped={true} /></Col>);

                    if (lang_page.qa_fields.status_val > 1 && lang_page.qa_fields.status_val != 32) { // 32 == unset
                        problem_labels[0].push(<Col xs={6}><label>Problem</label></Col>);
                        problem_fields[0].push(<Col xs={6}><StatiButtonGroup currentVal={lang_page.qa_fields.problem_bits} buttonSelected={this.statiSelect.bind(this, 'problem_bits', lang_page, false)} bits={this.props.constants.qa_problem_bits} grouped={false} /></Col>);

                        problem_labels[1].push(<Col xs={6}><label>Problem Desc</label></Col>);
                        problem_fields[1].push(<Col xs={6}><Input md type='text' defaultValue={lang_page.qa_fields.problem1_desc} onBlur={this.updateField.bind(this, 'problem1_desc', lang_page)}/></Col>);

                        problem_labels[2].push(<Col xs={6}><label>Problem Desc 2</label></Col>);
                        problem_fields[2].push(<Col xs={6}><Input md type='text' defaultValue={lang_page.qa_fields.problem2_desc} onBlur={this.updateField.bind(this, 'problem2_desc', lang_page)}/></Col>);
                    }
                }
            });

            // console.debug(status_labels);

            status = <div>
                <Row>{status_labels}</Row>
                <Row>{status_fields}</Row>
            </div>

            problem = <div>
                <Row>{problem_labels[0]}</Row>
                <Row>{problem_fields[0]}</Row>
                <Row>{problem_labels[1]}</Row>
                <Row>{problem_fields[1]}</Row>
                <Row>{problem_labels[2]}</Row>
                <Row>{problem_fields[2]}</Row>
            </div>

        }
        console.debug(details);

     

        // {this.props.detailPages.map((v, k) => {
        //                     return (
        //                         <div key={`${chunk.chunkvar}-${chunk.lang_id}`}>
        //                             <ChunkLabel chunk={chunk} />
                                    
        //                         </div>
        //                     );
        //                 })}

        // let details = map(this.props.detailPages, (lang_page) => {
        //     console.debug(lang_page)
        //     if (lang_page && lang_page.chunks) {
        //         return map(lang_page.chunks, (chunk) => {
        //             return (
        //                 <ChunkField chunk={chunk} updateFieldVal={this.updateChunkVal.bind(this, chunk)} readonly={(chunk.lang_id == 1)} />
        //             );
        //         })
        //     } else {
        //         return null;
        //     }
        // })


        let loading = []
        if (this.props.detailPages[0] && this.props.detailPages[0].chunks_loading)
            loading.push(<LoadingOverlay en={true} key='loading_en' />)

        if (this.props.detailPages[1] && this.props.detailPages[1].chunks_loading)
            loading.push(<LoadingOverlay es={true} key='loading_es' />);

        if (!details || !this.props.detailPage) return <div />
        console.debug(details);
        console.debug(status);
        console.debug(problem);
        return (
            <div className='cms-modal-detail chunks-detail'>
                <div className="modal-backdrop fade in"></div>
                <div className="cms-modal-detail-modal" id="modal-container" onClick={this.handleBackgroundClick.bind(this)}>
                    <div className="cms-modal-detail">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.closeWindow.bind(this)}><span aria-hidden="true">×</span></button>
                            <Grid>
                                <Row>
                                    <Col xs={8}>
                                        <h4 className="modal-title">{this.props.detailPage.PageID}: {this.props.detailPage.Title}: {this.props.detailPage.ed_chapter_Title}: Page {this.props.detailPage.pageno}</h4>
                                    </Col>
                                    <Col xs={2} className="pull-right">
                                        Launch: <PageLinks {...this.props} pageid={this.props.detailPage.PageID} />
                                    </Col>
                                </Row>
                            </Grid>
                        </div>
                        <div className="modal-body" onClick={this.closeWYSIWYG.bind(this)}>
                            <Grid>
                                <Row>
                                    <Col xs={3} className="pull-left" collapseLeft>
                                        <Button onClick={this.changeSelectedRow.bind(this, -1)}><i className="glyphicon glyphicon-chevron-left"></i> Previous Page</Button>
                                    </Col>

                                    <Col xsOffset={1} xs={2}>
                                        <Button bsStyle="info" onClick={this.doFixPage.bind(this, 1)}>Fix Page <i className="glyphicon glyphicon-apple"></i></Button>
                                    </Col>
                                    <Col xs={2}>
                                        <Button bsStyle="primary" onClick={this.doFixPage.bind(this, 3)}>Fix and Fill <i className="glyphicon glyphicon-education"></i></Button>
                                    </Col>

                                    <Col xs={3} className="pull-right" collapseRight>
                                        <Button className="pull-right" onClick={this.changeSelectedRow.bind(this, 1)}>Next Page<i className="glyphicon glyphicon-chevron-right"></i></Button>
                                    </Col>
                                </Row>
                                {details}
                                {status}
                                {problem}
                                {loading}
                            </Grid>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
} 