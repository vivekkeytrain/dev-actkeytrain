import React, { Component, PropTypes } from 'react';

import PageLinks from './PageLinks';

export default class PagePreviewHeader extends Component {

    render() {
        console.debug(this.props);

        if (!this.props.everythingPage) return <div />

        return (
            <div>
                <Col xs={8}>
                    <h4 className="modal-title">{this.props.everythingPage.pageid}: {this.props.everythingPage.page_title}: {this.props.everythingPage.topic_desc}: Page {this.props.everythingPage.pageno}</h4>
                </Col>
                <Col xs={2} className="pull-right">
                    Launch: <PageLinks {...this.props} pageid={this.props.everythingPage.pageid} />
                </Col>
            </div>
        )
    }
} 
