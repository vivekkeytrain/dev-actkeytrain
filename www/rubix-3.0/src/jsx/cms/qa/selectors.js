import { createSelector } from 'reselect';
import { chain, uniq, filter, map, find, merge, includes, sortBy } from 'lodash';
import { ModeFilters } from './actions';

/*  Get a unique set of units   */
function selectUniqueUnits(content, tags) {
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'unit_tag': tag});

            return {
                id: match.unit_id,
                tag: match.unit_tag,
                desc: match.unit_desc,
                seq: match.unit_seq,
                display: match.unit_display
            }
        })
        .value();
}

/*  Select a unique set of unit tags that will be used to create a unique set of units  */
function selectUnitTags(content) {
    return chain(content)
        .map('unit_tag')
        .uniq()
        .value();
}


function selectUniqueCourses(content, tags) {
    // console.debug('selectUniqueCourses', content, tags);
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'course_tag': tag});

            return {
                id: match.course_id,
                tag: match.course_tag,
                desc: match.course_desc,
                seq: match.course_seq,
                display: match.course_display
            }
        })
        .value();
}

function selectCourseTags(content, unit) {
    return chain(content)
        .filter({'unit_id': unit})
        .map('course_tag')
        .uniq()
        .value();
}

function selectUniqueLessons(content, tags) {
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'lesson_tag': tag});

            return {
                id: match.lesson_id,
                tag: match.lesson_tag,
                desc: match.lesson_desc,
                seq: match.lesson_seq,
                display: match.lesson_display
            }
        })
        .value();
}

function selectLessonTags(content, course) {
    return chain(content)
        .filter({'course_id': course})
        .map('lesson_tag')
        .uniq()
        .value();
}

function selectUniqueTopics(content, tags) {
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'topic_tag': tag});

            return {
                id: match.topic_id,
                tag: match.topic_tag,
                desc: match.topic_desc,
                seq: match.topic_seq,
                display: match.topic_display
            }
        })
        .value();
}

function selectTopicTags(content, lesson) {
    return chain(content)
        .filter({'lesson_id': lesson})
        .map('topic_tag')
        .uniq()
        .value();
}

/*  Content filtered by current selected state */
function selectContent(content, unit, course, lesson, topic) {
    // console.debug(content);
    // console.debug([unit, course, lesson, topic]);
    let filtered_content = {
        units: content,
        courses: [],
        lessons: [],
        topics: []
    }
    if (unit != '')
        filtered_content.courses = filter(content, {'unit_id': unit});
    
    if (course != '') 
        filtered_content.lessons = filter(content, {'course_id': course});

    if (lesson != '') 
        filtered_content.topics = filter(content, {'lesson_id': lesson});

    // if (topic != '') 
    //     filtered_content.topics = filter(content, {'topic_tag': topic});

    // console.debug(filtered_content);

    return filtered_content;
}

/*
{
    pages_by_tag: {
        'EN': {
            T1007: {
                isFetching: false,
                items: [5,6,7,8]
            },
            L105: {
                isFetching: false,
                items: [...]
            }
        }
    }
    pages: {
        'EN': {
            5: {
                ...
            },
            6: {
                ...
            }
        }
        'ES': {
            ...
        }
    }
}
*/

function getTagPages(pages, pages_by_tag, lang, tag) {
    // console.debug(pages);
    // console.debug(pages_by_tag);
    // console.debug(lang, tag);
    if (!(pages[lang] && pages_by_tag[lang] && pages_by_tag[lang][tag])) {
        return [];
    } else {
        let page_ids = pages_by_tag[lang][tag].items;
        let page_objs = pages[lang];
        // console.debug(pages_by_tag[lang]);
        // console.debug(page_ids);
        // let out_pages = chain(map(page_ids, (pageid) => {
        //     return pages[pageid]    //  Turn pageids into actual page objects
        // }))
        //     .sortBy('page_number')  //  Sort it
        //     .value()                //  Break the chain

        let out_pages = map(page_ids, (pageid) => {
            // console.debug(pageid);
            return page_objs[pageid]    //  Turn pageids into actual page objects
        });

        out_pages = sortBy(out_pages, 'page_number');

        // console.debug(out_pages);
        return out_pages;
    }
}

function selectPagesByTag(pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, mode, assigned_to) {
    // console.log('selectPagesByTag', lang, tag_type, unit, course, lesson, topic, mode);

    //  Only return pages if 'ALL' (-1) is selected
    if (tag_type === 'U' && course !== -1) return [];
    if (tag_type === 'C' && lesson !== -1) return [];
    if (tag_type === 'L' && topic !== -1) return [];

    let tag = tag_type;
    if (tag_type === 'U') tag += unit;
    if (tag_type === 'C') tag += course;
    if (tag_type === 'L') tag += lesson;
    if (tag_type === 'T') tag += topic;

    let tag_pages = getTagPages(pages, pages_by_tag, lang, tag);
    if (mode == ModeFilters.CORRECT) {
        tag_pages = filter(tag_pages, (page) => {
            // console.debug(page);
            return page.status_val > 1 && page.problem_bits > 0;
        });
    } else if (mode == ModeFilters.CHECK) {
        // console.debug('check mode');
        //  Pages with a fixed_by and status isn't perfect
        tag_pages = filter(tag_pages, (page) => {
            // console.debug(page.fixed_by);
            return page.status_val > 1 && page.fixed_by != null;
        });
    }

    if (assigned_to) {
        tag_pages = filter(tag_pages, {'assigned_to': assigned_to})
    }

    return tag_pages;
}

function selectPagesIsFetching(pages_by_tag, lang, unit, course, lesson, topic) {
    console.debug('selectPagesIsFetching', pages_by_tag, lang, unit, course, lesson, topic);
    if (!pages_by_tag[lang]) return false;

    const lang_pages = pages_by_tag[lang];

    let tag;
    if (topic && topic != -1)
        tag = 'T' + topic;
    else if (lesson && topic == -1)
        tag = 'L' + lesson;
    else if (course && lesson == -1)
        tag = 'C' + course;
    else if (unit && course == -1)
        tag = 'U' + unit;

    if (!lang_pages[tag]) return false;

    console.debug(tag);
    console.debug(lang_pages[tag]);
    return lang_pages[tag].isFetching;
    // const pages = state.pages_by_tag;
    // if (!pages[lang]) return true;

    // const lang_pages = pages[lang];
    // if (!lang_pages[tag]) return true;

    // if (lang_pages[tag].isFetching) {

    // if (topic && topic !== -1 && topic_pages) {
    //         pages = topic_pages;
    //     } else if (lesson && lesson !== -1 && lesson_pages) {
    //         pages = lesson_pages;
    //     } else if (course && course !== -1 && course_pages) {
    //         pages = course_pages;
    //     } else if (unit && unit_pages) {
    //         pages = unit_pages;
    //     } else {
    //         pages = [];
    //     }
}

// function selectLessonPages(pages, pages_by_tag, lang, lesson, mode) {
//     // console.log('selectLessonPages', mode);
//     //  In review mode you only get topic pages, not lesson pages
//     if (mode === ModeFilters.REVIEW) return [];

//     let tag = 'L' + lesson;
//     let tag_pages = getTagPages(pages, pages_by_tag, lang, tag);
//     // console.debug(tag_pages);

//     if (!tag_pages) return [];

//     if (mode == ModeFilters.CORRECT) {
//         // console.debug('correct mode');
//         //  Pages that have a problem and status isn't perfect
//         tag_pages = filter(tag_pages, (page) => {
//             // console.debug(page);
//             return page.status_val > 1 && page.problem_bits > 0;
//         });
//     } else if (mode == ModeFilters.CHECK) {
//         // console.debug('check mode');
//         //  Pages with a fixed_by and status isn't perfect
//         tag_pages = filter(tag_pages, (page) => {
//             // console.debug(page.fixed_by);
//             return page.status_val > 1 && page.fixed_by != null;
//         });
//     }

//     // console.debug(pages);
//     return tag_pages;
// }

// function selectTopicPages(pages, pages_by_tag, lang, topic, mode) {

//     //  If we haven't picked a topic or a lang then we don't have pages
//     let tag = 'T' + topic;
//     let tag_pages = getTagPages(pages, pages_by_tag, lang, tag);

//     if (!tag_pages) return [];

//         // let pages = chain(topic_pages[lang][topic].items)
//         //     .values()
//         //     .sortBy('page_number')
//         //     .value();
//     // console.debug(tag_pages);

//     // console.debug('mode', mode);

//     if (mode == ModeFilters.REVIEW) {
//         //  In the future perhaps this will be pages with no status
//         // console.debug('review mode');
//     } else if (mode == ModeFilters.CORRECT) {
//         // console.debug('correct mode');
//         //  Pages that have a problem and status isn't perfect
//         tag_pages = filter(tag_pages, (page) => {
//             return page.status_val > 1 && page.problem_bits > 0;
//         });
//     } else if (mode == ModeFilters.CHECK) {
//         // console.debug('check mode');
//         //  Pages with a fixed_by and status isn't perfect
//         tag_pages = filter(tag_pages, (page) => {
//             // console.debug(page.fixed_by);
//             return page.status_val > 1 && page.fixed_by != null;
//         });
//     }

//     // console.debug(pages);
//     return tag_pages;

//     // return (topic_pages[topic]) ? topic_pages[topic].items : {};
// }

function selectCourseTag(course, courses) {
    if (!course || !courses || course == -1) return '';
    console.debug('selectCourseTag', course, courses);
    console.debug(find(course, {'id': course}));
    return chain(courses).find({'id': course}).value().tag;
}

function selectSession(sessions, lesson_id, lang) {
    // console.debug('selectSession: ' + lesson_id);
    // console.debug(sessions);
    if (!lesson_id) return {};
    if (!sessions[lang]) return {};

    return sessions[lang][lesson_id] || {};
}

const contentSelector = state => state.editor_content.content;
const unitSelector = state => state.filters.unit;
const courseSelector = state => state.filters.course;
const lessonSelector = state => state.filters.lesson;
const topicSelector = state => state.filters.topic;
const assignedToSelector = state => state.filters.assigned_to;

const langSelector = state => state.filters.lang;
const modeSelector = state => state.filters.mode;
const selectedRowSelector = state => state.filters.selectedRow;

const sessionsSelector = state => state.filters.sessions;
const messagesSelector = state => state.filters.messages;

const pagesSelector = state => state.pages;
const tagPagesSelector = state => state.pages_by_tag;

const constantsSelector = state => state.constants;

const contentDataSelector = createSelector(
    contentSelector,
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    (content, unit, course, lesson, topic) => {
        return selectContent(content, unit, course, lesson, topic);
    }
);

const unitTagsSelector = createSelector(
    contentSelector,
    (content) => {
        return selectUnitTags(content);
    }
);

const unitsSelector = createSelector(
    [contentSelector, unitTagsSelector],
    (content, tags) => {
        return selectUniqueUnits(content, tags);
    }
);

const courseTagsSelector = createSelector(
    [contentDataSelector, unitSelector],
    (content, unit) => {
        return selectCourseTags(content.courses, unit);
    }
);

const coursesSelector = createSelector(
    [contentDataSelector, courseTagsSelector],
    (content, tags) => {
        return selectUniqueCourses(content.courses, tags);
    }
);

/*  Selector to get the course tag of the currently selected course_id */
const courseTagSelector = createSelector(
    [courseSelector, coursesSelector],
    (course, courses) => {
        return selectCourseTag(course, courses);
    }
);


const lessonTagsSelector = createSelector(
    [contentDataSelector, courseSelector],
    (content, course) => {
        return selectLessonTags(content.lessons, course);
    }
);

const lessonsSelector = createSelector(
    [contentDataSelector, lessonTagsSelector],
    (content, tags) => {
        return selectUniqueLessons(content.lessons, tags);
    }
);

const topicTagsSelector = createSelector(
    [contentDataSelector, lessonSelector],
    (content, lesson) => {
        return selectTopicTags(content.topics, lesson);
    }
);

const topicsSelector = createSelector(
    [contentDataSelector, topicTagsSelector],
    (content, tags) => {
        return selectUniqueTopics(content.topics, tags);
    }
);

const unitPagesSelector = createSelector(
    pagesSelector,
    tagPagesSelector,
    langSelector,
    () => 'U',
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    modeSelector,
    assignedToSelector,
    (pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, mode, assigned_to) => {
        return selectPagesByTag(pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, mode, assigned_to)
    }
);

const coursePagesSelector = createSelector(
    pagesSelector,
    tagPagesSelector,
    langSelector,
    () => 'C',
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    modeSelector,
    assignedToSelector,
    (pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, mode, assigned_to) => {
        return selectPagesByTag(pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, mode, assigned_to)
    }
);

const lessonPagesSelector = createSelector(
    pagesSelector,
    tagPagesSelector,
    langSelector,
    () => 'L',
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    modeSelector,
    assignedToSelector,
    (pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, mode, assigned_to) => {
        return selectPagesByTag(pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, mode, assigned_to)
    }
);

const topicPagesSelector = createSelector(
    pagesSelector,
    tagPagesSelector,
    langSelector,
    () => 'T',
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    modeSelector,
    assignedToSelector,
    (pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, mode, assigned_to) => {
        return selectPagesByTag(pages, pages_by_tag, lang, tag_type, unit, course, lesson, topic, mode, assigned_to)
    }
);

// selectPagesIsFetching(pages_by_tag, lang, unit, course, lesson, topic)
const pagesIsFetchingSelector = createSelector(
    tagPagesSelector,
    langSelector,
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    (pages_by_tag, lang, unit, course, lesson, topic) => {
        return selectPagesIsFetching(pages_by_tag, lang, unit, course, lesson, topic);
    }
);

//selectTopicPages(pages, pages_by_tag, lang, topic, mode) 
// const topicPagesSelector = createSelector(
//     pagesSelector,
//     tagPagesSelector,
//     langSelector,
//     topicSelector,
//     modeSelector,
//     (pages, tagPages, lang, topic, mode) => {
//         return selectTopicPages(pages, tagPages, lang, topic, mode);
//     }
// );

// const lessonPagesSelector = createSelector(
//     pagesSelector,
//     tagPagesSelector,
//     langSelector,
//     lessonSelector,
//     modeSelector,
//     (pages, tagPages, lang, lesson, mode) => {
//         return selectLessonPages(pages, tagPages, lang, lesson, mode)
//     })

const sessionSelector = createSelector(
    sessionsSelector,
    lessonSelector,
    langSelector,
    (sessions, lesson_id, lang) => {
        return selectSession(sessions, lesson_id, lang);
    }
)


export const filteredContentSelector = createSelector(
    contentDataSelector,
    unitSelector,
    unitsSelector,
    courseSelector,
    coursesSelector,
    courseTagSelector,
    lessonSelector,
    lessonsSelector,
    topicSelector,
    topicsSelector,
    langSelector,
    modeSelector,
    selectedRowSelector,
    unitPagesSelector,
    coursePagesSelector,
    lessonPagesSelector,
    topicPagesSelector,
    pagesIsFetchingSelector,
    constantsSelector,
    sessionSelector,
    messagesSelector,
    assignedToSelector,
    (content, unit, units, course, courses, course_tag, lesson, lessons, topic, topics, lang, mode, selected_row, unit_pages, course_pages, lesson_pages, topic_pages, pages_fetching, constants, session, messages, assigned_to) => {
        return {
            content,
            unit,
            units,
            course,
            courses,
            course_tag,
            lesson,
            lessons,
            topic,
            topics,
            lang,
            mode,
            selected_row,
            unit_pages,
            course_pages,
            lesson_pages,
            topic_pages,
            pages_fetching,
            constants,
            session, 
            messages,
            assigned_to
        }
    }
);


