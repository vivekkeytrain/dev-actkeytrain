import { combineReducers } from 'redux';
import { 
    SET_LANG, SET_UNIT, SET_COURSE, SET_LESSON, SET_TOPIC, SET_MODE, SET_USER, SET_SELECTED_ROW,
    ADD_MESSAGE, CLOSE_MESSAGE, SET_PEOPLE_FILTER,
    FETCH_EDITOR_CONTENT_REQUEST, FETCH_EDITOR_CONTENT_SUCCESS, FETCH_EDITOR_CONTENT_FAILURE,
    FETCH_PAGES_REQUEST, FETCH_PAGES_FAILURE, FETCH_PAGES_SUCCESS,
    UPDATE_FIELD_REQUEST, UPDATE_FIELD_SUCCESS, UPDATE_FIELD_FAILURE,
    FETCH_STATUS_VALS_REQUEST, FETCH_STATUS_VALS_FAILURE, FETCH_STATUS_VALS_SUCCESS, 
    FETCH_PROBLEM_BITS_REQUEST, FETCH_PROBLEM_BITS_FAILURE, FETCH_PROBLEM_BITS_SUCCESS,
    FETCH_PEOPLE_REQUEST, FETCH_PEOPLE_FAILURE, FETCH_PEOPLE_SUCCESS,
    DO_LOGIN_LESSON_REQUEST, DO_LOGIN_LESSON_SUCCESS, DO_LOGIN_LESSON_FAILURE,
    LangOptions, ModeFilters,
    fetchEditorContent,
    fetchPagesSuccess, fetchPagesRequest
} from './actions';
const { REVIEW } = ModeFilters;
const { EN } = LangOptions;
import { chain, map, fromPairs, sortBy } from 'lodash';


function sessions(state = {}, action) {
    switch (action.type) {
        case DO_LOGIN_LESSON_SUCCESS:
        case DO_LOGIN_LESSON_REQUEST:
        case DO_LOGIN_LESSON_FAILURE:
            if (!action.lang) return state;

            return Object.assign({}, state, {
                [action.lang]: lang_sessions(state[action.lang], action)
            });
        default:
            return state;
    }
}

function lang_sessions(state = {}, action) {
    // console.debug(state);
    // console.debug(action);
    switch (action.type) {
        case DO_LOGIN_LESSON_SUCCESS:
            return Object.assign({}, state, {
                [action.lesson_id]: {
                    session_id: action.session_id,
                    login_session_uid: action.login_session_uid,
                    isFetching: false,
                    lastUpdated: action.receivedAt
                }
            });
        case DO_LOGIN_LESSON_REQUEST:
            return Object.assign({}, state, {
                [action.lesson_id]: {
                    isFetching: true
                }
            });
        case DO_LOGIN_LESSON_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state.sessions, {
                [action.lesson_id]: {
                    isFetching: false
                }
            });
        default:
            return state;
    }
}

function filters(state = {
    lang: EN,
    mode: REVIEW,
    unit: '',
    course: '',
    lesson: '',
    topic: '',
    selectedRow: -1,
    sessions: {},
    messages: [],
    assigned_to: ''
}, action) {
    switch (action.type) {
        case SET_LANG:
            return Object.assign({}, state, {
                lang: action.lang
            });
        case SET_MODE:
            return Object.assign({}, state, {
                mode: action.mode
            });
        case SET_UNIT:
            return Object.assign({}, state, {
                unit: action.unit,
                course: '',
                lesson: '',
                topic: ''
            });
        case SET_COURSE:
            //  Can't set course if we haven't set unit
            if (state.unit == '') return state;

            return Object.assign({}, state, {
                course: action.course,
                lesson: '',
                topic: ''
            });
        case SET_LESSON:
            //  Can't set lesson if we haven't set unit and course
            if (state.unit == '' || state.course == '') return state;

            return Object.assign({}, state, {
                lesson: action.lesson,
                topic: ''
            });
        case SET_TOPIC:
            //  Can't set topic if we haven't set unit, course, and lesson
            if (state.unit == '' || state.course == '' || state.lesson == '') return state;

            return Object.assign({}, state, {
                topic: action.topic
            });
        case SET_SELECTED_ROW:
            return Object.assign({}, state, {
                selectedRow: action.row_index
            });
        case SET_PEOPLE_FILTER:
            return Object.assign({}, state, {
                assigned_to: action.person
            })

        case DO_LOGIN_LESSON_SUCCESS:
        case DO_LOGIN_LESSON_REQUEST:
            return Object.assign({}, state, {
                sessions: sessions(state.sessions, action)
            });

        case FETCH_EDITOR_CONTENT_FAILURE:
        case FETCH_PAGES_FAILURE:
        case UPDATE_FIELD_FAILURE:
        case FETCH_STATUS_VALS_FAILURE:
        case FETCH_PROBLEM_BITS_FAILURE:
        case FETCH_PEOPLE_FAILURE:
        case DO_LOGIN_LESSON_FAILURE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: 'danger',
                        message: action.ex.message,
                        visible: true
                    }
                ]
            });

        // case FETCH_EDITOR_CONTENT_SUCCESS:
        //     return Object.assign({}, state, {
        //         messages: [
        //             ...state.messages,
        //             {
        //                 type: 'success',
        //                 message: 'Message',
        //                 visible: true
        //             }
        //         ]
        //     });

        case ADD_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: action.message_type,
                        message: action.message,
                        visible: true
                    }
                ]
            });

        case CLOSE_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages.slice(0, action.index),
                    Object.assign({}, state.messages[action.index], {
                        visible: false
                    }),
                    ...state.messages.slice(action.index+1)
                ]
            });
        default:
            return state;

    }
}

function pages(state = {}, action) {
    // console.debug(state);
    switch (action.type) {
        case FETCH_PAGES_SUCCESS:
        case FETCH_PAGES_REQUEST:
        case FETCH_PAGES_FAILURE:
        case UPDATE_FIELD_SUCCESS:
        case UPDATE_FIELD_REQUEST:
        case UPDATE_FIELD_FAILURE:
            return Object.assign({}, state, {
                [action.lang]: actual_pages(state[action.lang], action)
            });
        default:
            return state;
    }
}


// function lang_pages(state = {}, action) {
//     console.debug(state);
//     console.debug(action);
//     switch (action.type) {
//         case FETCH_PAGES_SUCCESS:
//         case FETCH_PAGES_REQUEST:
//         case FETCH_PAGES_FAILURE:
//         case UPDATE_FIELD_SUCCESS:
//         case UPDATE_FIELD_REQUEST:
//         case UPDATE_FIELD_FAILURE:
//             return Object.assign({}, state, {
//                 [action.tag]: actual_pages(state[action.tag], action)
//             });
//         default:
//             return state;
//     }
// }


function actual_pages(state = {}, action) {
    // console.debug(action);
    // console.debug(state);
    switch (action.type) {
        case FETCH_PAGES_SUCCESS:
            // console.debug('SUCCESS SUCCESS SUCCESS');
            // console.debug(action.pages);
            let page_items = chain(action.pages)
                .map(page => {
                    // console.debug(page.page_number);
                    return [page.PageID, page];
                })
                .fromPairs()
                // .sortBy('page_number')
                .value();
            // console.debug(page_items);
            return Object.assign({}, state, page_items);
            // return page_items;

        case UPDATE_FIELD_SUCCESS:
        case UPDATE_FIELD_REQUEST:
        case UPDATE_FIELD_FAILURE:
            // console.debug(action.type);
            // let updated_items = Object.assign({}, state.items, {
            //     [action.pageid]: items(state[action.pageid], action)
            // });
            // console.debug(updated_items);

            return Object.assign({}, state, {
                [action.pageid]: items(state[action.pageid], action)
            })
        case FETCH_PAGES_REQUEST:
        case FETCH_PAGES_FAILURE:
        default:
            return state;
    }
}

function items(state = {}, action) {
    // console.debug(state);
    switch (action.type) {
        case UPDATE_FIELD_SUCCESS:
            let update = {};
            update[action.fieldName] = action.fieldVal;
            // console.debug(update);

            return Object.assign({}, state, update);
        case UPDATE_FIELD_FAILURE:
            // console.debug(action.ex);
        case UPDATE_FIELD_REQUEST:
        default: 
            return state;
    }
}

function pages_by_tag(state = {}, action) {
    switch (action.type) {
        case FETCH_PAGES_SUCCESS:
        case FETCH_PAGES_REQUEST:
        case FETCH_PAGES_FAILURE:
            return Object.assign({}, state, {
                [action.lang]: lang_pages_by_tag(state[action.lang], action)
            });
        default:
            return state;
    }
}

function lang_pages_by_tag(state = {}, action) {
    switch (action.type) {
        case FETCH_PAGES_SUCCESS:
        case FETCH_PAGES_REQUEST:
        case FETCH_PAGES_FAILURE:
            return Object.assign({}, state, {
                [action.tag]: actual_tag_pages(state[action.tag], action)
            });
        default:
            return state;
    }
}

function actual_tag_pages(state = {
    isFetching: false,
    items: {},
    lastUpdated: null
}, action) {
    switch (action.type) {
        case FETCH_PAGES_SUCCESS:
            // console.debug(action.pages);
            let page_items = map(action.pages, 'PageID');
            // console.debug(page_items);

            return Object.assign({}, state, {
                isFetching: false,
                items: page_items,
                lastUpdated: action.receivedAt
            });
        case FETCH_PAGES_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_PAGES_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state, {
                isFetching: false
            })
        default:
            return state;
    }
}







// function topic_pages(state = {}, action) {
//     // console.debug('topic_pages: ' + action.topic);
//     // console.debug(state[action.topic])
//     // console.debug('action.type: ' + action.type);
//     // console.debug(state);
//     switch (action.type) {
//         case FETCH_PAGES_SUCCESS:
//         case FETCH_PAGES_REQUEST:
//         case FETCH_PAGES_FAILURE:
//         case UPDATE_FIELD_SUCCESS:
//         case UPDATE_FIELD_REQUEST:
//         case UPDATE_FIELD_FAILURE:
//             return Object.assign({}, state, {
//                 [action.lang]: lang_pages(state[action.lang], action)
//             });
//         default:
//             return state;
//     }
// }


function editor_content(state = {
    isFetching: false,
    content: []
}, action) {
    switch (action.type) {
        case FETCH_EDITOR_CONTENT_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                content: action.content,
                lastUpdated: action.receivedAt
            });
        case FETCH_EDITOR_CONTENT_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_EDITOR_CONTENT_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

function statiResultToObj(result) {
    return chain(result).map((stati, index) => [stati.status_val, stati.status_desc]).fromPairs().value();
}

function peopleResultToArr(result) {
    return chain(result).map((person, index) => [person.userstr, person.userstr]).fromPairs().value();
}

function constants(state = {
    langs: LangOptions,
    modes: ModeFilters,
    status_val: {},
    problem_bits: {},
    people: [],
    user: ''
}, action) {
    switch (action.type) {
        case FETCH_STATUS_VALS_SUCCESS:
            return Object.assign({}, state, {
                status_val: statiResultToObj(action.vals)
            });

        case FETCH_PROBLEM_BITS_SUCCESS:
            return Object.assign({}, state, {
                problem_bits: statiResultToObj(action.vals)
            });

        case FETCH_PEOPLE_SUCCESS:
            return Object.assign({}, state, {
                people: peopleResultToArr(action.people)
            });

        case SET_USER:
            return Object.assign({}, state, {
                user: action.user
            })

        case FETCH_STATUS_VALS_FAILURE:
        case FETCH_PROBLEM_BITS_FAILURE:
        case FETCH_PEOPLE_FAILURE:
            console.debug(action.ex);
        
        case FETCH_STATUS_VALS_REQUEST:
        case FETCH_PROBLEM_BITS_REQUEST:
        case FETCH_PEOPLE_REQUEST:
        default:
            return state;
    }
}

const qaApp = combineReducers({
    filters,
    pages_by_tag,
    pages,
    editor_content,
    constants
});

// console.log(qaApp);

export default qaApp;


/*
{
    filters: {
        lang: 'EN',
        mode: 'QA',
        unit: 'KT',
        course: 'aplmath',
        lesson: 'amath4',
        topic: '01-money-time-and-quantity',
        topic_slug: 'aplmath_amath4_01-money-time-and-quantity'
    },
    pages_by_tag: {
        'EN': {
            T1007: {
                isFetching: false,
                items: [5,6,7,8],
                lastUpdated: 123545
            },
            L105: {
                isFetching: false,
                items: [...],
                lastUpdated: 123455
            }
        }
    }
    pages: {
        'EN': {
            5: {
                ...
            },
            6: {
                ...
            }
        }
        'ES': {
            ...
        }
    },
    editor_content: {
        isFetching: false,
        content: []
    }
}
*/