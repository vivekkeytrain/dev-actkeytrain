import { Component, PropTypes } from 'react';
import PageTableHeader from './PageTableHeader';
import PageRow from './PageRow';
import { chain, sortBy, forEach, debounce } from 'lodash';
import classNames from 'classnames';

//  http://babeljs.io/blog/2015/06/07/react-on-es6-plus/
export default class PageList extends Component {
    
    // state = {
    //     refreshTimer: null
    // }

    clearTimer() {
        console.debug(this.state);
        if (this.state && this.state.refreshTimer) {
            window.clearTimeout(this.state.refreshTimer);
            this.setState({
                refreshTimer: null
            })
        }
    }

    // componentWillUpdate() {
    // //     // let node = React.findDOMNode(this);
    //     let node = $(window).get(0);
    // //     console.debug(node);
    //     console.debug(node.scrollTop);
    // //     console.debug(node.offsetHeight);
    // //     console.debug(node.scrollHeight);
    // //     this.scrollPosition = node.scrollTop;
    // }

    componentDidUpdate() {
        if (this.props.selectedRow && this.props.selectedRow != -1 && this['pageRow_' + this.props.selectedRow] && this['pageRow_' + this.props.selectedRow]['row_' + this.props.selectedRow]) {
            //  Check to see if it's in view, if not, scroll
            let elem = $(this['pageRow_' + this.props.selectedRow]['row_' + this.props.selectedRow].getDOMNode());
            let win = $(window);

            let docViewTop = win.scrollTop();
            let docViewBot = docViewTop + win.height();

            let elemTop = elem.offset().top;
            let elemBot = elemTop + elem.height();

            // console.debug(elemBot, docViewBot, elemTop, docViewTop)
            if ((elemBot > docViewBot) || (elemTop < docViewTop))
                elem.get(0).scrollIntoView(false);
        }

        // console.debug(node.scrollTop);
        this.props.refreshSession();
        // console.debug(this.props.session.lastUpdated);

        // // this.clearTimer();

    }

    componentWillUnmount() {
        console.debug('componentWillUnmount');
        // this.clearTimer();
    }
    //<PageRow page={page} stati={this.props.stati} people={this.props.people} user={this.props.user} problemTypes={this.props.problemTypes} lang={this.props.lang} courseTag={this.props.courseTag} lessonId={this.props.lesson_id} session={this.props.session} onUpdatePageField={this.props.onUpdatePageField} onImmediateUpdatePageField={this.props.onImmediateUpdatePageField} key={page.PageID} />

    // const pages = state.pages_by_tag;
    // if (!pages[lang]) return true;

    // const lang_pages = pages[lang];
    // if (!lang_pages[tag]) return true;

    // if (lang_pages[tag].isFetching) {

    render() {
        // console.debug(this.props);
        // console.debug(this.props.pagesFetching);
        let row_count = (this.props.pages) ? this.props.pages.length : 0;

        let classes = classNames({
            'progress-bar': true,
            'active': true,
            'progress-bar-striped': true
        });

        let fetching;
        if (this.props.pagesFetching) {
            fetching =  <div className={classes} style={{width: '100%'}}>
                            <span><strong>Fetching Data, Please Hold ...</strong></span>
                        </div>
        }

        return (
            <div>
                <div>Row Count: {row_count}</div>
                {fetching}
                <Table striped hover collapsed>
                    <PageTableHeader />
                    <tbody>
                    {this.props.pages.map((page, index) =>
                        <PageRow page={page} {...this.props} index={index} key={page.PageID} ref={(ref) => this['pageRow_' + index] = ref} />
                    )}
                    </tbody>
                </Table>
            </div>
        )
    }
}


PageList.state = {
    refreshTimer: null
}
