import { Component } from 'react';
import PageLink from './PageLink';

import { LangOptions } from '../actions';

export default class PageLinks extends Component {
    // handleClick(lang, e) {
    //     e.preventDefault();
    //     this.props.onLessonClick(this.props.lessonID, this.props.courseTag, this.props.page.lesson, lang, this.props.page.PageID);
    // }

    // renderLink(lang) {
    //     // console.debug(this.props);
    //     let url = 'http://dev.run.keytrain.com/objects2/' + this.props.courseTag + '/' + this.props.page.lesson + '/' + lang + '/html5_course.htm#load/' + this.props.session.session_id + '/' + this.props.session.login_session_uid + '/pageid/' + this.props.page.PageID;
    //     // console.debug(url);
    //     let target="_lesson_" + lang

    //     return <a href={url} target={target}>{lang}</a>
    // }

    render() {
        let key = this.props.page.PageID + '_' + 'EN';
        let links = [<PageLink courseTag={this.props.courseTag} lesson={this.props.page.lesson} lang={LangOptions.EN} sessionId={this.props.session.session_id} loginSessionUID={this.props.session.login_session_uid} pageID={this.props.page.PageID} key={key} />]
        // let links = [this.renderLink('EN')];
        if (this.props.lang != LangOptions.EN) {
            key = this.props.page.PageID + '_' + this.props.lang
            links.push(' ');
            links.push(<PageLink courseTag={this.props.courseTag} lesson={this.props.page.lesson} lang={this.props.lang} sessionId={this.props.session.session_id} loginSessionUID={this.props.session.login_session_uid} pageID={this.props.page.PageID} key={key} />);
        }


        return <td>{links}</td>;
    }
}

