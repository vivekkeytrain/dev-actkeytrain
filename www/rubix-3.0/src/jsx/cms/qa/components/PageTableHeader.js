import { Component, PropTypes } from 'react';

export default class PageTableHeader extends Component {
    render() {
        return (
            <thead>
                <tr>
                    <th>PageID</th>
                    <th>#</th>
                    <th>Title</th>
                    <th>Link</th>
                    <th>Fields</th>
                    {/*<th>Reviewed By</th>
                    <th>Status</th>
                    <th>Problem</th>
                    <th>Problem Desc</th>
                    <th>Problem Desc 2</th>
                    <th>Solution</th>
                    <th>Details</th>
                    <th>Fix</th>
                    <th>Assigned To</th>
                    <th>Fixed By</th>*/}
                </tr>
            </thead>
        )
    }
}