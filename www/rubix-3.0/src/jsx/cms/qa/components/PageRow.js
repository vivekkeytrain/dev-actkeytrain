import { Component, PropTypes } from 'react';
import { map } from 'lodash';
import PageLinks from './PageLinks';
// import PageFieldSelect from './PageFieldSelect';
// import PageTextField from './PageTextField';
import EditableFields from './EditableFields';
import ReadOnlyFields from './ReadOnlyFields';

import classNames from 'classnames';

export default class PageRow extends Component {
    // onFieldChange(page_id, name, value) {
    //     // console.debug(arguments);
    //     // // console.debug(this.props.page);
    //     // console.debug(this);
    //     // console.debug(this.props);
    //     this.props.onUpdatePageField(page_id, name, value);
    // }

    // onImmediateFieldChange(page_id, name, value) {
    //     // console.debug(arguments);
    //     this.props.onImmediateUpdatePageField(page_id, name, value);
    // }

    // genKey(prefix) {
    //     return prefix + '_' + this.props.page.PageID;
    // }

    // renderPageFieldSelect(name, set) {
    //     // console.debug(set);
    //     return (
    //         <select name={name} value={this.props.page[name]} onChange={this.onFieldChange.bind(this, name)}>
    //             <option value="0"></option>
    //             {map(set, (stati, val) =>
    //                 <option value={val}>{stati}</option>
    //             )}
    //         </select>
    //     )
    // }

    // renderPersonSelect(name) {
    //     return (
    //         <select name={name} value={this.props.page[name]} onChange={this.onFieldChange.bind(this, name)}>
    //             <option value="0"></option>
    //             {this.props.people.map((person, index) =>
    //                 <option value={person}>{person}</option>
    //             )}
    //         </select>
    //     )
    // }

    // renderTextField(name) {
    //     return (
    //         <Input sm type="text" name={name} value={this.props.page[name]} onChange={this.onImmediateFieldChange.bind(this, name)} onBlur={this.onFieldChange.bind(this, name)} placeholder='blah' />
    //     )
    // }

    // renderEmptyTD(ind) {
    //     let key = this.genKey('td_' + ind);
    //     return <td key={key} />
    // }

/* ***************************************************** */
 

      renderEditable() {
        //$('.xeditable').editable({
        //  mode: 'inline'
        //});

        $('#address').editable({
          mode: 'popup',
          url: '/xeditable/address',
          value: {
            city: 'Moscow',
            street: 'Lenina',
            building: '12'
          },
          validate: function(value) {
            if(value.city == '') return 'city is required!';
          },
          display: function(value) {
            if(!value) {
              $(this).empty();
              return;
            }
            //var html = '<b>' + $('<div>').text(value.city).html() + '</b>, ' + $('<div>').text(value.street).html() + ' st., bld. ' + $('<div>').text(value.building).html();
            var html = '<b>' + $('<div>').text(value.city).html() + '...</b>';
            $(this).html(html);
          }
        });

      }

      componentWillMount() {


/**
Address2 editable input.
Internally value stored as {city: "Moscow", street: "Lenina", building: "15"}

@class address2
@extends abstractinput
@final
@example
<a href="#" id="address2" data-type="address2" data-pk="1">awesome</a>
<script>
$(function(){
    $('#address2').editable({
        url: '/post',
        title: 'Enter city, street and building #',
        value: {
            city: "Moscow", 
            street: "Lenina", 
            building: "15"
        }
    });
});
</script>
**/
(function ($) {
    "use strict";
    
    var Address2 = function (options) {
        this.init('address2', options, Address2.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(Address2, $.fn.editabletypes.abstractinput);

    $.extend(Address2.prototype, {
        /**
        Renders input from tpl

        @method render() 
        **/        
        render: function() {
           this.$input = this.$tpl.find('input');
        },
        
        /**
        Default method to show value in element. Can be overwritten by display option.
        
        @method value2html(value, element) 
        **/
        value2html: function(value, element) {
            if(!value) {
                $(element).empty();
                return; 
            }
            var html = $('<div>').text(value.city).html() + ', ' + $('<div>').text(value.street).html() + ' st., bld. ' + $('<div>').text(value.building).html();
            $(element).html(html); 
        },
        
        /**
        Gets value from element's html
        
        @method html2value(html) 
        **/        
        html2value: function(html) {        
          /*
            you may write parsing method to get value by element's html
            e.g. "Moscow, st. Lenina, bld. 15" => {city: "Moscow", street: "Lenina", building: "15"}
            but for complex structures it's not recommended.
            Better set value directly via javascript, e.g. 
            editable({
                value: {
                    city: "Moscow", 
                    street: "Lenina", 
                    building: "15"
                }
            });
          */ 
          return null;  
        },
      
       /**
        Converts value to string. 
        It is used in internal comparing (not for sending to server).
        
        @method value2str(value)  
       **/
       value2str: function(value) {
           var str = '';
           if(value) {
               for(var k in value) {
                   str = str + k + ':' + value[k] + ';';  
               }
           }
           return str;
       }, 
       
       /*
        Converts string to value. Used for reading value from 'data-value' attribute.
        
        @method str2value(str)  
       */
       str2value: function(str) {
           /*
           this is mainly for parsing value defined in data-value attribute. 
           If you will always set value by javascript, no need to overwrite it
           */
           return str;
       },                
       
       /**
        Sets value of input.
        
        @method value2input(value) 
        @param {mixed} value
       **/         
       value2input: function(value) {
           if(!value) {
             return;
           }
           this.$input.filter('[name="city"]').val(value.city);
           this.$input.filter('[name="street"]').val(value.street);
           this.$input.filter('[name="building"]').val(value.building);
       },       
       
       /**
        Returns value of input.
        
        @method input2value() 
       **/          
       input2value: function() { 
           return {
              city: this.$input.filter('[name="city"]').val(), 
              street: this.$input.filter('[name="street"]').val(), 
              building: this.$input.filter('[name="building"]').val()
           };
       },        
       
        /**
        Activates input: sets focus on the first field.
        
        @method activate() 
       **/        
       activate: function() {
            this.$input.filter('[name="city"]').focus();
       },  
       
       /**
        Attaches handler to submit form in case of 'showbuttons=false' mode
        
        @method autosubmit() 
       **/       
       autosubmit: function() {
           this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
           });
       }       
    });

    Address2.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div class="editable-address"><label><span>Cityz: </span><input type="text" name="city" class="input-sm form-control"></label></div>'+
             '<div class="editable-address"><label><span>Streeeet: </span><input type="text" name="street" class="input-sm form-control"></label></div>'+
             '<div class="editable-address"><label><span>Building No.: </span><input type="text" name="building" class="input-sm form-control"></label></div>',
             
        inputclass: ''
    });

    $.fn.editabletypes.address2 = Address2;

}(window.jQuery));




      }

      // componentDidMount() {
      //   this.renderEditable();
      // }

/* ***************************************************** */


    render() {
        // console.debug(this.props.page);

        // let url = 'http://es.run.keytrain.com/objects/' + this.props.courseTag + '/' + this.props.page.lesson + '/en/html5_course.htm#pageid/' + this.props.page.PageID;
        // console.debug(url);
        // let links = [<a href={url} target="_lesson">EN</a>]

        // if (this.props.lang != 'EN') {
        //     url = 'http://es.run.keytrain.com/objects/' + this.props.courseTag + '/' + this.props.page.lesson + '/' + this.props.lang + '/html5_course.htm#pageid/' + this.props.page.PageID
        //     links.push(' ');
        //     links.push(<a href={url} target="_lesson">{this.props.lang}</a>)
        // }

        let classes = classNames({
            warning: (this.props.selectedRow == this.props.index)
        });

        return (
            <tr onClick={this.props.onRowSelected.bind(this, this.props.index)} className={classes} ref={(ref) => this["row_" + this.props.index] = ref}>
                <td>{this.props.page.PageID}</td>
                <td>{this.props.page.pageno}</td>
                <td>{this.props.page.page_title}</td>
                <PageLinks {...this.props} />
                <td>{(this.props.selectedRow == this.props.index) ? <EditableFields {...this.props} /> : <ReadOnlyFields {...this.props} />}</td>
                {/* <td>
                    <a href='#' key={this.props.index} id='address' data-type='address2' data-pk='1' data-title='Please, fill address'></a>
                </td> */}
            </tr>
        )
    }
}
























