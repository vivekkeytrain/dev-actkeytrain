import { Component } from 'react';

import PageFieldSelect from './PageFieldSelect';
import PageTextField from './PageTextField';

export default class EditableFields extends Component {
    onFieldChange(page_id, name, value) {
        // console.debug(arguments);
        // // console.debug(this.props.page);
        // console.debug(this);
        // console.debug(this.props);
        this.props.onUpdatePageField(page_id, name, value);
    }

    onImmediateFieldChange(page_id, name, value) {
        // console.debug(arguments);
        this.props.onImmediateUpdatePageField(page_id, name, value);
    }

    genKey(prefix) {
        return prefix + '_' + this.props.page.PageID;
    }

    render() {
        return (
            <Container style={{overflow:'auto'}}>
                <Grid>
                    <Row>
                        <Col sm={3}>
                            <BLabel>Reviewed By</BLabel>
                            <PageFieldSelect name="reviewed_by" set={this.props.people} defaultVal={this.props.user} page={this.props.page} onImmediateFieldChange={(page_id, name, value) => this.onImmediateFieldChange(page_id, name, value)} onFieldChange={(page_id, name, value) => this.onFieldChange(page_id, name, value)} key={this.genKey('reviewed_by')} />
                        </Col>
                        <Col sm={3}>
                            <BLabel>Status</BLabel>
                            <PageFieldSelect name="status_val" set={this.props.stati} page={this.props.page} onFieldChange={(page_id, name, value) => this.onFieldChange(page_id, name, value)} key={this.genKey('status_val')} />
                        </Col>
                        {(() => {
                            if (this.props.page.status_val > 1) {
                                //  Only show the problem bits if status isn't perfect
                                return (
                                    <Col sm={3}>
                                        <BLabel>Problem</BLabel>
                                        <PageFieldSelect name="problem_bits" set={this.props.problemTypes} page={this.props.page} onFieldChange={(page_id, name, value) => this.onFieldChange(page_id, name, value)} key={this.genKey('problem_bits')} />
                                    </Col>
                                )
                            }
                        })()}
                        {(() => {
                            if (this.props.page.status_val > 1 && this.props.page.problem_bits > 0) {
                                //  Only show problem text field if problem_bits is set
                                return (
                                    <Col sm={3}>
                                        <BLabel>Problem Desc</BLabel>
                                        <PageTextField name="problem1_desc" page={this.props.page} onImmediateFieldChange={(page_id, name, value) => this.onImmediateFieldChange(page_id, name, value)} onFieldChange={(page_id, name, value) => this.onFieldChange(page_id, name, value)} key={this.genKey('problem1_desc')} />
                                    </Col>
                                );
                            } 
                        })()}
                        {(() => {
                            if (this.props.page.status_val > 1 && this.props.page.problem1_desc != '' && this.props.page.problem1_desc != null) {
                                //  Only show the second problem desc field if the first has data
                                return (
                                    <Col sm={3}>
                                        <BLabel>Problem Desc 2</BLabel>
                                        <PageTextField name="problem2_desc" page={this.props.page} onImmediateFieldChange={(page_id, name, value) => this.onImmediateFieldChange(page_id, name, value)} onFieldChange={(page_id, name, value) => this.onFieldChange(page_id, name, value)} key={this.genKey('problem2_desc')} />
                                    </Col>
                                );
                            } 
                        })()}
                        {(() => {
                            if (this.props.page.status_val > 1 && this.props.page.problem_bits > 0) {
                                //  Don't need the solution fields if we don't have a problem
                                return [
                                    <Col sm={3} key={this.genKey('solution_desc')}>
                                        <BLabel>Solution</BLabel>
                                        <PageTextField name="solution_desc" page={this.props.page} onImmediateFieldChange={(page_id, name, value) => this.onImmediateFieldChange(page_id, name, value)} onFieldChange={(page_id, name, value) => this.onFieldChange(page_id, name, value)} />
                                    </Col>,
                                    <Col sm={3} key={this.genKey('details_desc')}>
                                        <BLabel>Details</BLabel>
                                        <PageTextField name="details_desc" page={this.props.page} onImmediateFieldChange={(page_id, name, value) => this.onImmediateFieldChange(page_id, name, value)} onFieldChange={(page_id, name, value) => this.onFieldChange(page_id, name, value)} />
                                    </Col>,
                                    <Col sm={3} key={this.genKey('fix_desc')}>
                                        <BLabel>Fix</BLabel>
                                        <PageTextField name="fix_desc" page={this.props.page} onImmediateFieldChange={(page_id, name, value) => this.onImmediateFieldChange(page_id, name, value)} onFieldChange={(page_id, name, value) => this.onFieldChange(page_id, name, value)} />
                                    </Col>
                                ];
                            }
                        })()}
                        {(() => {
                            if (this.props.page.status_val > 1 && this.props.page.problem_bits > 0) {
                                //  Don't need the assigned_to and fixed_by if we don't have a problem
                                return [
                                    <Col sm={3} key={this.genKey('assigned_to')}>
                                        <BLabel>Assigned To</BLabel>
                                        <PageFieldSelect name="assigned_to" set={this.props.people} page={this.props.page} onFieldChange={(page_id, name, value) => this.onFieldChange(page_id, name, value)} />
                                    </Col>,
                                    <Col sm={3} key={this.genKey('fixed_by')}>
                                        <BLabel>Fixed By</BLabel>
                                        <PageFieldSelect name="fixed_by" set={this.props.people} page={this.props.page} onFieldChange={(page_id, name, value) => this.onFieldChange(page_id, name, value)} />
                                    </Col>
                                ]
                            }
                        })()}
                    </Row>
                </Grid>
            </Container>
        )
    }
}

