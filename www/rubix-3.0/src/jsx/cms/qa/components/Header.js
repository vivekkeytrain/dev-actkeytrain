import { Component } from 'react';
import classNames from 'classnames';
import { map } from 'lodash';

import HeaderButton from './HeaderButton';

export default class Header extends Component {
    renderButton(lang) {
        let classes = classNames({
            'btn': true,
            'btn-primary': true,
            'active': (lang == this.props.lang)
        });

        return <button type="button" className={classes} onClick={() => this.props.onSetLang(lang) }>{lang}</button>
    }

    render() {
        // console.log('render header');
        // console.debug(this.props);
        return (
            <Row>
                <Col sm={3}>
                    <BLabel>Language: </BLabel>
                    <ButtonGroup>
                        {map(this.props.langOptions, (lang, key) => 
                            <HeaderButton val={lang} label={key} active={this.props.lang} clickHandler={this.props.onSetLang} key={lang} />
                        )}
                    </ButtonGroup>
                </Col>
                <Col sm={4}>
                    <BLabel>Mode: </BLabel>
                    <ButtonGroup>
                        {map(this.props.modeFilters, (mode, key) => 
                            <HeaderButton val={mode} label={mode} active={this.props.mode} clickHandler={this.props.onSetMode} key={key} />
                        )}
                    </ButtonGroup>
                </Col>
                <Col sm={3}>
                    <Select control name="assigned_to" value={this.props.person} style={{display:'inline'}} onChange={this.props.onSetPeopleFilter}>
                        <option value="">-- Select Assigned To --</option>
                        {map(this.props.people, (person) => 
                            <option value={person}>{person}</option>
                        )}
                    </Select>
                </Col>
            </Row>
        )
    }
}

