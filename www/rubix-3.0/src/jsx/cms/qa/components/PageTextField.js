import { Component } from 'react';
import { map } from 'lodash';

export default class PageTextField extends Component {
    componentDidMount() {
        this.updateVal(this.props);
    }

    componentWillReceiveProps(props) {
        this.updateVal(props);
    }

    updateVal(props) {
        let rfield = this[`_${props.name}`];
        let dfield = React.findDOMNode(rfield);

        let val = props.page[props.name];

        if (val) 
            dfield.value = val;
        else
            dfield.value = '';
    }

    onImmediateFieldChange(name, e) {
        this.props.onImmediateFieldChange(this.props.page.PageID, name, e.target.value);
    }

    onFieldChange(name, e) {
        this.props.onImmediateFieldChange(this.props.page.PageID, name, e.target.value);
        this.props.onFieldChange(this.props.page.PageID, name, e.target.value);
    }
//{/*<Input sm width={500} type="text" name={this.props.name} value={this.props.page[this.props.name]} onChange={this.onImmediateFieldChange.bind(this, this.props.name)} onBlur={this.onFieldChange.bind(this, this.props.name)} />*/}
    render() {
        console.debug(this.props);

        return (
            
            <Textarea rows='3' name={this.props.name} defaultValue={this.props.page[this.props.name]} onBlur={this.onFieldChange.bind(this, this.props.name)} ref={(r) => this['_' + this.props.name] = r} />
        )
    }
}

