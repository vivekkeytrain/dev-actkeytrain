// import { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
// import { 
//     setLang, setUnit, setCourse, setLesson, setTopic, setMode,
//     LangOptions, ModeFilters, Stati, People, ProblemTypes,
//     fetchPages, updatePageField
// } from '../actions';

import { 
    LangOptions, ModeFilters,
    setUnit, setCourse, setLesson, setTopic, updatePageField, immediateUpdatePageField, setLang, setMode, setUser, setSelectedRow,
    fetchEditorContent, fetchPagesIfNeeded, fetchGenStati, fetchPeople, doLoginIfNeeded, closeMessage, setPeopleFilter
} from '../actions';

import { filteredContentSelector } from '../selectors';
import Header from '../components/Header';
import ContentTree from 'common/ContentTree';
import MessageCenter from 'common/MessageCenter';
import PageList from '../components/PageList';

import { map, chain, fromPairs, findIndex } from 'lodash';


class App extends React.Component {
    // state = {
    //     refreshTimer: null
    // }

    clearTimer() {
        if (this.state && this.state.refreshTimer) {
            window.clearTimeout(this.state.refreshTimer);
            this.setState({
                refreshTimer: null
            });
        }
    }

    componentDidMount() {
        console.log('componentDidMount');
        let { dispatch, lang, unit, course, lesson, topic } = this.props;
        dispatch(fetchEditorContent())
            .then(() => {
                /*  Check querystring for unit/course/lesson/topic data to auto-populate    */
                let q = (window && window.location.search.substring(1)) || '';
                // console.debug(q);
                if (q) {
                    let vars = q.split('&');
                    let query = chain(vars)
                        .map((p) => {
                            let pairs = p.split('=');
                            return [pairs[0], parseInt(pairs[1], 10)];
                        })
                        .fromPairs()
                        .value();
                    // console.debug(query);

                    if (query.unit_id) dispatch(setUnit(query.unit_id));
                    // console.debug('unit set', query.unit_id);
                    if (query.course_id) dispatch(setCourse(query.course_id));
                    // console.debug('course set', query.course_id);
                    if (query.lesson_id) dispatch(setLesson(query.lesson_id));
                    // console.debug('lesson set', query.lesson_id);
                    if (query.topic_id) dispatch(setTopic(query.topic_id));
                    // console.debug('topic_set', query.topic_id);
                    if (query.page_id) {
                        // Gotta figure out which row the page is on and setSelectedRow
                        dispatch(fetchPagesIfNeeded(query.unit_id, query.course_id, query.lesson_id, query.topic_id, lang))
                            .then(() => {
                                let row_ind = findIndex(this.props.topic_pages, {'PageID': query.page_id});
                                dispatch(setSelectedRow(row_ind));
                                // console.debug(this.pageList['pageRow_' + row_ind]['row_' + row_ind].getDOMNode());
                                this.pageList['pageRow_' + row_ind]['row_' + row_ind].getDOMNode().scrollIntoView(false);
                            })
                    } else {
                        dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang));
                    }
                } else {
                    unit = parseInt(localStorage.getItem('content_unit'), 10);
                    course = parseInt(localStorage.getItem('content_course'), 10);
                    lesson = parseInt(localStorage.getItem('content_lesson'), 10);
                    topic = parseInt(localStorage.getItem('content_topic'), 10);

                    if (unit) dispatch(setUnit(unit));
                    if (course) dispatch(setCourse(course));
                    if (lesson) dispatch(setLesson(lesson));
                    if (topic) dispatch(setTopic(topic));

                    dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang));
                }
            });
        dispatch(fetchGenStati('status_val'));
        dispatch(fetchGenStati('problem_bits'));
        dispatch(fetchPeople(lang));

        /*  Look for logged in user */
        /*  window.auth is inserted by nginx on the server and hardcoded for dev    */
        if (!window.auth) window.auth = 'Basic YmVuOmJlbg=='

        let auth = atob(window.auth.replace('Basic ', ''));
        // console.debug(auth);
        let auth_parts = auth.split(':');
        dispatch(setUser(auth_parts[0]));

        
            
    }

    componentWillUnmount() {
        this.clearTimer();
    }

    // handleTopicClick(topic) {
    //     console.debug('handleTopicClick');
    //     console.debug(arguments);
    //     const { dispatch, lang } = this.props; 
    //     dispatch(setTopic(topic));
    //     dispatch(fetchPages(topic, lang));
    // }

    render() {
        //  Injected by connect() call:
        const { dispatch, content, unit, units, course, courses, course_tag, lesson, lessons, topic, topics, lang, mode, selected_row, unit_pages, course_pages, topic_pages, lesson_pages, pages_fetching, constants, session, messages, assigned_to } = this.props;
        // const handleTopicClick = this.handleTopicClick;
        // const { dispatch, visiblePages, modeFilter, lang, unit, course, lesson, topic, mode, units, courses, lessons, topics } = this.props
        // const { dispatch, }
        // console.debug(this.props);

        let handleUnitClick = (unit) => {
            dispatch(setSelectedRow(-1));
            dispatch(setUnit(unit));
            dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang));
        }

        let handleCourseClick = (course) => {
            dispatch(setSelectedRow(-1));
            dispatch(setCourse(course));
            dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang));
        }

        let handleLessonClick = (lesson) => {
            dispatch(setSelectedRow(-1));
            dispatch(setLesson(lesson));
            dispatch(doLoginIfNeeded(lesson, lang));
            dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang));
        }

        let handleTopicClick = (topic) => {
            dispatch(setSelectedRow(-1));
            // console.debug('handleTopicClick');
            // console.debug(topic);
            dispatch(setTopic(topic));
            dispatch(fetchPagesIfNeeded(unit, course, lesson, topic, lang));
        }

        let handleUpdatePageField = (pageid, fieldName, fieldVal) => {
            dispatch(updatePageField(pageid, fieldName, fieldVal, lang))
        }

        let handleImmediateUpdatePageField = (pageid, fieldName, fieldVal) => {
            // console.debug(pageid, fieldName, fieldVal);
            dispatch(immediateUpdatePageField(pageid, fieldName, fieldVal, lang))
        }

        let handleLangClick = (lang) => {
            dispatch(setSelectedRow(-1));
            dispatch(setLang(lang));
            dispatch(fetchPagesIfNeeded(topic, course, lesson, topic, lang));
            dispatch(fetchPeople(lang));
            dispatch(doLoginIfNeeded(lesson, lang));
        }

        let handleModeClick = (mode) => {
            // console.debug(mode);
            dispatch(setMode(mode));
        }

        let refreshSession = () => {
            if (this.state && !this.state.refreshTimer && session && session.login_session_uid) {
                let time_left = (1000 * 60 * 15) - (Date.now() - session.lastUpdated) + 1000; //   15 minutes from last update (plus 1 second so it's after the doLoginIfNeeded check)
                // console.debug(time_left);

                let timer = window.setTimeout(() => {
                    dispatch(doLoginIfNeeded(lesson, lang));
                    this.clearTimer();
                    refreshSession();
                }, time_left);
                this.setState({
                    refreshTimer: timer
                });
            }
            // dispatch(doLoginIfNeeded(lesson, lang));
        }

        let handleMessageClose = (index) => {
            dispatch(closeMessage(index));
        }

        // let handlePageLinkClick = (course_tag, lesson_id, lang, session_id, login_session_uid, page_id) => {
        //     dispatch(doLoginIfNeeded(lesson_id, lang));
        // }

        // let handleLessonClick = (lesson_id, course_tag, lesson_tag, lang, page_id) => {
        //     dispatch(doLoginLesson(lesson_id, course_tag, lesson_tag, lang, page_id));
        // }

        // let handleTopicClick = (topic, e) => {
        //     console.debug('handleTopicClick');
        //     console.debug(topic);
        // }

        let pages;
        // if (topic) {
        //     //  If we have a topic then we'll always use the topic pages
        //     pages = topic_pages;
        // } else if (lesson && lesson_pages) {
        //     //  If we have a lesson and lesson pages that means we're in the proper mode, use lesson_pages
        //     pages = lesson_pages;
        // } else {
        //     pages = [];
        // }

        if (topic && topic !== -1 && topic_pages) {
            pages = topic_pages;
        } else if (lesson && lesson !== -1 && lesson_pages) {
            pages = lesson_pages;
        } else if (course && course !== -1 && course_pages) {
            pages = course_pages;
        } else if (unit && unit_pages) {
            pages = unit_pages;
        } else {
            pages = [];
        }

        // console.debug(pages);

        let containerStyle = {
            overflow: 'auto'
        }

        return (
            


          <Container id='body' style={containerStyle}>
            <Grid>
              <Row>
                <Col xs={12}>
                  {/* <PanelContainer plain > */}
                  <PanelContainer plain controlStyles='bg-green fg-white'>
                    <Panel style={{background: 'white'}}>


                        <PanelHeader className='bg-green fg-white' style={{margin: 0}}>
                            <Grid>
                                <Row>
                                    <Col xs={12}>
                                        <p>

                                        <ContentTree
                                            unit={unit}
                                            course={course}
                                            lesson={lesson}
                                            topic={topic}
                                            units={units}
                                            courses={courses}
                                            lessons={lessons}
                                            topics={topics}
                                            content={content}
                                            onUpdateUnit={unit =>
                                                handleUnitClick(unit)
                                            }
                                            onUpdateCourse={course =>
                                                handleCourseClick(course)
                                            }
                                            onUpdateLesson={lesson =>
                                                handleLessonClick(lesson)
                                            }
                                            onUpdateTopic={topic =>
                                                handleTopicClick(topic)
                                            } />

                                        </p>
                                   </Col>
                                </Row>
                            </Grid>
                        </PanelHeader>


                      <PanelBody>
                        <Grid>
                          <Row>
                            <Col xs={12} style={{padding: 25, overflow: 'auto'}}>
                                <MessageCenter onMessageClose={index => handleMessageClose(index)}>
                                    {messages.map((message, ind) => {
                                        return <div type={message.type} index={ind} visible={message.visible} key={"message_" + ind}>{message.message}</div>
                                    })}
                                </MessageCenter>
                <Header 
                    langOptions={LangOptions}
                    modeFilters={ModeFilters}
                    people={constants.people}
                    person={assigned_to}
                    lang={lang} 
                    onSetLang={lang => 
                        handleLangClick(lang)
                    } 
                    mode={mode}
                    onSetMode={mode =>
                        handleModeClick(mode) 
                    } 
                    onSetPeopleFilter={e => 
                        dispatch(setPeopleFilter(e.target.value))
                    } />

                <PageList 
                    ref={(ref) => this.pageList = ref}
                    pages={pages}
                    stati={constants.status_val}
                    people={constants.people}
                    user={constants.user}
                    problemTypes={constants.problem_bits}
                    lang={lang}
                    courseTag={course_tag}
                    lessonId={lesson}
                    session={session}
                    refreshSession={refreshSession}
                    pagesFetching={pages_fetching}
                    selectedRow={selected_row}
                    onImmediateUpdatePageField={(pageid, fieldName, fieldVal) =>
                        handleImmediateUpdatePageField(pageid, fieldName, fieldVal)
                    }
                    onUpdatePageField={(pageid, fieldName, fieldVal) =>
                        handleUpdatePageField(pageid, fieldName, fieldVal)
                    } 
                    onRowSelected={row_index =>
                        dispatch(setSelectedRow(row_index))
                    } />


                            </Col>
                          </Row>
                        </Grid>
                      </PanelBody>
                    </Panel>
                  </PanelContainer>
                </Col>
              </Row>
            </Grid>
          </Container>


        )
    }
}

App.state = {
    refreshTimer: null
}

// App.propTypes = {
//     visiblePages: PropTypes.arrayOf(PropTypes.shape({

//     }))
// }

// let t = connect(unitsContentSelector);
// console.debug(t);
// t = t(coursesContentSelector);
// console.debug(t);

// export default t;
export default connect(filteredContentSelector)(App);

// export default connect(unitsContentSelector)(coursesContentSelector)(lessonContentSelector)(topicContentSelector)(App)