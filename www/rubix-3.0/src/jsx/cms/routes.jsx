import { Route, Router, IndexRoute, Redirect } from 'react-router';
import BrowserHistory from 'react-router/lib/BrowserHistory';
import HashHistory from 'react-router/lib/HashHistory';

import Blank from 'routes/blank';
import Pagelist from 'routes/pagelist';
import QA from 'routes/qa';
import FindPage from 'routes/findpage';
import Attachments from 'routes/attachments';
import SVGManager from 'routes/svgmanager';
import PageIssues from 'routes/pageissues';

export default (withHistory, onUpdate) => {
  const history = withHistory?
                  (Modernizr.history ?
                    new BrowserHistory
                  : new HashHistory)
                : null;

  return (
    <Router history={history} onUpdate={onUpdate}>
      <Route path='/' component={Pagelist} />
      <Route path='/qa/' component={QA} />
      <Route path='/findpage/' component={FindPage} />
      <Route path='/attachments/' component={Attachments} />
      <Route path='/svgmanager/' component={SVGManager} />
      <Route path='/page-issues/' component={PageIssues} />
    </Router>
  );
};
