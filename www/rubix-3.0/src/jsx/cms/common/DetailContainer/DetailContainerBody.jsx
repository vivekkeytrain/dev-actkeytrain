import React, { Component, PropTypes } from 'react';

export default class DetailContainerBody extends Component {
    bodyClick(e) {
        if (typeof this.props.handleBodyClick === 'function') {
            this.props.handleBodyClick(e);
        }
    }

    render() {
        return (
            <div className="modal-body" onClick={this.bodyClick.bind(this)}>
                <Grid>
                    {this.props.children}
                </Grid>
            </div>
        )
    }
}

// DetailContainerBody.propTypes = {
//     handleBodyClick: PropTypes.func
// }