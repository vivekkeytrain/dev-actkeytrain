import { Component, PropTypes } from 'react';
// import { isUndefined } from 'lodash';


import ContentTreeSelect from './ContentTreeSelect';





/*******************************************************
 2015-11-05 JNN
 Some code I found that might be useful to base the 
 build of Selects on.
*******************************************************/

/*

var MyParent = React.createClass({
    getInitialState: function() {
        return {
            childSelectValue: undefined
        }
    },
    changeHandler: function(e) {
        this.setState({
            childSelectValue: e.target.value
        })
    },
    render: function() {
        return (
            <div>
                <MySelect 
                    url="http://foo.bar"
                    value={this.state.childSelectValue}
                    onChange={this.changeHandler} 
                />
            </div>
        )
    }
});

var MySelect = React.createClass({
    propTypes: {
        url: React.PropTypes.string.isRequired
    },
    getInitialState: function() {
        return {
            options: []
        }
    },
    componentDidMount: function() {
        // get your data
        $.ajax({
            url: this.props.url,
            success: this.successHandler
        })
    },
    successHandler: function(data) {
        // assuming data is an array of {name: "foo", value: "bar"}
        for (var i = 0; i < data.length; i++) {
            var option = data[i];
            this.state.options.push(
                <option key={i} value={option.value}>{option.name}</option>
            );
        }
        this.forceUpdate();
    },
    render: function() {
        return this.transferPropsTo(
            <select>{this.state.options}</select>
        )
    }
});

*/







export default class ContentTree extends Component {
    // handleClick(clickHandler, option, e) {
    //     console.debug(arguments);
    //     e.preventDefault();
    //     clickHandler(option);
    // }

    // handleChange(clickHandler, e) {
    //     clickHandler(parseInt(e.target.value, 10));
    // }

    // renderLI(label, currentVal, option, index, clickHandler) {
    //     // console.debug(label, currentVal, option, index, clickHandler)
    //     let key = label + '_' + index;

    //     let styles = {};

    //     //if (option.id == currentVal) {
    //     //    styles = {
    //     //        textDecoration: 'underline',
    //     //        fontWeight: 'bold',
    //     //        color: '#000000'
    //     //    }
    //     //}

    //     //return <li key={key}><a href='#' style={styles} onClick={this.handleClick.bind(this, clickHandler, option.id)}>{option.display}</a></li>

    //     return <option key={key} value={option.id}>{option.display}</option>

    //     //onClick={this.handleClick.bind(this, clickHandler, option.id)}

    // }



    // renderTree(label, currentVal, options, clickHandler) {

    //     return (



    //         <FormGroup>
    //         <Label sm={2} htmlFor={label} control>{label}</Label>
    //         <Col sm={10}>
    //             <Select control id={label} onChange={this.handleChange.bind(this, clickHandler)} value={currentVal}>
    //             <option value="" >-----</option>
    //             {options.map((option, index) =>
    //                 this.renderLI(label, currentVal, option, index, clickHandler)
    //             )}
    //             </Select>
    //         </Col>
    //         </FormGroup>


    //     )
    // }

    render() {
        // console.debug('render ContentTree');
        // console.debug(this.props.units); 
        return (
            <Grid>
            <Row>
            <Col>



            <Grid>
            <Row>
            <Col xs={12} collapseLeft collapseRight>
            
            <Form>
             
                <ContentTreeSelect label={'Unit'} options={this.props.units} currentVal={this.props.unit} changeHandler={this.props.onUpdateUnit} />
                <ContentTreeSelect label={'Course'} options={this.props.courses} currentVal={this.props.course} changeHandler={this.props.onUpdateCourse} />
                <ContentTreeSelect label={'Lesson'} options={this.props.lessons} currentVal={this.props.lesson} changeHandler={this.props.onUpdateLesson} />
                <ContentTreeSelect label={'Topic'} options={this.props.topics} currentVal={this.props.topic} changeHandler={this.props.onUpdateTopic} /> 
            </Form>

            </Col>
            </Row>
            </Grid>


            
            </Col>
            </Row>
            </Grid>
        )
    }
}

ContentTree.propTypes = {
    onUpdateUnit: PropTypes.func.isRequired,
    onUpdateCourse: PropTypes.func.isRequired,
    onUpdateLesson: PropTypes.func.isRequired,
    onUpdateTopic: PropTypes.func.isRequired
}