import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { 
    LangOptions, setUnit, setCourse, setLesson, setTopic, setLang, setSelectedRow, setUser, closeMessage,
    fetchEditorContent, fetchAttachmentsIfNeeded, doLoginIfNeeded, immediateUpdateAttachField, updateAttachField, fetchAttachmentData, fetchPageDetail
} from '../actions';

import { filteredContentSelector, selectCourseTag, selectLessonTag } from '../selectors';
import ContentTree from 'common/ContentTree';
import MessageCenter from 'common/MessageCenter';
import PageList from '../components/PageList';

import { invert, map, chain, fromPairs, findIndex } from 'lodash';


class App extends Component {
    // state = {
    //     refreshTimer: null
    // }

    clearTimer() {
        if (this.state && this.state.refreshTimer) {
            window.clearTimeout(this.state.refreshTimer);
            this.setState({
                refreshTimer: null
            });
        }
    }

    componentDidMount() {
        let { dispatch, lang, unit, course, lesson, topic } = this.props;
        // dispatch(doLogin());
        dispatch(fetchEditorContent())
            .then(() => {
                let q = (window && window.location.search.substring(1)) || '';

                if (q) {
                    let vars = q.split('&');
                    let query = chain(vars)
                        .map((p) => {
                            let pairs = p.split('=');
                            return [pairs[0], parseInt(pairs[1], 10)];
                        })
                        .fromPairs()
                        .value();

                    if (query.unit_id) dispatch(setUnit(query.unit_id));
                    if (query.course_id) dispatch(setCourse(query.course_id));
                    if (query.lesson_id) dispatch(setLesson(query.lesson_id));
                    if (query.topic_id) dispatch(setTopic(query.topic_id));

                    if (query.attach_id) {
                        dispatch(fetchAttachmentsIfNeeded(query.unit_id, query.course_id, query.lesson_id, query.topic_id, lang))
                            .then(() => {
                                let row_ind = findIndex(this.props.topic_attachments, {'attach_id': query.attach_id});
                                dispatch(setSelectedRow(row_ind));
                                // console.debug(this.pageList['pageRow_' + row_ind]['row_' + row_ind].getDOMNode());
                                this.pageList['tableRow_' + row_ind]['row_' + row_ind].getDOMNode().scrollIntoView(false);
                            })
                    } else {
                        dispatch(fetchAttachmentsIfNeeded(unit, course, lesson, topic, lang));
                    }
                } else {
                    unit = parseInt(localStorage.getItem('content_unit'), 10);
                    course = parseInt(localStorage.getItem('content_course'), 10);
                    lesson = parseInt(localStorage.getItem('content_lesson'), 10);
                    topic = parseInt(localStorage.getItem('content_topic'), 10);

                    if (unit) dispatch(setUnit(unit));
                    if (course) dispatch(setCourse(course));
                    if (lesson) dispatch(setLesson(lesson));
                    if (topic) dispatch(setTopic(topic));

                    dispatch(fetchAttachmentsIfNeeded(unit, course, lesson, topic, lang));
                }
            });

        /*  Look for logged in user */
        /*  window.auth is inserted by nginx on the server and hardcoded for dev    */
        if (!window.auth) window.auth = 'Basic YmVuOmJlbg=='

        let auth = atob(window.auth.replace('Basic ', ''));
        // console.debug(auth);
        let auth_parts = auth.split(':');
        dispatch(setUser(auth_parts[0]));

        // dispatch(doLoginIfNeeded(lesson, 'en'));
        // dispatch(doLoginIfNeeded(lesson, 'es'));
    }

    render() {
        //  Injected by connect() call:
        const { dispatch, content, unit, units, course, courses, course_tag, lesson, lessons, lesson_tag, topic, topics, lang, all_attachments, unit_attachments, course_attachments, attachments_fetching, topic_attachments, lesson_attachments, constants, all_sessions, enSession, esSession, selectedRow, displayFieldsHash, editViewDisplayFields, readOnlyFields, editFieldsHash, pages, messages, attachments_list } = this.props;

        let handleUnitClick = (unit) => {
            dispatch(setSelectedRow(-1));
            dispatch(setUnit(unit));
            dispatch(fetchAttachmentsIfNeeded(unit, course, lesson, topic, lang));
        }

        let handleCourseClick = (course) => {
            dispatch(setSelectedRow(-1));
            dispatch(setCourse(course));
            dispatch(fetchAttachmentsIfNeeded(unit, course, lesson, topic, lang));
        }

        let handleLessonClick = (lesson) => {
            dispatch(setSelectedRow(-1));
            dispatch(setLesson(lesson))
            dispatch(doLoginIfNeeded(lesson, LangOptions.EN));
            dispatch(doLoginIfNeeded(lesson, LangOptions.ES));
            dispatch(fetchAttachmentsIfNeeded(unit, course, lesson, topic, lang));
        }

        let handleTopicClick = (topic) => {
            dispatch(setSelectedRow(-1));
            dispatch(setTopic(topic));
            dispatch(fetchAttachmentsIfNeeded(unit, course, lesson, topic, lang));
        }

        let handleLangClick = (lang) => {
            dispatch(setSelectedRow(-1));
            dispatch(setLang(lang_id));
            dispatch(fetchAttachmentsIfNeeded(unit, course, lesson, topic, lang));
        }

        let handlePageLinkClick = (pageid, lang_id) => {
            console.debug('handlePageLinkClick', pageid, lang_id);
            dispatch(fetchPageDetail(pageid))
                .then(() => {
                    console.debug(this.props.pages);
                    let this_page = this.props.pages[pageid];
                    console.debug(this_page);
                    let lesson_id = this_page.lesson_id;
                    dispatch(doLoginIfNeeded(lesson_id, lang_id))
                        .then(() => {
                            console.debug(this.props);
                            let all_sessions = this.props.all_sessions;
                            let lang_code = invert(LangOptions)[lang_id];
                            let session = all_sessions[lang_id][lesson_id];

                            let course_tag = selectCourseTag(this_page.course_id, this.props.courses);
                            let lesson_tag = selectLessonTag(this_page.lesson_id, this.props.lessons);

                            let url = 'http://es.run.keytrain.com/objects2/' + course_tag + '/' + lesson_tag + '/' + lang_code + '/html5_course.htm#load/' + session.session_id + '/' + session.login_session_uid + '/pageid/' + pageid;
                            window.open(url, '_lesson_' + lang_code);
                            // dispatch(openPage(pageid, lang_code));
                        })
                })
            // console.debug(pageid, lang_id);
            // console.debug(all_pages);
            // let this_page = all_pages[1][pageid]; // For now we're only loading english pages, so this_page is en no matter which link clicked
            // console.debug(this_page);
            // let lesson_id = this_page.lesson_id;
            // console.debug(lesson_id);
            // dispatch(doLoginIfNeeded(lesson_id, lang_id))
            //     .then(() => {
            //             let all_sessions = this.props.all_sessions;
            //             let session = all_sessions[lang_id][lesson_id];
            //             let lang_code = invert(LangOptions)[lang_id];
                        
            //             if (!unit) dispatch(setUnit(this_page.unit_id));
            //             if (!course) dispatch(setCourse(this_page.course_id));

            //             console.debug(this.props);

            //             let course_tag = selectCourseTag(this_page.course_id, this.props.courses);
            //             let lesson_tag = selectLessonTag(this_page.lesson_id, this.props.lessons);

            //             let url = 'http://es.run.keytrain.com/objects2/' + course_tag + '/' + lesson_tag + '/' + lang_code + '/html5_course.htm#load/' + session.session_id + '/' + session.login_session_uid + '/pageid/' + pageid;
            //             window.open(url, '_lesson_' + lang_code);
            //             dispatch(openPage(pageid, lang_code));
            //         }
            //     );
        }

        let refreshSession = () => {
            if (this.state && !this.state.refreshTimer) {
                if ((enSession && enSession.login_session_uid)|| (esSession && esSession.login_session_uid)) {
                    let time_left = (1000 * 60 * 15) - (Date.now() - enSession.lastUpdated) + 1000; //   15 minutes from last update (plus 1 second so it's after the doLoginIfNeeded check)
                    // console.debug(time_left);

                    let timer = window.setTimeout(() => {
                        dispatch(doLoginIfNeeded(lesson, LangOptions.EN));
                        dispatch(doLoginIfNeeded(lesson, LangOptions.ES));
                        this.clearTimer();
                        refreshSession();
                    }, time_left);
                    this.setState({
                        refreshTimer: timer
                    });
                }
            }
            // dispatch(doLoginIfNeeded(lesson, lang));
        }

        let handleAttachFieldUpdate = (attach_id, fieldName, fieldVal) => {
            dispatch(updateAttachField(attach_id, fieldName, fieldVal, lang, constants.user))
                .then(() => {
                    dispatch(fetchAttachmentData(attach_id, lang))
                })
        }

        let handleMessageClose = (index) => {
            dispatch(closeMessage(index));
        }

        // let attachments;

        // if (topic && topic !== -1 && topic_attachments) {
        //     attachments = topic_attachments;
        // } else if (lesson && lesson !== -1 && lesson_attachments) {
        //     attachments = lesson_attachments;
        // } else if (course && course !== -1 && course_attachments) {
        //     attachments = course_attachments;
        // } else if (unit && unit_attachments) {
        //     attachments = unit_attachments;
        // } else {
        //     attachments = [];
        // }

        // console.log(attachments);

        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PanelContainer controlStyles='bg-blue fg-white'>
                                <PanelHeader className='bg-blue fg-white' style={{margin: 0}}>
                                    <Grid>
                                        <Row>
                                            <Col xs={12}>
                                                <h3>Attachments</h3>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelHeader>
                                <PanelBody>
                                    <Grid>
                                        <Row>
                                            <Col xs={12} style={{padding: 25}}>
                                                <MessageCenter onMessageClose={index => handleMessageClose(index)}>
                                                    {messages.map((message, ind) => {
                                                        return <div type={message.type} index={ind} visible={message.visible} key={"message_" + ind} >{message.message}</div>
                                                    })}
                                                </MessageCenter>
                <ContentTree
                    unit={unit}
                    course={course}
                    lesson={lesson}
                    topic={topic}
                    units={units}
                    courses={courses}
                    lessons={lessons}
                    topics={topics}
                    content={content}
                    onUpdateUnit={unit =>
                        handleUnitClick(unit)
                    }
                    onUpdateCourse={course =>
                        handleCourseClick(course)
                    }
                    onUpdateLesson={lesson =>
                        handleLessonClick(lesson)
                    }
                    onUpdateTopic={topic =>
                        handleTopicClick(topic)
                    } />
                <PageList 
                    ref={(ref) => this.pageList = ref}
                    attachments={attachments_list}
                    displayFields={constants.displayFields} 
                    displayFieldsHash={displayFieldsHash}
                    editViewDisplayFields={editViewDisplayFields}
                    editFields={constants.editFields}
                    editFieldsHash={editFieldsHash}
                    readOnlyFields={readOnlyFields}
                    selectedRow={selectedRow}
                    courseTag={course_tag}
                    lessonTag={lesson_tag}
                    enSession={enSession}
                    esSession={esSession}
                    refreshSession={refreshSession}
                    attachmentsFetching={attachments_fetching}
                    onRowSelected={row_index => {
                            console.debug(attachments_list);
                            console.debug(attachments_list[row_index]);
                            dispatch(fetchAttachmentData(attachments_list[row_index].attach_id, 1));
                            dispatch(fetchAttachmentData(attachments_list[row_index].attach_id, 2));
                            dispatch(setSelectedRow(row_index));
                        }
                    } 
                    onPageLinkClick={(pageid, lang) => 
                        handlePageLinkClick(pageid, lang)
                    } 
                    onUpdateAttachField={(attach_id, fieldName, fieldVal) =>
                        handleAttachFieldUpdate(attach_id, fieldName, fieldVal)
                    }
                    onImmediateUpdateAttachField={(attach_id, fieldName, fieldVal) =>
                        dispatch(immediateUpdateAttachField(attach_id, fieldName, fieldVal, lang, constants.user))
                    } />
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelBody>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        )
    }
}

export default connect(filteredContentSelector)(App);
