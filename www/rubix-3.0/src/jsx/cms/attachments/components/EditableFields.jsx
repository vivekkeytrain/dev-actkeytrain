import { Component } from 'react';

import EditableField from './EditableField';

export default class EditableFields extends Component {
    render() {
        //  onChange={this.onImmediateFieldChange.bind(this, field.name)}
        return (
            <Container style={{overflow:'auto'}}>
                <Grid>
                    <Row>
                        {
                            this.props.editFields.map((field, ind) => {
                                return (
                                    <Col sm={4} key={field.name + '_' + this.props.index}>
                                        <BLabel>{(field.label) ? field.label : field.name}</BLabel>
                                        <EditableField name={field.name} {...this.props} />
                                    </Col>
                                )
                            })
                        }
                    </Row>
                </Grid>
            </Container>
        )
    }
}

