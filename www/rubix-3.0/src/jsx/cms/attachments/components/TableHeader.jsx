import React, { Component, PropTypes } from 'react';

export default class TableHeader extends Component {
    render() {
        // console.debug(this.props);
        return (
            <thead>
                <tr key="thead_tr">
                    <th key="th_pagelinks">Page Link</th>
                    {
                        this.props.displayFields.map((field, ind) => {
                            return <th data-sortable-col key={'th_' + ind}>{field.label ? field.label : field.name}</th>
                        })
                    }
                    <th key="th_other"></th>
                </tr>
            </thead>
        )
    }
}

