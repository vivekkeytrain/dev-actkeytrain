import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

import TableHeader from './TableHeader';
import TableRow from './TableRow';

export default class PageList extends Component {
    constructor(props) {
        // console.debug('constructor');
        // console.debug(props);
        super(props);

        this.state = {
            rows: []
        }
    }

    componentDidMount() {
        // document.body.addEventListener('click', () => this.hideDetails());
    }

    componentDidUnmount() {
        // document.body.removeEventListener('click', () => this.hideDetails());
    }

    componentDidUpdate() {
        this.props.refreshSession();
    }

    componentWillReceiveProps(props) {
        // console.debug('componentWillReceiveProps');
        // console.debug(props);
        let rows = Object.assign([], props.attachments);

        // if (this.state && this.state.sortField) {
        //     rows = sortByOrder(rows, [this.state.sortField], [this.state.sortDir]);
        // }

        if (props.attachments) {
            this.setState({
                rows: rows
            })
        }
        // console.debug(this.state);
    }

    componentDidUpdate() {
        // console.debug('componentDidUpdate');

        //  First kill the buttons
        $('th[data-sortable-col] > button').contents().unwrap();

        //  Then recreate them
        $('.tablesaw').sortable.prototype._init.call($('.tablesaw'));

        //  For some reason we get double buttons sometimes. Take care of that.
        $('th[data-sortable-col] > button > button').contents().unwrap();

    }

    // hideDetails() {
    //     if (this.props && this.props.selectedRow)
    //         this.props.onRowUnselect();
    // }

    // handleRowClick(row_index, row_data, e) {
    //     let target = $(e.target).is('tr') ? $(e.target) : $(e.target).parents('tr');
    //     let dataTable = React.findDOMNode(this.refs.dataTable);
    //     this.props.onRowSelect(row_data, $(target).offset().top - $('#body').offset().top - $(target).height());
    // }

    // renderCell(name, row) {
    //     let displayField = find(this.props.displayFields, {'name': name});

    //     let cellData = row[name];
    //     if (typeof displayField.displayFunc === 'function') {
    //         let dfunc = displayField.displayFunc.bind(this, cellData, row);
    //         cellData = dfunc();
    //     }

    //     if (typeof cellData === 'boolean') {
    //         cellData = (cellData) ? <i className="glyphicon glyphicon-ok"></i> : <i className="glyphicon glyphicon-minus"></i>;
    //     }

    //     // console.debug('renderCell', name, cellData);
    //     return cellData;
    // }


    // onPageLinkClick(pageid, lang_id, e) {
    //     //  Don't want it to open the PageDetail
    //     e.stopPropagation();
    //     e.preventDefault();

    //     this.props.onPageLinkClick(pageid, lang_id);
    // }

    // renderPageLinks(pageID) {
    //     let en_url = 'http://dev.run.keytrain.com/objects2/' + this.props.courseTag + '/' + this.props.lessonTag + '/en/html5_course.htm#load/' + this.props.enSession.session_id + '/' + this.props.enSession.login_session_uid + '/pageid/' + pageID;
    //     let es_url = 'http://dev.run.keytrain.com/objects2/' + this.props.courseTag + '/' + this.props.lessonTag + '/es/html5_course.htm#load/' + this.props.esSession.session_id + '/' + this.props.esSession.login_session_uid + '/pageid/' + pageID;

    //     let en_key = pageID + "_en";
    //     let es_key = pageID + "_es";

    //     return [<a href={en_url} target="_lesson_en" key={en_key} onClick={this.onPageLinkClick.bind(this, pageID, LangOptions.EN)}>EN</a>, ' / ', <a href={es_url} target="_lesson_es" key={es_key} onClick={this.onPageLinkClick.bind(this, pageID, LangOptions.ES)}>ES</a>];
    // }

    render() {
        let row_count = (this.props.attachments) ? this.props.attachments.length : 0;

        let classes = classNames({
            'progress-bar': true,
            'active': true,
            'progress-bar-striped': true
        })

        let fetching;
        if (this.props.attachmentsFetching) {
            fetching =  <div className={classes} style={{width: '100%'}}>
                            <span><strong>Fetching Data, Please Hold ...</strong></span>
                        </div>
        }

        console.debug(this.props);
        // console.debug(this.state.rows);
        return (
            <div>
                <div>Row Count: {row_count}</div>
                {fetching}
                <Table
                    ref='dataTable'
                    bordered
                    striped
                    className='tablesaw'
                    data-sortable>
                    <TableHeader displayFields={this.props.displayFields} displayFieldsHash={this.props.displayFieldsHash} editViewDisplayFields={this.props.editViewDisplayFields} />
                    <tbody>
                        {
                            this.state.rows.map((row, ind) => {
                                return <TableRow row={row} index={ind} key={'row_' + ind} {...this.props} ref={(ref) => this['tableRow_' + ind] = ref} />
                            })
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}

