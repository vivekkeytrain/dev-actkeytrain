import { Component } from 'react';

export default class EditableField extends Component {
    // onFieldChange(attach_id, name, value) {
    //     console.debug(arguments);
    //     // // console.debug(this.props.page);
    //     // console.debug(this);
    //     // console.debug(this.props);
    //     this.props.onUpdateAttachField(attach_id, name, value);
    // }

    // onImmediateFieldChange(attach_id, name, value) {
    //     console.debug(arguments);
    //     this.props.onImmediateUpdateAttachField(attach_id, name, value);
    // }

    onImmediateFieldChange(name, e) {
        this.props.onImmediateUpdateAttachField(this.props.row.attach_id, name, e.target.value);
    }

    onFieldChange(name, e) {
        this.props.onUpdateAttachField(this.props.row.attach_id, name, e.target.value);
    }

    render() {
        //  onChange={this.onImmediateFieldChange.bind(this, field.name)}
        // console.debug(this.props);

        let editField = this.props.editFieldsHash[this.props.name]

        let input = '';
        switch (editField.input_type) {
            case 'text':
                input = <input type='text' className='form-control' name={this.props.name} onChange={this.onImmediateFieldChange.bind(this, this.props.name)} onBlur={this.onFieldChange.bind(this, this.props.name)} value={this.props.row[this.props.name]} />
                break
            case 'textarea':
                input = <textarea className='form-control' rows='3' name={this.props.name} onBlur={this.onFieldChange.bind(this, this.props.name)}>{this.props.row[this.props.name]}</textarea>
                break
            default:
                console.debug('Uknown input type', editField)
        }

        return input;
    }
}

