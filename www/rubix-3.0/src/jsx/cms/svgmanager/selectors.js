import { createSelector } from 'reselect';

import { chain, uniq, find, map, filter, forEach, sortBy, orderBy, pick, omit } from 'lodash';

import { LangOptions, DETAIL_FIELDS, EDITABLE_FIELDS } from './actions';

/*  Get a unique set of units   */
function selectUniqueUnits(content, tags) {
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'unit_tag': tag});

            return {
                id: match.unit_id,
                tag: match.unit_tag,
                desc: match.unit_desc,
                seq: match.unit_seq,
                display: match.unit_display
            }
        })
        .value();
}

/*  Get a unique set of units   */
function selectUniqueUnits(content, tags) {
    return map(tags, (tag) => {
        let match = find(content, {'unit_tag': tag});

        return {
            id: match.unit_id,
            tag: match.unit_tag,
            desc: match.unit_desc,
            seq: match.unit_seq,
            display: match.unit_display
        }
    });
}

/*  Select a unique set of unit tags that will be used to create a unique set of units  */
function selectUnitTags(content) {
    return chain(content)
        .map('unit_tag')
        .uniq()
        .value();
}


function selectUniqueCourses(content, tags) {
    // console.debug('selectUniqueCourses', content, tags);
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'course_tag': tag});

            return {
                id: match.course_id,
                tag: match.course_tag,
                desc: match.course_desc,
                seq: match.course_seq,
                display: match.course_display
            }
        })
        .value();
}

function selectCourseTags(content, unit) {
    // console.debug('selectCourseTags', content, unit);
    return chain(content)
        .filter({'unit_id': unit})
        .map('course_tag')
        .uniq()
        .value();
}

function selectCourseTag(course, courses) {
    if (!course || !courses || course == -1) return '';
    // console.debug('selectCourseTag', course, courses);
    // console.debug(find(course, {'id': course}));
    return chain(courses).find({'id': course}).value().tag;
}

function selectUniqueLessons(content, tags) {
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'lesson_tag': tag});

            return {
                id: match.lesson_id,
                tag: match.lesson_tag,
                desc: match.lesson_desc,
                seq: match.lesson_seq,
                display: match.lesson_display
            }
        })
        .value();
}

function selectLessonTags(content, course) {
    return chain(content)
        .filter({'course_id': course})
        .map('lesson_tag')
        .uniq()
        .value();
}

function selectUniqueTopics(content, tags) {
    return chain(tags)
        .map((tag) => {
            let match = find(content, {'topic_tag': tag});

            return {
                id: match.topic_id,
                tag: match.topic_tag,
                desc: match.topic_desc,
                seq: match.topic_seq,
                display: match.topic_display
            }
        })
        .value();
}

function selectTopicTags(content, lesson) {
    return chain(content)
        .filter({'lesson_id': lesson})
        .map('topic_tag')
        .uniq()
        .value();
}

/*  Content filtered by current selected state */
function selectContent(content, unit, course, lesson, topic) {
    // console.debug(content);
    // console.debug([unit, course, lesson, topic]);
    let filtered_content = {
        units: content,
        courses: [],
        lessons: [],
        topics: []
    }
    if (unit != '')
        filtered_content.courses = filter(content, {'unit_id': unit});
    
    if (course != '') 
        filtered_content.lessons = filter(content, {'course_id': course});

    if (lesson != '') 
        filtered_content.topics = filter(content, {'lesson_id': lesson});

    // console.debug(filtered_content);

    return filtered_content;
}

function selectContentTag(unit, course, lesson, topic) {
    // console.debug('selectContentTag', unit, course, lesson, topic);
    let tag;
    if (topic && topic != -1)
        tag = 'T' + topic;
    else if (lesson && topic == -1)
        tag = 'L' + lesson;
    else if (course && lesson == -1)
        tag = 'C' + course;
    else if (unit && course == -1)
        tag = 'U' + unit;

    return tag;
}

function selectImageDetailsByTag(images, images_by_tag, content_tag, sort_field, sort_dir, qa_status) {
    // console.debug('selectImageDetailsByTag', /*images, images_by_tag,*/ content_tag, sort_field, sort_dir);

    // console.debug('tag', tag);
    if (!images_by_tag[content_tag]) {
        return [];
    } else {
        let image_ids = images_by_tag[content_tag].items;
        // console.debug(image_ids);

        let out_images = image_ids;
        // let out_images = map(image_ids, (page_image) => {
        //     // return images[image_id];
        //     // let out = Object.assign({}, images[page_image.image_id], page_image);
        //     // let out = Object.assign({}, omit(page_image, DETAIL_FIELDS), images[page_image.image_id]);
        //     // let out = Object.assign({}, page_image, pick(images[page_image.image_id], EDITABLE_FIELDS));
        //     // let out = Object.assign({}, page_image, images[page_image.image_id]);
        //     // let out = Object.assign({}, images[page_image.image_id], page_image);
        //     let out = images[page_image.image_id];
        //     // console.debug(page_image.image_id, out.qa_problem_bits);
        //     return out;
        // });

        // console.debug('qa_status', qa_status);
        // console.debug(out_images);
        if (qa_status) {
            // out_images = filter(out_images, {'qa_status_val': qa_status});
            out_images = filter(out_images, (image) => {
                // console.debug(image);
                // console.debug(image.qa_status_val, qa_status, (image.qa_status_val&qa_status));
                return (image.qa_status_val&qa_status) > 0;
            })
        }
        // console.debug(out_images);

        if (sort_field) {
            out_images = orderBy(out_images, [sort_field], [sort_dir]);
        } else {
            out_images = sortBy(out_images, 'row_seq');
        }

        // console.debug(out_images);
        return out_images;
    }
}

function selectIsFetching(content_tag, images_by_tag) {
    // console.debug('selectIsFetching', content_tag, images_by_tag);
    if (!images_by_tag || !images_by_tag[content_tag]) return false;

    // console.debug(images_by_tag[content_tag]);

    return images_by_tag[content_tag].isFetching;
}

function selectRowImages(images, tag_images, selected_row) {
    // console.debug(images);
    // console.debug(tag_images);
    if (selected_row == -1) return [];
    let tag_image = tag_images[selected_row];
    let image = images[tag_image.image_id];

    let image_es;
    if (image.image_id_es) {
        image_es = images[image.image_id_es];
    }
    // console.debug(image.qa_problem_bits, image_es.qa_problem_bits);
    console.debug(image, image_es)
    return [image, image_es, tag_image];
}

function selectSession(sessions, lesson_id) {
    console.debug('selectSession', sessions, lesson_id);
    if (!lesson_id) return {};

    let ret_sessions = {}
    forEach(LangOptions, (lang_id, lang_code) => {
        // console.debug(lang_id, lang_code);
        // console.debug(sessions[lang_id], sessions[lang_id][lesson_id]);
        // return (sessions[lang_code] && sessions[lang_code][lesson_id]) || {};
        if (sessions[lang_id] && sessions[lang_id][lesson_id]) ret_sessions[lang_code] = sessions[lang_id][lesson_id];
    });

    // console.debug('ret_sessions', ret_sessions);
    return ret_sessions;
}

function selectDisplayFieldsOn(displayFields) {
    return filter(displayFields, {'isOn': true});
}

function selectVisibleFields(images, displayFields) {
    let avail_fields = map('name', displayFields);

    return map(images, (obj) => {
        return pick(obj, avail_fields);
    });
}

const contentSelector = state => state.editor_content.content;
const unitSelector = state => state.filters.unit;
const courseSelector = state => state.filters.course;
const lessonSelector = state => state.filters.lesson;
const topicSelector = state => state.filters.topic;
const selectedRowSelector = state => state.filters.selectedRow;
const qaStatusSelector = state => state.filters.qa_status;

const sessionsSelector = state => state.filters.sessions;
const messagesSelector = state => state.filters.messages;

const imagesSelector = state => state.images;
const imagesByTagSelector = state => state.images_by_tag;

const displayFieldsSelector = state => state.filters.displayFields;

const sortFieldSelector = state => state.filters.sort_field;
const sortDirSelector = state => state.filters.sort_dir;

const usernameSelector = state => state.filters.username;

const constantsSelector = state => state.constants;

const contentDataSelector = createSelector(
    contentSelector,
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    (content, unit, course, lesson, topic) => {
        return selectContent(content, unit, course, lesson, topic);
    }
);

const unitTagsSelector = createSelector(
    contentSelector,
    (content) => {
        return selectUnitTags(content);
    }
);

const unitsSelector = createSelector(
    [contentSelector, unitTagsSelector],
    (content, tags) => {
        return selectUniqueUnits(content, tags);
    }
);

const courseTagsSelector = createSelector(
    [contentDataSelector, unitSelector],
    (content, unit) => {
        return selectCourseTags(content.courses, unit);
    }
);

const coursesSelector = createSelector(
    [contentDataSelector, courseTagsSelector],
    (content, tags) => {
        return selectUniqueCourses(content.courses, tags);
    }
);

/*  Selector to get the course tag of the currently selected course_id */
const courseTagSelector = createSelector(
    [courseSelector, coursesSelector],
    (course, courses) => {
        return selectCourseTag(course, courses);
    }
);


const lessonTagsSelector = createSelector(
    [contentDataSelector, courseSelector],
    (content, course) => {
        return selectLessonTags(content.lessons, course);
    }
);

const lessonsSelector = createSelector(
    [contentDataSelector, lessonTagsSelector],
    (content, tags) => {
        return selectUniqueLessons(content.lessons, tags);
    }
);

const topicTagsSelector = createSelector(
    [contentDataSelector, lessonSelector],
    (content, lesson) => {
        return selectTopicTags(content.topics, lesson);
    }
);

const topicsSelector = createSelector(
    [contentDataSelector, topicTagsSelector],
    (content, tags) => {
        return selectUniqueTopics(content.topics, tags);
    }
);

const displayFieldsOnSelector = createSelector(
    displayFieldsSelector,
    (displayFields) => {
        return selectDisplayFieldsOn(displayFields);
    }
);

const visibleImageFields = createSelector(
    imagesSelector,
    displayFieldsOnSelector,
    (images, displayFields) => {
        return selectVisibleFields(images, displayFields);
    }
);

const contentTagSelector = createSelector(
    unitSelector,
    courseSelector,
    lessonSelector,
    topicSelector,
    (unit, course, lesson, topic) => {
        return selectContentTag(unit, course, lesson, topic);
    }
);

const imageDetailsByTagSelector = createSelector(
    imagesSelector,
    imagesByTagSelector,
    contentTagSelector,
    sortFieldSelector,
    sortDirSelector,
    qaStatusSelector,
    (images, images_by_tag, content_tag, sort_field, sort_dir, qa_status) => {
        return selectImageDetailsByTag(images, images_by_tag, content_tag, sort_field, sort_dir, qa_status);
    }
);

const isFetchingSelector = createSelector(
    contentTagSelector,
    imagesByTagSelector,
    (content_tag, images_by_tag) => {
        return selectIsFetching(content_tag, images_by_tag);
    }
);

const rowImagesSelector = createSelector(
    imagesSelector,
    imageDetailsByTagSelector,
    selectedRowSelector,
    (images, tag_images, selected_row) => {
        return selectRowImages(images, tag_images, selected_row);
    }
);

const sessionSelector = createSelector(
    sessionsSelector,
    lessonSelector,
    (sessions, lesson_id) => {
        return selectSession(sessions, lesson_id);
    }
);



export const filteredContentSelector = createSelector(
    contentDataSelector,
    unitSelector,
    unitsSelector,
    courseSelector,
    coursesSelector,
    courseTagSelector,
    lessonSelector,
    lessonsSelector,
    topicSelector,
    topicsSelector,
    messagesSelector,
    selectedRowSelector,
    imageDetailsByTagSelector,
    rowImagesSelector,
    sessionsSelector,
    sessionSelector,
    displayFieldsSelector,
    displayFieldsOnSelector,
    sortFieldSelector,
    sortDirSelector,
    usernameSelector,
    isFetchingSelector,
    constantsSelector,
    contentTagSelector,
    (content, unit, units, course, courses, course_tag, lesson, lessons, topic, topics, messages, selected_row, images, row_images, all_sessions, sessions, displayFields, displayFieldsOn, sort_field, sort_dir, username, is_fetching, constants, content_tag) => {
        return {
            content,
            unit,
            units,
            course,
            courses,
            course_tag,
            lesson,
            lessons,
            topic,
            topics,
            messages, 
            selected_row,
            images,
            row_images, 
            all_sessions,
            sessions,
            displayFields, 
            displayFieldsOn,
            sort_field, 
            sort_dir,
            username, 
            is_fetching, 
            constants, 
            content_tag
        }
    }
);