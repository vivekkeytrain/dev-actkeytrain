import { connect } from 'react-redux';

import {
    setUnit, setCourse, setLesson, setTopic, setSelectedRow, setFieldImmediately,
    fetchEditorContent, fetchImagesIfNeeded, fetchImageDetail, closeMessage,
    loadDisplayFields, updateLocalDisplayField, setSortField, fetchAppConstants,
    doLoginIfNeeded, setUser, updateField, updateQAStatus, refetchImages, setLiveUpdate
} from '../actions';

import { filteredContentSelector } from '../selectors';
import MessageCenter from 'common/MessageCenter';
import ContentTree from 'common/ContentTree';
import ImageList from '../components/ImageList';
import Filters from '../components/Filters';

class App extends React.Component {
    clearTimer() {
        if (this.state && this.state.refreshTimer) {
            window.clearTimeout(this.state.refreshTimer);
            this.setState({
                refreshTimer: null
            });
        }
    }

    componentDidMount() {
        let { dispatch, unit, course, lesson, topic, selected_row } = this.props;

        dispatch(loadDisplayFields());

        dispatch(fetchEditorContent())
            .then(() => {
                dispatch(fetchImagesIfNeeded(unit, course, lesson, topic));
            });
        dispatch(doLoginIfNeeded(lesson, 1))
            .then(() => {
                dispatch(doLoginIfNeeded(lesson, 2)); // running these in parallel seems to fail more often than not
            });
        

        if (!window.auth) window.auth = 'Basic YmVuOmJlbg=='

        let auth = atob(window.auth.replace('Basic ', ''));
        // console.debug(auth);
        let auth_parts = auth.split(':');
        dispatch(setUser(auth_parts[0]));

        dispatch(fetchAppConstants());
    }

    render() {
        console.debug(this.props);
        const { dispatch, unit, units, course, courses, course_tag, lesson, lessons, topic, topics, content, selected_row, images, row_images, all_sessions, sessions, sort_field, sort_dir, username, constants, content_tag, messages } = this.props;

        let handleUnitClick = (unit) => {
            console.debug('handleUnitClick', unit);
            dispatch(setSelectedRow(-1));
            dispatch(setUnit(unit));
            dispatch(fetchImagesIfNeeded(unit, course, lesson, topic));
        }

        let handleCourseClick = (course) => {
            console.debug('handleCourseClick', course);
            dispatch(setSelectedRow(-1));
            dispatch(setCourse(course));
            dispatch(fetchImagesIfNeeded(unit, course, lesson, topic));
        }

        let handleLessonClick = (lesson) => {
            console.debug('handleLessonClick', lesson);
            dispatch(setSelectedRow(-1));
            dispatch(setLesson(lesson));
            if (lesson != -1)
                dispatch(setTopic(-1));
            dispatch(doLoginIfNeeded(lesson, 1));
            dispatch(doLoginIfNeeded(lesson, 2));
            dispatch(fetchImagesIfNeeded(unit, course, lesson, (lesson != -1) ? -1 : topic));
        }

        let handleTopicClick = (topic) => {
            console.debug('handleTopicClick', topic);
            dispatch(setSelectedRow(-1));
            dispatch(setTopic(topic));
            dispatch(fetchImagesIfNeeded(unit, course, lesson, topic));
        }

        let handleRowSelect = (index) => {
            // console.trace();
            console.debug('handleRowSelect', index, images);

            let image = images[index];
            console.debug(image);

            if (image) {
                //  Load up the details for both english and spanish versions of image
                dispatch(fetchImageDetail(image.image_id, null, content_tag))
                    .then(
                        dispatch(fetchImageDetail(image.image_id_es, image, content_tag))
                            .then(
                                dispatch(doLoginIfNeeded(image.lesson_id, 1))
                                    .then(
                                        dispatch(doLoginIfNeeded(image.lesson_id, 2))
                                            .then(dispatch(setSelectedRow(index)))
                                    )
                            )
                    );
                
            } else {
                dispatch(setSelectedRow(index))
            }
            
        }

        let refreshSession = () => {
            if (this.state && !this.state.refreshTimer && sessions && sessions['EN'].login_session_uid) {
                let time_left = (1000 * 60 * 15) - (Date.now() - sessions['EN'].lastUpdated) + 1000; //   15 minutes from last update (plus 1 second so it's after the doLoginIfNeeded check)
                // console.debug(time_left);

                let timer = window.setTimeout(() => {
                    dispatch(doLoginIfNeeded(lesson, 1));
                    dispatch(doLoginIfNeeded(lesson, 2));
                    this.clearTimer();
                    refreshSession();
                }, time_left);
                this.setState({
                    refreshTimer: timer
                });
            }
            // dispatch(doLoginIfNeeded(lesson, lang));
        }

        return (
            <Container id='body' style={{overflow: 'auto'}}>
                <Grid>
                    <Row>
                        <Col xs={12}>
                            <PanelContainer plain controlStyles='bg-green fg-white'>
                                <Panel style={{background: 'white', minHeight: '500px'}}>
                                    <PanelHeader className='bg-gree fg-white' style={{margin: 0}}>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <p>
                                                        <ContentTree
                                                            unit={unit}
                                                            course={course}
                                                            lesson={lesson}
                                                            topic={topic}
                                                            units={units}
                                                            courses={courses}
                                                            lessons={lessons}
                                                            topics={topics}
                                                            content={content}
                                                            onUpdateUnit={unit =>
                                                                handleUnitClick(unit)
                                                            }
                                                            onUpdateCourse={course =>
                                                                handleCourseClick(course)
                                                            }
                                                            onUpdateLesson={lesson =>
                                                                handleLessonClick(lesson)
                                                            }
                                                            onUpdateTopic={topic =>
                                                                handleTopicClick(topic)
                                                            } />
                                                    </p>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelHeader>
                                    <PanelBody>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <Filters
                                                        displayFields={this.props.displayFields}
                                                        displayFieldsOn={this.props.displayFieldsOn}
                                                        isFetching={this.props.is_fetching}
                                                        constants={constants}
                                                        setQAStatusFilter={(e) => 
                                                            dispatch(updateQAStatus(parseInt(e.target.value)))
                                                        }
                                                        onUpdateDisplayField={(field_name, isOn) => 
                                                            dispatch(updateLocalDisplayField(field_name, isOn)) 
                                                        } 
                                                        forceRefresh={() => 
                                                            dispatch(fetchImagesIfNeeded(unit, course, lesson, topic, true))
                                                        } />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col xs={12}>
                                                    <p>
                                                    <ImageList
                                                        ref={(ref) => this.imageList = ref}
                                                        images={images} 
                                                        rowImages={row_images}
                                                        allSessions={all_sessions}
                                                        sessions={sessions}
                                                        selectedRow={selected_row} 
                                                        sortField={sort_field}
                                                        sortDir={sort_dir}
                                                        displayFields={this.props.displayFieldsOn}
                                                        constants={constants}
                                                        setFieldImmediately={(image_id, field_name, field_val) => 
                                                            dispatch(setFieldImmediately(image_id, field_name, field_val))
                                                        }
                                                        setSelectedRow={(index) => 
                                                            handleRowSelect(index)
                                                        } 
                                                        setSortField={(field_name, sort_dir) => 
                                                            dispatch(setSortField(field_name, sort_dir))
                                                        }
                                                        onUpdateField={(image_id, field_name, field_val) => {
                                                            dispatch(updateField(image_id, field_name, field_val, username))
                                                                .then(() => {
                                                                    console.debug('now fetch details');
                                                                    dispatch(fetchImageDetail(image_id, null, content_tag))
                                                                })
                                                        }}
                                                        doRefetchImages={(image_id) => 
                                                            dispatch(refetchImages(image_id, content_tag))
                                                        }
                                                        onLiveUpdateChange={(islive, image_id) => 
                                                            dispatch(setLiveUpdate(islive, image_id))
                                                        }
                                                        />
                                                    </p>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelBody>
                                    <MessageCenter 
                                        onMessageClose={(index) => 
                                            dispatch(closeMessage(index))
                                        }>
                                        {this.props.messages.map((message, ind) => {
                                            return <div type={message.type} index={ind} visible={message.visible} key={"message_" + ind}>{message.message}</div>
                                        })}
                                    </MessageCenter>
                                </Panel>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
}

App.state = {
    refreshTimer: null
}

export default connect(filteredContentSelector)(App);