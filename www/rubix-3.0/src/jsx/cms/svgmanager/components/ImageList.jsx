import { Component, PropTypes } from 'react';
import ImageTableHeader from './ImageTableHeader';
import ImageRow from './ImageRow';
import classNames from 'classnames';
import ImageDetailView from './ImageDetailView';
// import SVGView from './SVGView';

import { isEqual } from 'lodash';

//  http://babeljs.io/blog/2015/06/07/react-on-es6-plus/
export default class ImageList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            highlightedRow: -1
        }
    }

    componentWillReceiveProps(props) {
        console.debug('componentWillReceiveProps');
        if (!isEqual(props.images, this.props.images)) {
            this.setState({
                highlightedRow: -1
            })
        }
    }


    // componentDidUpdate() {
    //     if (this.props.selectedRow && this.props.selectedRow != -1 && this['pageRow_' + this.props.selectedRow] && this['pageRow_' + this.props.selectedRow]['row_' + this.props.selectedRow]) {
    //         //  Check to see if it's in view, if not, scroll
    //         let elem = $(this['pageRow_' + this.props.selectedRow]['row_' + this.props.selectedRow].getDOMNode());
    //         let win = $(window);

    //         let docViewTop = win.scrollTop();
    //         let docViewBot = docViewTop + win.height();

    //         let elemTop = elem.offset().top;
    //         let elemBot = elemTop + elem.height();

    //         // console.debug(elemBot, docViewBot, elemTop, docViewTop)
    //         if ((elemBot > docViewBot) || (elemTop < docViewTop))
    //             elem.get(0).scrollIntoView(false);
    //     }

    //     // console.debug(node.scrollTop);
    //     this.props.refreshSession();
    //     // console.debug(this.props.session.lastUpdated);

    //     // // this.clearTimer();

    // }

    //<PageRow page={page} stati={this.props.stati} people={this.props.people} user={this.props.user} problemTypes={this.props.problemTypes} lang={this.props.lang} courseTag={this.props.courseTag} lessonId={this.props.lesson_id} session={this.props.session} onUpdatePageField={this.props.onUpdatePageField} onImmediateUpdatePageField={this.props.onImmediateUpdatePageField} key={page.PageID} />

    changeSelectedImage(increment, by_page=false) {
        let new_selected_index = this.props.selectedRow + increment;

        if (new_selected_index < 0) new_selected_index = this.props.images.length-1;
        if (new_selected_index >= this.props.images.length) new_selected_index = 0;

        let new_row = this.props.images[new_selected_index];

        if (!by_page) {
            while (new_row.is_first_page != 1) {
                new_selected_index += increment;

                if (new_selected_index < 0) new_selected_index = this.props.images.length-1;
                if (new_selected_index >= this.props.images.length) new_selected_index = 0;

                new_row = this.props.images[new_selected_index];
                console.debug(new_row);
            }
        }

        this.props.setSelectedRow(new_selected_index);
        this.setHighlightedRow(new_selected_index);
    }

    setHighlightedRow(index) {
        this.setState({
            'highlightedRow': index
        });
    }

    render() {
        console.debug(this.props);
        // console.debug(this.props.pagesFetching);
        let row_count = (this.props.images) ? this.props.images.length : 0;

        // let classes = classNames({
        //     'progress-bar': true,
        //     'active': true,
        //     'progress-bar-striped': true
        // });

        let detailView = (this.props.selectedRow != -1) ? <ImageDetailView {...this.props} changeSelectedImage={this.changeSelectedImage.bind(this)} /> : '';
        // console.debug(detailView);
        // let detailView = '';

        let fetching;
        // if (this.props.pagesFetching) {
        //     fetching =  <div className={classes} style={{width: '100%'}}>
        //                     <span><strong>Fetching Data, Please Hold ...</strong></span>
        //                 </div>
        // }

        return (
            <div>
                <div>Row Count: {row_count}</div>
                <table
                    className='table-striped table-bordered tablesaw tablesaw-stack tablesaw-sortable image-table'>
                    <ImageTableHeader displayFields={this.props.displayFields} sortField={this.props.sortField} sortDir={this.props.sortDir} setSortField={this.props.setSortField} />
                    <tbody>
                    {this.props.images.map((image, index) =>
                        <ImageRow image={image} {...this.props} index={index} key={index} ref={(ref) => this['imageRow_' + index] = ref} highlighted={(this.state.highlightedRow == index)} setHighlightedRow={this.setHighlightedRow.bind(this)} />
                    )}
                    </tbody>
                </table>
                {detailView}
            </div>
        )
    }
}


