import React, { Component, PropTypes } from 'react';

export default class SVGPreview extends Component {
    constructor(props) {
        super(props);

        this.state = {
            position: 'left'
        }
    }

    closeWindow() {
        this.props.hidePreview();
        // this.props.onRowSelect(-1, null);
    }

    handleBackgroundClick(e) {
        if (e.target.id === 'modal-container') this.closeWindow();
    }
    
    unescapeSVGCode(str) {
        if (!str) return str;

        // console.debug(str);
        // alert(str);
        // console.debug(atob(str));
        // console.debug(escape(atob(str)));
        // console.debug(decodeURIComponent(escape(atob(str))));
        str = escape(atob(str));
        // console.debug(str);
        try {
            str = decodeURIComponent(str);
        } catch (e) { // Malformed URI. Unicode issue.
            str = unescape(str);
        }
        // console.debug(str);

        let r = /\\x([\d\w]{2})/gi;
        str = str.replace(r, (match, grp) => {
            return String.fromCharCode(parseInt(grp, 16)); 
        });

        r = /\\u([\d\w]{4})/gi;
        str = str.replace(r, (match, grp) => {
            return String.fromCharCode(parseInt(grp, 16)); 
        });
        return str;
    }

    dangerousHTML() {
        let str = this.props.image.svg_code;

        if (!str) str = '';
        if (str && str.trim().indexOf('<') != 0) {
            // str = this.unescapeSVGCode(decodeURIComponent(escape(atob(this.props.image.svg_code))));
            str = this.unescapeSVGCode(this.props.image.svg_code);
        }

        return {
            __html: str
        }
    }

    moveImage(position) {
        this.setState({
            position
        })
    }

    render() {
        console.debug(this.props);

        let left, right;
        if (this.state.position == 'left') {
            left = <div dangerouslySetInnerHTML={this.dangerousHTML()} />
        } else {
            right = <div dangerouslySetInnerHTML={this.dangerousHTML()} />
        }

        return (
            <div className='cms-modal-detail'>
                <div className="modal-backdrop fade in"></div>
                <div className="cms-modal-detail-modal svg-preview" id="modal-container" onClick={this.handleBackgroundClick.bind(this)}>
                    <div className="cms-modal-detail">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.closeWindow.bind(this)}><span aria-hidden="true">×</span></button>
                            <h4 className="modal-title">Preview</h4>
                        </div>
                        <div className="modal-body">
                            <Grid>
                                <Row>
                                    <Col xs={4} className="pull-left" collapseLeft>
                                        <Button onClick={this.moveImage.bind(this, 'left')} active={(this.state.position == 'left')}><i className="glyphicon glyphicon-chevron-left"></i> Left</Button>
                                    </Col>
                                    <Col xs={4} className="pull-right" collapseRight>
                                        <Button className="pull-right" onClick={this.moveImage.bind(this, 'right')} active={(this.state.position == 'right')}>Right<i className="glyphicon glyphicon-chevron-right"></i></Button>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={7}>
                                        {left}
                                    </Col>
                                    <Col sm={5}>
                                        {right}
                                    </Col>
                                </Row>
                            </Grid>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
} 