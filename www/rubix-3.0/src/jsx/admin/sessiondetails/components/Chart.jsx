import React, { Component } from 'react';
import { forEach, map, flatten, compact, invert } from 'lodash';
import d3 from 'd3';
import tip from 'd3-tip';
// import { VictoryChart } from 'victory';

import { TOTALS_COLORS, DisplayModes, LangOptions, DATA_FIELDS } from '../actions';

const   margin = {top: 10, right: 75, bottom: 150, left: 75};
const   possible_colors = ['#339966', '#FF6666', '#FFCC66', '#FF0066', '#003399', '#333366', '#3399CC'];

// class Axis extends Component {

//     render() {
//         let transform, label;
//         switch (this.props.orientation) {
//             case 'bottom':
//             case 'top':
//                 transform = `transform="translate(0,${this.props.height})"`;
//                 label = (this.props.label) ? <text  y='-40' dy='.71em' style={{'textAnchor': 'end'}}>ES Count</text> : ''
//                 break;
//             case 'right':
//                 transform = `transform="translate(0,${this.props.width})"`;
//             case 'left':
//                 label = (this.props.label) ? <text transform='rotate(-90)' y='-40' dy='.71em' style={{'textAnchor': 'end'}}>ES Count</text> : ''

//         }

        

//         return <g 
//     }
// }

// Axis.defaultProps = {
//     orientation: 'bottom'
// }

class Bar extends Component {
    constructor(props) {
        super(props);

        this.props = {
            width: 0,
            height: 0,
            offset: 0,
            yOffset: 0
        }
    }

    showTip(e) {
        let toolTip = this.props.toolTip;
        // let avg = this.props.answerAvgs[this.props.question][this.props.index]
        let avg = this.props.avg;

        let text = this.props.val;
        // let text = ((this.props.lang) ? '<div style="text-align: center; width: 100%">' + this.props.lang.toUpperCase() + '</div>' : '') + ((this.props.index == 0) ? '<i>SKIPPED</i>' : this.props.questionAnswers[this.props.question][this.props.index-1]) + '<div style="text-align: center; width: 100%">' + this.props.sum + ' (' + Math.round(avg * 1000) / 10 + '%)</div>';
        toolTip.html(text).show(e.target);
    }

    hideTip(e) {
        let toolTip = this.props.toolTip;
        toolTip.hide(e.target);
    }

    render() {
        // console.debug(this.props);
        //onMouseOver={this.showTip.bind(this)} onMouseOut={this.hideTip.bind(this)}
        return (
            <rect fill={this.props.color} width={this.props.width} height={this.props.height} x={this.props.offset} y={this.props.availableHeight - this.props.height + this.props.yOffset} onMouseOver={this.showTip.bind(this)} onMouseOut={this.hideTip.bind(this)} />
        );
    }
}

Bar.defaultProps = {
    width: 0,
    height: 0,
    offset: 0,
    yOffset: 0
}

export default class Chart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            xAxis: null,
            toolTip: null,
            dataObj: {},
            xObj: null,
            ysObj: null,
            xTicks: null
        }
    }

    componentDidMount() {
        let toolTip = tip()
            .attr('class', 'd3-tip');

        let svg = d3.select('#svg_chart');
        svg.call(toolTip);

        this.setState({
            toolTip
        });
    }

    componentWillUnmount() {
        d3.select('#svg_chart').selectAll(".tick").remove();
        d3.selectAll('.d3-tip').remove();
    }

    componentWillReceiveProps(props) {
        // console.debug(this.state);
        console.debug(props);

        let dataObj, xTicks, x, ys;
        if (props.chartDataObj) {
            dataObj = props.chartDataObj.dataObj;
            xTicks = props.chartDataObj.xTicks;
            x = props.chartDataObj.xObj;
            ys = props.chartDataObj.ysObj;
        }


        // let modeObj = DisplayModes[props.mode];
        // console.debug(modeObj);
        // if (props.mode) {
        //     if (props.lang) {
        //         let lang_code = invert(LangOptions)[props.lang];
        //         modeObj = props[modeObj['lang']];
                
        //         if (!modeObj) return;

        //         dataObj = modeObj.data[lang_code];
        //         xTicks = modeObj.xTicks[lang_code];
        //         x = modeObj.xs[lang_code];
        //         ys = modeObj.ys[lang_code];
        //     } else {
        //         modeObj = props[modeObj['all']];

        //         if (!modeObj) return;

        //         dataObj = modeObj.data;
        //         xTicks = modeObj.xTicks;
        //         x = modeObj.x;
        //         ys = modeObj.ys;
        //     }
        // }

        // console.debug(dataObj, xTicks, x, ys);

        this.setState({
            dataObj: dataObj,
            xObj: x,
            ysObj: ys,
            xTicks: xTicks
        });
    }

    componentDidUpdate() {
        this.updateAxes();
    }

    updateAxes() {
        console.debug(this.state);

        let xTicks = this.state.xTicks;
        let ys = this.state.ysObj;

        d3.select('#svg_chart').selectAll(".tick").remove();

        if (xTicks && xTicks.length) {
            let x = d3.scale.ordinal().rangeRoundBands([50, this.props.width-50], .1).domain(xTicks);
            // console.debug(x.domain());
            // let xAxis = d3.svg.axis().scale(x).orient('bottom').tickValues(xTicks);
            let xAxis = d3.svg.axis().scale(x).orient('bottom');
            console.debug(xTicks);

            d3.select("#xaxis")
                .attr("transform", "translate(0," + this.props.height + ")")
                .call(xAxis)
                    .selectAll('text')
                    .attr("dy", ".15em")
                    .attr("dx", "-.8em")
                    .attr("dy", ".35em")
                    .attr("transform", "rotate(-65)")
                    .style("text-anchor", "end");
        }

        // ys = props.sessionDataTotalsVictory.ys;

        forEach(ys, (y, i) => {
            let orient = (i==0) ? 'left' : 'right';
            let yAxis = d3.svg.axis().scale(y.scale).orient(orient);
            // console.debug(yAxis);
            d3.select('#yaxis-' + i).call(yAxis);
        });
        // console.debug(ys[0].domain());
        // let yAxis = d3.svg.axis().scale(ys[0]).orient('left');

        // d3.select('#yaxis')
        //     .call(yAxis);
    }

    render() {
        console.debug(this.props);
        console.debug(this.state);

        let bars = map(this.state.dataObj, (vals) => {
            // console.debug(vals);
            if (vals.x && vals.y)
                return <Bar color={vals.fill} offset={vals.x} width={vals.width} height={this.props.height-vals.y} availableHeight={this.props.height} val={vals.label} toolTip={this.state.toolTip} />
            else
                return ''
        }, this);

        let yaxes = map(this.state.ysObj, (y, i) => {
            if (!y.scale) return '';

            let transform, yVal;

            //  Put an axis on the right if we have 3 and this is the third or if we have 2 and this is the second
            if (this.state.ysObj.length > 1 && i == (this.state.ysObj.length-1)) transform = `translate(${this.props.width},0)`;
            /*  
                Rules for yVal (y-axis text label position):
                *   If we only have 1 axis the axis is on the left and text is to the right (+8)
                *   If we have 2 axes: 1st axis: +8, 2nd axis: -20
                *   If we have 3 axes: 1st axis: -70, 2nd axis: +50, 3rd axis: -20
            */

            if (this.state.ysObj.length == 1) {
                yVal = 8
            } else if (this.state.ysObj.length == 2) {
                if (i == 0) yVal = 8;
                else if (i == 1) yVal = -20;
            } else if (this.state.ysObj.length == 3) {
                if (i == 0) yVal = -70;
                else if (i == 1) yVal = 50;
                else if (i == 2) yVal = -20;
            }

            // if (i == 0) yVal = -70;
            // else if (i == 1) yVal = 50;
            // else if (i == 2) yVal = -20;
            // yVal = (i == 0) ? -20 : 10;
            // console.debug(transform);
            return  <g className="y axis" id={"yaxis-"+i} fill={TOTALS_COLORS[i]} transform={transform}>
                        <text transform='rotate(-90)' y={yVal} dy='.71em' style={{'textAnchor': 'end'}}>{y.label}</text>
                    </g>
        })

        console.debug(yaxes);

        return <svg width={this.props.width+margin.left+margin.right} height={this.props.height+margin.top+margin.bottom} style={{'float': 'left'}} id="svg_chart">
            <g transform={'translate(' + margin.left + ',' + margin.top + ')'} id="margin">
                {bars}
                <g className="x axis" id="xaxis" />
                {yaxes}
            </g>
        </svg>

    }
}