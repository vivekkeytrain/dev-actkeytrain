import React, { Component } from 'react';
import { map, isUndefined, includes } from 'lodash';

import { DisplayModes, GranularityOptions, DATA_FIELDS, urlFormatDate, isUTCDate } from '../actions';

import LangFilter from '../../common/LangFilter';

import classNames from 'classnames';

export default class Filters extends Component {
    constructor(props) {
        super(props);

        this.state = {
            required_fields: ['mode', 'granularity']
        }
    }

    componentWillMount() {
        console.debug('componentWillMount', this.props);
        if (this.props.mode) {
            if (this.props.mode.indexOf('Total Usage by Metric') == 0 || this.props.mode.indexOf('Course Metric Comparison') == 0) {
                this.setRequiredFields(['metric']);
            // } else if (this.props.mode.indexOf('Total Usage by Language') == 0) {
            //     this.setRequiredFields(['lang']);
            } else if (this.props.mode.indexOf('Usage by Course') == 0) {
                this.setRequiredFields(['unit', 'course']);
            } else if (this.props.mode.indexOf('Course Metric Comparison') == 0) {
                this.setRequiredFields(['multiContent']);
            }
        }
        console.debug('state now', this.state);
    }

    componentWillReceiveProps(props) {
        console.debug('componentWillReceiveProps', props);
        if (props.mode) {
            if (props.mode.indexOf('Total Usage by Metric') == 0 || props.mode.indexOf('Course Metric Comparison') == 0) {
                this.setRequiredFields(['metric']);
            // } else if (props.mode.indexOf('Total Usage by Language') == 0) {
            //     this.setRequiredFields(['lang']);
            } else if (props.mode.indexOf('Usage by Course') == 0) {
                // this.setRequiredFields(['unit', 'course']);
                this.setRequiredFields(['unit']);
            } else if (props.mode.indexOf('Course Metric Comparison') == 0) {
                this.setRequiredFields(['multiContent']);
            }
        }
        console.debug('state now', this.state);
    }

    onUpdateStartDate(e) {
        this.props.onUpdateStartDate(e.target.value);
    }

    onUpdateEndDate(e) {
        this.props.onUpdateEndDate(e.target.value);
    }

    onUpdateDisplayMode(e) {
        this.props.onUpdateDisplayMode(e.target.value);
    }

    onUpdateGranularity(e) {
        this.props.onUpdateGranularity(e.target.value);
    }

    onUpdateMetric(e) {
        this.props.onUpdateMetric(e.target.value);
    }

    onUpdateUnit(e) {
        this.props.onUpdateUnit(parseInt(e.target.value, 10));
    }

    onUpdateCourse(e) {
        this.props.onUpdateCourse(parseInt(e.target.value, 10));
    }

    onUpdateMultiContent(e) {
        this.props.onUpdateMultiContent(map(e.target.selectedOptions, (opt, ind) => opt.value));
    }

    onLangChange(e) {
        this.props.onLangChange(parseInt(e.target.value, 10));
    }

    incrementDate(initial_date, direction, granularity) {
        console.debug('incrementDate', direction, granularity);
        let out_date;
        if (isUTCDate(initial_date)) 
            out_date = new Date(initial_date + 'T12:00:00');
        else
            out_date = new Date(initial_date);

        // let out_date = new Date(urlFormatDate(initial_date) + 'T12:00:00');
        console.debug(out_date);
        if (out_date == 'Invalid Date') return initial_date;

        initial_date = new Date(out_date);

        if (granularity == 'Weekly') {
            let increment = (direction == 'plus') ? 7 : -7;
            out_date.setDate(out_date.getDate() + increment);
        } else if (granularity == 'Monthly') {
            let increment = (direction == 'plus') ? 1 : -1;
            out_date.setMonth(out_date.getMonth() + increment);

            /*
                12/31 -> 11/31 == 12/1
                1/31 -> 2/31 == 3/3

                Let's try to be smart with this, so we'll follow these rules/patterns:
                * If it's the last day of the month we'll keep it as the last day of the month: 
                    12/31 -> 11/30 -> 10/31 -> 9/30 ...
                * If it's not the last day of the month we'll do our best to stay on the same date
                    1/27 -> 2/27 -> 3/27
                    1/28 -> 2/28 -> 3/31 -> 4/30 -> 5/31
                * The trigger is when out_date.getMonth != initial_date.getMonth + increment
            */

            // console.debug(out_date);
            // console.debug(out_date.getMonth(), (initial_date.getMonth() + increment));
            // if (out_date.getMonth() != (initial_date.getMonth() + increment)) {
            //     console.debug('assertion failed');
            //     console.debug(out_date, initial_date);

            //  Check to see if it should be the last day of the month
            let new_date = new Date(initial_date);
            new_date.setDate(new_date.getDate() + 1);
            console.debug(new_date);
            if (new_date.getMonth() != initial_date.getMonth()) {
                //  adding a day moved us to a different month, must be the last day
                //  reset the out_date to the beginning of the month and then readd the increment. 
                //  from there we'll find the last day of the month
                out_date = new Date(initial_date);
                out_date.setDate(1);
                console.debug(out_date);
                out_date.setMonth(out_date.getMonth() + increment);
                out_date.setDate(28); // close to the end of the month but sure to not go over

                //  move up until it reaches a new month and then back down one day
                let this_month = out_date.getMonth();
                do {
                    console.debug(this_month, out_date);
                    out_date.setDate(out_date.getDate() + 1);
                } while (this_month == out_date.getMonth());

                out_date.setDate(out_date.getDate() - 1);

                console.debug(out_date);
            }
            // }

            // while (out_date.getMonth() == initial_date.getMonth()) {
            //     out_date.setDate(out_date.getDate() + increment);
            //     console.debug(out_date);
            // }
        }
        console.debug(out_date);
        return urlFormatDate(out_date);
    }

    handleStartDatePlus(e) {
        this.props.onUpdateStartDate(this.incrementDate(this.props.startDate, 'plus', this.props.granularity));
    }

    handleStartDateMinus(e) {
        this.props.onUpdateStartDate(this.incrementDate(this.props.startDate, 'minus', this.props.granularity));
    }

    handleEndDatePlus(e) {
        this.props.onUpdateEndDate(this.incrementDate(this.props.endDate, 'plus', this.props.granularity));
    }

    handleEndDateMinus(e) {
        this.props.onUpdateEndDate(this.incrementDate(this.props.endDate, 'minus', this.props.granularity));
    }

    // componentDidMount() {
    //     $('input.floatlabel').floatlabel({
    //       slideInput: false,
    //       labelStartTop: '5px',
    //       labelEndTop: '-2px'
    //     });
    // }

    setRequiredFields(requiredFields) {
        this.setState({
            'required_fields': ['mode', 'granularity'].concat(requiredFields)
        });

        console.debug('state', this.state);
    }


    requiredClasses(fieldName) {
        console.debug('requiredClasses', fieldName, this.props[fieldName], this.state.required_fields);
        console.debug(this.props);
        let is_required = (includes(this.state.required_fields, (fieldName)) && (this.props[fieldName] == '' || this.props[fieldName] == null));

        return classNames({
            'form-control': true,
            'required': is_required
        })
    }

    requiredMissing() {
        console.debug('requiredMissing', this.state.required_fields);
        let missing = false;
        this.state.required_fields.forEach((field) => {
            console.debug(field, this.props[field]);
            if (isUndefined(this.props[field]) || this.props[field] == '' || this.props[field] == null) missing = true;
        });

        console.debug('requiredMissing', missing);
        return missing;
    }

    render() {
        console.log(this.props);

        let metric_selector, unit_selector, course_selector, multi_selector, fetch_button;
        if (this.props.mode && (this.props.mode.indexOf('Total Usage by Metric') == 0 || this.props.mode.indexOf('Course Metric Comparison') == 0)) {
            metric_selector =   <Col sm={6} collapseLeft>
                                    <select id="metric" onChange={this.onUpdateMetric.bind(this)} value={this.props.metric} className={this.requiredClasses('metric')}>
                                        <option value="" >Select Metric</option>
                                        {map(DATA_FIELDS, (val, key) => 
                                            <option key={key} value={key}>{val}</option>
                                        )}
                                    </select>
                                </Col>
        }

        

        if (this.props.mode && this.props.mode.indexOf('Usage by Course') == 0) {
            unit_selector =     <Col sm={6} collapseLeft>
                                    <select id="unit" onChange={this.onUpdateUnit.bind(this)} value={this.props.unit} className={this.requiredClasses('unit')}>
                                        <option value="" >Select Unit</option>
                                        {map(this.props.units, (unit, ind) => 
                                            <option key={ind} value={unit.id}>{unit.display}</option>
                                        )}
                                    </select>
                                </Col>

            course_selector =   <Col sm={3} collapseLeft>
                                    <select id="course" onChange={this.onUpdateCourse.bind(this)} value={this.props.course} className={this.requiredClasses('course')}>
                                        <option value="" >-- ALL COURSES --</option>
                                        {map(this.props.courses, (course, ind) => 
                                            <option key={ind} value={course.id}>{course.display}</option>
                                        )}
                                    </select>
                                </Col>
        }

        if (this.props.mode && this.props.mode.indexOf('Course Metric Comparison') == 0) {
            multi_selector = <Col sm={3} collapseLeft>
                                    <select multiple className={this.requiredClasses('multiContent')} id="metric" onChange={this.onUpdateMultiContent.bind(this)} value={this.props.multiContent} size={5}>
                                        {map(this.props.contentHierarchy, (unit, ind) => 
                                            <optgroup label={unit.desc}>
                                                <option value={"u" + unit.id}>-- ALL COURSES --</option>
                                            {map(unit.courses, (course, cind) => 
                                                <option key={cind} value={"c" + course.id}>{course.desc}</option>
                                            )}
                                            </optgroup>
                                        )}
                                    </select>
                                </Col>
        }

        if (this.props.filtersAreDirty && !this.requiredMissing()) {
            let button_style = (!this.props.isFetching) ? 'danger' : 'primary';

            fetch_button = <Col sm={3} collapseLeft>
                                <Button bsStyle={button_style} md onClick={this.props.onTextFilterFetch}>{(this.props.isFetching) ? <i className='glyphicon glyphicon-refresh spinning'></i> : 'Fetch Data'}</Button>
                            </Col>
        }

        return (
            <Row>
                <Col sm={12}>
                    <Grid>
                        <Row style={{marginBottom: "4px"}}>
                            <Col sm={12} collapseLeft>
                                <select id="mode" onChange={this.onUpdateDisplayMode.bind(this)} value={this.props.mode} className={this.requiredClasses('mode')}>
                                    <option value="" >Select Display Mode</option>
                                    {map(DisplayModes, (val, key) => 
                                        <option key={key} value={key}>{key}</option>
                                    )}
                                </select>
                            </Col>
                        </Row>

                        <Row style={{marginBottom: "4px"}}>
                            <Col sm={6}>
                                <Row style={{marginBottom: "4px"}}>
                                    <Col sm={6}>
                                        <Row>
                                            <Col sm={12} collapseLeft>
                                                <select id="lang" onChange={this.onLangChange.bind(this)} value={this.props.lang} className={this.requiredClasses('lang')}>
                                                    <option value="" >Select Language</option>
                                                    {map(this.props.langs, (val, key) => 
                                                        <option key={val} value={val}>{key}</option>
                                                    )}
                                                </select>
                                            </Col>
{/*
                                            <LangFilter 
                                                langs={this.props.langs} 
                                                lang={this.props.lang}
                                                onLangChange={this.props.onLangChange}
                                            />
*/}
                                        </Row>

                                    </Col>

                                    {metric_selector}
                                    {unit_selector}
                                </Row>
                                <Row style={{marginBottom: "4px"}}>
                                    <Col sm={6}>
                                        <Row>
                                            <Col sm={11} collapseLeft collapseRight>
                                                <Input md className="floatlabel" type='text' placeholder="Start Date" name="start_date" value={this.props.startDate} onChange={this.onUpdateStartDate.bind(this)} />
                                            </Col>
                                            <Col sm={1} collapseLeft>
                                                <Grid collapse style={{lineHeight: 1, marginLeft: "3px"}}>
                                                    <Row>
                                                        <Col sm={12}><i className="glyphicon glyphicon-menu-up" style={{cursor: "pointer"}} onClick={this.handleStartDatePlus.bind(this)}></i></Col>
                                                        
                                                    </Row>
                                                    <Row collapse>
                                                        <Col sm={12}><i className="glyphicon glyphicon-menu-down" style={{cursor: "pointer"}} onClick={this.handleStartDateMinus.bind(this)}></i></Col>
                                                    </Row>
                                                </Grid>
                                            </Col>
                                        </Row>
                                    </Col>

                                    <Col sm={6}>
                                        <Row>
                                            <Col sm={11} collapseLeft collapseRight>
                                                <Input md className="floatlabel" type='text' placeholder="End Date" name="end_date" value={this.props.endDate} onChange={this.onUpdateEndDate.bind(this)} />
                                            </Col>
                                            <Col sm={1} collapseLeft>
                                                <Grid collapse style={{lineHeight: 1, marginLeft: "3px"}}>
                                                    <Row>
                                                        <Col sm={12}><i className="glyphicon glyphicon-menu-up" style={{cursor: "pointer"}} onClick={this.handleEndDatePlus.bind(this)}></i></Col>
                                                        
                                                    </Row>
                                                    <Row collapse>
                                                        <Col sm={12}><i className="glyphicon glyphicon-menu-down" style={{cursor: "pointer"}} onClick={this.handleEndDateMinus.bind(this)}></i></Col>
                                                    </Row>
                                                </Grid>
                                            </Col>
                                        </Row>
                                        
                                    </Col>
                                </Row>

                                <Row style={{marginBottom: "4px"}}>
                                    <Col sm={6} collapseLeft>
                                        <select control id="mode" onChange={this.onUpdateGranularity.bind(this)} value={this.props.granularity} className={this.requiredClasses(['granularity'])}>
                                            <option value="" >Select Interval</option>
                                            {map(GranularityOptions, (val, key) => 
                                                <option key={key} value={val}>{val}</option>
                                            )}
                                        </select>
                                    </Col>

                                    {fetch_button}
                                </Row>
                            </Col>
                            
                            {course_selector}
                            {multi_selector}
                        </Row>

                        <Row style={{marginBottom: "4px"}}>
                            
                        </Row>
                        
                    </Grid>
                </Col>
            </Row>
        )
    }
}

Filters.state = {
    required_fields: ['mode', 'granularity']
}
