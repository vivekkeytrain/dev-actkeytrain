import { combineReducers } from 'redux';
import { 
    urlFormatDate,
    LangOptions, DisplayModes, START_DATE_DEFAULT, GranularityOptions,
    ADD_MESSAGE, CLOSE_MESSAGE,
    FETCH_EDITOR_CONTENT_REQUEST, FETCH_EDITOR_CONTENT_SUCCESS, FETCH_EDITOR_CONTENT_FAILURE,
    SET_START_DATE, SET_END_DATE, SET_GRANULARITY, SET_LANG, SET_MODE, SET_METRIC, SET_MULTI_CONTENT, SET_UNIT, SET_COURSE, SET_LESSON,
    FETCH_SESSION_DATA_REQUEST, FETCH_SESSION_DATA_SUCCESS, FETCH_SESSION_DATA_FAILURE
} from './actions';

import { some, map } from 'lodash';

/*  Given a date return the day from the end of the week, i.e. Saturday */
export function endOfWeek(d) {
    return new Date(
        d.getFullYear(),
        d.getMonth(),
        d.getDate() + 6 - d.getDay()
    );
}

/*  Given a date return the day from the start of the week, i.e. Sunday */
export function startOfWeek(d) {
    return new Date(
        d.getFullYear(),
        d.getMonth(),
        d.getDate() - d.getDay()
    );
}


let end_date = urlFormatDate(endOfWeek(new Date()));
let start_date = startOfWeek(new Date());
start_date.setMonth(start_date.getMonth()-6);
start_date = urlFormatDate(start_date);

function filters(state = {
    start_date: start_date, //START_DATE_DEFAULT,
    end_date: end_date, //'2015-12-31',
    granularity: GranularityOptions[0], // Weekly
    metric: '',
    lang: null,
    mode: DisplayModes[0],
    content_arr: [],
    unit: null,
    course: null,
    lesson: null,
    messages: []
}, action) {
    switch (action.type) {
        case SET_START_DATE:
            return Object.assign({}, state, {
                start_date: action.start_date
            });
        case SET_END_DATE:
            return Object.assign({}, state, {
                end_date: action.end_date
            });
        case SET_GRANULARITY:
            return Object.assign({}, state, {
                granularity: action.granularity
            });
        case SET_LANG:
            return Object.assign({}, state, {
                lang: action.lang
            });
        case SET_MODE:
            return Object.assign({}, state, {
                mode: action.mode
            });
        case SET_METRIC:
            return Object.assign({}, state, {
                metric: action.metric
            });
        case SET_MULTI_CONTENT:
            return Object.assign({}, state, {
                content_arr: action.content_arr
            })
        case SET_UNIT:
            return Object.assign({}, state, {
                unit: action.unit,
                course: ''
            });
        case SET_COURSE:
            //  Can't set course if we haven't set unit
            if (state.unit == '') return state;

            return Object.assign({}, state, {
                course: action.course
            });

        case FETCH_EDITOR_CONTENT_FAILURE:
        case FETCH_SESSION_DATA_FAILURE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: 'danger',
                        message: action.ex.message,
                        visible: true
                    }
                ]
            });

        case ADD_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: action.message_type,
                        message: action.message,
                        visible: true
                    }
                ]
            });

        case CLOSE_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages.slice(0, action.index),
                    Object.assign({}, state.messages[action.index], {
                        visible: false
                    }),
                    ...state.messages.slice(action.index+1)
                ]
            });

        default:
            return state
    }
}

function editor_content(state = {
    isFetching: false,
    content: []
}, action) {
    switch (action.type) {
        case FETCH_EDITOR_CONTENT_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                content: action.content,
                lastUpdated: action.receivedAt
            });
        case FETCH_EDITOR_CONTENT_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_EDITOR_CONTENT_FAILURE:
            // console.debug(action.ex);
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

function session_data(state = {}, action) {
    // console.debug(action);
    switch (action.type) {
        case FETCH_SESSION_DATA_REQUEST:
            return Object.assign({}, state, {
                 [`${action.segment_start_date}-${action.segment_end_date}`]: session_data_by_date(state[`${action.start_date}-${action.end_date}`], action)
            })
        case FETCH_SESSION_DATA_SUCCESS:
        case FETCH_SESSION_DATA_FAILURE:
            return Object.assign({}, state, {
                [`${action.start_date}-${action.end_date}`]: session_data_by_date(state[`${action.start_date}-${action.end_date}`], action)
            });
        default:
            return state;
    }
}

function session_data_by_date(state = {
    isFetching: false,
    data: []
}, action) {
    // console.debug(action);
    switch (action.type) {
        case FETCH_SESSION_DATA_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_SESSION_DATA_SUCCESS:
            let data = map(action.data, (row) => {
                row.time_seconds = Math.round(row.time_seconds / 3600);
                return row;
            });
            // console.debug(data);

            return Object.assign({}, state, {
                isFetching: false,
                data
            });
        case FETCH_SESSION_DATA_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

function session_data_segments(state = {}, action) {
    switch (action.type) {
        case FETCH_SESSION_DATA_REQUEST:
            return Object.assign({}, state, {
                [action.granularity]: session_data_by_granularity(state[action.granularity], action)
            });
        default:
            return state;
    }
}

function session_data_by_granularity(state = {}, action) {
    console.debug('session_data_by_granularity', action);
    switch (action.type) {
        case FETCH_SESSION_DATA_REQUEST:
            console.debug(`${urlFormatDate(action.filter_start_date)}-${urlFormatDate(action.filter_end_date)}`);
            return Object.assign({}, state, {
                [`${urlFormatDate(action.filter_start_date)}-${urlFormatDate(action.filter_end_date)}`]: session_data_granularity_segments(state[`${urlFormatDate(action.filter_start_date)}-${urlFormatDate(action.filter_end_date)}`], action)
            });
        default:
            return state;
    }
}

function session_data_granularity_segments(state=[], action) {
    switch (action.type) {
        case FETCH_SESSION_DATA_REQUEST:
            //  Check to see if we already have this segment, in which case we return the current state (nothing changes)
            //  This is essentially an upsert
            let findFunc = (row) => {
                return (row.start_date === action.segment_start_date && row.end_date === action.segment_end_date);
            }

            if (some(state, findFunc)) {
                return state;
            } else {
                return [
                    ...state,
                    {
                        start_date: action.segment_start_date,
                        end_date: action.segment_end_date
                    }
                ]
            }
        default:
            return state;
    }
}

const sessionApp = combineReducers({
    filters,
    editor_content,
    session_data,
    session_data_segments
})

export default sessionApp;