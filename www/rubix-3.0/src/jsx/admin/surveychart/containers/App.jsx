import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { forEach } from 'lodash';
import { 
    fetchQuestions, fetchSurveyData, fetchSurveyAverageData, LangOptions, setLang
} from '../actions';

import { filteredContentSelector } from '../selectors';
import Chart from '../components/Chart';
import Questions from '../components/Questions';
import LangFilter from '../../common/LangFilter';

class App extends Component {
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchQuestions());
        dispatch(fetchSurveyData());
        forEach(LangOptions, (v, k) => {
            dispatch(fetchSurveyAverageData(v));
        })
    }

    render() {
        //  Injected by connect() call:
        const { dispatch, questions, question_ids, question_answers, answer_vals, answer_sums_max, answer_sums_max_by_lang, answer_sums_arr, answer_sums_arr_by_lang, answer_avgs, answer_avgs_by_lang, lang, survey_average_data } = this.props;

        // <Chart questionIds={question_ids} answerVals={answer_vals} answerSumsArr={answer_sums_arr} answerSumsMax={answer_sums_max} />
        console.debug(this.props);
        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PanelContainer controlStyles='bg-blue fg-white'>
                                <PanelHeader className='bg-blue fg-white' style={{margin: 0}}>
                                    <Grid>
                                        <Row>
                                            <Col xs={12}>
                                                <h3>Survey Results</h3>
                                                <p>
                                                    This chart shows results from the post-lesson survey presented to learners. Currently the report covers responses for all time (beginning 9/1/2015).<br />
                                                    Bars show quantity of responses for each discrete answer for each question. Hover over a bar for details.<br />
                                                    Circles show the average response. Hover over a circle for exact value.<br />
                                                    When showing both English and Spanish results, counts are equalized and there are two sets of Y axis values (Spanish on the right).<br />
                                                    For questions except Q04 (audio speed), 1=best; 5=worst.<br />
                                                    Questions are listed below the chart.<br />
                                                </p>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelHeader>
                                <PanelBody>
                                    <Grid>
                                        <Row>
                                            <Col xs={12} style={{padding: 25}}>
                                                <LangFilter 
                                                    langs={LangOptions} 
                                                    lang={lang}
                                                    onLangChange={(lang_id) => {
                                                        dispatch(setLang(lang_id))
                                                    }}
                                                />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs={12}>
                                                <Chart 
                                                    width={1200} 
                                                    height={700} 
                                                    lang={lang}
                                                    questionIds={question_ids}
                                                    answerSumsMax={answer_sums_max}
                                                    answerSumsMaxByLang={answer_sums_max_by_lang}
                                                    answerSumsArr={answer_sums_arr}
                                                    answerSumsArrByLang={answer_sums_arr_by_lang}
                                                    answerVals={answer_vals}
                                                    answerAvgs={answer_avgs}
                                                    answerAvgsByLang={answer_avgs_by_lang}
                                                    questionAnswers={question_answers} 
                                                    surveyAvgData={survey_average_data} />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs={12}>
                                                <Questions questions={questions} />
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelBody>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        )
    }
}

export default connect(filteredContentSelector)(App);
