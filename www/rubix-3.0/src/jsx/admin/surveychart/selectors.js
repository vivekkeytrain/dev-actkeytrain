import { createSelector } from 'reselect';
import { LangOptions } from './actions';

import { forEach, fill, fromPairs, map, reduce, max, filter, invert, keys, zip } from 'lodash';

function selectQuestionIds(questions) {
    // console.debug('selectQuestionIds', questions);
    return map(questions, 'id');
}

function selectQuestionAnswers(questions) {
    return fromPairs(zip(map(questions, 'id'), map(questions, 'answers')));
}

/*  An array of possible answer vals   */
function selectAnswerVals(questions) {
    let answer_vals = [0];
    forEach(questions, (question) => {
        forEach(question.answers, (answer, ind) => {
            answer_vals[ind+1] = ind+1; //  0 is unanswered
        });
    });

    return answer_vals;
}

/*  Object of question ids to an array of all answer vals for that question  */
function selectSurveyQuestionAnswers(survey_data, question_ids) {
    // console.debug('selectSurveyQuestionAnswers', survey_data, question_ids);
    let answers = map(question_ids, (question_id) => {
        return map(survey_data, question_id);
    });
    // console.debug(zip(question_ids, answers));
    let question_answers = fromPairs(zip(question_ids, answers));

    // let question_answers = fromPairs([question_ids, map(question_ids, (question_id) => {
    //     // console.debug(question_id, map(survey_data, question_id));
    //     return map(survey_data, question_id);
    // })]);
    // console.debug(question_answers);
    return question_answers;
}

function selectSurveyQuestionAnswersByLang(survey_data, question_ids) {
    /*
        {
            'en': {
                'Q1': [1, 2, 3, 2, 1, ...]
            }
        }
    */
    let retObj = {};
    forEach(keys(LangOptions), (lang_code) => {
        let lang_answers = filter(survey_data, (row) => {
            return row.lang_code == lang_code.toLowerCase();
        });

        retObj[lang_code.toLowerCase()] = fromPairs(zip(question_ids, map(question_ids, (question_id) => {
            return map(lang_answers, question_id);
        })));
    })
    return retObj;
}

/*  Count of each answer in each question   */
function selectAnswerSums(question_ids, answer_vals, survey_data, survey_question_answers) {
    // console.debug('selectAnswerSums', question_ids, answer_vals, survey_question_answers);
    // console.debug(survey_question_answers);
    let answer_sums = {};
    forEach(question_ids, (id) => {
        //  An initially 0-filled counter for each possible answer
        let question_answer_sums = fill(Array(answer_vals.length), 0);
        // console.debug(question_answer_sums);

        //  Get all the survey answers for this question
        // let question_answers = map(survey_data, id);
        let question_answers = survey_question_answers[id];
        // console.debug(question_answers);

        //  Add up all of the answers at each possible val
        forEach(question_answers, (answer_val) => {
            // console.debug(answer_val);
            question_answer_sums[answer_val]++;
        });
        // console.debug(question_answer_sums);
        answer_sums[id] = question_answer_sums;
    });

    // console.debug(answer_sums);
    return answer_sums;
}

function selectAnswerSumsByLang(question_ids, answer_vals, survey_data, survey_question_answers) {
    // console.debug(survey_question_answers);
    let answer_sums = {};
    forEach(question_ids, (id) => {
        //  An initially 0-filled counter for each possible answer
        let question_answer_sums = {};
        forEach(keys(LangOptions), (key) => {
            question_answer_sums[key.toLowerCase()] = fill(Array(answer_vals.length), 0);

            let question_answers = survey_question_answers[key.toLowerCase()][id];

            forEach(question_answers, (answer_val) => {
                question_answer_sums[key.toLowerCase()][answer_val]++;
            })
        });
        
        answer_sums[id] = question_answer_sums;
    });

    // console.debug(answer_sums);
    return answer_sums;
}

function selectAnswerSumMax(answer_sums) {
    return reduce(answer_sums, (memo, vals, question_id) => {
        return max([memo, max(vals)]);
    })
}

function selectAnswerSumMaxByLang(answer_sums) {
    let retObj = {};
    forEach(answer_sums, (langs, question_id) => {
        forEach(langs, (vals, lang_code) => {
            if (!retObj[lang_code]) retObj[lang_code] = 0;
            retObj[lang_code] = max([retObj[lang_code], max(vals)]);
        });
    });

    return retObj;
}

function selectAnswerSumsArray(answer_sums) {
    return map(answer_sums, (v, k) => {
        return { 'question': k, 'sums': v };
    });
}

function selectAnswerAvgs(answer_sums) {
    let answer_avgs = {};
    // console.debug(answer_sums);
    forEach(answer_sums, (question_answer_sums, question_id) => {
        // console.debug(question_id, question_answer_sums);
        var sum = reduce(question_answer_sums, (memo, num) => {
            return memo + num;
        });
        // console.debug(sum);
        answer_avgs[question_id] = map(question_answer_sums, (count) => {
            return count / sum;
        });
    });

    return answer_avgs;
}

function selectAnswerAvgsByLang(answer_sums) {
    let answer_avgs = {};
    // console.debug(answer_sums);
    forEach(answer_sums, (question_answer_sums, question_id) => {
        forEach(keys(question_answer_sums), (lang_code) => {
            let sum = reduce(question_answer_sums[lang_code], (memo, num) => {
                return memo + num;
            });
            // console.debug(sum);
            if (!answer_avgs[question_id]) answer_avgs[question_id] = {};
            answer_avgs[question_id][lang_code] = map(question_answer_sums[lang_code], (count) => {
                return count / sum;
            });
        })
    });

    return answer_avgs;
}

function selectFilteredSurveyData(survey_data, lang_id) {
    if (lang_id) {
        let lang_code = invert(LangOptions)[lang_id].toLowerCase();
        console.debug(lang_code);

        survey_data = filter(survey_data, (row) => {
            // console.debug(row);
            return row.lang_code == lang_code;
        });
    }

    return survey_data;
}

/*  A hash, keyed on question_id, containing the sums, avgs, and answer text for each question  */

const questionsSelector = state => state.questions.items;
const rawSurveyDataSelector = state => state.survey_data.data;
const surveyAverageDataSelector = state => state.survey_average_data;
const langSelector = state => state.filters.lang;

const surveyDataSelector = createSelector(
    rawSurveyDataSelector,
    langSelector,
    (questions, lang) => {
        return selectFilteredSurveyData(questions, lang);
    }
)

const questionIdsSelector = createSelector(
    questionsSelector,
    (questions) => {
        return selectQuestionIds(questions);
    }
);

const questionAnswersSelector = createSelector(
    questionsSelector,
    (questions) => {
        return selectQuestionAnswers(questions);
    }
);

const answerValsSelector = createSelector(
    questionsSelector,
    (questions) => {
        return selectAnswerVals(questions);
    }
)

const surveyQuestionAnswersSelector = createSelector(
    surveyDataSelector,
    questionIdsSelector,
    (survey_data, question_ids) => {
        return selectSurveyQuestionAnswers(survey_data, question_ids);
    }
)

const surveyQuestionAnswersByLangSelector = createSelector(
    surveyDataSelector,
    questionIdsSelector,
    (survey_data, question_ids) => {
        return selectSurveyQuestionAnswersByLang(survey_data, question_ids);
    }
)

const answerSumsSelector = createSelector(
    questionIdsSelector,
    answerValsSelector,
    surveyDataSelector,
    surveyQuestionAnswersSelector,
    (question_ids, answer_vals, survey_data, survey_question_answers) => {
        return selectAnswerSums(question_ids, answer_vals, survey_data, survey_question_answers);
    }
);

const answerSumsByLangSelector = createSelector(
    questionIdsSelector,
    answerValsSelector,
    surveyDataSelector,
    surveyQuestionAnswersByLangSelector,
    (question_ids, answer_vals, survey_data, survey_question_answers) => {
        return selectAnswerSumsByLang(question_ids, answer_vals, survey_data, survey_question_answers);
    }
);

const answerSumMaxSelector = createSelector(
    answerSumsSelector,
    (answer_sums) => {
        return selectAnswerSumMax(answer_sums);
    }
);

const answerSumMaxByLangSelector = createSelector(
    answerSumsByLangSelector,
    (answer_sums) => {
        return selectAnswerSumMaxByLang(answer_sums);
    }
);

const answerSumsArraySelector = createSelector(
    answerSumsSelector,
    (answer_sums) => {
        return selectAnswerSumsArray(answer_sums);
    }
);

const answerSumsArrayByLangSelector = createSelector(
    answerSumsByLangSelector,
    (answer_sums) => {
        return selectAnswerSumsArray(answer_sums);
    }
);

const answerAvgsSelector = createSelector(
    answerSumsSelector,
    (answer_sums) => {
        return selectAnswerAvgs(answer_sums);
    }
);

const answerAvgsByLangSelector = createSelector(
    answerSumsByLangSelector,
    (answer_sums) => {
        return selectAnswerAvgsByLang(answer_sums);
    }
);

// questions, question_ids, question_answers, answer_vals, answer_sums_max, answer_sums_arr, answer_avgs
export const filteredContentSelector = createSelector(
    questionsSelector,
    questionIdsSelector,
    questionAnswersSelector,
    answerValsSelector,
    answerSumMaxSelector,
    answerSumMaxByLangSelector,
    answerSumsArraySelector,
    answerSumsArrayByLangSelector,
    answerAvgsSelector,
    answerAvgsByLangSelector,
    langSelector,
    surveyAverageDataSelector,
    (questions, question_ids, question_answers, answer_vals, answer_sums_max, answer_sums_max_by_lang, answer_sums_arr, answer_sums_arr_by_lang, answer_avgs, answer_avgs_by_lang, lang, survey_average_data) => {
        return {
            questions,
            question_ids,
            question_answers,
            answer_vals,
            answer_sums_max, 
            answer_sums_max_by_lang,
            answer_sums_arr,
            answer_sums_arr_by_lang,
            answer_avgs,
            answer_avgs_by_lang,
            lang,
            survey_average_data
        }
    }
);