import React, { Component } from 'react';
import d3 from 'd3';
import tip from 'd3-tip';

import { forEach, map, flatten, compact, chain, keys, fill, includes, invert } from 'lodash';

import { LangOptions } from '../actions';

const   margin = {top: 10, right: 55, bottom: 30, left: 60};
// const   possible_colors = ['#339966', '#FF6666', '#FFCC66', '#FF0066', '#003399', '#333366', '#3399CC'];
// // const   darker_colors = ['#CC6699', '#009999', '#003399', '#00FF99', '#FFCC66', '#CCCC99', '#CC6633'];
// const   darker_colors = ['#194D33', '#FF0000', '#FFAA00', '#99003D', '#001133', '#111122', '#1F5C7A'];

const   possible_colors = ['#339966', '#333366', '#FF6666', '#3399CC', '#FFCC66', '#FF0066', '#003399'];
const   darker_colors = ['#194D33', '#111122', '#FF0000', '#1F5C7A', '#FFAA00', '#99003D', '#001133'];

const   avg_colors = ['red', 'green', 'blue', 'yellow'];

//     width = 1100 - margin.left - margin.right,
//     height = 700 - margin.top - margin.bottom;


// let y = d3.scale.linear().range([height, 0]);

// let xAxis = d3.svg.axis().scale(x).orient('bottom');
// let yAxisLeft = d3.svg.axis().scale(y).orient("left");

// class ToolTip extends Component {
//     render() {
//         let text = (this.props.index == 0) ? <i>SKIPPED</i> : <div style={{'text-align': center, 'width': '100%'}}>{this.props.questionAnswers}
//         return (
//             <div className="d3-tip n" style={{'position': 'absolute', 'opacity': 0}}>

//             </div>
//         )
//     }
// }

class Bar extends Component {
    constructor(props) {
        super(props);

        this.props = {
            width: 0,
            height: 0,
            offset: 0,
            yOffset: 0
        }
    }

    showTip(e) {
        let toolTip = this.props.toolTip;
        // let avg = this.props.answerAvgs[this.props.question][this.props.index]
        let avg = this.props.avg;

        let text = ((this.props.lang) ? '<div style="text-align: center; width: 100%">' + this.props.lang.toUpperCase() + '</div>' : '') + ((this.props.index == 0) ? '<i>SKIPPED</i>' : this.props.questionAnswers[this.props.question][this.props.index-1]) + '<div style="text-align: center; width: 100%">' + this.props.sum + ' (' + Math.round(avg * 1000) / 10 + '%)</div>';
        toolTip.html(text).show(e.target);
    }

    hideTip(e) {
        let toolTip = this.props.toolTip;
        toolTip.hide(e.target);
    }

    render() {
        //onMouseOver={this.showTip.bind(this)} onMouseOut={this.hideTip.bind(this)}
        return (
            <rect fill={this.props.color} width={this.props.width} height={this.props.height} x={this.props.offset} y={this.props.availableHeight - this.props.height + this.props.yOffset} onMouseOver={this.showTip.bind(this)} onMouseOut={this.hideTip.bind(this)} />
        );
    }
}

Bar.defaultProps = {
    width: 0,
    height: 0,
    offset: 0,
    yOffset: 0
}

class Circle extends Component {
    //return <circle stroke="black" fill={avg_colors[ind]} r={x.rangeBand() / 8} cx={x(question_id) + (x.rangeBand() / 2)} cy={y3(avg)} />
    constructor(props) {
        super(props);

        this.props = {
            fill: null,
            r: 0,
            cx: 0,
            cy: 0,
            avg: 0,
            lang: ''
        }
    }

    showTip(e) {
        let toolTip = this.props.toolTip;
        toolTip.html(`<div style="text-align: center; width: 100%">${this.props.lang.toUpperCase()}</div><div style="text-align: center; width: 100%">${Math.round(this.props.avg*10)/10}</div>`).show(e.target);
    }

    hideTip(e) {
        this.props.toolTip.hide(e.target);
    }

    render() {
        return <circle stroke="black" fill={this.props.fill} r={this.props.r} cx={this.props.cx} cy={this.props.cy} onMouseOver={this.showTip.bind(this)} onMouseOut={this.hideTip.bind(this)} />
    }
}

Circle.defaultProps = {
    fill: null,
    r: 0,
    cx: 0,
    cy: 0,
    avg: 0,
    lang: ''
}

export default class Chart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            x: null,
            y: null,
            y2: null,
            y3: null,
            xAxis: null,
            yAxis: null,
            yAxisRight: null,
            yAxisRight2: null,
            width: props.width,
            height: props.height,
            color: null,
            darkerColor: null,
            toolTip: null
        }
    }

    componentDidMount() {
        let width = this.props.width;
        let height = this.props.height;

        let svg = d3.select('#svg_chart');
        let x = d3.scale.ordinal().rangeRoundBands([0, width], .1);
        let xAxis = d3.svg.axis().scale(x).orient('bottom');
        
        let y = d3.scale.linear().range([height, 0]);
        let y2 = d3.scale.linear().range([height, 0]);
        let y3 = d3.scale.linear().range([height, 0]).domain([0, 5]);

        let yAxis = d3.svg.axis().scale(y).orient('left');
        let yAxisRight = d3.svg.axis().scale(y2).orient('left');
        let yAxisRight2 = d3.svg.axis().scale(y3).orient('right');

        let color = d3.scale.ordinal();
        color.range(possible_colors).domain(this.props.answerVals);

        let darkerColor = d3.scale.ordinal();
        darkerColor.range(darker_colors).domain(this.props.answerVals);
        
        let toolTip = tip()
            .attr('class', 'd3-tip');

        svg.call(toolTip);

        this.setState({
            x: x, 
            xAxis: xAxis,
            y: y,
            y2: y2,
            y3: y3,
            yAxis: yAxis,
            yAxisRight: yAxisRight,
            yAxisRight2: yAxisRight2,
            width: width,
            height: height,
            color: color,
            darkerColor: darkerColor,
            toolTip: toolTip
        });
    }

    componentWillUnmount() {

    }

    componentWillReceiveProps(props) {
        console.debug(props);
        console.debug(this.state);

        if (!this.state.x) return;

        let x = this.state.x;
        x.domain(props.questionIds);

        let xAxis = this.state.xAxis;
        let svg = d3.select('#svg_chart');
        d3.select("#xaxis")
            .attr("transform", "translate(0," + this.state.height + ")")
            .call(xAxis);

        let y = this.state.y;
        let y2 = this.state.y2;
        let y3 = this.state.y3;

        if (!props.lang) {
            y.domain([0, props.answerSumsMaxByLang['en']]);
            y2.domain([0, props.answerSumsMaxByLang['es']]);

            let yAxisRight = this.state.yAxisRight;
            d3.select('#yaxisRight').call(yAxisRight);

            
        } else {
            y.domain([0,props.answerSumsMax]);
        }

        let yAxis = this.state.yAxis;
        d3.select('#yaxis')
            .call(yAxis);

        let yAxisRight2 = this.state.yAxisRight2;
        d3.select('#yaxisRight2').call(yAxisRight2);

    }

    render() {
        // console.debug(this.props);
        // console.debug(this.state);

        let color = this.state.color;
        let darkerColor = this.state.darkerColor;
        let x = this.state.x;
        let y = this.state.y;
        let y2 = this.state.y2;
        let y3 = this.state.y3;
        let answer_vals = this.props.answerVals;
        let answer_sums_arr = this.props.answerSumsArr;
        let answer_sums_arr_by_lang  = this.props.answerSumsArrByLang;

        let bars, yAxisRight, circles = [];
        if (this.props.lang) {
            let lang_code = invert(LangOptions)[this.props.lang].toLowerCase();

            bars = chain(answer_sums_arr).map((sums_obj) => {
                return map(answer_vals, (val, i) => {
                    if (sums_obj.sums[i]) {
                        return <Bar 
                            width={x.rangeBand() / answer_vals.length} 
                            height={this.props.height - y(sums_obj.sums[i])}
                            offset={x(sums_obj.question) + (x.rangeBand() / answer_vals.length * i)}
                            availableHeight={this.props.height}
                            color={color(i)}
                            toolTip={this.state.toolTip}
                            question={sums_obj.question}
                            index={i}
                            sum={sums_obj.sums[i]}
                            questionAnswers={this.props.questionAnswers}
                            avg={this.props.answerAvgsByLang[sums_obj.question][lang_code][i]}
                        />
                    }
                }, this);
            }, this).flatten().compact().value();

            yAxisRight = <g>
                            <g className="y axis" id="yaxisRight2" transform={"translate(" + this.props.width + ",0)"}>
                                <text transform='rotate(-90)' y='10' dy='.71em' x="-15" style={{'textAnchor': 'end'}}>AVG</text>
                            </g>
                        </g>

            
            circles = map(this.props.surveyAvgData[lang_code].data, (avg, question_id) => {
                if (includes(this.props.questionIds, question_id)) {
                    return <Circle fill={avg_colors[0]} r={x.rangeBand() / 8} cx={x(question_id) + (x.rangeBand() / 2)} cy={y3(avg)} avg={avg} toolTip={this.state.toolTip} lang={lang_code} />
                }
            });

        } else {
            //  Stacked bars
            // bars = chain(answer_sums_arr_by_lang).map((sums_obj) => {
            //     let yOffsets = fill(Array(answer_vals.length), 0);
            //     let answer_bars = [];
            //     forEach(keys(sums_obj.sums), (lang_code, ind) => {
            //         answer_bars.push(map(answer_vals, (val, i) => {
            //             // console.debug(lang_code, sums_obj.sums[lang_code][i], y(sums_obj.sums[lang_code][i]), yOffsets[i]);
            //             let height = this.props.height - y(sums_obj.sums[lang_code][i]);
            //             if (sums_obj.sums[lang_code][i]) {
            //                 let bar_color = (i == 0) ? color(i) : darkerColor(i);
            //                 let bar = <Bar 
            //                     width={x.rangeBand() / answer_vals.length} 
            //                     height={this.props.height - y(sums_obj.sums[lang_code][i])}
            //                     yOffset={yOffsets[i]}
            //                     offset={x(sums_obj.question) + (x.rangeBand() / answer_vals.length * i)}
            //                     availableHeight={this.props.height}
            //                     color={bar_color}
            //                     toolTip={this.state.toolTip}
            //                     question={sums_obj.question}
            //                     index={i}
            //                     sum={sums_obj.sums[lang_code][i]}
            //                     questionAnswers={this.props.questionAnswers}
            //                     answerAvgs={this.props.answerAvgs}
            //                 />
            //                 yOffsets[i] += height;

            //                 return bar;
            //             }
            //         }, this));
            //         // console.debug(answer_bars);
                    
            //     }, this);
            //     return answer_bars;
            // }, this).flatten().compact().value();

            console.debug(answer_sums_arr_by_lang);
            bars = chain(answer_sums_arr_by_lang).map((sums_obj) => {
                let answer_bars = [];
                forEach(keys(sums_obj.sums), (lang_code, ind) => {
                    answer_bars.push(map(answer_vals, (val, i) => {
                        // console.debug(val);
                        // console.debug(sums_obj.sums);
                        // console.debug(sums_obj.sums[lang_code]);
                        // console.debug(sums_obj.sums[lang_code][i]);
                        if (sums_obj.sums[lang_code][i]) {
                            let bar_color, 
                                height, 
                                offset, 
                                width = x.rangeBand() / answer_vals.length / 2 * .8;
                            if (lang_code == 'en') {
                                bar_color = color(i);
                                height = this.props.height - y(sums_obj.sums[lang_code][i]);
                                offset = x(sums_obj.question) + (x.rangeBand() / answer_vals.length * i)
                            } else {
                                bar_color = darkerColor(i);
                                height = this.props.height - y2(sums_obj.sums[lang_code][i]);
                                offset = x(sums_obj.question) + (x.rangeBand() / answer_vals.length * i) + width;
                            }
                            console.debug(lang_code, i, sums_obj.sums[lang_code][i], height);
                            return <Bar 
                                width={width} 
                                height={height}
                                offset={offset}
                                availableHeight={this.props.height}
                                color={bar_color}
                                toolTip={this.state.toolTip}
                                question={sums_obj.question}
                                index={i}
                                sum={sums_obj.sums[lang_code][i]}
                                questionAnswers={this.props.questionAnswers}
                                avg={this.props.answerAvgsByLang[sums_obj.question][lang_code][i]}
                                lang={lang_code}
                            />
                        }
                    }));
                });
                return answer_bars;
            }).flatten().compact().value();

            console.debug(bars);


            yAxisRight = <g>
                            <g className="y axis" id="yaxisRight" transform={"translate(" + this.props.width + ",0)"}>
                                <text transform='rotate(-90)' y='-40' dy='.71em' style={{'textAnchor': 'end'}}>ES Count</text>
                            </g>
                            <g className="y axis" id="yaxisRight2" transform={"translate(" + this.props.width + ",0)"}>
                                <text transform='rotate(-90)' y='10' dy='.71em' x="-15" style={{'textAnchor': 'end'}}>AVG</text>
                            </g>
                        </g>

            let ind = 0;
            forEach(this.props.surveyAvgData, (v, k) => {
                circles.push(
                    map(v.data, (avg, question_id) => {
                        if (includes(this.props.questionIds, question_id)) {
                            return <Circle fill={avg_colors[ind]} r={x.rangeBand() / 8} cx={x(question_id) + (x.rangeBand() / 2)} cy={y3(avg)} avg={avg} toolTip={this.state.toolTip} lang={k} />
                        }
                    })
                );
                ind++;
            });

        }

        // if (this.props.surveyAvgData['all']) {
        //     circles = map(this.props.surveyAvgData['all'].data, (avg, question_id) => {
        //         if (includes(this.props.questionIds, question_id)) {
        //             return <circle fill="black" r={x.rangeBand() / 8} cx={x(question_id) + (x.rangeBand() / 2)} cy={y3(avg)} />
        //         }
        //     });
        // }


        // console.debug(circles);

        // let bars = compact(flatten(map(this.props.answerVals, (val, i) => {
        //     let x = this.state.x;
        //     let y = this.state.y;
        //     let answer_vals = this.props.answerVals;
        //     let answer_sums_arr = this.props.answerSumsArr;

        //     return map(answer_sums_arr, (sums_obj) => {
        //         // console.debug(sums_obj);
        //         // console.debug('sums_obj.sums['+i+']', sums_obj.sums[i]);
        //         // console.debug(color(i));
        //         if (sums_obj.sums[i]) {
        //             return <Bar 
        //                 width={x.rangeBand() / answer_vals.length} 
        //                 height={this.props.height - y(sums_obj.sums[i])}
        //                 offset={x(sums_obj.question) + (x.rangeBand() / answer_vals.length * i)}
        //                 availableHeight={this.props.height}
        //                 color={color(i)}
        //                 toolTip={this.state.toolTip}
        //                 question={sums_obj.question}
        //                 index={i}
        //                 sum={sums_obj.sums[i]}
        //                 questionAnswers={this.props.questionAnswers}
        //                 answerAvgs={this.props.answerAvgs}
        //             />
        //         } else {
        //             return '';
        //         }
        //     }, this);

        //     // 
        // }, this)));

        // console.debug(bars);

        return (
            <svg width={this.state.width+margin.left+margin.right} height={this.state.height+margin.top+margin.bottom} style={{'float': 'left'}} id="svg_chart">
                <g transform={'translate(' + margin.left + ',' + margin.top + ')'} id="margin">
                    <g className="x axis" id="xaxis" />
                    <g className="y axis" id="yaxis">
                        <text transform='rotate(-90)' y='6' dy='.71em' style={{'textAnchor': 'end'}}>Count</text>
                    </g>
                    {yAxisRight}
                    {bars}
                    {circles}
                </g>
            </svg>
        )
    }

    // constructor(props) {
    //     super(props);

    //     this.state = {
    //         svg: null
    //     }
    // }

    // componentWillReceiveProps(props) {
    //     console.debug(props);
    // }

    // componentDidMount() {
    //     console.debug(this.props);
    //     let svg = d3.select('#chart').append('svg')
    //             .attr('width', width + margin.left + margin.right)
    //             .attr('height', height + margin.top + margin.bottom)
    //             .style('float', 'left')
    //         .append('g')
    //             .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        

    //     // $('svg').after($('<div />').attr('id', 'questions'));

        

    //     svg.append("g")
    //         .attr("class", "x axis")
    //         .attr("transform", "translate(0," + height + ")")
    //         .call(xAxis);

    //     svg.append('g')
    //         .attr('class', 'y axis')
    //         .call(yAxisLeft)
    //     .append('text')
    //         .attr('transform', 'rotate(-90)')
    //         .attr('y', 6)
    //         .attr('dy', '.71em')
    //         .style('text-anchor', 'end')
    //         .text('Count');

    //     console.debug(svg);
    //     this.setState({
    //         svg: svg
    //     })

        
    // }

    // render() {
    //     console.debug('render');
    //     if (this.state && this.state.svg) {
    //         let svg = this.state.svg;

    //         svg.selectAll("*").remove();

    //         let color = d3.scale.ordinal();

    //         let possible_colors = ['#339966', '#FF6666', '#FFCC66', '#FF0066', '#003399', '#333366', '#3399CC'];

    //         x.domain(this.props.questionIds);
    //         color.range(possible_colors).domain(this.props.answerVals);

    //         y.domain([0, this.props.answerSumsMax]);

    //         forEach(this.props.answerVals, (val, i) => {
    //             console.debug(val);
    //             // for (var i=0; i<question_vals.length; i++) {
    //                     // var q = questions[val];

    //             let tip_func = function(i) {
    //                 return function(d) {
    //                     // console.debug(d);
    //                     return ((i == 0) ? '<i>SKIPPED</i>' : d.counts.answers[i-1]) + '<div style="text-align: center; width: 100%">' + d.counts.sums[i] + ' (' + Math.round(d.counts.avgs[i] * 1000) / 10 + '%)</div>';
    //                     // return d.counts.sums[i];
    //                 }
    //             }

    //             // let tip = d3.tip()
    //             //     .attr('class', 'd3-tip')
    //             //     .html(tip_func(i));

    //             // svg.call(tip);

    //             svg.selectAll('.answer-' + i)
    //                 .data(this.props.answerSumsArr)
    //             .enter().append('rect')
    //                 .attr('width', x.rangeBand() / this.props.answerVals.length)
    //                 .attr('y', function(d) {
    //                     // console.debug(d.counts.sums[i]);
    //                     return y(d.counts.sums[i]);
    //                 })
    //                 .attr('height', function(d) {
    //                     return height - y(d.counts.sums[i]);
    //                 })
    //                 .attr('x', function(d) {
    //                     // console.debug(d);
    //                     // console.debug(d.question);
    //                     // console.debug(x(d.question));
    //                     return x(d.question) + (x.rangeBand() / this.props.answerVals.length * i);
    //                     // return x(question_objs[d.question]) + (x.rangeBand() / answer_vals.length * i);
    //                 })
    //                 .attr('class', 'answer-' + i)
    //                 .style('fill', function(d) {
    //                     return color(i);
    //                 })
    //                 // .on('mouseover', tip.show)
    //                 // .on('mouseout', tip.hide);
    //         });
    //     }

    //     return (<div id="chart" />)
    // }
}