import React, { Component } from 'react';
// import Question from './Question';

export default class Questions extends Component {
    render() {
        // console.debug(this.props);
        return (
            <div>
                {this.props.questions.map((question, ind) => 
                    <div key={"question_" + ind}>{(ind+1)} ) {question.question}</div>
                )}
            </div>
        )
    }
}


