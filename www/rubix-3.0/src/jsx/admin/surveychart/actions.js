import fetchJsonp from 'fetch-jsonp';

import { sum, values } from 'lodash';

/*  Action types    */
export const SET_LANG = 'SET_LANG';

/*  Async action types  */

export const FETCH_QUESTIONS_REQUEST = 'FETCH_QUESTIONS_REQUEST';
export const FETCH_QUESTIONS_SUCCESS = 'FETCH_QUESTIONS_SUCCESS';
export const FETCH_QUESTIONS_FAILURE = 'FETCH_QUESTIONS_FAILURE';

export const FETCH_SURVEY_DATA_REQUEST = 'FETCH_SURVEY_DATA_REQUEST';
export const FETCH_SURVEY_DATA_SUCCESS = 'FETCH_SURVEY_DATA_SUCCESS';
export const FETCH_SURVEY_DATA_FAILURE = 'FETCH_SURVEY_DATA_FAILURE';

export const FETCH_SURVEY_AVERAGE_DATA_REQUEST = 'FETCH_SURVEY_AVERAGE_DATA_REQUEST';
export const FETCH_SURVEY_AVERAGE_DATA_SUCCESS = 'FETCH_SURVEY_AVERAGE_DATA_SUCCESS';
export const FETCH_SURVEY_AVERAGE_DATA_FAILURE = 'FETCH_SURVEY_AVERAGE_DATA_FAILURE';

/*  Other constants */

export const LangOptions = {
    EN: 1,
    ES: 2
};

/*  Action creators */

export function setLang(lang) {
    return { 
        type: SET_LANG,
        lang
    }
}

function fetchQuestionsRequest() {
    return { type: FETCH_QUESTIONS_REQUEST };
}

function fetchQuestionsSuccess(questions) {
    return { 
        type: FETCH_QUESTIONS_SUCCESS,
        questions
    }
}

function fetchQuestionsFailure(ex) {
    return {
        type: FETCH_QUESTIONS_FAILURE,
        ex
    }
}

export function fetchQuestions() {
    return function(dispatch) {
        dispatch(fetchQuestionsRequest());

        return fetchJsonp('http://es.run.keytrain.com/cmiscorm/survey_questions.asp?mode=1')
            .then(
                response => response.json(),
                ex => dispatch(fetchQuestionsFailure(ex))
            )
            .then(json => dispatch(fetchQuestionsSuccess(json)))
            .catch(ex => dispatch(fetchQuestionsFailure(ex)));
    }
}

function fetchSurveyDataRequest() {
    return { type: FETCH_SURVEY_DATA_REQUEST };
}

function fetchSurveyDataSuccess(data) {
    // console.debug(data);
    return {
        type: FETCH_SURVEY_DATA_SUCCESS,
        data
    }
}

function fetchSurveyDataFailure(ex) {
    return {
        type: FETCH_SURVEY_DATA_FAILURE,
        ex
    }
}

export function fetchSurveyData() {
    return function(dispatch) {
        dispatch(fetchSurveyDataRequest());

        return fetchJsonp('http://dev.admapi.actkeytrain.com/dbwebapi/dbo.rpt_sp_survey_data_raw', {
            timeout: 20000
        })
            .then(
                response => response.json(),
                ex => dispatch(fetchSurveyDataFailure(ex))
            )
            .then(json => dispatch(fetchSurveyDataSuccess(json.ResultSets[0])))
            // .catch(ex => dispatch(fetchSurveyDataFailure(ex)));
    }
}

function fetchSurveyAverageDataRequest(lang_id) {
    return { 
        type: FETCH_SURVEY_AVERAGE_DATA_REQUEST,
        lang_id
    };
}

function fetchSurveyAverageDataSuccess(lang_id, data) {
    return {
        type: FETCH_SURVEY_AVERAGE_DATA_SUCCESS,
        lang_id,
        data
    }
}

function fetchSurveyAverageDataFailure(ex) {
    return {
        type: FETCH_SURVEY_AVERAGE_DATA_FAILURE,
        ex
    }
}

export function fetchSurveyAverageData(lang_id) {
    console.debug('fetchSurveyAverageData', lang_id);
    if (!lang_id) lang_id = sum(values(LangOptions));


    return function(dispatch) {
        dispatch(fetchSurveyAverageDataRequest(lang_id));

        console.debug(`http://dev.admapi.actkeytrain.com/dbwebapi/dbo.rpt_sp_survey_data_raw?outflags=2&lang_id=${lang_id}`);
        return fetchJsonp(`http://dev.admapi.actkeytrain.com/dbwebapi/dbo.rpt_sp_survey_data_raw?outflags=2&lang_id=${lang_id}`, {
            timeout: 20000
        })
            .then(
                response => response.json(),
                ex => dispatch(fetchSurveyAverageDataFailure(ex))
            )
            .then(json => dispatch(fetchSurveyAverageDataSuccess(lang_id, json.ResultSets[0][0])))
            // .catch(ex => dispatch(fetchSurveyAverageDataFailure(ex)));
    }
}