import { Route, Router } from 'react-router';
import BrowserHistory from 'react-router/lib/BrowserHistory';
import HashHistory from 'react-router/lib/HashHistory';

import Pagelist from 'routes/pagelist';
import SurveyChart from 'routes/surveychart';
import Announcements from 'routes/announcements';
import SurveyComments from 'routes/surveycomments';
import SessionChart from 'routes/sessionchart';
import EventViewer from 'routes/eventviewer';

import Dashboard from 'routes/dashboard';

export default (withHistory, onUpdate) => {
  const history = withHistory?
                  (Modernizr.history ?
                    new BrowserHistory
                  : new HashHistory)
                : null;
  return (
    <Router history={history} onUpdate={onUpdate}>
      <Route path='/' component={Pagelist} />
      <Route path='/survey-chart/' component={SurveyChart} />
      <Route path='/announcements/' component={Announcements} />
      <Route path='/survey-comments/' component={SurveyComments} />
      <Route path='/session-chart/' component={SessionChart} />
      <Route path='/event-viewer/' component={EventViewer} />
      
    <Route path='/active-logins/' component={Dashboard} />
</Router>
  );
};
