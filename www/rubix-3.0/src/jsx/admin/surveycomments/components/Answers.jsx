import React, { Component } from 'react';

import Answer from 'surveycomments/components/Answer';

export default class Answers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            barsRendered: false
        }
    }

    showAnswerBars() {
        // console.log('showAnswerBars');
        // console.log(this.refs);
        if (!this.state.barsRendered) {
            _.map(this.props.answers, function(v, k) {
                this.refs[this.props.datum.row_id + '-' + k].showAnswerBar();
            }, this);
            this.setState({'barsRendered': true});
        } 
    }

    renderAnswers() {
        // console.log(this.props.answers);
        console.debug(this.props);
        if (this.props.selectedRow !== this.props.datum.row_id) return '';

        var answers = _.map(this.props.answers, (v, k) => {
            var question = this.props.questions[k];
            var answer = v[this.props.datum[k]-1];
            console.log(k, question);
            // console.log(answer);
            var key = this.props.datum.row_id + '-' + k;
            var answer_id = 'answer-' + key;

            if (this.props.datum[k] != null) {
                return (
                    <Answer question={question} answer={answer} key={key} answerID={answer_id} answerVal={this.props.datum[k]} ref={key} />
                )
            } else {
                return <span />
            }
        });

        return answers;
    }

    render() {
        var answers_id = 'answers-' + this.props.datum.row_id;
        return (
            <div className="row answers" id={answers_id}>
                {this.renderAnswers()}
            </div>
        )
    }
}