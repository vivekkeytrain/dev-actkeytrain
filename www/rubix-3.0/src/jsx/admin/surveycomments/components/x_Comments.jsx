import React, { Component } from 'react';
import classNames from 'classnames';

import Comment from 'surveycomments/components/Comment';
import Loading from 'surveycomments/components/Loading';
import Answers from 'surveycomments/components/Answers';

import { map, findIndex, chain, pluck, reduce, defaults, forEach, keys, filter } from 'lodash';

const slice_size = 10;

export default class Comments extends Component {

    constructor(props) {
        // console.log(props);
        super(props);
        this.state = {
            comments: props.comments,
            loading: true,
            stati_filter: 1,
            displayed_rows: slice_size
        }
    }


    componentDidMount() {
        // console.log(this.props);
        // this.setState({
        //     'comments': this.props.comments,
        //     'displayed_rows': slice_size
        // });

        window.addEventListener('scroll', this.handleScroll.bind(this));

        if ($('body').height() <= $(window).height()) {
            this.setState({'loading': true, displayed_rows: slice_size});
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    componentDidUpdate() {
        // console.log(this.state.comments.length + ' < ' + this.state.displayed_rows);
        if (this.state.comments.length >= this.state.displayed_rows && $('body').height() <= $(window).height()) {
            this.setState({'loading': true, displayed_rows: this.state.displayed_rows+slice_size});
        }
    }

    handleDoneLoad(row_id) {
        // console.log('handleDoneLoad: ' + row_id);
        
        var comments = this.state.comments;
        var index = findIndex(comments, function(v) { return v.row_id == row_id });
        // console.log(index);
        comments[index]['loaded'] = true;

        // console.log(this.state.displayed_rows);
        // console.log(
        //     _.chain(comments.slice(0, this.state.displayed_rows))
        //                     .map(
        //                         function(el) {
        //                             return _.defaults(el, {'loaded': false})
        //                         }
        //                     )
        //                     .pluck('loaded')
        //                     .value()
        // )
        var all_loaded = chain(comments.slice(0, this.state.displayed_rows))
                            .map(
                                function(el) {
                                    return defaults(el, {'loaded': false})
                                }
                            )
                            .pluck('loaded')
                            .reduce(
                                function(m, v) {
                                    // console.log(m + ' && ' + v);
                                    return m && v;
                                }, true
                            )
                            .value();

        // console.log('all_loaded: ' + all_loaded);

        // this.setState({'loaded': all_loaded});
        this.setState({'loading': !all_loaded});
    }

    handleScroll(e) {
        // console.log('handleScroll');
        // console.log(this);
        // console.log(e);
        // console.log(window.innerHeight);
        // console.log(document.body.scrollTop);
        // console.log($('body').scrollTop());
        // console.log(document.body.offsetHeight);
        if (((window.innerHeight + $(document).scrollTop()) / document.body.offsetHeight) > .9) {
            // var comments = _.sortBy(this.props.comments.slice(0, this.state.comments.length+slice_size), 'comment_stati');
            // this.setState({'comments': comments, 'loading': true});
            var displayed_rows = (this.state) ? this.state.displayed_rows+slice_size : slice_size;
            this.setState({'loading': true, displayed_rows: displayed_rows});
            // this.handleFilter(this.state.stati_filter);
            // console.log('comment length: ' + comments.length);
        }
    }

    showHide(row_id, e) {
        parent = $(e.target).parents('.comment-answers');
        parent.toggleClass('rounded').removeClass('no-border');
        parent.prev('.comment-answers').toggleClass('no-border');

        // parent.find('.comment').toggleClass('no-border');
        // parent.find('.answers').toggleClass('thick-border');
        // console.log(this.getDOMNode());
        // console.log('showHide');
        // console.log('#answers-' + row_id);
        // console.log($('#answers-' + row_id));
        // this.refs['answers-' + row_id].show();
        $('#answers-' + row_id).toggle();

        // console.log('refs');
        // console.log(this.refs);
        this.refs['answers-' + row_id].showAnswerBars();

    }

    removeSiblingStati(stati_group, current_stati, new_stati) {
        // console.log('before: ' + current_stati);
        if (chain(stati_group).keys().contains(new_stati).value()) {
            // console.log('contains ' + new_stati);
            forEach(keys(stati_group), function(v) {
                // console.log(current_stati + '&= ~' + v);
                current_stati &= ~v;
            });
        }
        

        // console.log('after: ' + current_stati);
        return current_stati;
    }

    updateRowCommentStatus(valname, row_id, stati) {
        // console.log('updateRowCommentStatus: ' + valname + ', ' + row_id + ', ' + stati);
        var comments = this.state.comments;
        // var comments = this.filteredComments(this.state.stati_filter);

        var index = findIndex(comments, function(v) { return v.row_id == row_id });
        // console.log('before: ' + comments[index].comment_stati);
        // comments[index].comment_stati = stati|(comments[index].comment_stati&4);//(comments[index].comment_stati|stati);
        // console.log('after: ' + comments[index].comment_stati);
        // comments = _.sortBy(comments, 'comment_stati');

        var current_stati = comments[index][valname];
        // console.log('before: ' + current_stati);
        if (valname == 'cat_stati') {
            
            if ((current_stati&stati) > 0) {
                //  Stati bit already set, unset it
                comments[index].cat_stati &= ~stati;
            } else {
                //  Stati bit not set, set it
                comments[index].cat_stati |= stati;
            }
        } else {
            if (current_stati == stati)
                comments[index][valname] = 0;
            else
                comments[index][valname] = stati;

        }
        // switch (valname) {
        //     case 'useful_val':
        //     case 'custsent_val':
        //     case 'disp_stati':
        //         comments[index][valname] = stati;
        //         break;
        //     case 'comment_stati':
        //         comments[index].comment_stati |= stati;
        // }
        // console.log('after: ' + comments[index][valname]);


        //  Determine if this is an exclusive stati or additive
        // if (_.contains(additive_stati, stati)) {
        //     comments[index].comment_stati |= stati;
        // } else {
        //     // Gotta remove sibling stati first
        //     comments[index].comment_stati = this.removeSiblingStati(
        //         usefullness_stati, this.removeSiblingStati(
        //             sentiment_stati, this.removeSiblingStati(
        //                 disposition_stati, comments[index].comment_stati, stati
        //             ), stati
        //         ), stati
        //     ) | stati;
        // }

        var comment = comments[index];
        // console.log(comment.row_id);
        // console.log(comment.login_session_uid);

        $.ajax({
            url: 'http://dev.admapi.actkeytrain.com/dbwebapi/dbo.api_sp_set_stati/jsonp',
            dataType: 'jsonp',
            data: {
                row_id: comment.row_id,
                luid_str: comment.luid_str,
                stati_col: valname,
                stati_val: comments[index][valname]
            },
            success: function(data) {
                // console.log(data);
            },
            error: function() {
                console.log(arguments);
            }
        })

        // console.log('new stati: ' + comments[index].comment_stati);


        this.setState({'comments': comments});
        // this.handleFilter(this.state.stati_filter);
    }

    filteredComments(stati) {
        var comments = this.state.comments;
        var displayed_rows = this.state.displayed_rows;
        // console.log('filteredComments: ' + stati);
        // console.log('displayed_rows: ' + displayed_rows);
        // console.log('slice_size: ' + slice_size);
        // console.log(comments.length);

        var filtered_stati = this.state ? this.state.stati_filter : 1;
        // console.log('filtered_stati: ' + filtered_stati);

        comments = chain(comments)
            .filter(function(c) {
                // console.log(disposition_stati);
                // // console.log(c.comment_stati + ': ' + (c.comment_stati&3));
                // if (stati == 0) {
                //     //  New means no pos/neg, could be useless
                //     // console.log(c.comment_stati + ': ' + (c.comment_stati&3));
                //     return ((c.comment_stati&3) == 0)
                // } else {
                //     console.log(c.comment_stati + '&' + stati + ': ' + (c.comment_stati&stati));
                //     return ((c.comment_stati&stati) == stati);
                // }
                //  Filter out all those with a disposition
                // if (stati == 0) {
                //     return ((c.comment_stati&filtered_stati) == 0);
                // } else {
                //     return ((c.comment_stati&stati) == stati);
                // }
                // console.log(c.disp_stati + ' == ' + filtered_stati);
                return (c.disp_stati == filtered_stati);
            })
            // .sortBy('comment_stati')
            .value()
            .slice(0, (displayed_rows > slice_size) ? displayed_rows : slice_size);

        // comments = comments.slice(0, (displayed_rows > slice_size) ? displayed_rows : slice_size);
        // console.log(comments);


        // console.log(comments.length);
        // console.log((displayed_rows > slice_size) ? displayed_rows : slice_size);
        return comments;
    }

    handleFilter(stati) {
        // console.log('handleFilter');
        // console.log('handleFilter: ' + stati);
        // var comments = this.filteredComments(stati);
        // comments = _.filter(comments, function(c) {
        //     // console.log(c.comment_stati&3);
        //     if (stati == 0) {
        //         //  New means no pos/neg, could be useless
        //         return ((c.comment_stati&3) == 0)
        //     } else {
        //         return ((c.comment_stati&stati) == stati);
        //     }
        // }).slice(0, (this.state.comments.length > slice_size) ? this.state.comments.length : slice_size);
        // console.log(this.state.comments.length);
        // console.log(comments.length);
        // this.setState({'stati_filter': stati, 'comments': comments, 'displayed_rows': comments.length});
        // this.setState({'stati_filter': stati});

        var self = this;
        this.setState({
            'comments': [],
            'loading': true
        })
        get_comments(stati, function(comments) {
            // console.log(comments);
            self.setState({
                'comments': comments,
                'stati_filter': stati,
                'loading': false
            })
        });
        
    }

    renderComments() {
        // console.log('renderComments');
        var questions = this.props.questions;
        var question_ids = keys(this.props.questions);
        var answers = this.props.answers;
        var comments = map(this.filteredComments(0), function(comment, index) {
        // var comments = _.map(this.filteredComments(this.state.stati_filter), function(comment, index) {
        // var comments = _.map(this.state.comments, function(comment, index) {
            // console.log(comment);
            var answer_key = "answers-" + comment.row_id;
            return (
                <Row className="comment-answers" key={comment.row_id} onClick={this.showHide.bind(this, comment.row_id)}>
                    <Col md={12}>
                        <Comment datum={comment} questions={questions} questionIds={question_ids} key={comment.row_id} updateRowCommentStatus={this.updateRowCommentStatus} rowID={comment.row_id} handleDoneLoad={this.handleDoneLoad.bind(this)} {...this.props} />
                        <Answers datum={comment} answers={answers} questions={questions} key={answer_key} ref={answer_key} />
                    </Col>
                </Row>
            )
        }, this);
        
        return comments;
    }

    renderDispositions() {

        // console.log(disposition_stati);

        var buttons = map(this.props.dispositionStati, function(v, k) {
            // var active = (this.state && this.state.stati_filter == k) || false;
            // var classes = classNames({
            //     'btn': true,
            //     'btn-primary': true,
            //     'active': active
            // });

            var key = 'disposition-' + k;
            return <Button bsStyle='primary' active={this.state.stati_filter == k} onClick={this.handleFilter.bind(this, k)} key={key}>{v}</Button>
        }, this);

        return buttons;
    }

    render() {
        // console.log('render');
        var comments_block;

        var loading;
        if (this.state.loading)
            loading = <Loading />;

        var active = (this.state && this.state.stati_filter == 0) || false;
        var new_classes = classNames({
            'btn': true,
            'btn-primary': true,
            'active': active
        });

        return (
            <Container>
                <Row className="no-border">
                    <Col md={12}>
                        <ButtonGroup>
                            {this.renderDispositions()}
                        </ButtonGroup>
                    </Col>
                </Row>
                <Jumbotron>
                    <Col md={6}><h4>Comment</h4></Col>
                    <Col md={1}><h4></h4></Col>
                    <Col md={3}><h4>Comment Quality</h4></Col>
                </Jumbotron>
                {this.renderComments()}
                {loading}
            </Container>
        )
    }
}

// Comments.state = {
//     //comments: this.props.comments,
//     // display_comments: this.filteredComments(0),
//     loading: true,
//     stati_filter: 1,
//     // displayed_rows: slice_size
// }