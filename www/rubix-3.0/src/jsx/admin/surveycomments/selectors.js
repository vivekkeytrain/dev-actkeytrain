import { createSelector } from 'reselect';
import { LangOptions } from './actions';

import { map, chain, fromPairs, invert, filter } from 'lodash';

function selectQuestions(questions_data) {
    return chain(questions_data).map((question) => {
        return [question.id, question.question];
    }).fromPairs().value();
    // return map(questions_data, 'question');
}

function selectQuestionIds(questions_data) {
    return map(questions_data, 'id');
}

function selectAnswers(questions_data) {
    return chain(questions_data).map((question) => {
        return [question.id, question.answers];
    }).fromPairs().value();
}

function selectSurveyRowData(survey_data, survey_rows, mode, lang_id) {
    console.debug('selectSurveyRowData', survey_data, survey_rows, mode, lang_id);
    let mode_data = survey_data[mode];
    if (!mode_data) return [];
    mode_data = mode_data[lang_id];
    if (!mode_data) return [];

    // console.debug(mode_data);
    mode_data = map(mode_data.data, (row_id) => {
        return survey_rows[row_id];
    });

    // if (lang_id) {
    //     let lang_code = invert(LangOptions)[lang_id].toLowerCase();
    //     mode_data = filter(mode_data, (row) => {
    //         // console.debug(row.lang_code + ' === ' + lang_code);
    //         return row.lang_code === lang_code;
    //     });
    // } 
    
    return mode_data;
    
}

function selectSurveyComments(survey_row_data, displayed_rows) {
    console.debug('selectSurveyComments', survey_row_data, displayed_rows);
    return survey_row_data.slice(0, displayed_rows);
}

function selectCommentsFetching(survey_data, mode, lang) {
    console.debug('selectCommentsFetching');
    let mode_data = survey_data[mode];
    if (!mode_data) return false;
    if (!mode_data[lang]) return false;

    console.debug(mode_data[lang]);

    return mode_data[lang].isFetching;
}

const questionsDataSelector = state => state.questions.items;
const surveyDataSelector = state => state.survey_data;
const surveyRowsSelector = state => state.survey_rows;
const statiSelector = state => state.stati;
const modeSelector = state => state.filters.mode;
const langSelector = state => state.filters.lang;
const displayedRowCountSelector = state => state.filters.displayed_rows;
const selectedRowSelector = state => state.filters.selected_row;

const questionsSelector = createSelector(
    questionsDataSelector,
    (questions_data) => {
        return selectQuestions(questions_data);
    }
);

const questionIdsSelector = createSelector(
    questionsDataSelector,
    (questions_data) => {
        return selectQuestionIds(questions_data);
    }
);

const answersSelector = createSelector(
    questionsDataSelector,
    (questions_data) => {
        return selectAnswers(questions_data);
    }
);

const surveyRowDataSelector = createSelector(
    surveyDataSelector,
    surveyRowsSelector,
    modeSelector,
    langSelector,
    (survey_data, survey_rows, mode, lang) => {
        return selectSurveyRowData(survey_data, survey_rows, mode, lang);
    }
);

const surveyCommentsSelector = createSelector(
    surveyRowDataSelector,
    displayedRowCountSelector,
    (survey_row_data, displayed_rows) => {
        return selectSurveyComments(survey_row_data, displayed_rows);
    }
);

const commentsFetchingSelector = createSelector(
    surveyDataSelector,
    modeSelector,
    langSelector,
    (survey_data, mode, lang) => {
        return selectCommentsFetching(survey_data, mode, lang);
    }
)

export const filteredContentSelector = createSelector(
    questionsSelector,
    questionIdsSelector,
    answersSelector,
    commentsFetchingSelector,
    surveyCommentsSelector,
    statiSelector,
    modeSelector,
    langSelector,
    displayedRowCountSelector,
    selectedRowSelector,
    (questions, question_ids, answers, comments_fetching, survey_comments, stati, mode, lang, displayed_rows, selected_row) => {
        return { questions, question_ids, answers, comments_fetching, survey_comments, stati, mode, lang, displayed_rows, selected_row };
    }
);