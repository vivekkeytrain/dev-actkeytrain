import { combineReducers } from 'redux';
import { 
    FETCH_QUESTIONS_REQUEST, FETCH_QUESTIONS_SUCCESS, FETCH_QUESTIONS_FAILURE, 
    FETCH_SURVEY_DATA_REQUEST, FETCH_SURVEY_DATA_SUCCESS, FETCH_SURVEY_DATA_FAILURE,
    FETCH_STATI_REQUEST, FETCH_STATI_SUCCESS, FETCH_STATI_FAILURE,
    SET_STATI_REQUEST, SET_STATI_SUCCESS, SET_STATI_FAILURE,
    SET_LANG, SET_MODE, SET_DISPLAYED_ROW_COUNT, SET_SELECTED_ROW,
    LangOptions, slice_size
} from './actions';

import { chain, map, fromPairs } from 'lodash';

const { EN } = LangOptions;

function questions(state = {
    isFetching: false,
    items: []
}, action) {
    switch (action.type) {
        case FETCH_QUESTIONS_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_QUESTIONS_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                items: action.questions
            });
        case FETCH_QUESTIONS_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

/*
survey_data: {
    <disp_stati>: {
        isFetching: false,
        data: [130926, 130925, ...]
    },
    ...
}

survey_rows: {
    130926: { ... },
    130925: { ... }
}
*/

function survey_data(state = {
    isFetching: false,
    data: []
}, action) {
    // console.debug('survey_data', action);
    switch (action.type) {
        case FETCH_SURVEY_DATA_REQUEST:
        case FETCH_SURVEY_DATA_SUCCESS:
        case FETCH_SURVEY_DATA_FAILURE:
            return Object.assign({}, state, {
                [action.disp_stati]: survey_lang_data(state[action.disp_stati], action)
            })
        default:
            return state;
    }
}

function survey_lang_data(state = {}, action) {
    switch (action.type) {
        case FETCH_SURVEY_DATA_REQUEST:
        case FETCH_SURVEY_DATA_SUCCESS:
        case FETCH_SURVEY_DATA_FAILURE:
            return Object.assign({}, state, {
                [action.lang_id]: survey_data_rows(state[action.lang_id], action)
            })
        default:
            return state;
    }
}

function survey_data_rows(state = [], action) {
    // console.debug('survey_data_rows', action);
    switch (action.type) {
        case FETCH_SURVEY_DATA_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_SURVEY_DATA_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                data: map(action.data, 'row_id')
            });
        case FETCH_SURVEY_DATA_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

function survey_rows(state = {}, action) {
    switch (action.type) {
        case FETCH_SURVEY_DATA_SUCCESS:
            let survey_items = chain(action.data)
                .map(survey => {
                    return [survey.row_id, survey]
                })
                .fromPairs()
                .value()

            return Object.assign({}, state, survey_items);
        case SET_STATI_SUCCESS:
            return Object.assign({}, state, {
                [action.row_id]: survey_row(state[action.row_id], action)
            })
        default:
            return state;
    }
}

function survey_row(state = {}, action) {
    switch (action.type) {
        case SET_STATI_SUCCESS:
            return Object.assign({}, state, {
                [action.stati_col]: action.stati_val
            });
        default:
            return state;
    }
}

/*
stati: {
    disp_stati: {
        isFetching: false,
        items: [
            {
                status_val: 1,
                status_tag: 'disp_new',
                status_desc: 'new'
            },
            ...
        ]
    }
}

*/

function stati(state = {}, action) {
    switch (action.type) {
        case FETCH_STATI_REQUEST:
        case FETCH_STATI_SUCCESS:
        case FETCH_STATI_FAILURE:
            return Object.assign({}, state, {
                [action.valname]: stati_bits(state.bits, action)
            });
        default:
            return state;
    }
}

function stati_bits(state = {
    isFetching: false,
    items: []
}, action) {
    console.debug('stati_bits', action);
    switch (action.type) {
        case FETCH_STATI_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_STATI_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                items: action.data
            });
        case FETCH_STATI_FAILURE:
        default:
            return state;
    }
}

function filters(state = {
    lang: '',
    mode: 0,
    displayed_rows: slice_size,
    selected_row: -1
}, action) {
    switch (action.type) {
        case SET_LANG:
            return Object.assign({}, state, {
                lang: action.lang
            });
        case SET_MODE:
            return Object.assign({}, state, {
                mode: action.mode
            });
        case SET_DISPLAYED_ROW_COUNT:
            return Object.assign({}, state, {
                displayed_rows: action.count
            });
        case SET_SELECTED_ROW:
            return Object.assign({}, state, {
                selected_row: action.row_id
            });
        default:
            return state;
    }
}

const surveyApp = combineReducers({
    questions,
    survey_data,
    survey_rows,
    stati,
    filters
});

export default surveyApp;