import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
    LangOptions, slice_size,
    setMode, setLang, fetchQuestions, fetchSurveyData, fetchStati, setStati, setDisplayedRowCount, setSelectedRow
} from '../actions';
import Comments from '../components/Comments';
// import LangFilter from '../../common/LangFilter';
import Filters from '../components/Filters';
import { filteredContentSelector } from '../selectors';

class App extends Component {
    componentDidMount() {
        console.debug('componentDidMount');
        const { dispatch } = this.props;

        dispatch(fetchStati('disp_stati'))
            .then(() => {
                console.debug(this.props);
                let disposition_stati = this.props.stati.disp_stati;
                console.debug(this.props.stati);
                dispatch(setMode(disposition_stati.items[0].status_val));
                dispatch(fetchSurveyData(this.props.mode, this.props.lang));
            });
        dispatch(fetchStati('comment_stati'));
        dispatch(fetchStati('useful_val'));
        dispatch(fetchStati('custsent_val'));
        dispatch(fetchStati('cat_stati'));
        dispatch(fetchQuestions());

        window.addEventListener('scroll', this.handleScroll.bind(this));

        if ($('body').height() <= $(window).height()) {
            this.displayMoreComments();
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll.bind(this));
    }

    handleScroll(e) {
        if (((window.innerHeight + $(document).scrollTop()) / document.body.offsetHeight) > .95) {
            // console.debug(arguments);
            // console.debug(this);
            this.displayMoreComments();
        }
    }

    displayMoreComments() {
        const { dispatch } = this.props;
        dispatch(setDisplayedRowCount(this.props.displayed_rows + slice_size));
    }


    render() {
        console.debug(this.props);
        let { dispatch, survey_comments, displayed_rows, questions, question_ids, selected_row, stati, answers, comments_fetching, lang, mode } = this.props;

        let modes = [];
        if (stati.disp_stati && stati.disp_stati.items) modes = stati.disp_stati.items;

        // let classes = classNames({
        //     'progress-bar': true,
        //     'active': true,
        //     'progress-bar-striped': true
        // })

        // let fetching_bar;
        // console.debug('FETCHING', comments_fetching);
        // if (true) {
        //     fetching_bar = <div className={"progress-bar active progress-bar-striped"} style={{width: '100%'}}>
        //                     <span><strong>Fetching Data, Please Hold ...</strong></span>
        //                  </div>
        // }

        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PanelContainer controlStyles='bg-blue fg-white'>
                                <PanelHeader className='bg-blue fg-white' style={{margin: 0}}>
                                    <Grid>
                                        <Row>
                                            <Col xs={12}>
                                                <h3>Comments</h3>
                                                <p>
                                                    This report shows responses to the post-lesson survey presented to learners. Newest responses are shown first.<br />

                                                    You may choose to show surveys for only one delivery language or show all.<br />

                                                    You may choose to click to rate comments for usefulness, sentiment, and category (all optional). Then you may choose to dispose as desired:<br />

                                                    --  Archive simply saves the survey and retains the results.<br />
                                                    --  Favorite marks the survey as a favorite one that you can go back to later in the Favorites list.<br />
                                                    --  Delete marks the survey as essentially useless, and its results will NOT BE INCLUDED in the results summary.<br />
                                                </p>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelHeader>
                                <PanelBody>
                                    <Grid>
                                        <Row>
                                            <Col xs={12} style={{padding: 25}}>
                                                <Filters
                                                    mode={mode} 
                                                    modes={modes}
                                                    onModeChange={(mode) => {
                                                        dispatch(fetchSurveyData(mode, lang))
                                                            .then(dispatch(setMode(mode)))
                                                    }}
                                                    langs={LangOptions} 
                                                    lang={lang}
                                                    onLangChange={(lang_id) => {
                                                        dispatch(fetchSurveyData(this.props.mode, lang_id))
                                                            .then(dispatch(setLang(lang_id)));
                                                    }}
                                                />
                                                <Row>
                                                    <Col sm={12}>
                                                        <Comments 
                                                            comments={survey_comments}
                                                            questions={questions}
                                                            questionIds={question_ids}
                                                            displayedRows={displayed_rows}
                                                            selectedRow={selected_row}
                                                            stati={stati}
                                                            answers={answers}
                                                            commentsFetching={comments_fetching}
                                                            handleScroll={this.handleScroll}
                                                            displayMoreComments={() => this.displayMoreComments()}
                                                            updateStati={(row_id, luid_str, stati_col, stati_val, current_stati) => dispatch(setStati(row_id, luid_str, stati_col, stati_val, current_stati))}
                                                            setSelectedRow={(row_id) => dispatch(setSelectedRow(row_id))}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </PanelBody>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
}

export default connect(filteredContentSelector)(App);