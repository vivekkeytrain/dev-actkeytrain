import { combineReducers } from 'redux';
import { 
    SESSION_TYPES, START_DATE_DEFAULT,
    SET_SESSION_TYPE, SET_DISPLAY_BY_FISCAL_YEAR, 
    FETCH_SESSION_DATA_REQUEST, FETCH_SESSION_DATA_SUCCESS, FETCH_SESSION_DATA_FAILURE
} from './actions';

function filters(state = {
    start_date: START_DATE_DEFAULT,
    session_type: 'khours',
    display_by_fiscal_year: false
}, action) {
    switch (action.type) {
        case SET_SESSION_TYPE:
            return Object.assign({}, state, {
                session_type: action.session_type
            });
        case SET_DISPLAY_BY_FISCAL_YEAR:
            return Object.assign({}, state, {
                display_by_fiscal_year: action.display_by_fiscal_year
            });
        default:
            return state;
    }
}

function session_data(state = {}, action) {
    switch (action.type) {
        case FETCH_SESSION_DATA_REQUEST:
        case FETCH_SESSION_DATA_SUCCESS:
        case FETCH_SESSION_DATA_FAILURE:
            return Object.assign({}, state, {
                [action.start_date]: session_data_by_date(state[action.start_date], action)
            });
        default:
            return state;
    }
}

function session_data_by_date(state = {}, action) {
    switch (action.type) {
        case FETCH_SESSION_DATA_REQUEST:
        case FETCH_SESSION_DATA_SUCCESS:
        case FETCH_SESSION_DATA_FAILURE:
            let key = action.by_fiscal_year ? 'fiscal' : 'calendar';
            return Object.assign({}, state, {
                [key]: session_data_by_fiscal(state[key], action)
            });
        default:
            return state;
    }
}

function session_data_by_fiscal(state = {
    isFetching: false,
    data: []
}, action) {
    switch (action.type) {
        case FETCH_SESSION_DATA_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_SESSION_DATA_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                data: action.data
            })
        case FETCH_SESSION_DATA_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

const sessionApp = combineReducers({
    filters,
    session_data
});

export default sessionApp;