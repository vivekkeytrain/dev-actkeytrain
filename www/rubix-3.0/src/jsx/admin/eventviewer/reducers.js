import { combineReducers } from 'redux';
import {
    DEFAULT_ROW_COUNT,
    FETCH_EVENT_TYPES_REQUEST, FETCH_EVENT_TYPES_SUCCESS, FETCH_EVENT_TYPES_FAILURE,
    FETCH_EVENTS_REQUEST, FETCH_EVENTS_SUCCESS, FETCH_EVENTS_FAILURE,
    SET_SELECTED_EVENT, SET_START_DATE, SET_END_DATE, SET_ROW_COUNT, SET_EVENT_STATUS, SET_EVENT_CAT, SET_EVENT_TYPE,
    ADD_MESSAGE, CLOSE_MESSAGE, SET_SORT_FIELD
} from './actions';

function filters(state = {
    selected_event: -1,
    start_date: null,
    end_date: null,
    row_count: DEFAULT_ROW_COUNT,
    event_status: -1,
    event_category: -1,
    event_type: -1,
    messages: [],
    sort_field: 'event_id',
    sort_dir: 'desc'
}, action) {
    switch (action.type) {
        case SET_SELECTED_EVENT:
            return Object.assign({}, state, {
                selected_event: action.event_id
            });
        case SET_START_DATE:
            return Object.assign({}, state, {
                start_date: action.start_date
            });
        case SET_END_DATE:
            return Object.assign({}, state, {
                end_date: action.end_date
            });
        case SET_ROW_COUNT:
            return Object.assign({}, state, {
                row_count: action.row_count
            });
        case SET_EVENT_STATUS:
            return Object.assign({}, state, {
                event_status: action.event_status
            });
        case SET_EVENT_CAT:
            return Object.assign({}, state, {
                event_category: action.event_cat
            });
        case SET_EVENT_TYPE:
            return Object.assign({}, state, {
                event_type: action.event_type
            });

        case FETCH_EVENT_TYPES_FAILURE:
        case FETCH_EVENTS_FAILURE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: 'danger',
                        message: action.ex.message,
                        visible: true
                    }
                ]
            })

        case ADD_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages,
                    {
                        type: action.message_type,
                        message: action.message,
                        visible: true
                    }
                ]
            });

        case CLOSE_MESSAGE:
            return Object.assign({}, state, {
                messages: [
                    ...state.messages.slice(0, action.index),
                    Object.assign({}, state.messages[action.index], {
                        visible: false
                    }),
                    ...state.messages.slice(action.index+1)
                ]
            });

        case SET_SORT_FIELD:
            return Object.assign({}, state, {
                sort_field: action.field_name,
                sort_dir: action.sort_dir
            });

        default:
            return state;
    }
}

function event_types(state = {
    isFetching: false,
    items: []
}, action) {
    switch (action.type) {
        case FETCH_EVENT_TYPES_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_EVENT_TYPES_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                items: action.event_types
            });
        case FETCH_EVENT_TYPES_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            });
        default:
            return state;
    }
}

function events(state = {
    isFetching: false,
    events: [],
    event_stats: []
}, action) {
    switch (action.type) {
        case FETCH_EVENTS_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case FETCH_EVENTS_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                events: action.events[0],
                event_stats: action.events[1]
            });
        case FETCH_EVENTS_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            })
        default:
            return state;
    }
}

const eventApp = combineReducers({
    filters,
    event_types,
    events
});

export default eventApp;