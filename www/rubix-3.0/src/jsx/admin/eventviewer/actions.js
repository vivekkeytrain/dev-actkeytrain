import fetchJsonp from 'fetch-jsonp';

/*  Action types    */

export const SET_SELECTED_EVENT = 'SET_SELECTED_EVENT';
export const SET_START_DATE = 'SET_START_DATE';
export const SET_END_DATE = 'SET_END_DATE';
export const SET_ROW_COUNT = 'SET_ROW_COUNT';
export const SET_EVENT_TYPE = 'SET_EVENT_TYPE';
export const SET_EVENT_STATUS = 'SET_EVENT_STATUS';
export const SET_EVENT_CAT = 'SET_EVENT_CAT';
export const ADD_MESSAGE = 'ADD_MESSAGE';
export const CLOSE_MESSAGE = 'CLOSE_MESSAGE';
export const SET_SORT_FIELD = 'SET_SORT_FIELD';

/*  Async action types  */

export const FETCH_EVENT_TYPES_REQUEST = 'FETCH_EVENT_TYPES_REQUEST';
export const FETCH_EVENT_TYPES_SUCCESS = 'FETCH_EVENT_TYPES_SUCCESS';
export const FETCH_EVENT_TYPES_FAILURE = 'FETCH_EVENT_TYPES_FAILURE';


export const FETCH_EVENTS_REQUEST = 'FETCH_EVENTS_REQUEST';
export const FETCH_EVENTS_SUCCESS = 'FETCH_EVENTS_SUCCESS';
export const FETCH_EVENTS_FAILURE = 'FETCH_EVENTS_FAILURE';

/*  Other constants */

export const DEFAULT_ROW_COUNT = 100;
export const STAT_FIELDS = {
    event_stats_id: 'ID',
    stats_date: 'Date',
    stats_int: 'Int',
    stats_out: 'Out',
    stats_str: 'Str',
    stats_tag: 'Tag',
    stats_timestamp: 'Timestamp',
    substatus: 'Substatus'
}

export const dbTZOffset = 6;


/*  Action creators */

export function setSelectedEvent(event_id) {
    return {
        type: SET_SELECTED_EVENT,
        event_id
    }
}

export function setStartDate(start_date) {
    return {
        type: SET_START_DATE,
        start_date
    }
}

export function setEndDate(end_date) {
    return {
        type: SET_END_DATE,
        end_date
    }
}

export function setRowCount(row_count) {
    return {
        type: SET_ROW_COUNT,
        row_count
    }
}

export function setEventCat(event_cat) {
    return {
        type: SET_EVENT_CAT,
        event_cat
    }
}

export function setEventType(event_type) {
    return {
        type: SET_EVENT_TYPE,
        event_type
    }
}

export function setEventStatus(event_status) {
    return {
        type: SET_EVENT_STATUS,
        event_status
    }
}

export function addMessage(message_type, message) {
    return {
        type: ADD_MESSAGE,
        message_type,
        message
    }
}

export function closeMessage(index) {
    return {
        type: CLOSE_MESSAGE,
        index
    }
}

export function setSortField(field_name, sort_dir) {
    return {
        type: SET_SORT_FIELD,
        field_name,
        sort_dir
    }
}

function fetchEventTypesRequest() {
    return { type: FETCH_EVENT_TYPES_REQUEST };
}

function fetchEventTypesSuccess(event_types) {
    return {
        type: FETCH_EVENT_TYPES_SUCCESS,
        event_types
    };
}

function fetchEventTypesFailure(ex) {
    return {
        type: FETCH_EVENT_TYPES_FAILURE,
        ex
    };
}

export function fetchEventTypes() {
    return function(dispatch) {
        dispatch(fetchEventTypesRequest());
        return fetchJsonp('http://dev.admapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_event_type_list')
            .then(
                response => response.json(),
                ex => dispatch(fetchEventTypesFailure(ex))
            )
            .then(
                json => dispatch(fetchEventTypesSuccess(json.ResultSets[0]))
            )
            .catch(ex => dispatch(fetchEventTypesFailure(ex)));
    };
}

function fetchEventsRequest(event_cat, event_type, start_date, end_date, row_count) {
    return { 
        type: FETCH_EVENTS_REQUEST,
        event_cat,
        event_type,
        start_date,
        end_date,
        row_count
    };
}

function fetchEventsSuccess(event_cat, event_type, start_date, end_date, row_count, events) {
    return {
        type: FETCH_EVENTS_SUCCESS,
        event_cat,
        event_type,
        start_date,
        end_date,
        row_count,
        events
    };
}

function fetchEventsFailure(ex) {
    return {
        type: FETCH_EVENTS_FAILURE,
        ex
    };
}

export function fetchEvents(event_cat, event_type, start_date, end_date, row_count) {
    return function(dispatch) {
        dispatch(fetchEventsRequest(event_cat, event_type, start_date, end_date, row_count));

        let url = 'http://dev.admapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_events2?';
        if (event_cat !== -1) url += 'event_cat_id=' + event_cat + '&';
        if (event_type !== -1) url += 'event_type_id=' + event_type + '&';
        if (start_date) url += 'startdate=' + start_date + '&';
        if (end_date) url += 'stopdate=' + end_date + '&';
        if (row_count) url += 'showcount=' + row_count + '&';
        console.debug(url);

        return fetchJsonp(url, {
            timeout: 20000
        })
            .then(
                response => response.json(),
                ex => dispatch(fetchEventsFailure(ex))
            )
            .then(
                json => {
                    dispatch(fetchEventsSuccess(event_cat, event_type, start_date, end_date, row_count, json.ResultSets))
                }
            )
            .catch(ex => dispatch(fetchEventsFailure(ex)));
    }
}