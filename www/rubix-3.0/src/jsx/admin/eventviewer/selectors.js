import { createSelector } from 'reselect';

import { chain, uniq, map, filter, find, sortBy, orderBy } from 'lodash';

function selectEventCats(event_types, ids) {
    console.debug('selectEventCats', event_types, ids);
    //  Esentially a unique-on function
    console.debug(find(event_types, {'event_cat_id': 1}));
    return map(ids, (id) => {
        //  Return the first match, all others will be the same
        return find(event_types, {'event_cat_id': id});
    });
}

function selectEventCatIds(event_types) {
    return chain(event_types)
        .map('event_cat_id')
        .uniq()
        .value();
}

function selectEventTypes(event_types, event_cat) {
    console.debug('selectEventTypes', event_types, event_cat);
    if (event_cat === -1) return event_types;

    return filter(event_types, {'event_cat_id': event_cat});
}

function selectCombinedEvents(events, event_stats, sort_field, sort_dir) {
    return chain(events).map((event) => {
        event.stats = chain(event_stats).filter({'event_id': event.event_id}).sortBy('stats_timestamp').value();
        return event;
    }).orderBy([sort_field], [sort_dir]).value();
}

const filtersSelector = state => state.filters;
const selectedEventSelector = state => state.filters.selected_event;
const startDateSelector = state => state.filters.start_date;
const endDateSelector = state => state.filters.end_date;
const rowCountSelector = state => state.filters.row_count;
const eventStatusSelector = state => state.filters.event_status;
const eventCategorySelector = state => state.filters.event_category;
const eventTypeSelector = state => state.filters.event_type;
const messagesSelector = state => state.filters.messages;
const sortFieldSelector = state => state.filters.sort_field;
const sortDirSelector = state => state.filters.sort_dir;

const allEventTypesSelector = state => state.event_types.items;

const eventsSelector = state => state.events.events;
const eventStatsSelector = state => state.events.event_stats;
const eventsIsFetchingSelector = state => state.events.isFetching;

const eventCatIdsSelector = createSelector(
    allEventTypesSelector,
    (event_types) => {
        return selectEventCatIds(event_types);
    }
);

const eventCatsSelector = createSelector(
    allEventTypesSelector,
    eventCatIdsSelector,
    (event_types, ids) => {
        return selectEventCats(event_types, ids);
    }
);

const eventTypesSelector = createSelector(
    allEventTypesSelector,
    eventCategorySelector,
    (event_types, event_cat) => {
        return selectEventTypes(event_types, event_cat);
    }
);

const combinedEventsSelector = createSelector(
    eventsSelector,
    eventStatsSelector,
    sortFieldSelector,
    sortDirSelector,
    (events, event_stats, sort_field, sort_dir) => {
        return selectCombinedEvents(events, event_stats, sort_field, sort_dir);
    }
);

const eventStatiSelector = () => [];

export const filteredContentSelector = createSelector(
    filtersSelector,
    selectedEventSelector,
    startDateSelector,
    endDateSelector,
    rowCountSelector,
    eventStatusSelector,
    eventCategorySelector,
    eventTypeSelector,
    messagesSelector,
    eventCatsSelector,
    eventTypesSelector,
    eventStatiSelector,
    combinedEventsSelector,
    eventsIsFetchingSelector,
    (filters, selected_event, start_date, end_date, row_count, event_status, event_cat, event_type, messages, event_cats, event_types, event_stati, events, events_fetching) => {
        return {
            filters,
            selected_event,
            start_date,
            end_date,
            row_count,
            event_status,
            event_cat,
            event_type,
            messages,
            event_cats,
            event_types,
            event_stati,
            events,
            events_fetching
        }
    }
);