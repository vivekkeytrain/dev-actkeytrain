import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

import SortHeader from './SortHeader';
import EventRow from './EventRow';
import EventStatsList from './EventStatsList';

import { dbTZOffset } from '../actions';

export default class EventsList extends Component {
    // componentDidUpdate() {
    //     // console.debug('componentDidUpdate');

    //     //  First kill the buttons
    //     $('th[data-sortable-col] > button').contents().unwrap();

    //     //  Then recreate them
    //     $('.tablesaw').sortable.prototype._init.call($('.tablesaw'));

    //     //  For some reason we get double buttons sometimes. Take care of that.
    //     $('th[data-sortable-col] > button > button').contents().unwrap();

    // }

    onRowSelected(event, e) {
        console.debug(arguments);
        console.debug(this);
        if (event.event_id === this.props.selectedEvent) { this.props.onRowSelected(-1); }
        else { this.props.onRowSelected(event.event_id); }
    }

    formatTimestamp(timestamp) {
        let tzoffset = '-' + ((dbTZOffset < 10) ? '0' : '') + dbTZOffset + ':00'
        let d = new Date(timestamp + tzoffset);
        // console.debug(timestamp + tzoffset);
        // console.debug(d);
        let m = ((d.getMonth() < 10) ? '0' : '') + (d.getMonth()+1);
            let dy = ((d.getDate() < 10) ? '0' : '') + d.getDate();
        let h = ((d.getHours() < 10) ? '0' : '') + d.getHours();
        // console.debug(h + ' - ' + dbTZOffset + ' + ' + (d.getTimezoneOffset() / 60))
        // h = h - dbTZOffset + (d.getTimezoneOffset() / 60);
        let min = ((d.getMinutes() < 10) ? '0' : '') + d.getMinutes();
        let s = ((d.getSeconds() < 10) ? '0' : '') + d.getSeconds();


        return d.getFullYear() + String.fromCharCode(8209) + m + String.fromCharCode(8209) + dy + ' ' + h + ':' + min + ':' + s;
    }

    

    render() {
        let row_count = this.props.events ? this.props.events.length : 0;

        let classes = classNames({
            'progress-bar': true,
            'active': true,
            'progress-bar-striped': true
        })

        let fetching;
        if (this.props.eventsFetching) {
            fetching =  <div className={classes} style={{width: '100%'}}>
                            <span><strong>Fetching Data, Please Hold ...</strong></span>
                        </div>
        }
        console.debug(this.props);
        return (
            <div>
                <div>Row Count: {row_count}</div>
                {fetching}
                <Table
                    key='dataTable'
                    ref='dataTable'
                    bordered
                    striped
                    className='tablesaw tablesaw-sortable'>
                    <thead>
                        <tr>
                            <SortHeader name="event_id" label="ID" sortField={this.props.sortField} sortDir={this.props.sortDir} setSortField={this.props.setSortField} />
                            <SortHeader name="event_type_tag" label="Type Tag" sortField={this.props.sortField} sortDir={this.props.sortDir} setSortField={this.props.setSortField} />
                            <SortHeader name="event_timestamp" label="Time" sortField={this.props.sortField} sortDir={this.props.sortDir} setSortField={this.props.setSortField} />
                            <SortHeader name="event_status" label="Status" sortField={this.props.sortField} sortDir={this.props.sortDir} setSortField={this.props.setSortField} />
                            <SortHeader name="event_statdesc" label="Status Desc" sortField={this.props.sortField} sortDir={this.props.sortDir} setSortField={this.props.setSortField} />
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.events.map((event, ind) => {
                                return [
                                <EventRow event={event} formatTimestamp={this.formatTimestamp} onRowSelected={this.onRowSelected.bind(this, event)} />,
                                (event.event_id === this.props.selectedEvent) ? <EventStatsList stats={event.stats} formatTimestamp={this.formatTimestamp} onRowSelected={this.onRowSelected.bind(this, event)} /> : ''
                                ]
                            }, this)
                        }
                    </tbody>
                </Table>
            </div>
        );
    }
}