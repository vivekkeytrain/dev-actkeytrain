import React, { Component, PropTypes } from 'react';

import classNames from 'classnames';

export default class SortHeader extends Component {
    handleSortHeaderClick(e) {
        console.debug(this);
        e.preventDefault();
        e.target.blur();
        let sort_dir = (this.props.sortField == this.props.name && this.props.sortDir == 'asc') ? 'desc' : 'asc';
        this.props.setSortField(this.props.name, sort_dir);
    }

    render() {
        let classes = classNames({
            'sortable-head': true,
            'sortable-descending': (this.props.sortField == this.props.name && this.props.sortDir == 'desc'),
            'sortable-ascending': (this.props.sortField == this.props.name && this.props.sortDir == 'asc')
        })

        return (
            <th data-sortable-col="true" className={classes}>
                <button onClick={this.handleSortHeaderClick.bind(this)}>{this.props.label}</button>
            </th>
        )
    }
} 