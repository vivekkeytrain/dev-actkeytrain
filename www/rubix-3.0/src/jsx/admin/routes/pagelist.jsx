import classNames from 'classnames';
import SidebarMixin from 'global/jsx/sidebar_component';

import Header from 'common/header';
import Sidebar from 'common/sidebar';
import Footer from 'common/footer';

import { Link } from 'react-router'

import LoremIpsum from 'global/jsx/loremipsum';

import fetchJsonp from 'fetch-jsonp';


var DashboardPanel = React.createClass({

  render() {

    return (

      <PanelContainer>
        <Link to={this.props.path}>
        <Panel style={{background: 'white'}}>
          <PanelHeader className={classNames('text-center', this.props.classes)}>
            <Grid>
              <Row>
                <Col xs={12} className='fg-white'>
                  <h3>{this.props.label}</h3>
                </Col>
              </Row>
            </Grid>
          </PanelHeader>
          <PanelBody>
            <Grid>
              <Row>
                <Col xs={12}>
                <div>
                  <p>
                    {this.props.children}
                  </p>
                </div>
                </Col>
              </Row>
            </Grid>
          </PanelBody>
        </Panel>
        </Link>
      </PanelContainer>

    );
  }
});

class Body extends React.Component {
  render() {
    let user;
    if (window.auth) {
      let auth = atob(window.auth.replace('Basic ', ''));
      // console.debug(auth);
      let auth_parts = auth.split(':');
      user = auth_parts[0];
    }

    let roles = window.roles ? window.roles : '';

    // |admin|cms|reports|survey|events|

    let panels = [];
    if (roles.indexOf('|survey|') !== -1) {
      panels.push(
            <Col sm={4} smCollapseRight key="survey-chart">
              <DashboardPanel 
                label='Survey Results Summary'
                path='/survey-chart/'
                classes='bg-blue'>
                <span>View chart of responses from the survey presented to learners upon first-time completion of each final quiz.</span>
              </DashboardPanel>
            </Col>
      );

      panels.push(
            <Col sm={4} key="survey-coments">
              <DashboardPanel 
                label='Survey Comments'
                path='/survey-comments/'
                classes='bg-green'>
                <span>View individual learner survey responses along with comments. Rate comments for usefulness, sentiment, category; and choose to archive, delete or mark as favorite.</span>
              </DashboardPanel>
            </Col>
      );
    }
    /********** for announcement starts here ************/
    if (roles.indexOf('|announcements|') !== -1) {
      panels.push(
            <Col sm={4} smCollapseRight key="announcements">
              <DashboardPanel 
                label='Announcements'
                path='/announcements/'
                classes='bg-blue'>
                <span>View announcements of sytstem</span>
              </DashboardPanel>
            </Col>
      );

    }
    /********************/

    if (roles.indexOf('|reports|') !== -1) {
      panels.push(
            <Col sm={4} key="course-usage-reports">
              <DashboardPanel 
                label='Course Usage Reports'
                path='/course-usage-reports/'
                classes='bg-blue'>
                <span>View curriculum usage over time. Choose your date range and interval, then slice and dice it however you want! We've got summaries of the key metrics of sites, learners and hours as well as breakdowns by language, unit and course.</span>
              </DashboardPanel>
            </Col>
      );

      panels.push(
            <Col sm={4} key="session-chart">
              <DashboardPanel 
                label='Session Data Chart'
                path='/session-chart/'
                classes='bg-red'>
                <span>View a chart of raw usage data by hours and sessions by month over a period of years.</span>
              </DashboardPanel>
            </Col>
      );
    }

    if (roles.indexOf('|events|') !== -1) {
      panels.push(
            <Col sm={4} key="event-viewer">
              <DashboardPanel 
                label='Event Viewer'
                path='/event-viewer/'
                classes='bg-yellow'>
                <span>View a log of system events. This is primarily for development and operations usage.</span>
              </DashboardPanel>
            </Col>
      );
    }

    panels.push(
      <Col sm={4} key="dashboard">
        <DashboardPanel 
          label='Active Login Map'
          path='/active-logins/'
          classes='bg-yellow'>
          <span>View a live map of active logins.</span>
        </DashboardPanel>
      </Col>
    );

    return (
      <Container id='body'>
        <Grid>
          <Row>
            {panels}
          </Row>
        </Grid>
      </Container>
    );
  }
}

@SidebarMixin
export default class extends React.Component {
  render() {
    console.debug(this.context);
    var classes = classNames({
      'container-open': this.props.open
    });

    return (
      <Container id='container' className={classes}>
        <Header />
        <Sidebar />
        <Body />
        <Footer />
      </Container>
    );
  }
}
