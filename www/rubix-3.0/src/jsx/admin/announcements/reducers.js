import { combineReducers } from 'redux';
import {
    INSERT_LOGIN, DELETE_LOGIN, INSERT_HISTORY, SET_HISTORY_TIMEFRAME, SET_HISTORY_FETCH_TIMER,
    FETCH_LOGINS_SUCCESS, FETCH_LOGINS_FAILURE, FETCH_LOGINS_REQUEST,
    FETCH_LOGINS_HISTORY_SUCCESS, FETCH_LOGINS_HISTORY_FAILURE, FETCH_LOGINS_HISTORY_REQUEST
} from './actions';
import { uniq, chain, filter, map, remove } from 'lodash';

function logins(state = {
    kt: [],
    cr: [],
    raw: [],
}, action) {
    let pkg, raw;
    switch (action.type) {
        case INSERT_LOGIN:
            pkg = (action.pkg_id == 1) ? 'kt' : 'cr';

            return Object.assign({}, state, {
                [pkg]: state[pkg].concat([{
                    student_id: action.student_id,
                    insert_date: action.insert_date
                }]),
                raw: [
                    ...state.raw,
                    {
                        student_id: action.student_id,
                        pkg_id: action.pkg_id,
                        statecode: action.statecode,
                        insert_date: action.insert_date,
                        initial: false
                    }
                ]
            })

            break;
        case DELETE_LOGIN:
            pkg = (action.pkg_id == 1) ? 'kt' : 'cr';

            let logins = state[pkg].slice();
            remove(logins, (row) => {
                return row.student_id == action.student_id;
            });

            raw = state.raw.slice();
            remove(raw, (row) => {
                return row.student_id == action.student_id;
            })

            return Object.assign({}, state, {
                [pkg]: logins,
                raw
            })

            break;
        case FETCH_LOGINS_SUCCESS:
            let kt = chain(action.logins)
                    .filter({'pkg_id': 1})
                    // .map('student_id')
                    .map((login) => {
                        return { 
                            student_id: login.student_id, 
                            insert_date: login.insert_date }
                    })
                    .value();

            let cr = chain(action.logins)
                    .filter({'pkg_id': 2})
                    // .map('student_id')
                    .map((login) => {
                        return { 
                            student_id: login.student_id, 
                            insert_date: login.insert_date }
                    })
                    .value();

            raw = map(action.logins, (login) => {
                return Object.assign({}, login, {
                    initial: true
                })
            });

            console.error(raw);

            return Object.assign({}, state, {
                kt,
                cr,
                raw
            })

        default:
            return state;
    }
}

function history(state = {
    timeframe: 'day',
    kt: [],
    cr: []
}, action) {
    let pkg;
    switch (action.type) {
        case SET_HISTORY_TIMEFRAME:
            return Object.assign({}, state, {
                timeframe: action.timeframe
            });
            break;
        case SET_HISTORY_FETCH_TIMER:
            return Object.assign({}, state, {
                timer: action.timer
            });
            break;
        case INSERT_HISTORY:
            pkg = (action.pkg_id == 1) ? 'kt' : 'cr';

            return Object.assign({}, state, {
                [pkg]: state[pkg].concat([{
                    insert_date: action.insert_date,
                    count: action.count
                }])
            });

            break;
        case FETCH_LOGINS_HISTORY_SUCCESS:
            let kt = chain(action.history)
                    .map((row) => {
                        return {
                            insert_date: row.insert_date_interval,
                            count: parseInt(row['kt_logins_total'], 10)
                        }
                    })
                    .value();

            let cr = chain(action.history)
                    .map((row) => {
                        return {
                            insert_date: row.insert_date_interval,
                            count: parseInt(row['cr_logins_total'], 10)
                        }
                    })
                    .value();

            return Object.assign({}, state, {
                kt,
                cr
            })

        default:
            return state;
    }
}

const dashboardApp = combineReducers({
    logins,
    history
});

export default dashboardApp;
