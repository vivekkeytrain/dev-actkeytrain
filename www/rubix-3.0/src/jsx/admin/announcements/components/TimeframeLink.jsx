import React, { Component } from 'react';
import classNames from 'classnames';
import { upperFirst } from 'lodash';

export default class TimeframeLink extends Component {
    setTimeframe(e) {
        e.preventDefault();
        this.props.setTimeframe(this.props.timeframe);
    }

    render() {
        let classes = classNames({
            'text-danger': (this.props.timeframe === this.props.timeframeSet)
        });

        return <a href="#" className={classes} onClick={this.setTimeframe.bind(this)}>Past {upperFirst(this.props.timeframe)}</a>
    }
}

// const TimeframeLink = (props) => {
//     console.debug(props);
//     // let classes = classNames({
//     //     'text-danger': (props.timeframe === props.timeframeSet)
//     // });
//     return <div>Hello</div>;

//     // return <a href="#" onClick={props.setTimeframe.bind(this, props.timeframe)}>Past {props.timeframe ? props.timeframe.toUpperCase() : ''}</a>
// }

// export default TimeframeLink;