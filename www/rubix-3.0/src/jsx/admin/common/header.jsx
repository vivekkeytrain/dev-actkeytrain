import { State, Navigation } from 'react-router';
import classNames from 'classnames';
import { Link } from 'react-router';

class Brand extends React.Component {

  render() {
    return (
      <NavHeader {...this.props} className='hidden-xs' >
          <NavBrand tabIndex='-1' href='/'>
            <img src='/imgs/logo.png' alt='ACT Career Curriculum' width='203' height='30' />
          </NavBrand>
      </NavHeader>
    );
  }
}

var PageMenuItem = React.createClass({
  mixins: [State, Navigation],
  componentDidMount(){
    if (window.location.pathname == this.props.path){
      $('#pages_label').html(this.props.label);
    }    
  },
  render() {
    const ACTIVE = { color: 'red' }
    return (
      <MenuItem {...this.props}>
        <Link to={this.props.path} activeStyle={ACTIVE}>{this.props.label}</Link>
      </MenuItem>
    );
  }
});



var HeaderNavigation = React.createClass({
  mixins: [State, Navigation],

  itemSelect() {
    //console.log(this);
    //$(this).toggle();
  },

  render() {
    
    var hidemenu = false; //window.location.pathname == '/';

    var props = {
      ...this.props
      ,hidden: hidemenu
      ,className: classNames('text-center', this.props.className)
    };

    let roles = window.roles ? window.roles : '';
    let menu_items = [];
    // |admin|cms|reports|survey|events|

    if (roles.indexOf('|survey|') !== -1) {
      menu_items.push(
        <PageMenuItem path='/survey-chart/' label='Survey Results Summary' key='survey-chart' />
      );

      menu_items.push(
        <PageMenuItem path='/announcements/' label='Announcements' key='announcements' />
      );
      menu_items.push(
        <PageMenuItem path='/survey-comments/' label='Survey Comments' key='survey-comments' />
      );
    }

    if (roles.indexOf('|reports|') !== -1) {
      menu_items.push(
        <PageMenuItem path='/course-usage-reports/' label='Course Usage Reports' key='course-usage-reports' />
      );

      menu_items.push(
        <PageMenuItem path='/session-chart/' label='Session Data Chart' key='session-chart' />
      );
    }

    if (roles.indexOf('|events|') !== -1) {
      menu_items.push(
        <PageMenuItem path='/event-viewer/' label='Event Viewer' key='session-chart' />
      );
    }

    if (roles.indexOf('|reports|') !== -1) {
      menu_items.push(
        <PageMenuItem path='/active-logins/' label='Active Logins' key='active-logins' />
      );
    }


    return (
      <NavContent {...props} >
        <Nav>
          <NavItem dropdown >
            <DropdownButton nav>
              <span id='pages_label'>Pages</span> <Icon bundle='fontello' glyph='down-open-mini' /> 
            </DropdownButton>
            <Menu alignRight ref='page-menu' id='notifications-menu' bsStyle='theme' onItemSelect={this.itemSelect}>
              {menu_items}
            </Menu>
          </NavItem>
        </Nav>
      </NavContent>
    );
  }
});

export default class Header extends React.Component {
  render() {
    console.debug(this.context);
    return (
      <Grid id='navbar' {...this.props}>
        <Row>
          <Col xs={12}>
            <NavBar fixedTop id='rubix-nav-header'>
              <Container>
                <Row>
                  <Col xs={12}>
                    <Brand />
                    <HeaderNavigation />
                  </Col>
                </Row>
              </Container>
            </NavBar>
          </Col>
        </Row>
      </Grid>
    );
  }
}
