import { Component, PropTypes } from 'react';
import classNames from 'classnames';

class Message extends Component {
    closeButtonClick(e) {
        e.preventDefault();
        this.props.onMessageClose(this.props.index);
    }

    render() {
        console.debug(this.props);

        let classes = classNames({
            'alert': true,
            'alert-success': (this.props.type === 'success'),
            'alert-info': (this.props.type === 'info'),
            'alert-warning': (this.props.type === 'warning'),
            'alert-danger': (this.props.type === 'danger'),
            'hidden': (!this.props.visible)
        })

/*
            <Alert success={this.props.type === 'success'} info={this.props.type === 'info'} warning={this.props.type === 'warning'} danger={this.props.type === 'danger'} collapseBottom dismissible>{this.props.message}</Alert>
            */
        return (
            <div role="alert" className={classes}>
                <div>
                    <button role="button" type="button" className="close" onClick={this.closeButtonClick.bind(this)}>
                        <span aria-hidden="true">×</span>
                        <span className="sr-only">Close</span>
                    </button>
                    <span>{this.props.message}</span>
                </div>
            </div>


        )
    }
    
}

Message.propTypes = {
    message: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired
}

export default class MessageCenter extends Component {

    onMessageClose(message_index) {
        this.props.onMessageClose(message_index);
    }


    render() {
        if (!this.props.children) return '';

        return (
            <div>
                {React.Children.map(this.props.children, (message) => {
                    return <Message type={message.props.type} message={message.props.children} index={message.props.index} visible={message.props.visible} key={"message_" + message.props.index} onMessageClose={this.onMessageClose.bind(this)} />
                }, this)}
            </div>
        );
    }
}


