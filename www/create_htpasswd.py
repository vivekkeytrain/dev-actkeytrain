import sys, json, urllib, datetime, subprocess

def main():
    htpasswd_flags = ''
    if len(sys.argv) < 2:
        htpasswd_flags = '-n'
    else:
        htpasswd_flags = sys.argv[1]

    d = datetime.datetime.now()
    s = '%s%02d%02d' % (d.year, d.month, d.day)
    secret = int(s) * 2

    url = 'http://dev.admapi.actkeytrain.com/dbwebapi/dbo.api_sp_get_auth/json?secret=%s&optflags=1' % secret

    resp = urllib.urlopen(url)
    data = json.loads(resp.read())

    print data

    for row in data['ResultSets'][0]:
        user, password = row.get('authrow').split(':')

        # print user
        # print password

        subprocess.call(['htpasswd', '-i', '-b', htpasswd_flags, user, password])


if __name__ == '__main__':
    sys.exit(main()) 